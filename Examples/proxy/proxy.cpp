// proxy.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "ProxyServer.h"

#pragma comment(lib, "../../Bin/RLib_d.lib")

// -t http -p 8080
// -t socket5 -p 22

int _tmain(int argc, _TCHAR* argv[])
{
    if (argc != 5) 
    {
        return -1;
    }

    CProxyServer server;
    server.RunServer(argv[2], _ttoi(argv[4]));

	return 0;
}

