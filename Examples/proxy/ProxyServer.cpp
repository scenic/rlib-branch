#include "StdAfx.h"
#include "ProxyServer.h"
#include <locale>

CProxyServer::CProxyServer(void)
{

}

CProxyServer::~CProxyServer(void)
{
}

void CProxyServer::RunServer(const TCHAR* pszType, USHORT uSvrPort)
{
    m_pThreadPool = new ::System::Threading::ThreadPool;
    if (m_SvrSocket.Bind(uSvrPort) == SOCKET_ERROR) 
    {
        printf("bind error!!!");
        return;
    }

    m_SvrSocket.Listen();
    setlocale(LC_ALL, "chs");
    _tprintf(_T("服务运行中, 类型:%s 监听端口%d...\r\n"), pszType, uSvrPort);
    
    sockaddr_in           ClientAddr;
    System::Net::Sockets *ClientSocket;
    int len = sizeof(sockaddr_in), FailedCount = 0;
    while ((ClientSocket = m_SvrSocket.Accept(&ClientAddr, &len)) != NULL)
    {
        ProxyInfo* pInfo = new ProxyInfo;
        if (NULL != pInfo) 
        {
            pInfo->pServer = this;
            pInfo->pClientSocket = ClientSocket;

            m_pThreadPool->AddTask(System::Threading::WaitCallback(&CProxyServer::HttpProxyWorker), pInfo);
        }
        else 
        {
            printf("failed...");
            ClientSocket->Close();
        }
    }
}

void CProxyServer::HttpProxyWorker(ProxyInfo* pProxyInfo)
{
    pProxyInfo->RecvClient();

    //@todo
    pProxyInfo->pClientSocket->Send("暂未实现代理功能", 8);

    
}

void CProxyServer::Socket5ProxyWorker(ProxyInfo* pProxyInfo)
{
    //@todo
}
