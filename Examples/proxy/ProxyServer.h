#pragma once

#include "../../RLib/RLib.h"
#include "../../RLib/RLib_ThreadPool.h"

class ProxyInfo;
class CProxyServer
{
public:
    CProxyServer(void);
    ~CProxyServer(void);

public:
    void RunServer(const TCHAR* pszType, USHORT uSvrPort);

protected:
    static void HttpProxyWorker(ProxyInfo* pProxyInfo);
    static void Socket5ProxyWorker(ProxyInfo* pProxyInfo);

protected:
    ::System::Threading::ThreadPool*  m_pThreadPool;
    ::System::Net::Sockets   m_SvrSocket;
};

class ProxyInfo
{
public:
    ProxyInfo() : pServerSocket(NULL), pClientSocket(NULL), ipos(0) {}
    ~ProxyInfo() 
    {
        if (pServerSocket) 
        {
            delete pServerSocket;
        }
        if (pClientSocket) 
        {
            delete pClientSocket;
        }

    }

    int RecvClient()
    {
        return pClientSocket->Recv(buff + ipos, sizeof(buff) - ipos);
    }

public:
    CProxyServer* pServer;
    ::System::Net::Sockets* pClientSocket;
    ::System::Net::Sockets* pServerSocket;

    BOOL m_bError;
    int ipos;
    char buff[4*1024];
};

