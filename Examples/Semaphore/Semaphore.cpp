/********************************************************************
	Created:	2012/05/26  16:08
	Filename: 	Loader.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "../../RLib/RLib_Import.h"

LONG __cdecl InterlockedAdd (__inout __drv_interlocked LONG volatile *Addend,
	__in LONG Value)
{
	__asm
	{
		mov ecx, Addend;
		mov eax, Value;
		lock xadd [ecx], eax;
	}
}

#ifdef _DEBUG
#pragma comment(lib, "../../bin/RLib_d.lib")
#else
#pragma comment(lib, "../../bin/RLib.lib")
#endif

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace System::Threading;
//-------------------------------------------------------------------------
// A semaphore that simulates a limited resource pool.
//
static Semaphore *_pool;

// A padding interval to make the output more orderly.
volatile static long _padding;
//-------------------------------------------------------------------------
class Example
{
public:
	static void Main()
	{
		// Create a semaphore that can satisfy up to three
		// concurrent requests. Use an initial count of zero,
		// so that the entire semaphore count is initially
		// owned by the main program thread.
		//
		_pool = new Semaphore( 0,3 );

		// Create and start five numbered threads.
		//
		for ( int i = 1; i <= 5; i++ )
		{
			Thread *t = new Thread(
				ParameterizedThreadStart( Worker ) , (Object)i);

			t->IsBackground = true;

			// Start the thread
			//
			t->Start();
		}

		// Wait for half a second, to allow all the
		// threads to start and to block on the semaphore.
		//
		Thread::SleepEx( 500 );

		// The main thread starts out holding the entire
		// semaphore count. Calling Release(3) brings the
		// semaphore count back to its maximum value, and
		// allows the waiting threads to enter the semaphore,
		// up to three at a time.
		//
		printf( "Main thread calls Release(3).\n" );
		_pool->Release( 3 );

		printf( "Main thread exits.\n" );
	}

private:
	static void Worker( Object num )
	{
		// Each worker thread begins by requesting the
		// semaphore.
		printf( "Thread {%d} begins and waits for the semaphore.\n", int(num) );
		_pool->WaitOne();

		// A padding interval to make the output more orderly.
		int padding = InterlockedAdd( &_padding, 100 );

		printf( "Thread {%d} enters the semaphore.\n", int(num) );

		// The thread's "work" consists of sleeping for
		// about a second. Each thread "works" a little
		// longer, just to make the output more orderly.
		//
		Thread::SleepEx( 1000 + padding );

		printf( "Thread {%d} releases the semaphore.\n", int(num) );
		printf( "Thread {%d} previous semaphore count: {1}\n",
			int(num), _pool->Release() );
	}
};
//-------------------------------------------------------------------------
int main()
{
	Example::Main();
	Thread::SleepEx(8888);
	RLIB_Delete(_pool);
	return 0;
}