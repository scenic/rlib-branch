/********************************************************************
	Created:	2012/08/09  21:01
	Filename: 	Stdfx.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MyStruct.h"

//-------------------------------------------------------------------------

char *RequestInfo::get_header_value_ansi(char *headers, char *name)
{
	char *pheader = strstr(headers, name);
	if (pheader != nullptr)
	{
		pheader += (strlen(name) + 2);
		char *pheaderEnd = strstr(pheader, "\r\n");
		name = (char*)RLIB_GlobalAlloc((pheaderEnd - pheader) + 1);
		memcpy(name, pheader, (pheaderEnd - pheader));
		pheader = name;
	}
	return pheader;
}


//-------------------------------------------------------------------------

bool RequestInfo::parse_http_response(char **ppRequestLine, char **ppRequestLineEnd,
	char **ppHeaders, int *pHeadersBytes, char **ppData, int *pDataBytes)
{
	//分割HTTP请求报文
	char *tp = strstr(this->pbuf, "\r\n") + (sizeof("\r\n") - 1 /*结尾'\0'*/);
	if(tp == nullptr) return false;
	char *pRequestLine    = (char *)RLIB_GlobalAlloc((tp - this->pbuf) + 1 /*结尾'\0'*/);
	char *pRequestLineEnd = pRequestLine + (tp - this->pbuf);
	memcpy(pRequestLine, this->pbuf, tp - this->pbuf);
	char *tp2 = strstr(tp, "\r\n\r\n") + (sizeof("\r\n\r\n") - 1);
	if(tp2 == nullptr)
	{
		AppBase::Collect(pRequestLine);
		return false;
	}
	int   HeadersBytes = (tp2 - tp);
	char *pHeaders     = (char *)RLIB_GlobalAlloc(HeadersBytes + 1);
	memcpy(pHeaders, tp, HeadersBytes);
	//判断是否有附加数据
	char *pData   = nullptr;
	int DataBytes = this->buf_size - (tp2 - this->pbuf);
	if (this->pbuf[0] == 'P' && DataBytes > 0)
	{
		pData = (char *)RLIB_GlobalAlloc(DataBytes + 1);
		memcpy(pData, tp2, DataBytes);
	}
	*ppRequestLine    = pRequestLine;
	*ppRequestLineEnd = pRequestLineEnd;
	*ppHeaders        = pHeaders;
	*pHeadersBytes    = HeadersBytes;
	*ppData           = pData;
	*pDataBytes       = DataBytes;
	return true;
}

//-------------------------------------------------------------------------

bool RequestInfo::parse_host_name(char *RecvBuf)
{
	char *pHost = strstr(RecvBuf, "Host: ");
	if (pHost == nullptr)
	{
		this->LastError = T("Not Found Host");
		return false; 
	}
	pHost += (sizeof("Host: ") - 1);

	char *pHostEnd = strstr(pHost, "\r\n");
	if (pHostEnd == nullptr)
	{
		this->LastError = T("Invalid Host");
		return false; 
	}
	int nHost = pHostEnd - pHost;
	if (nHost > MAX_HOST_NAME)
	{
		this->LastError = T("Invalid Host");
		return false;
	}
	char hostname[MAX_HOST_NAME] = {0};
	memcpy(hostname, pHost, nHost);
	hostname[nHost] = 0;

	char *pPort = strstr(hostname, ":");
	if (pPort == nullptr)
	{
		this->Port      = 80;
		this->HostName  = hostname;
		return true;
	}
	this->Port = atoi(pPort + (sizeof(":") - 1));

	hostname[pPort - hostname] = 0;
	this->HostName             = hostname;

	return true;
}

//-------------------------------------------------------------------------

bool RequestInfo::Prepare()
{
	buf_size = 0;
	pbuf     = recv_data(&buf_size);
	if (pbuf == nullptr)
	{
		return false;
	} //if

	return true;
}

//-------------------------------------------------------------------------

void RequestInfo::ShowToWebUser(LPCTSTR pstrout/* = nullptr*/, LPCTSTR RequestedURL/* = nullptr*/)
{
	char *pstr   = "HTTP/1.1 502 Bad Gateway\r\n" 
		"Server: Easy-Proxy\r\n""Connection: close\r\n" 
#ifdef _UNICODE
		"Content-Type: text/html; charset=utf-16\r\n"
#else
		"Content-Type: text/html; charset=gb2312\r\n" 
#endif // _UNICODE
		"\r\n";
	int pstrbyte = strlen(pstr);
	if(!ClientSocket->Send(pstr, &pstrbyte)) goto exit;

#ifdef _UNICODE
	byte utf16[] = {0xffU, 0xfeU };
	this->SendToUser(utf16, sizeof(utf16));
#endif

	LPCTSTR pstrbody = T("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n")
#ifdef _UNICODE
		T("<HTML>\r\n<HEAD>\r\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-16\">\r\n")
#else
		T("<HTML>\r\n<HEAD>\r\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=gb2312\">\r\n")
#endif // _UNICODE
		T("<TITLE>Server Error</TITLE>\r\n")
		T("<STYLE type=\"text/css\">body {font-family:\"Verdana\";font-weight:normal;font-size: .7em;color:black;} \r\n")
		T("p {font-family:\"Verdana\";font-weight:normal;color:black;margin-top: -5px}\r\n")
		T("b {font-family:\"Verdana\";font-weight:bold;color:black;margin-top: -5px}\r\n")
		T("H1 { font-family:\"Verdana\";font-weight:normal;font-size:18pt;color:red }\r\n")
		T("H2 { font-family:\"Verdana\";font-weight:normal;font-size:14pt;color:maroon }\r\n")
		T("pre {font-family:\"Lucida Console\";font-size: .9em}\r\n")
		T(".marker {font-weight: bold; color: black;text-decoration: none;}\r\n")
		T(".version {color: gray;}\r\n")
		T(".error {margin-bottom: 10px;}\r\n")
		T(".expandable { text-decoration:underline; font-weight:bold; color:navy; cursor:hand; }\r\n")
		T("</STYLE>\r\n")
		T("</HEAD>\r\n<body bgcolor=\"white\"><span>\r\n")
		T("<H1>Server Error in Web Application.<hr width=100% size=1 color=silver></H1>\r\n")
		T("<h2> <i>");

	pstrbyte = TSIZE(TLEN(pstrbody) - 1);
	if(!ClientSocket->Send((char *)pstrbody, &pstrbyte)) goto exit;

	if (pstrout == nullptr) pstrout = T("未知错误");
	pstrbyte = TSIZE(TLEN(pstrout) - 1);
	if(!ClientSocket->Send((char *)pstrout, &pstrbyte)) goto exit;

	pstrbody = T("</i> </h2></span>\r\n<b> Requested URL: </b>");
	pstrbyte = TSIZE(TLEN(pstrbody) - 1);
	if(!ClientSocket->Send((char *)pstrbody, &pstrbyte)) goto exit;

	if (RequestedURL == nullptr) RequestedURL = T("nullptr");
	pstrbyte = TSIZE(TLEN(RequestedURL) - 1);
	if(!ClientSocket->Send((char *)RequestedURL, &pstrbyte)) goto exit;

	pstrbody = T("<br><br>\r\n</BODY>\r\n</HTML>");
	pstrbyte = TSIZE(TLEN(pstrbody) - 1);
	if(!ClientSocket->Send((char *)pstrbody, &pstrbyte)) goto exit;
exit:
	ClientSocket->Shutdown();
}

//-------------------------------------------------------------------------

bool RequestInfo::SendToUser(LPCVOID pbytes, int nbytes)
{
	return ClientSocket->Send((char *)pbytes, &nbytes);
}

//-------------------------------------------------------------------------

char *RequestInfo::recv_data(int *pbuf_size/* = nullptr OUT*/)
{
	int block     = ClientSocket->GetRcvBuf(); //分块大小
	int buf_size  = block;                     //缓冲区大小
	int free_size = buf_size;                  //缓冲区大小 
	char *buf     = (char *)RLIB_GlobalAlloc(free_size);
	bool retend   = false; //是否收完请求头 \r\n\r\n之前   
	int retval    = 0;     //已接收大小
	int retcur    = 0;     //最近一次接收大小
	int postlen   = 0;     //记录POST数据大小(如果有) + 头部大小
begin_recv: 
	while ((retcur = ClientSocket->Recv(buf + retval, free_size)) > 0)
	{
		retval += retcur;
		if (!retend)//如果没收完头部
		{
			char *pend = strstr(buf, "\r\n\r\n");
			if (pend != nullptr)//收完头部
			{
				if ((pend - buf) > 4096)
				{
					this->LastError = T("Request Header Too Long");
					goto err_catch;
				} //if

				if (*buf != 'P')
				{
					//非PUT、POST方式 则判断数据已收完
					goto recv_success;
				}

				char *post_len_str = get_header_value_ansi(buf, "Content-Length");
				if (post_len_str == nullptr)
				{
					this->LastError = T("Content-Length Required");
					goto err_catch;
				}

				postlen = Int32::TryParse(post_len_str);
				postlen += ((pend -  buf) + 4);
				retend = true;

				AppBase::Collect(post_len_str);
			}
			//判断附加数据是否接收完毕
			if (retend && (retval >= postlen))
			{
				goto recv_success;
			}
			free_size -= retcur;
check_enough_memory: 
			if (free_size <= 0)
			{
				free_size += block;
				buf_size  += block;
				auto p    = (char *)AppBase::GetUsingPool()->ReAlloc(buf, buf_size);
				if (p == nullptr)
				{
					this->LastError = T("内存不足");
					goto err_catch;
				} //if
				buf = p;
				goto check_enough_memory;
			}
			goto begin_recv; //循环直到收完头部
		}
		//此处是附加数据的接收过程, 判断是否接收完毕
		if (retval >= postlen)
		{
			goto recv_success;
		}
		free_size -= retcur;
check_enough_memory_2: 
		if (free_size <= 0)
		{
			free_size += block;
			buf_size  += block;
			auto p    = (char *)AppBase::GetUsingPool()->ReAlloc(buf, buf_size);
			if (p == nullptr)
			{
				this->LastError = T("内存不足");
				goto err_catch;
			} //if
			buf = p;
			goto check_enough_memory_2;
		}
		continue;
	}

	if(retcur == SOCKET_ERROR) this->LastError = T("接收请求时发生异常, 请检查客户端");

err_catch: 
	if (pbuf_size != nullptr) *pbuf_size = 0;
	if (buf != nullptr) AppBase::Collect(buf);
	return nullptr;

recv_success:
	if (pbuf_size != nullptr)
	{
		*pbuf_size = AppBase::GetUsingPool()->ReSize(buf, retval) ? retval : buf_size;
	} //if

	return buf;
}

//-------------------------------------------------------------------------

void RequestInfo::SendSeverResponseHeader(int Code,
	LPCTSTR pDescription, LPCTSTR pHeaders/* = nullptr*/)
{
	String strTmp = String(T("HTTP/1.1 ")) + (Int32(Code).ToString() + T(" ")
		+ pDescription + T("\r\n"));

	if (pHeaders != nullptr)
	{
		strTmp += pHeaders;
	} //if

	strTmp += 
		T("Server: Microsoft-IIS/7.5\r\n")
		T("X-Powered-By: C++\r\n")
		T("Connection: close\r\n\r\n");

	this->SendToUser(strTmp.ToMultiByte(), strTmp.GetMultiByteSize());
} 