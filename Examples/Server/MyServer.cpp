/********************************************************************
	Created:	2012/07/29  20:05
	Filename: 	MyClient.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MyServer.h"

//-------------------------------------------------------------------------

void Server::DoMonitoringTask(Server *pMySelf)
{
	if (pMySelf->pWorkingSocket == nullptr)
	{
		if ((pMySelf->pWorkingSocket = new System::Net::Sockets) == nullptr)
		{
			String Err(MAX_NUM_REASONS);
			System::Net::Sockets::GetLastError(Err, TSIZE(MAX_NUM_REASONS));
			printf(Err.ToMultiByte());
			goto return_alert;
		} //if

		if (pMySelf->pWorkingSocket->GetLastException()->HResult != STATUS_SUCCESS)
		{
			goto output_err;
		}

		if (pMySelf->pWorkingSocket->Bind(80) == SOCKET_ERROR)
		{
			goto output_err;
		}

		if (pMySelf->pWorkingSocket->Listen() == SOCKET_ERROR)
		{
			goto output_err;
		}

		printf("服务运行中, 监听端口80...\r\n");
	} //if

	//printf("线程%d开始监听...\r\n", GetCurrentThreadId());

	sockaddr_in           ClientAddr;
	System::Net::Sockets *ClientSocket;
	int len = sizeof(sockaddr_in), FailedCount = 0;
	while ((ClientSocket = pMySelf->pWorkingSocket->Accept(&ClientAddr, &len)) != NULL)
	{
		RequestInfo *pInfo = new RequestInfo();
		if (pInfo != nullptr)
		{
#ifdef ENABLE_LEADER_FOLLOWER
			while(!pMySelf->pThreadPool->AddTask(Threading::WaitCallback(Server::DoMonitoringTask),
				pMySelf));
#endif // ENABLE_LEADER_FOLLOWER

			System::IO::Memory::memcpy(&pInfo->ClientAddr, &ClientAddr, sizeof(sockaddr_in));
			pInfo->ClientSocket = ClientSocket;
			pInfo->pServer      = pMySelf;

#ifdef ENABLE_LEADER_FOLLOWER
			//printf("线程%d接受%s请求...\n", GetCurrentThreadId(), Net::Sockets::Ipv4AddressToString(ClientAddr.sin_addr));
			pMySelf->DoUserRequestTask(pInfo);
			//printf("        线程%d完成%s请求.\n", GetCurrentThreadId(), Net::Sockets::Ipv4AddressToString(ClientAddr.sin_addr));
			/*Int32 bytes = */AppBase::GetUsingPool()->TryGCCollect();
			//if (bytes > 1024) printf("        成功释放 %d KB内存.\n", bytes / 1024);
			//必须, 将当前工作线程返回给线程池
			//printf("\n");
			return;
#else
			if(pMySelf->pThreadPool->AddTask(Threading::WaitCallback(BeginDoUserRequestTask),
				pInfo))
			{
				printf("派遣成功\n");
				continue;
			}
			printf("派遣失败\n");
#endif // ENABLE_LEADER_FOLLOWER
		} //if

		FailedCount++;
		printf("内存申请失败, 内存不足吗?计数:%d", FailedCount);
		delete ClientSocket;

		if (FailedCount >= 10)
		{
			printf("失败次数累计10次, 监听被迫自动中止.");
			goto done;
		} //if
	}

	//除非服务器发生异常, 否则永远不能执行到这里

	if (pMySelf->pWorkingSocket->GetLastException()->HResult != STATUS_SUCCESS)
	{
output_err:
		printf(String(pMySelf->pWorkingSocket->GetLastException()->Message).ToMultiByte());
	} //if

done:
	delete pMySelf->pWorkingSocket;

return_alert:
	pMySelf->pMainThread->Alert();

	printf("\n服务已关闭.");
	return;
}

//-------------------------------------------------------------------------

void Server::BeginDoUserRequestTask(RequestInfo *pInfo)
{
	pInfo->pServer->DoUserRequestTask(pInfo);
}

//-------------------------------------------------------------------------

void Server::DoUserRequestTask(RequestInfo *pInfo)
{
	pInfo->ClientSocket->SetSndTimeO(8000);
	pInfo->ClientSocket->SetRcvTimeO(1000);

	RequestHeader header;

	if(!pInfo->Prepare())
	{
		goto exit_collect;
	}

	if (!pInfo->parse_http_response(&header.p_ori_request_line, &header.p_ori_request_line_end,
		&header.p_ori_headers, &header.ori_headers_bytes, &header.p_ori_data, &header.ori_data_bytes))
	{
		pInfo->LastError = T("请求报文无效, 请更换浏览器再试");
		goto exit_collect;
	} //if

	//-------------------------------------------------------------------------

	if ((header.p_ori_request_line_end - header.p_ori_request_line) < sizeof("GET / HTTP/1.1") - 1) 
	{
invalid_line:
		pInfo->LastError = T("请求行无效, 请更换浏览器再试");
		goto exit;
	}

	if(header.p_ori_request_line[0] != 'G' && header.p_ori_request_line[0] != 'P' && header.p_ori_request_line[1] != 'O')
	{
		pInfo->LastError = T("仅支持GET和POST方法");
		goto exit;
	}

	char *tp, *tp2;
	if ((tp = strstr(header.p_ori_request_line, " ")) == nullptr)
	{
		goto invalid_line;
	}
	tp += 1;
	if ((tp2 = strstr(tp, "http://")) == tp)
	{
		tp = strstr(tp2 + sizeof("http://") - 1, "/");
		if (tp == nullptr)
		{
			pInfo->LastError = T("请求行无效, 如果使用代理访问, 请更换或关闭代理再试");
			goto exit;
		} //if
	} //if

	if ((tp2 = strstr(tp, " ")) == nullptr)
	{
		goto invalid_line;
	}
	if (!pInfo->parse_host_name(header.p_ori_headers))
	{
		//已经设置错误
		goto exit;
	}

	header.ori_request_path_bytes = tp2 - tp;
	header.p_ori_request_path     = (char *)RLIB_GlobalAlloc(header.ori_request_path_bytes + 1);
	memcpy(header.p_ori_request_path, tp, header.ori_request_path_bytes);
	if (header.ori_request_path_bytes == 1 && header.p_ori_request_path[0] == '/')
	{
		pInfo->SendSeverResponseHeader(301, T("Moved"), T("Location: Default.cpp\r\n"));
		goto exit_2;
	} //if

	//-------------------------------------------------------------------------
	tp = strstr(header.p_ori_request_path, "?");
	if (tp != nullptr)
	{
		tp[0] = '\0';
	} //if
	header.RequestURL = Net::Uri::UrlDecode
		(String(header.p_ori_request_path));
	if (tp != nullptr)
	{
		tp[0] = '?';
		header.RequestQuery = String(tp + 1);
	} //if

	this->DoResponseUserTask(pInfo, &header);
	pInfo->ClientSocket->Shutdown();

	//-------------------------------------------------------------------------
exit_2:
	AppBase::Collect(header.p_ori_request_path);

exit:
	if (header.p_ori_data != nullptr) AppBase::Collect(header.p_ori_data);
	AppBase::Collect(header.p_ori_headers);
	AppBase::Collect(header.p_ori_request_line);

exit_collect:
	if (pInfo->LastError != nullptr)
		pInfo->ShowToWebUser(pInfo->LastError);

	delete pInfo->ClientSocket;
	delete pInfo;
}
