/********************************************************************
	Created:	2012/08/10  14:42
	Filename: 	MySite.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MyServer.h"

//-------------------------------------------------------------------------

#define ResponseWrite(Content)       this->pRequestInfo->SendToUser(Content, sizeof(Content) - sizeof(TCHAR));
#define ResponseWriteString(Content) this->pRequestInfo->SendToUser(LPCTSTR(Content), Content.CanReadSize);

#define _ResponseWrite(Content)       _this->pRequestInfo->SendToUser(Content, sizeof(Content) - sizeof(TCHAR));
#define _ResponseWriteString(Content) _this->pRequestInfo->SendToUser(LPCTSTR(Content), Content.CanReadSize);

//-------------------------------------------------------------------------

#define HTTP_PAGE __declspec(dllexport)
typedef void (*HTTP_PAGE_HANDLER)(class HttpContext *_this);

/************************************************************************/
/* ������վ���                                                          */
/************************************************************************/
class HttpContext
{
public:
	RequestInfo   *pRequestInfo;
	RequestHeader *pRequestHeader;

public:
	void OnPageLoad();
	void OnResponse();
	void OnPageBegin();
	void SetPageTitle(LPCTSTR title);
	void OnPageEnd();

public:
	void NewHref(String Name, String Url, bool crlf = true);
	String LoadHttpPage(String Url, String Cookie, String Referer);
public:
	/// <summary>
	/// Ѱ��Ĭ��ҳ��
	/// </summary>
	static bool TryFindDefaultPage(String dir)
	{
		return false;//��֧�ֹ�
	}
	/// <summary>
	/// ���Լ���ҳ��
	/// </summary>
	static bool TryLoadPage(String path)
	{
		return (path.LastIndexOfR(T(".cpp")) == INT(path.Length));
	}
};