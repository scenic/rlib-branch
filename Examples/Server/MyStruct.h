/********************************************************************
	Created:	2012/03/03  17:32
	Filename: 	Program.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifdef _DEBUG
    #pragma comment(lib, "../../bin/RLib_d.lib")
#else
    #pragma comment(lib, "../../bin/RLib.lib")
#endif

#include "../../RLib/RLib_Import.h"
using namespace System;
using namespace System::Collections::Generic;

//符号定义
#define CONFIG_FILE_NAME        T("config.ini")
#define GLOBAL_LOG_FILE_NAME    T("global.log")
#define MAX_HOST_NAME           256


/************************************************************************/
/* 定义用户请求报文结构                                                  */
/************************************************************************/
typedef struct RequestHeader
{
	//原始请求数据
	int   ori_headers_bytes, ori_data_bytes, ori_request_path_bytes;
	char *p_ori_request_line, *p_ori_request_line_end, *p_ori_headers, 
		*p_ori_data, *p_ori_request_path;
	//用户请求地址
	String RequestURL;
	//用户附加请求
	String RequestQuery;
	//对应服务器文件路径
	String RequestPath;

public:
	String GetQueryString(String findName, String defaultValue = Nothing)
	{
		findName += T("=");
		if (this->RequestQuery.IndexOf(findName) != -1)
		{
			String a = this->RequestQuery.Match(findName, T("&"));
			if (a.IsNullOrEmpty())
			{
				a = this->RequestQuery.Substring(this->RequestQuery.IndexOfR(findName));
				if (a.IsNullOrEmpty())
				{
					return defaultValue;
				} //if
			} //if
			return a;
		}
		return defaultValue;
	}
	String GetHeaderValue(char *name)
	{
		char *pheader = strstr(this->p_ori_headers, name);
		if (pheader != nullptr)
		{
			pheader += (strlen(name) + 2);
			char *pheaderEnd = strstr(pheader, "\r\n");
			String str;
			str.Copy(pheader, pheaderEnd - pheader);
			return str;
		}
		return Nothing;
	}
}RequestHeader;


/************************************************************************/
/* 定义用户请求信息结构                                                  */
/************************************************************************/
struct RequestInfo
{
public:
	RequestInfo():LastError(nullptr),pbuf(nullptr){};
	~RequestInfo(){ if (pbuf != nullptr) AppBase::Collect(pbuf); }
public:
	LPCTSTR               LastError;
public:
	class Server         *pServer;
	sockaddr_in           ClientAddr;                      
	System::Net::Sockets *ClientSocket;  
public:
	String                HostName;
	UInt32                Port;
public:
	char                 *pbuf;
	int                   buf_size;
public:
	static char *get_header_value_ansi(char *headers, char *name);
public:
	bool parse_http_response(char **ppRequestLine, char **ppRequestLineEnd,
		char **ppHeaders, int *pHeadersBytes, char **ppData, int *pDataBytes);
	bool parse_host_name(char *RecvBuf);
	char *recv_data(int *pbuf_size = nullptr OUT);
public:
	bool Prepare();
public:
	void ShowToWebUser(LPCTSTR pstrout = nullptr, LPCTSTR RequestedURL = nullptr);
	bool SendToUser(LPCVOID pbytes, int nbytes);
	/// <summary>
	/// 负责发送服务器响应标头
	/// </summary>
	void SendSeverResponseHeader(int Code, LPCTSTR pDescription, LPCTSTR pHeaders = nullptr);
};