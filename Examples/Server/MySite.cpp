/********************************************************************
	Created:	2012/08/10  15:03
	Filename: 	MySite.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MySite.h"
using namespace System::Net;

//-------------------------------------------------------------------------

String HttpContext::LoadHttpPage(String Url, String Cookie, String Referer)
{
	auto r = WebClient::GetHttpRequest(Url);
	if (r == nullptr)
	{
		return Nothing;
	} //if
	if(!Cookie.IsNullOrEmpty()) r->Headers.Add("Cookie", Cookie.ToMultiByte());
	r->Referer = Referer;

	auto s = WebClient::GetResponseText(Nothing, r);
	delete r;

	return s;
}

//-------------------------------------------------------------------------

void HttpContext::NewHref(String Name, String Url, bool crlf/* = true*/)
{
	ResponseWrite(T("<a href=\""));
	ResponseWriteString(Url);
	ResponseWrite(T("\">"));
	ResponseWriteString(Name);
	ResponseWrite(T("</a>"));
	if (crlf) ResponseWrite(T("<br/>\r\n"));
}

//-------------------------------------------------------------------------

void HttpContext::OnPageLoad()
{
	String PageName = this->pRequestHeader->RequestURL.ToLower();
	if (PageName.StartsWith(T('/')))
		PageName = PageName.Substring(1);
	PageName = PageName.Replace(T("/"), T("$"));
	PageName.SetAt(PageName.LastIndexOf(T(".")), T('_'));

	String ProcName = T("?");
	ProcName += PageName;
	ProcName += T("@@YAXPAVHttpContext@@@Z");

	HMODULE hPages             = GetModuleHandle(nullptr);
	HTTP_PAGE_HANDLER hHandler = (HTTP_PAGE_HANDLER)GetProcAddress
		(hPages, ProcName.ToMultiByte());
	if (hHandler != nullptr)
	{
		hHandler(this);
	} //if
	else
	{
		OnResponse();
		OnPageBegin();
		SetPageTitle(T("额, 找不到请求的页面"));
		ResponseWrite(T("<big>404, 看你懂不懂...</big>"));
	}

	OnPageEnd();
}

//-------------------------------------------------------------------------

void HttpContext::OnResponse()
{
	this->pRequestInfo->SendSeverResponseHeader(200, T("OK"), T("Content-Type: text/vnd.wap.wml\r\n"));
}

//-------------------------------------------------------------------------

void HttpContext::SetPageTitle(LPCTSTR title)
{
	ResponseWrite(T("<card id=\"main\" title=\""));
	this->pRequestInfo->SendToUser(title, TSIZE(TLEN(title) - 1));
	ResponseWrite(T("\">\r\n"));
	ResponseWrite(T("<p>\r\n"));
}

//-------------------------------------------------------------------------

void HttpContext::OnPageBegin()
{
#ifdef _UNICODE
	byte utf16[] = {0xffU, 0xfeU };
	this->pRequestInfo->SendToUser(utf16, sizeof(utf16));
#endif

#ifdef _UNICODE
	ResponseWrite(T("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n"));
#else
	ResponseWrite(T("<?xml version=\"1.0\" encoding=\"gb2312\"?>\r\n"));
#endif // _UNICODE

	ResponseWrite(T("<!DOCTYPE wml PUBLIC \"-//WAPFORUM//DTD WML 1.1//EN\" \"http://www.wapforum.org/DTD/wml_1.1.xml\">\r\n"));
	ResponseWrite(T("<wml>\r\n"));
	ResponseWrite(T("<head>\r\n<meta http-equiv=\"Cache-Control\" content=\"max-age=0\"/>\r\n</head>\r\n"));
}

//-------------------------------------------------------------------------

void HttpContext::OnPageEnd()
{
	ResponseWrite(T("\r\n</p>\r\n</card>\r\n</wml>"));
}