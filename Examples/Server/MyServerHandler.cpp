/********************************************************************
	Created:	2012/08/10  10:15
	Filename: 	MyServerHandler.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MySite.h"

//-------------------------------------------------------------------------
// 对于数据文件, 直接原样发送
//-------------------------------------------------------------------------
void SendFileToUser(RequestInfo *pRequestInfo, RequestHeader *pRequestHeader)
{
// 	//断点续传简单支持
// 	long begin  = -1, end = 0;
// 	auto pRange = pRequestInfo->get_header_value_ansi(pRequestHeader->pHeaders,
// 		"Range");//bytes=409600-
// 	if (pRange != nullptr)
// 	{
// 		auto pRangeSep = pRange + sizeof("bytes=") - 1;
// 		begin = Int32::TryParse(pRangeSep);
// 
// 		pRangeSep = strstr(pRangeSep, "-");
// 		if (pRangeSep != nullptr)
// 			end   = Int32::TryParse(pRangeSep + 1);
// 
// 		RLIB_GlobalCollect(pRange);
// 
// 		assert(end == 0);//不支持这个值
// 	} //if
	
	//发送数据文件
	auto pFile = IO::File::ReadStream(pRequestHeader->RequestPath);
	if (pFile == nullptr)
	{
		pRequestInfo->ShowToWebUser(T("文件暂时不可用"), pRequestHeader->RequestURL);
		return;
	} //if
	
	pRequestInfo->SendSeverResponseHeader(200, T("OK"),
		String(T("Content-Length: ")) + Int32(pFile->Length).ToString()
		+ T("\r\n"));

	int  blocksize = pRequestInfo->ClientSocket->GetSndBuf();
	int  filesize  = 0, filesize_all = pFile->Length;
	if (filesize_all > 0)
	{
		auto pchBuffer = (char *)RLIB_GlobalAlloc(blocksize);

// 		if (begin > 0)
// 		{
// 			pFile->Position = begin;
// 			filesize_all   -= begin;
// 		} //if
		while (filesize_all > 0)
		{
			filesize = pFile->Read(pchBuffer, blocksize);
			if(!pRequestInfo->SendToUser(pchBuffer, filesize))
			{
				break;
			}
			filesize_all -= filesize;
		}

		AppBase::Collect(pchBuffer);
	} //if

	delete pFile;
}

//-------------------------------------------------------------------------

void ListDirectoryAndFiles(RequestInfo *pRequestInfo, String &RequestPath)
{
	pRequestInfo->SendSeverResponseHeader(200, T("OK"));
	pRequestInfo->SendToUser("<html><body>", sizeof("<html><body>") -1);

	WIN32_FIND_DATA wfd = {0};
	auto hFindFile      = FindFirstFile(RequestPath + T("*.*"), &wfd);
	while(FindNextFile(hFindFile, &wfd) == TRUE)
	{
		String strFile = String(LPCTSTR(wfd.cFileName));
		if (strFile != T(".."))
		{
			strFile = String("<a href=\"") + 
				((strFile.IndexOf(T(".")) == -1) ? (strFile + T("/")) : strFile)
				+ T("\">") + strFile + T("</a>\r\n<br/>");
			pRequestInfo->SendToUser(strFile.ToMultiByte(), strFile.GetMultiByteSize());
		} //if
	}

	pRequestInfo->SendToUser("</html></body>", sizeof("</html></body>") -1);
}

//-------------------------------------------------------------------------

void Server::DoResponseUserTask(RequestInfo *pRequestInfo, RequestHeader *pRequestHeader)
{
	pRequestHeader->RequestPath = IO::Path(String(T("webroot")) + pRequestHeader->RequestURL).FullPath;
	//判断是否为文件夹, 嗯, 这个判断方法是不完全正确的, 但是暂时就这样了
	if (pRequestHeader->RequestPath.EndsWith(T('\\')))
	{
		//先判断 Default.*** 是否存在
		if (HttpContext::TryFindDefaultPage(pRequestHeader->RequestPath))
		{

		} //if

		//判断文件夹是否存在
		if (!IO::File::Exist(pRequestHeader->RequestPath))
		{
			pRequestInfo->ShowToWebUser(T("请求的文件夹不存在"), pRequestHeader->RequestURL);
			return;
		} //if

		ListDirectoryAndFiles(pRequestInfo, pRequestHeader->RequestPath);
		return;
	} //if

	//这里可以是虚拟路径
	if (HttpContext::TryLoadPage(pRequestHeader->RequestPath))
	{
		HttpContext context;
		context.pRequestInfo   = pRequestInfo;
		context.pRequestHeader = pRequestHeader;
		context.OnPageLoad();

		return;
	} //if

	//判断文件是否存在
	if (!IO::File::Exist(pRequestHeader->RequestPath))
	{
		pRequestInfo->ShowToWebUser(T("请求的文件不存在"), pRequestHeader->RequestURL);
		return;
	} //if

	SendFileToUser(pRequestInfo, pRequestHeader);
}