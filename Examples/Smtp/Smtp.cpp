/********************************************************************
	Created:	2012/05/26  16:08
	Filename: 	Loader.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "../../RLib/RLib_Import.h"

#ifdef _DEBUG
#pragma comment(lib, "../../bin/RLib_d.lib")
#else
#pragma comment(lib, "../../bin/RLib.lib")
#endif

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace System::Net::Mail;
//-------------------------------------------------------------------------
void test_smtp_by_rrrfff()
{
	SmtpClient smtp;
	if(!smtp.Connect(T("smtp.163.com"), 25))
	{
		MailException *ex = smtp.GetLastException();
		//ex->Message;
		return;
	}
	if(!smtp.Login(T("rlib_smtpclient"), T("rlib123456")))
	{
		MailException *ex = smtp.GetLastException();
		//ex->Message;
		return;
	}

	MailMessage mailmsg;
	mailmsg.From     = T("rlib_smtpclient@163.com");
	mailmsg.Subject  = T("感谢使用RLIB开源框架");
	mailmsg.Body     = T("这封信来自RLib...");
	mailmsg.Sender   = T("rlib_smtpclient@163.com");

	MailAddress mailaddr;
	mailaddr.Address = T("rrrfff@foxmail.com");

	mailmsg.To->Add(mailaddr);
	Attachment attach;
	attach.ContentType = "application/octet-stream";
	attach.Encoding    = "Base64";
	attach.Name        = "photo.png";
	attach.Path        = "C:\\rlib.png";
	mailmsg.Attachments->Add(attach);

	if(!smtp.Send(mailmsg))
	{
		MailException *ex = smtp.GetLastException();
		//ex->Message;
		return;
	}
	if(!smtp.Logout())
	{
		MailException *ex = smtp.GetLastException();
		//ex->Message;
		return;
	}
}
//-------------------------------------------------------------------------
int __stdcall WinMain( __in HINSTANCE /*hInstance*/, __in_opt HINSTANCE /*hPrevInstance*/, __in LPSTR /*lpCmdLine*/, __in int /*nShowCmd*/ )
{
	test_smtp_by_rrrfff();
	return 0;
}