/********************************************************************
	Created:	2012/05/26  16:08
	Filename: 	Loader.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "../../RLib/RLib_Import.h"

#ifdef _DEBUG
#pragma comment(lib, "../../bin/RLib_d.lib")
#else
#pragma comment(lib, "../../bin/RLib.lib")
#endif

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace System::Net;
using namespace System::Text;
//-------------------------------------------------------------------------
void testHEAD()
{
	/// <summary>
	/// HTTP HEAD 获取网页响应头
	/// </summary>
	auto pRequest = WebClient::GetHttpRequest(T("http://blog.csdn.net/rrrfff"));
	//Not supported: WebClient::GetHttpRequest(T("blog.csdn.net/rrrfff"));
	pRequest->Method = T("HEAD");
	//获取完整响应流 
	auto pStream  = pRequest->GetResponseStream();
	if (pStream == nullptr)
	{
		auto pException = pRequest->GetLastException();
		//Handle Error Here. Don't delete pException
		delete pRequest;
		return;
	}
	//响应信息使用ANSI编码, 现在转换成Unicode以便更好的在C++中处理
	pStream->Position     = 0;//从起始位置开始转换
	auto pStreamConverted = Encoder::ToWideChar(ASCIIEncoding, pStream);
	if (pStreamConverted == nullptr)
	{
		//Unkonwn Error, 可能是原始数据错误
		//Don't delete pStream but do so:
		delete pRequest;
		return;
	}
	//pStream->Position   = 0;
	String Text(pStreamConverted->Length / sizeof(wchar_t));
	pStreamConverted->Read(Text, pStreamConverted->Length);
	//Delete Objects
	delete pStreamConverted;
	delete pRequest;
	//Show to user
}
void testGET()
{
	String Text = Net::WebClient::GetResponseText(T("http://blog.csdn.net/rrrfff"));
}
void testPOST()
{
	/// <summary>
	/// HTTP POST
	/// </summary>
	auto pRequest         = WebClient::GetHttpRequest(T("http://blog.csdn.net/rrrfff/comment/submit?id=6170653"));
	pRequest->Referer     = T("http://blog.csdn.net/rrrfff/article/details/6170653");
	pRequest->Method      = T("POST");
	pRequest->ContentType = "application/x-www-form-urlencoded; charset=gb2312";
	pRequest->Headers.Add("Cookie", "UN=rrrfff; UserName=rrrfff; UserInfo=rlibynbWgCLc0o2zJNhs4HrlibSMNJShEYey1aeH9kz1SW8o");
	//post data
	String postData       = T("commentid=&content=rlib&replyId=");
	pRequest->ContentLength = postData.GetMultiByteSize();
	pRequest->GetRequestStream()->Write(postData.ToMultiByte(), pRequest->ContentLength);
	//获取响应Body
	String Text = WebClient::GetResponseText(Nothing, pRequest);
	delete pRequest; 
	//Show to user
}
//-------------------------------------------------------------------------
int __stdcall WinMain( __in HINSTANCE /*hInstance*/, __in_opt HINSTANCE /*hPrevInstance*/, __in LPSTR /*lpCmdLine*/, __in int /*nShowCmd*/ )
{
	testHEAD();
	testGET();
	testPOST();
	return 0;
}