// MyTest.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <regex>
#include <iostream>
#include <string>

#pragma comment(lib, "../../Bin/RLib_d.lib")

int _tmain(int argc, _TCHAR* argv[])
{
    //::System::Net::HttpRequest req;
    String Text = Net::WebClient::GetResponseText(T("http://www.baidu.com/"));
    Text.ToMultiByte();
    //typedef std::match_results<const char*> cmatch;
    std::string str;

    std::tr1::cmatch res;
    str = "<h2>Egg prices</h2>";
    //std::tr1::regex rx("<h(.)>([^<]+)");
    //std::tr1::regex_search(str.c_str(), res, rx);
    std::tr1::regex rx("<title>([^<]+)");
    std::tr1::regex_search(Text.ToMultiByte(), res, rx);
    //_ASSERT(res.size() == 3);
    std::cout << res[1] << ". " << res[2] << "\n";

    {

        std::string str = "Hello world work";
        std::tr1::regex rx("w(\\w{4})");
        std::string replacement = "xxxx\\1";
        std::tr1::regex replacexx("ssss\\1");
        std::string str2 = std::tr1::regex_replace(str, rx, replacement);
        std::cout << str2 << "\n";
    }

	return 0;
}

