/********************************************************************
	Created:	2012/03/03  17:33
	Filename: 	Program.cpp
	Author:		rrrfff
	Input:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MyServer.h"
//-------------------------------------------------------------------------
int main()
{
	HANDLE hMainThread = GetCurrentThread();
	if(!DuplicateHandle(GetCurrentProcess(), hMainThread,
		GetCurrentProcess(), &hMainThread, 0, FALSE, DUPLICATE_SAME_ACCESS))
	{
		return -1;
	}

	Threading::ThreadPool *pThreadPool = new Threading::ThreadPool();

	Server *pServer      = new Server;
	pServer->pMainThread = new Threading::Thread(hMainThread, GetCurrentThreadId(), TRUE);
	pServer->pThreadPool = pThreadPool;

	//线程需要自我销毁
	pServer->pMainThread->IsSuppressChangeState = true;

	pThreadPool->AddTask(Threading::WaitCallback(Server::DoMonitoringTask),
		pServer);

	pThreadPool->Dispatch();

	pServer->pMainThread->Sleep();

	delete pServer->pThreadPool;
	delete pServer->pMainThread;
	delete pServer;

	return 0;
}
