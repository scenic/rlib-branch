/********************************************************************
	Created:	2012/03/03  17:32
	Filename: 	Program.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifdef _DEBUG
    #pragma comment(lib, "../../bin/RLib_d.lib")
#else
    #pragma comment(lib, "../../bin/RLib.lib")
#endif

#include "../../RLib/RLib_Import.h"
using namespace System;
using namespace System::Collections::Generic;

/************************************************************************/
/* 定义用户请求信息结构                                                  */
/************************************************************************/
struct RequestInfo
{
public:
	RequestInfo():LastError(nullptr){};
public:
	LPCTSTR               LastError;
	LPSTR                 RequestText; 
	size_t                RequestTextSize;
public:
	class Server         *pServer;
	sockaddr_in           ClientAddr;                      
	System::Net::Sockets *ClientSocket;  
public:
	bool Send(LPCVOID pbytes, int nbytes);
	bool RecvLine(int *pnbytes = nullptr);
	void DoCoreTask();
};