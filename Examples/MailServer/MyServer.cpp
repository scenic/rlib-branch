/********************************************************************
	Created:	2012/07/29  20:05
	Filename: 	MyClient.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MyServer.h"

//-------------------------------------------------------------------------

void Server::DoMonitoringTask(Server *pMySelf)
{
	if (pMySelf->pWorkingSocket == nullptr)
	{
		if ((pMySelf->pWorkingSocket = new System::Net::Sockets) == nullptr)
		{
			String Err(MAX_NUM_REASONS);
			System::Net::Sockets::GetLastError(Err, TSIZE(MAX_NUM_REASONS));
			printf(Err.ToMultiByte());
			goto return_alert;
		} //if

		if (pMySelf->pWorkingSocket->GetLastException()->HResult != STATUS_SUCCESS)
		{
			goto output_err;
		}

		if (pMySelf->pWorkingSocket->Bind(25) == SOCKET_ERROR)
		{
			goto output_err;
		}

		if (pMySelf->pWorkingSocket->Listen() == SOCKET_ERROR)
		{
			goto output_err;
		}

		printf("服务运行中, 监听端口25...\r\n");
	} //if

	//printf("线程%d开始监听...\r\n", GetCurrentThreadId());

	sockaddr_in           ClientAddr;
	System::Net::Sockets *ClientSocket;
	int len = sizeof(sockaddr_in), FailedCount = 0;
	while ((ClientSocket = pMySelf->pWorkingSocket->Accept(&ClientAddr, &len)) != NULL)
	{
		RequestInfo *pInfo = new RequestInfo();
		if (pInfo != nullptr)
		{
#ifdef ENABLE_LEADER_FOLLOWER
			while(!pMySelf->pThreadPool->AddTask(Threading::WaitCallback(Server::DoMonitoringTask),
				pMySelf));
#endif // ENABLE_LEADER_FOLLOWER

			System::IO::Memory::memcpy(&pInfo->ClientAddr, &ClientAddr, sizeof(sockaddr_in));
			pInfo->ClientSocket = ClientSocket;
			pInfo->pServer      = pMySelf;

#ifdef ENABLE_LEADER_FOLLOWER
			//printf("线程%d接受%s请求...\n", GetCurrentThreadId(), Net::Sockets::Ipv4AddressToString(ClientAddr.sin_addr));
			pMySelf->DoUserRequestTask(pInfo);
			//printf("        线程%d完成%s请求.\n", GetCurrentThreadId(), Net::Sockets::Ipv4AddressToString(ClientAddr.sin_addr));
			/*Int32 bytes = */AppBase::GetUsingPool()->TryGCCollect();
			//if (bytes > 1024) printf("        成功释放 %d KB内存.\n", bytes / 1024);
			//必须, 将当前工作线程返回给线程池
			//printf("\n");
			return;
#else
			if(pMySelf->pThreadPool->AddTask(Threading::WaitCallback(BeginDoUserRequestTask),
				pInfo))
			{
				printf("派遣成功\n");
				continue;
			}
			printf("派遣失败\n");
#endif // ENABLE_LEADER_FOLLOWER
		} //if

		FailedCount++;
		printf("内存申请失败, 内存不足吗?计数:%d", FailedCount);
		delete ClientSocket;

		if (FailedCount >= 10)
		{
			printf("失败次数累计10次, 监听被迫自动中止.");
			goto done;
		} //if
	}

	//除非服务器发生异常, 否则永远不能执行到这里

	if (pMySelf->pWorkingSocket->GetLastException()->HResult != STATUS_SUCCESS)
	{
output_err:
		printf(String(pMySelf->pWorkingSocket->GetLastException()->Message).ToMultiByte());
	} //if

done:
	delete pMySelf->pWorkingSocket;

return_alert:
	pMySelf->pMainThread->Alert();

	printf("\n服务已关闭.");
	return;
}

//-------------------------------------------------------------------------

void Server::BeginDoUserRequestTask(RequestInfo *pInfo)
{
	pInfo->pServer->DoUserRequestTask(pInfo);
}

//-------------------------------------------------------------------------

void Server::DoUserRequestTask(RequestInfo *pInfo)
{
	pInfo->RequestTextSize = pInfo->ClientSocket->GetRcvBuf();
	pInfo->RequestText     = (RLIB_Type(pInfo->RequestText))RLIB_GlobalAlloc(pInfo->RequestTextSize);

	if(pInfo->RequestText != nullptr)
	{
		pInfo->DoCoreTask();
		RLIB_GlobalCollect(pInfo->RequestText);
	}

	delete pInfo->ClientSocket;
	delete pInfo;
}
