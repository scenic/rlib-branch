/********************************************************************
	Created:	2012/07/29  19:26
	Filename: 	MyProxy.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MyStruct.h"
#define ENABLE_LEADER_FOLLOWER
/************************************************************************/
/* 定义服务器框架                                                        */
/************************************************************************/
class Server
{
public:
	/// <summary>
	/// 负责将指针成员置为初始值, 重要!
	/// </summary>
	Server()
	{
		this->pWorkingSocket = nullptr;
		this->pMainThread    = nullptr;
		this->pThreadPool    = nullptr;
	}

public:
	/// <summary>
	/// 服务器监听使用的Socket
	/// </summary>
	Net::Sockets          *pWorkingSocket;
	/// <summary>
	/// 服务器主线程, 这个与框架无关, 仅用来提供资源清理方法
	/// </summary>
	Threading::Thread     *pMainThread;
	/// <summary>
	/// 服务器采用Leader - Follower模式, 这是使用的线程池
	/// </summary>
	Threading::ThreadPool *pThreadPool;

public:
	/// <summary>
	/// 处理用户请求, 过滤非法请求
	/// </summary>
	void DoUserRequestTask(RequestInfo *pInfo);

public:
	/// <summary>
	/// 执行服务监听任务, 并处理用户请求
	/// </summary>
	static void DoMonitoringTask(Server *pMySelf);
	/// <summary>
	/// 处理用户请求, 过滤非法请求
	/// </summary>
	static void BeginDoUserRequestTask(RequestInfo *pInfo);
};