/********************************************************************
	Created:	2012/08/09  21:01
	Filename: 	Stdfx.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "MyStruct.h"

//-------------------------------------------------------------------------

bool RequestInfo::Send(LPCVOID pbytes, int nbytes)
{
	return ClientSocket->Send((char *)pbytes, nbytes);
}

//-------------------------------------------------------------------------

bool RequestInfo::RecvLine(int *pnbytes/* = nullptr*/)
{
	int nbytes = 0, bytes;
	memset(this->RequestText, 0, this->RequestTextSize);
continue_recv:
	while((bytes = ClientSocket->Recv(this->RequestText + nbytes, this->RequestTextSize - 1 - nbytes)) > 0)
	{
		nbytes += bytes;
		this->RequestText[nbytes] = '\0';
		if (strstr(this->RequestText, "\n") != nullptr)
		{
			break;
		} //if
	}
	if (bytes < 0 && ClientSocket->GetLastException() != nullptr)
	{
		if (ClientSocket->GetLastException()->HResult == WSAETIMEDOUT) 
			goto continue_recv;
		else 
		{
			return false;
		}
	} //if
	this->RequestText[nbytes] = '\0';
	if(pnbytes != nullptr) *pnbytes = nbytes;
	return true;
}