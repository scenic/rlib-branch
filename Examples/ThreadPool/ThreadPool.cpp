/********************************************************************
	Created:	2012/05/26  16:08
	Filename: 	Loader.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "../../RLib/RLib_Import.h"

#ifdef _DEBUG
#pragma comment(lib, "../../bin/RLib_d.lib")
#else
#pragma comment(lib, "../../bin/RLib.lib")
#endif

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace System::Threading;
//-------------------------------------------------------------------------
static ThreadPool *pool;
//-------------------------------------------------------------------------
class Example
{
public:
	// This thread procedure performs the task.
	static void ThreadProc( Object stateInfo )
	{
		printf( "Hello from the thread pool.\n" );
		Sleep(100);

		Int32 i  = (int)stateInfo;
		Int32 t  = GetCurrentThreadId();
		String s = T("ID: ");
		s       += i.ToString() + T("  线程: ") + t.ToString() +
			T("  总数: ") + Int32(pool->GetThreads()).ToString() +
			T("  空闲: ") + Int32(pool->GetAvailableThreads()).ToString() +
			T("  最大: ") + Int32(pool->GetMaxThreads()).ToString() +
			T("  最小: ") + Int32(pool->GetMinThreads()).ToString() +
			T("\n");
		printf(s.ToMultiByte());
	}
};
//-------------------------------------------------------------------------
void test_threadpool_by_rrrfff()
{
	pool = new ThreadPool(4, 2000);
	for(int i = 0; i < 1000; i++)
	{
		// Queue the task.
		pool->AddTask(WaitCallback( Example::ThreadProc ), (int *)i);

		if((2000 - i) > 1) pool->SetMaxThreads(2000 - i);
	}
	// If you comment out the Sleep, the main thread exits before
	// the thread pool task runs. 
	Thread::SleepEx(5000);
	delete pool;
}
//-------------------------------------------------------------------------
int main()
{
	test_threadpool_by_rrrfff();
	return 0;
}