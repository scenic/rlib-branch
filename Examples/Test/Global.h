/********************************************************************
	Created:	2013/01/27  15:02
	Filename: 	Global.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "../../RLib/RLib_Import.h"

#ifdef _DEBUG
#pragma comment(lib, "../../bin/RLib_d.lib")
#else
#pragma comment(lib, "../../bin/RLib.lib")
#endif

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;

//-------------------------------------------------------------------------

extern bool ConvertJpegToBmp(String JpegFile, String BmpFile);

//-------------------------------------------------------------------------

extern String RecognizeImage(String ImageFile);