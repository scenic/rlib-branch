/********************************************************************
	Created:	2012/05/26  16:08
	Filename: 	Loader.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "../../RLib/RLib_Import.h"

#define _CSTDIO_
#define _CSTRING_
#define _CWCHAR_

#include <iostream>
#include <stdlib.h>

#ifdef _DEBUG
#pragma comment(lib, "../../bin/RLib_d.lib")
#else
#pragma comment(lib, "../../bin/RLib.lib")
#endif

//#pragma comment(linker, "/ENTRY:RLIBMain")

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;

/************************************************************************
 * 
 */
inline void ShowMsg(const TCHAR *pMsg, HWND hWnd = GetForegroundWindow(), const TCHAR *pTitle = T("��ܰ��ʾ"))
{
	MessageBoxEx(hWnd, pMsg, pTitle, MB_SETFOREGROUND|MB_ICONWARNING, NULL);
}