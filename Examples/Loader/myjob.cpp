/********************************************************************
	Created:	2012/05/26  16:07
	Filename: 	Loader.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "TX.h"
#include "LdrSup.h"
#include "bin.h"
#pragma comment(lib, "../../RLib/native/Ntdll.lib")
using namespace System::Runtimes;
//-------------------------------------------------------------------------

#define ret_with_clean    true;
#define ret_without_clean false;

//-------------------------------------------------------------------------

IO::FileStream             *logfile = NULL;
Threading::CriticalSection *loglock = NULL;

//-------------------------------------------------------------------------
bool create_log_file()
{
	if ((logfile = IO::File::Create(T("D:\\RLib\\bin\\logfile.ini"), File::CreateNewMode,
		File::ReadWriteAccess, File::TemporaryAttribute, File::ReadShare)) != NULL)
	{
		byte bom[sizeof(int)] = { 0xffU, 0xfeU };
		logfile->Write(bom, 2);
		loglock = new Threading::CriticalSection();
		return loglock != NULL;
	} 
	else
	{
		MessageBoxEx(NULL, T("创建记录文件失败"), T("Error"), 0, 0);
		return false;
	}
}

//-------------------------------------------------------------------------
void write_log(String msg)
{
	if (logfile == NULL)
	{
		if(!create_log_file()) return;
	} //if
	loglock->Enter();
	{
		RLIB_PStreamWriteWS(logfile, (msg + T("\r\n")));
	}
	loglock->Leave();
}

//-------------------------------------------------------------------------
#pragma check_stack(off)
_declspec(naked) void hook_log(TCHAR *param)
{
	__asm
	{
		push        ebp  
		mov         ebp, esp  
		sub         esp, 40h  
		push        ebx  
		push        esi  
		push        edi
		push        ecx
		push        edx
		push        eax
	}
	{
		//do_my_jod(param);
		__asm
		{
			mov     dword ptr [ebp + 8], eax
		}
	}
	__asm
	{

		pop         eax
		pop         edx
		pop         ecx
		pop         edi  
		pop         esi  
		pop         ebx  
		mov         esp, ebp  
		pop         ebp

//		jmp         dword ptr [origin_function]  
	}
}

//-------------------------------------------------------------------------

template <typename Callback_Function> Callback_Function GetDllProcAddressByName
	(const TCHAR *Dll IN, const TCHAR *ExportName IN, typename Callback_Function)
{
	HMODULE hDll = NULL;
	if (GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, Dll, &hDll) == FALSE)
	{
		MessageBoxEx(NULL, T("获取模块句柄失败"), Dll, 0, 0);
	} //if
	return (Callback_Function)GetProcAddress(hDll, String(ExportName).ToMultiByte());
}


//-------------------------------------------------------------------------
#pragma comment(lib,"Ws2_32.lib")

//-------------------------------------------------------------------------
RLIB_Type(send) *prev_send;

int WSAAPI hook_send(__in SOCKET s, __in_bcount(len) const char FAR * buf,
	__in int len, __in int flags)
{
	len = prev_send(s, buf, len, flags);
	write_log(T("hook_send catch"));
	loglock->Enter();
	if (len > 0) logfile->Write(buf, len);
	loglock->Leave();
	return len;
}

RLIB_Type(recv) *prev_recv;

int WSAAPI hook_recv(__in SOCKET s, __out_bcount_part(len, return) __out_data_source(NETWORK) char FAR * buf,
	__in int len, __in int flags)
{
	len = prev_recv(s, buf, len, flags);
	write_log(T("hook_recv catch"));
	loglock->Enter();
	if (len > 0) logfile->Write(buf, len);
	loglock->Leave();
	return len;
}
//-------------------------------------------------------------------------

bool myjob()
{
	prev_send = Runtimes::Detour::RedirectProcedure(hook_send, send);
	prev_recv = Runtimes::Detour::RedirectProcedure(hook_recv, recv);

	ShowMsg(T("Hook Done!"), NULL);
	return ret_without_clean;

//	return ret_with_clean;
}