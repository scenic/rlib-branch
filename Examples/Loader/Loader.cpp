/********************************************************************
	Created:	2012/05/26  16:07
	Filename: 	Loader.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "Loader.h"
#include "LdrSup.h"
#include "bin.h"
#pragma comment(lib, "../../RLib/native/Ntdll.lib")
using namespace System::Runtimes;

//-------------------------------------------------------------------------

extern bool myjob();
static wchar_t              BaseDllDir[MAX_PATH] = {0};

//-------------------------------------------------------------------------

DWORD WINAPI threadproc(void *pimage)
{
	LdrSup::SetDllFindDirectory(BaseDllDir);

	if (!LdrSup::FixImports(0, false))
	{
		ShowMsg(T("修复导入表失败"), NULL, T("Error"));
	} //if
	
	//ShowMsg(T("Do My Job!"));
	if (myjob())
	{
		//ShowMsg(T("My Job Done!"));
		goto exit_and_clean;
	} //if
	//ShowMsg(T("My Job Done!"));
	AppBase::ExitThread(STATUS_SUCCESS);
exit_and_clean:
	 __asm
	 {
	 	push 0
	 	push -2
		push 0
	 	push pimage
	 	push -1
	 	push dword ptr [NtTerminateThread]
	 	jmp dword ptr [NtUnmapViewOfSection]
	 }
	return STATUS_SUCCESS;
}

HANDLE open_process_by_pid(int pid IN)
{
	HANDLE handle_opened;
	OBJECT_ATTRIBUTES oa = {0};
	CLIENT_ID cid = { (HANDLE)pid, 0 };
	if(STATUS_SUCCESS != NtOpenProcess(&handle_opened, PROCESS_VM_OPERATION|PROCESS_CREATE_THREAD, &oa, &cid))
	{
		return NULL;
	}
	return handle_opened;
}

int open_process_by_name(PWSTR name IN)
{
	const int DEF_BUF_SIZE = 4 * 4 * 4 * 4;

	int bytes = DEF_BUF_SIZE, pid = 0, length = wcslen(name);

	auto lpProcessInfo = (PSYSTEM_PROCESS_INFORMATION)RLIB_GlobalAlloc(bytes);
	// 由于事先并不知道需要多少空间来存储进程信息, 因而采用循环测试法
	while(true)
	{
		// 动态分配空间, 用来存储进程信息
		auto lpTempPtr = (PSYSTEM_PROCESS_INFORMATION)AppBase::GetUsingPool()->ReAlloc(lpProcessInfo, bytes);
		if (lpTempPtr == NULL )
		{
			goto ret;
		}
		lpProcessInfo = lpTempPtr;

		// 枚举进程信息
		auto status = NtQuerySystemInformation (SystemProcessInformation, lpProcessInfo, bytes, 0 ) ;
		if (!NT_SUCCESS(status))
		{
			// 检测是否返回缓冲区不够大
			if ( status == STATUS_INFO_LENGTH_MISMATCH )
			{
				bytes += DEF_BUF_SIZE ;
				continue ;
			}
			goto ret;
		}
		break;
	}
	auto pProcessInfo = lpProcessInfo;
	while ( pProcessInfo->NextEntryOffset != 0 )
	{
		if ( pProcessInfo->ImageName.Buffer != NULL )
		{
			if (wcslen(pProcessInfo->ImageName.Buffer) == length && wcsstr(pProcessInfo->ImageName.Buffer, name) == pProcessInfo->ImageName.Buffer)
			{
				pid = (int)pProcessInfo->ProcessId;
			} //if
		}
		pProcessInfo = (PSYSTEM_PROCESS_INFORMATION)((ULONG)pProcessInfo + pProcessInfo->NextEntryOffset) ;
	}
ret:
	AppBase::Collect(lpProcessInfo);
	return pid;
}

bool copy_to_process(int pid IN, HANDLE *pdest_process_handle OUT, void **ppremote_image_base OUT)
{
	bool result           = false;
	void *pdest           = NULL;
	HANDLE section_handle = NULL;
	if (LdrSup::DuplicateImage32(&section_handle, pdest, AppBase::GetImageBaseAddress()))
	{
		*pdest_process_handle = open_process_by_pid(pid);
		if (*pdest_process_handle == NULL)
		{
			STARTUPINFO         si = {0};
			PROCESS_INFORMATION pi = {0};
			if(TRUE != CreateProcess(T("C:\\Program Files\\Tencent\\QQ2012\\BIN\\QQ.exe"),
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, &si, &pi))
			{
				ShowMsg(T("创建进程失败"));
				return false;
			}
			*pdest_process_handle = pi.hProcess;
		} //if
		if (*pdest_process_handle != NULL)
		{
			if( (*ppremote_image_base = LdrSup::MapImage(section_handle, *pdest_process_handle)) )
			{
				result = LdrSup::RelocateImage32(pdest, *ppremote_image_base, AppBase::GetImageBaseAddress());
			}
			LdrSup::UnMapImage(AppBase::GetCurrentProcess(), pdest);
			LdrSup::CloseNativeHandle(section_handle);
		}
		if (result == false)
		{
			LdrSup::CloseNativeHandle(*pdest_process_handle);
			*pdest_process_handle = NULL;
		} //if
	}
	return result;
}

bool Run(HANDLE *phandle, HANDLE *pdest_process_handle, void **ppimage)
{
	String StartupPath = String(AppBase::GetStartupPath());
	String::FastStringCopy(BaseDllDir, StartupPath, StartupPath.Length);

	*pdest_process_handle = *ppimage = NULL;

	if(copy_to_process(open_process_by_name(L"QQ.exe"), pdest_process_handle, ppimage))
	{
		Threading::Thread *thread = new Threading::Thread(LdrSup::RunImage(*pdest_process_handle, *ppimage, threadproc, *ppimage, true), 0, FALSE);
		if (thread == NULL)
		{
			*phandle = NULL;
		} 
		else
		{
			*phandle = thread;
			thread->Resume();
			delete thread;
		}
		LdrSup::CloseNativeHandle(*pdest_process_handle);
	}
	return NULL;
}

int __stdcall WinMain( __in HINSTANCE hInstance, __in_opt HINSTANCE hPrevInstance, __in LPSTR lpCmdLine, __in int nShowCmd )
{
	Run((void **)&hPrevInstance, (void **)&lpCmdLine, (void **)&nShowCmd);
	return 0;
}