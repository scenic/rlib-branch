/********************************************************************
	Created:	2012/06/06  20:14
	Filename: 	RLib_LdrSup.cpp
	Author:		Lactoferrin, rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "LdrSup.h"
using namespace LdrSup;
#define CurrentProcess (HANDLE)-1

//-------------------------------------------------------------------------


typedef struct _RELOC_ENTRY
{
    unsigned short Offset: 12, Type: 4;
} RELOC_ENTRY,  *PRELOC_ENTRY;

//-------------------------------------------------------------------------


typedef struct _BASE_RELOCATION
{
    DWORD VirtualAddress;
    DWORD SizeOfBlock;
    RELOC_ENTRY RelocArray[1];
} BASE_RELOCATION,  *PBASE_RELOCATION;

//-------------------------------------------------------------------------


template <typename tchar> static bool sames(const tchar *str1, unsigned len1, const tchar *str2, unsigned len2, bool icase = false)
{
    if (len1 == len2)
    {
        if (str1 == str2)
        {
            return true;
        }
        if (!str1 || !str2)
        {
            return false;
        }
        const char *p1 = reinterpret_cast<const char *> (str1);
        const char *p2 = reinterpret_cast<const char *> (str2);
        if (icase)
        for (unsigned off = 0; off<len1; off += sizeof(tchar))
        {
            tchar c1 = reinterpret_cast<const tchar &> (p1[off]);
            tchar c2 = reinterpret_cast<const tchar &> (p2[off]);
            if (c1 >= 'A' && c1 <= 'Z')
            {
                c1 += 'a' - 'A';
            }
            if (c2 >= 'A' && c2 <= 'Z')
            {
                c2 += 'a' - 'A';
            }
            if (c1 != c2)
            {
                return false;
            }
            if (!c1)
            {
                break;
            }
        }
        else
        for (unsigned off = 0; off < len1; off += sizeof(tchar))
        {
            tchar c1 = reinterpret_cast < const tchar & > (p1[off]);
            tchar c2 = reinterpret_cast < const tchar & > (p2[off]);
            if (c1 != c2)
            {
                return false;
            }
            if (!c1)
            {
                break;
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

//-------------------------------------------------------------------------

bool LdrSup::ValidateHeaders(const void *BaseAddress, PIMAGE_DOS_HEADER *DosHeader, PIMAGE_NT_HEADERS32 *NtHeaders)
{
    PIMAGE_DOS_HEADER dosHeader = static_cast < PIMAGE_DOS_HEADER > (const_cast < void * > (BaseAddress));
    if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE)
    {
        return false;
    }
    PIMAGE_NT_HEADERS32 ntHeaders = reinterpret_cast < PIMAGE_NT_HEADERS32 > (reinterpret_cast < size_t > (dosHeader) + dosHeader->e_lfanew);
    if (ntHeaders->Signature != IMAGE_NT_SIGNATURE || ntHeaders->OptionalHeader.Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC)
    {
        return false;
    }
    if (DosHeader)
    {
         *DosHeader = dosHeader;
    }
    if (NtHeaders)
    {
         *NtHeaders = ntHeaders;
    }
    return true;
}

//-------------------------------------------------------------------------

bool LdrSup::ValidateHeaders(const void *BaseAddress, PIMAGE_DOS_HEADER *DosHeader, PIMAGE_NT_HEADERS64 *NtHeaders)
{
    PIMAGE_DOS_HEADER dosHeader = static_cast < PIMAGE_DOS_HEADER > (const_cast < void * > (BaseAddress));
    if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE)
    {
        return false;
    }
    PIMAGE_NT_HEADERS64 ntHeaders = reinterpret_cast < PIMAGE_NT_HEADERS64 > (reinterpret_cast < size_t > (dosHeader) + dosHeader->e_lfanew);
    if (ntHeaders->Signature != IMAGE_NT_SIGNATURE || ntHeaders->OptionalHeader.Magic != IMAGE_NT_OPTIONAL_HDR64_MAGIC)
    {
        return false;
    }
    if (DosHeader)
    {
         *DosHeader = dosHeader;
    }
    if (NtHeaders)
    {
         *NtHeaders = ntHeaders;
    }
    return true;
}

//-------------------------------------------------------------------------

template <typename TypeOfNtHeaders> size_t GetFileOffsetGT(const void *BaseAddress, size_t RVA, PIMAGE_SECTION_HEADER *SectionHeader)
{
    size_t base = (size_t)BaseAddress;
    typename TypeOfNtHeaders *ntheaders = (typename TypeOfNtHeaders*)(base + ((PIMAGE_DOS_HEADER)base)->e_lfanew);
    PIMAGE_SECTION_HEADER sectionheader = IMAGE_FIRST_SECTION(ntheaders);
    for (WORD i = 0; i<ntheaders->FileHeader.NumberOfSections && (size_t)sectionheader<base + ntheaders->OptionalHeader.SizeOfHeaders; ++i, ++sectionheader)
    {
        if (RVA >= sectionheader->VirtualAddress && RVA<sectionheader->VirtualAddress + sectionheader->SizeOfRawData)
        {
            if (SectionHeader)
            {
                 *SectionHeader = sectionheader;
            }
            return RVA - sectionheader->VirtualAddress + sectionheader->PointerToRawData;
        }
    }
    return 0;
}

//-------------------------------------------------------------------------

size_t LdrSup::GetFileOffset32(const void *BaseAddress, size_t RVA, PIMAGE_SECTION_HEADER *SectionHeader)
{
    return GetFileOffsetGT < IMAGE_NT_HEADERS32 > (BaseAddress, RVA, SectionHeader);
}

//-------------------------------------------------------------------------

size_t LdrSup::GetFileOffset64(const void *BaseAddress, size_t RVA, PIMAGE_SECTION_HEADER *SectionHeader)
{
    return GetFileOffsetGT < IMAGE_NT_HEADERS64 > (BaseAddress, RVA, SectionHeader);
}

//-------------------------------------------------------------------------

template <typename TypeOfNtHeaders> DWORD AlignSectionsGT(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable)
{
    PIMAGE_DOS_HEADER DosHeader;
    typename TypeOfNtHeaders *NtHeaders;
    if (!ValidateHeaders(Source, &DosHeader, &NtHeaders))
    {
        return 0;
    }
    DWORD ImageSize = NtHeaders->OptionalHeader.SizeOfImage;
    OBJECT_ATTRIBUTES oa = makeoa(0);
    LARGE_INTEGER MaximumSize = 
    {
        ImageSize, 0
    };
    HANDLE hSection;
    if (NtCreateSection(&hSection, SECTION_MAP_READ | SECTION_MAP_WRITE | SECTION_MAP_EXECUTE, &oa, &MaximumSize, PAGE_EXECUTE_READWRITE, SEC_COMMIT, 0) >= 0)
    {
        LARGE_INTEGER SectionOffset = 
        {
            0, 0
        };
        SIZE_T ViewSize = 0;
        if (NtMapViewOfSection(hSection, CurrentProcess, &Destination, 0, ImageSize, &SectionOffset, &ViewSize, Inheritable ? ViewShare : ViewUnmap, 0, PAGE_EXECUTE_READWRITE) >= 0)
        {
            memcpy(Destination, Source, NtHeaders->OptionalHeader.SizeOfHeaders);
            PIMAGE_SECTION_HEADER SectionHeader = IMAGE_FIRST_SECTION(NtHeaders);
            for (WORD i = 0; i < NtHeaders->FileHeader.NumberOfSections; ++i)
            {
                DWORD BytesToCopy = min(SectionHeader[i].SizeOfRawData, SectionHeader[i].Misc.VirtualSize);
                memcpy(reinterpret_cast < char * > (Destination) + SectionHeader[i].VirtualAddress, reinterpret_cast < const char * > (Source) + SectionHeader[i].PointerToRawData, BytesToCopy);
            }
        }
        else
        {
            NtClose(hSection);
            return 0;
        }
        if (SectionHandle)
        {
             *SectionHandle = hSection;
        }
        else
        {
            NtClose(hSection);
        }
        return ImageSize;
    }
    else
    {
        return 0;
    }
}

//-------------------------------------------------------------------------

DWORD LdrSup::AlignSections32(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable)
{
    return AlignSectionsGT < IMAGE_NT_HEADERS32 > (SectionHandle, Destination, Source, Inheritable);
}

//-------------------------------------------------------------------------

DWORD LdrSup::AlignSections64(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable)
{
    return AlignSectionsGT < IMAGE_NT_HEADERS64 > (SectionHandle, Destination, Source, Inheritable);
}

//-------------------------------------------------------------------------

template <typename TypeOfNtHeaders> DWORD DuplicateImageGT(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable)
{
    PIMAGE_DOS_HEADER DosHeader;
    typename TypeOfNtHeaders *NtHeaders;
    if (!ValidateHeaders(Source, &DosHeader, &NtHeaders))
    {
        return 0;
    }
    DWORD ImageSize = NtHeaders->OptionalHeader.SizeOfImage;
    OBJECT_ATTRIBUTES oa = makeoa(0);
    LARGE_INTEGER MaximumSize = 
    {
        ImageSize, 0
    };
    HANDLE hSection;
    if (NtCreateSection(&hSection, SECTION_MAP_READ | SECTION_MAP_WRITE | SECTION_MAP_EXECUTE, &oa, &MaximumSize, PAGE_EXECUTE_READWRITE, SEC_COMMIT, 0) >= 0)
    {
        LARGE_INTEGER SectionOffset = 
        {
            0, 0
        };
        SIZE_T ViewSize = 0;
        if (NtMapViewOfSection(hSection, CurrentProcess, &Destination, 0, ImageSize, &SectionOffset, &ViewSize, Inheritable ? ViewShare : ViewUnmap, 0, PAGE_EXECUTE_READWRITE) >= 0)
        {
            memcpy(Destination, Source, ImageSize);
        }
        else
        {
            NtClose(hSection);
            return 0;
        }
        if (SectionHandle)
        {
             *SectionHandle = hSection;
        }
        else
        {
            NtClose(hSection);
        }
        return ImageSize;
    }
    else
    {
        return 0;
    }
}

//-------------------------------------------------------------------------

DWORD LdrSup::DuplicateImage32(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable)
{
    return DuplicateImageGT < IMAGE_NT_HEADERS32 > (SectionHandle, Destination, Source, Inheritable);
}

//-------------------------------------------------------------------------

DWORD LdrSup::DuplicateImage64(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable)
{
    return DuplicateImageGT < IMAGE_NT_HEADERS64 > (SectionHandle, Destination, Source, Inheritable);
}

//-------------------------------------------------------------------------

template <typename TypeOfNtHeaders> bool RelocateImageGT(void *BaseAddress, void *RelocateTo, void *RelocateFrom)
{
    UINT_PTR base = reinterpret_cast<UINT_PTR> (BaseAddress), target = reinterpret_cast<UINT_PTR> (RelocateTo ? RelocateTo : BaseAddress);
    PIMAGE_DOS_HEADER DosHeader;
    typename TypeOfNtHeaders *NtHeaders;
    if (!ValidateHeaders(BaseAddress, &DosHeader, &NtHeaders))
    {
        return false;
    }
    LONGLONG Displacement = target - (RelocateFrom ? (UINT_PTR)RelocateFrom: NtHeaders->OptionalHeader.ImageBase);
    if (Displacement == 0)
    {
        return true;
    }
    DWORD RelocRVA = NtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress;
    DWORD RelocSize = NtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size;
    if (RelocRVA && RelocSize)
    {
        PBASE_RELOCATION relocbase = reinterpret_cast<PBASE_RELOCATION> (base + RelocRVA);
        UINT_PTR relocend = base + RelocRVA + RelocSize;
        while (relocbase->VirtualAddress && reinterpret_cast<UINT_PTR> (relocbase)<relocend)
        {
            for (DWORD i = 0; i<(relocbase->SizeOfBlock - 8) / sizeof(RELOC_ENTRY); ++i)
            {
                switch (relocbase->RelocArray[i].Type)
                {
                    case IMAGE_REL_BASED_HIGH:
                        *reinterpret_cast<UNALIGNED INT16 *> (base + relocbase->VirtualAddress + relocbase->RelocArray[i].Offset) += HIWORD(Displacement);
                        break;
                    case IMAGE_REL_BASED_LOW:
                        *reinterpret_cast<UNALIGNED INT16 *> (base + relocbase->VirtualAddress + relocbase->RelocArray[i].Offset) += LOWORD(Displacement);
                        break;
                    case IMAGE_REL_BASED_HIGHLOW:
                        *reinterpret_cast<UNALIGNED INT32 *> (base + relocbase->VirtualAddress + relocbase->RelocArray[i].Offset) += static_cast<INT32> (Displacement);
                        break;
                    case IMAGE_REL_BASED_HIGHADJ:
                        {
                            LONG v = MAKELONG(*reinterpret_cast<UNALIGNED INT16 *> (base + relocbase->VirtualAddress + relocbase->RelocArray[i + 1].Offset), *reinterpret_cast<UNALIGNED INT16 *> (base + relocbase->VirtualAddress + relocbase->RelocArray[i].Offset));
                            v += static_cast<LONG> (Displacement) + 0x8000;
                            *reinterpret_cast<UNALIGNED INT16 *> (base + relocbase->VirtualAddress + relocbase->RelocArray[i].Offset) += HIWORD(v);
                        }
                    case IMAGE_REL_BASED_DIR64:
                        *reinterpret_cast < UNALIGNED INT64 * > (base + relocbase->VirtualAddress + relocbase->RelocArray[i].Offset) += static_cast < INT64 > (Displacement);
                        break;
                    default:
                        break;
                }
            }
            reinterpret_cast < UINT_PTR & > (relocbase) += relocbase->SizeOfBlock;
        }
#pragma warning(disable:4244)
        NtHeaders->OptionalHeader.ImageBase = (ULONGLONG)RelocateTo;
        return true;
    }
    else
    {
        return false;
    }
}

//-------------------------------------------------------------------------

bool LdrSup::RelocateImage32(void *BaseAddress, void *RelocateTo, void *RelocateFrom)
{
    return RelocateImageGT < IMAGE_NT_HEADERS32 > (BaseAddress, RelocateTo, RelocateFrom);
}

//-------------------------------------------------------------------------

bool LdrSup::RelocateImage64(void *BaseAddress, void *RelocateTo, void *RelocateFrom)
{
    return RelocateImageGT < IMAGE_NT_HEADERS64 > (BaseAddress, RelocateTo, RelocateFrom);
}

//-------------------------------------------------------------------------

template <typename TypeOfNtHeaders> DWORD GetExportedRVAGT(const void *BaseAddress, const char *Name)
{
    UINT_PTR base = reinterpret_cast<UINT_PTR> (BaseAddress);
    PIMAGE_DOS_HEADER DosHeader;
    typename TypeOfNtHeaders *NtHeaders;
    if (!ValidateHeaders(BaseAddress, &DosHeader, &NtHeaders))
    {
        return false;
    }
    DWORD ExpRVA = NtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
    DWORD ExpSize = NtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size;
    if (ExpRVA && ExpSize)
    {
        PIMAGE_EXPORT_DIRECTORY ExpDir = reinterpret_cast<PIMAGE_EXPORT_DIRECTORY> (base + ExpRVA);
        PDWORD NameList = reinterpret_cast<PDWORD> (base + ExpDir->AddressOfNames);
        PWORD OrdinalList = reinterpret_cast<PWORD> (base + ExpDir->AddressOfNameOrdinals);
        PDWORD FunctionList = reinterpret_cast<PDWORD> (base + ExpDir->AddressOfFunctions);
        for (DWORD i = 0; i<ExpDir->NumberOfNames; ++i)
        {
            const char *ExpName = reinterpret_cast<const char *> (base + NameList[i]);
            if (sames(Name, (unsigned) - 1, ExpName, (unsigned) - 1))
            {
                WORD Ordinal = OrdinalList[i];
                return FunctionList[Ordinal];
            }
        }
    }
    return 0;
}

//-------------------------------------------------------------------------

DWORD LdrSup::GetExportedRVA32(const void *BaseAddress, const char *Name)
{
    return GetExportedRVAGT < IMAGE_NT_HEADERS32 > (BaseAddress, Name);
}

//-------------------------------------------------------------------------

DWORD LdrSup::GetExportedRVA64(const void *BaseAddress, const char *Name)
{
    return GetExportedRVAGT < IMAGE_NT_HEADERS64 > (BaseAddress, Name);
}

//-------------------------------------------------------------------------

template <typename TypeOfNtHeaders> bool SetExportedValueGT(void *BaseAddress, const char *Name, const void *Buffer, size_t Length)
{
    DWORD RVA = GetExportedRVAGT<TypeOfNtHeaders> (BaseAddress, Name);
    if (RVA)
    {
        char *p = reinterpret_cast<char *> (BaseAddress) + RVA;
        memcpy(p, Buffer, Length);
        return true;
    }
    else
    {
        return false;
    }
}

//-------------------------------------------------------------------------

bool LdrSup::SetExportedValue32(void *BaseAddress, const char *Name, const void *Buffer, size_t Length)
{
    return SetExportedValueGT < IMAGE_NT_HEADERS32 > (BaseAddress, Name, Buffer, Length);
}

//-------------------------------------------------------------------------

bool LdrSup::SetExportedValue64(void *BaseAddress, const char *Name, const void *Buffer, size_t Length)
{
    return SetExportedValueGT < IMAGE_NT_HEADERS64 > (BaseAddress, Name, Buffer, Length);
}

//-------------------------------------------------------------------------

void *LdrSup::MapImage(HANDLE SectionHandle, HANDLE ProcessHandle, void *BaseAddress, bool Inheritable)
{
    LARGE_INTEGER SectionOffset = 
    {
        0, 0
    };
    SIZE_T ViewSize = 0;
    return NtMapViewOfSection(SectionHandle, ProcessHandle, &BaseAddress, 0, 0, &SectionOffset, &ViewSize, Inheritable ? ViewShare : ViewUnmap, 0, PAGE_EXECUTE_READWRITE) >= 0 ? BaseAddress : 0;
}

//-------------------------------------------------------------------------

void *LdrSup::MapImage(HANDLE SectionHandle, DWORD ProcessId, void *BaseAddress, bool Inheritable)
{
    HANDLE ProcessHandle;
    OBJECT_ATTRIBUTES oa = makeoa(0);
    CLIENT_ID cid = 
    {
        (HANDLE)ProcessId, 0
    };
    if (NtOpenProcess(&ProcessHandle, PROCESS_VM_OPERATION, &oa, &cid) == 0)
    {
        BaseAddress = LdrSup::MapImage(SectionHandle, ProcessHandle, BaseAddress, Inheritable);
        NtClose(ProcessHandle);
    }
    return BaseAddress;
}

//-------------------------------------------------------------------------

HANDLE LdrSup::RunImage(HANDLE ProcessHandle, void *BaseAddress, void *EntryPoint, void *Parameter, bool CreateSuspended)
{
    MEMORY_BASIC_INFORMATION mbi;
    ULONG r;
    if (NtQueryVirtualMemory(CurrentProcess, EntryPoint, MemoryBasicInformation, &mbi, sizeof mbi, &r) == 0)
    {
        HANDLE ThreadHandle;
        CLIENT_ID ClientId;
		LPVOID Result;
        if ((Result = (LPVOID)RtlCreateUserThread(ProcessHandle, 0, CreateSuspended, 0, 0, 0, 
			(PUSER_THREAD_START_ROUTINE)((size_t)BaseAddress - (size_t)mbi.AllocationBase + (size_t)EntryPoint), Parameter, 
			&ThreadHandle, &ClientId)) == 0)
        {
            return ThreadHandle;
        }
		Result = nullptr;
    }
    return 0;
}

//-------------------------------------------------------------------------

HANDLE LdrSup::RunImage(HANDLE ProcessHandle, void *BaseAddress, DWORD EntryPointRVA, void *Parameter, bool CreateSuspended)
{
    HANDLE ThreadHandle;
    CLIENT_ID ClientId;
    if (RtlCreateUserThread(ProcessHandle, 0, CreateSuspended, 0, 0, 0, (PUSER_THREAD_START_ROUTINE)((size_t)BaseAddress + EntryPointRVA), Parameter, &ThreadHandle, &ClientId) == 0)
    {
        return ThreadHandle;
    }
    return 0;
}

//-------------------------------------------------------------------------

DWORD LdrSup::GetEntryPoint32(const void *BaseAddress)
{
    PIMAGE_DOS_HEADER DosHeader = static_cast < PIMAGE_DOS_HEADER > (const_cast < void * > (BaseAddress));
    PIMAGE_NT_HEADERS32 NtHeaders = reinterpret_cast < PIMAGE_NT_HEADERS32 > (reinterpret_cast < size_t > (DosHeader) + DosHeader->e_lfanew);
    return NtHeaders->OptionalHeader.AddressOfEntryPoint;
}

//-------------------------------------------------------------------------

DWORD LdrSup::GetEntryPoint64(const void *BaseAddress)
{
    PIMAGE_DOS_HEADER DosHeader = static_cast < PIMAGE_DOS_HEADER > (const_cast < void * > (BaseAddress));
    PIMAGE_NT_HEADERS64 NtHeaders = reinterpret_cast < PIMAGE_NT_HEADERS64 > (reinterpret_cast < size_t > (DosHeader) + DosHeader->e_lfanew);
    return NtHeaders->OptionalHeader.AddressOfEntryPoint;
}

//-------------------------------------------------------------------------

#ifdef _M_X64
extern "C" __PPEB WINAPI GetPEB64();
__PPEB WINAPI LdrSup::GetPEB()
{
    return GetPEB64();
}

//-------------------------------------------------------------------------

#else 
__declspec(naked) PPEB WINAPI LdrSup::GetPEB()
{
    __asm mov eax, fs: [30h]
	__asm ret
}

//-------------------------------------------------------------------------

#endif 

HMODULE LdrSup::GetModuleBase(const wchar_t *Name, wint_t NameLength)
{
    PLIST_ENTRY ListHead = &GetPEB()->Ldr->InLoadOrderModuleList;
    for (PLIST_ENTRY node = ListHead->Flink; node != ListHead; node = node->Flink)
    {
        PLDR_DATA_TABLE_ENTRY mod = reinterpret_cast < PLDR_DATA_TABLE_ENTRY > (node);
        if (sames(mod->BaseDllName.Buffer, mod->BaseDllName.Length, Name, NameLength, true))
        {
            return reinterpret_cast < HMODULE > (mod->DllBase);
        }
    }
    return 0;
}

//-------------------------------------------------------------------------

extern "C" bool FixLdrImports()
{
    static bool LdrFixed = false;
    if (LdrFixed)
    {
        return true;
    }
    const wchar_t sntdll[] = L"ntdll.dll";
    char *ntdll = reinterpret_cast < char * > (GetModuleBase(sntdll, sizeof sntdll - sizeof(wchar_t)));
    if (!ntdll)
    {
        return false;
    }
    void **pNtQueryVirtualMemory,  **pLdrLoadDll,  **pLdrGetProcedureAddress,  **pRtlAnsiStringToUnicodeString;
#ifdef _M_X64
    extern void *__imp_NtQueryVirtualMemory;
    pNtQueryVirtualMemory = &__imp_NtQueryVirtualMemory;
    extern void *__imp_LdrLoadDll;
    pLdrLoadDll = &__imp_LdrLoadDll;
    extern void *__imp_LdrGetProcedureAddress;
    pLdrGetProcedureAddress = &__imp_LdrGetProcedureAddress;
    extern void *__imp_RtlAnsiStringToUnicodeString;
    pRtlAnsiStringToUnicodeString = &__imp_RtlAnsiStringToUnicodeString;
#else 
    __asm
    {
        mov pNtQueryVirtualMemory, offset NtQueryVirtualMemory
		mov pLdrLoadDll, offset LdrLoadDll
		mov pLdrGetProcedureAddress, offset LdrGetProcedureAddress
		mov pRtlAnsiStringToUnicodeString, offset RtlAnsiStringToUnicodeString
    }
#endif 
	if(*pNtQueryVirtualMemory != ntdll + GetExportedRVAGT< IMAGE_NT_HEADERS >(ntdll, "NtQueryVirtualMemory"))
	{
		*pNtQueryVirtualMemory = ntdll + GetExportedRVAGT < IMAGE_NT_HEADERS > (ntdll, "NtQueryVirtualMemory");
	}
	if (*pLdrLoadDll != ntdll + GetExportedRVAGT < IMAGE_NT_HEADERS > (ntdll, "LdrLoadDll"))
	{
		*pLdrLoadDll = ntdll + GetExportedRVAGT < IMAGE_NT_HEADERS > (ntdll, "LdrLoadDll");
	}
	if(*pLdrGetProcedureAddress != ntdll + GetExportedRVAGT < IMAGE_NT_HEADERS > (ntdll, "LdrGetProcedureAddress"))
	{
		*pLdrGetProcedureAddress = ntdll + GetExportedRVAGT < IMAGE_NT_HEADERS > (ntdll, "LdrGetProcedureAddress");
	}
	if(*pRtlAnsiStringToUnicodeString != ntdll + GetExportedRVAGT < IMAGE_NT_HEADERS > (ntdll, "RtlAnsiStringToUnicodeString"))
	{
		*pRtlAnsiStringToUnicodeString = ntdll + GetExportedRVAGT < IMAGE_NT_HEADERS > (ntdll, "RtlAnsiStringToUnicodeString");
	}
    LdrFixed = true;
    return true;
}

//-------------------------------------------------------------------------

bool LdrSup::FixLdrImports()
{
    return ::FixLdrImports();
}

//-------------------------------------------------------------------------

static wchar_t *DllFindDirectory = NULL;

//-------------------------------------------------------------------------

void LdrSup::SetDllFindDirectory(const wchar_t *path)
{
	DllFindDirectory = (wchar_t *)path;//'A;B;C'
}

//-------------------------------------------------------------------------

NTSTATUS LdrSup::LoadDll(IN PWSTR DllPath OPTIONAL, IN PULONG DllCharacteristics OPTIONAL, IN PUNICODE_STRING DllName, OUT PVOID *DllHandle)
{
	if (DllPath != NULL)
	{
		return LdrLoadDll(DllPath, DllCharacteristics, DllName, DllHandle);
	} //if
	auto status = LdrLoadDll(NULL, DllCharacteristics, DllName, DllHandle);
	if (status != STATUS_SUCCESS && DllFindDirectory != NULL)
	{
		status = LdrLoadDll(DllFindDirectory, DllCharacteristics, DllName, DllHandle);
	} //if
	return status;
}

//-------------------------------------------------------------------------

bool LdrSup::FixImports(void *BaseAddress, bool FastFix)
{
    if (!::FixLdrImports())
    {
        return false;
    }
    if (BaseAddress == 0)
    {
        MEMORY_BASIC_INFORMATION mbi;
        ULONG r;
        if (NtQueryVirtualMemory(CurrentProcess, LdrSup::FixImports, MemoryBasicInformation, &mbi, sizeof mbi, &r) == 0)
        {
            BaseAddress = mbi.AllocationBase;
        }
        else
        {
            return false;
        }
    }
    bool suc = true;
    UINT_PTR base = reinterpret_cast < UINT_PTR > (BaseAddress);
    PIMAGE_DOS_HEADER DosHeader;
    PIMAGE_NT_HEADERS NtHeaders;
    if (!ValidateHeaders(BaseAddress, &DosHeader, &NtHeaders))
    {
        return false;
    }
    DWORD ImpRVA = NtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress;
    DWORD ImpSize = NtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size;
    if (ImpRVA && ImpSize)
    {
        PIMAGE_IMPORT_DESCRIPTOR ImpDir = reinterpret_cast < PIMAGE_IMPORT_DESCRIPTOR > (base + ImpRVA);
        UINT_PTR ImpEnd = base + ImpRVA + ImpSize;
        while (ImpDir->Name && reinterpret_cast < UINT_PTR > (ImpDir) < ImpEnd)
        {
            HMODULE ModuleBase;
            ANSI_STRING AnsiModuleName;
            UNICODE_STRING ModuleName;
            AnsiModuleName.Buffer = reinterpret_cast < char * > (base + ImpDir->Name);
            AnsiModuleName.Length = AnsiModuleName.MaximumLength = static_cast < USHORT > (strlen(AnsiModuleName.Buffer));
            ModuleName.Length = ModuleName.MaximumLength = (AnsiModuleName.Length + 1) *sizeof(wchar_t);
            ModuleName.Buffer = static_cast < wchar_t * > (_alloca(ModuleName.Length));
            RtlAnsiStringToUnicodeString(&ModuleName, &AnsiModuleName, 0);
// #ifdef _DEBUG
// 			MessageBoxEx(NULL, ModuleName.Buffer, L"���Դ���", 0, 0);
// #endif // _DEBUG
			if (LoadDll(0, 0, &ModuleName, (PVOID *)&ModuleBase) == 0)
            {
                PIMAGE_THUNK_DATA OriginalFirstThunk = reinterpret_cast < PIMAGE_THUNK_DATA > (base + ImpDir->OriginalFirstThunk), FirstThunk = reinterpret_cast < PIMAGE_THUNK_DATA > (base + ImpDir->FirstThunk);
                for (int i = 0; OriginalFirstThunk[i].u1.AddressOfData; ++i)
                {
                    if (FastFix && OriginalFirstThunk[i].u1.AddressOfData != FirstThunk[i].u1.AddressOfData)
                    {
                        continue;
                    }
                    bool IsOrdinal = reinterpret_cast < INT_PTR & > (OriginalFirstThunk[i].u1.AddressOfData) < 0;
                    void *addr;
                    if (IsOrdinal)
                    {
                        if (LdrGetProcedureAddress(ModuleBase, 0, static_cast < WORD > (OriginalFirstThunk[i].u1.Ordinal), &addr))
                        {
                            suc = false;
                            continue;
                        }
                    }
                    else
                    {
                        PIMAGE_IMPORT_BY_NAME ImpName;
                        ANSI_STRING Name;
                        ImpName = reinterpret_cast < PIMAGE_IMPORT_BY_NAME > (base + OriginalFirstThunk[i].u1.AddressOfData);
                        Name.Buffer = reinterpret_cast < PCHAR > (ImpName->Name);
                        Name.Length = Name.MaximumLength = static_cast < USHORT > (strlen(Name.Buffer) *sizeof(char));
                        if (LdrGetProcedureAddress(ModuleBase, &Name, 0, &addr))
                        {
                            suc = false;
                            continue;
                        }
                    }
                    reinterpret_cast < void * & > (FirstThunk[i].u1.Function) = addr;
                }
            }
            else
            {
                suc = false;
            }
            ++ImpDir;
        }
    }
    return suc;
}

//-------------------------------------------------------------------------

bool LdrSup::IsWow64Process(HANDLE ProcessHandle)
{
    ULONG_PTR v;
    if (NtQueryInformationProcess(ProcessHandle, ProcessWow64Information, &v, sizeof v, 0) >= 0)
    {
        return v != 0;
    }
    else
    {
        return false;
    }
}

//-------------------------------------------------------------------------

NTSTATUS LdrSup::UnMapImage(HANDLE ProcessHandle, void *BaseAddress)
{
	auto status = (LPVOID)NtUnmapViewOfSection(ProcessHandle, BaseAddress);
	return (NTSTATUS)status;
}


//-------------------------------------------------------------------------

NTSTATUS LdrSup::CloseNativeHandle(HANDLE ProcessHandle)
{
	return NtClose(ProcessHandle);
}