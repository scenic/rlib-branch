/********************************************************************
	Created:	2012/06/23  11:18
	Filename: 	TX.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "Loader.h"
//#pragma comment(lib, "../../bin/QQ/Common.lib")
//-------------------------------------------------------------------------
#define _TX
#ifdef _TX
class export CTXStringA
{
public:/*guess member*/
	TCHAR *unused[64];
public:
	/// <summary>
	/// ?Append@CTXStringA@@QAEXABV1@@Z
	/// </summary>
	void  Append(class CTXStringA const &);
	/// <summary>
	/// ?Append@CTXStringA@@QAEXPBD@Z
	/// </summary>
	void  Append(char const *);
	/// <summary>
	/// ?Append@CTXStringA@@QAEXPBDH@Z
	/// </summary>
	void  Append(char const *,int);
	/// <summary>
	/// ?AppendChar@CTXStringA@@QAEXD@Z
	/// </summary>
	void  AppendChar(char);
	/// <summary>
	/// ??0CTXStringA@@QAE@UtagEN@@PB_WH@Z
	/// </summary>
	CTXStringA(struct tagEN,wchar_t const *,int);
	/// <summary>
	/// ??0CTXStringA@@QAE@UtagGBK@@PB_WH@Z
	/// </summary>
	CTXStringA(struct tagGBK,wchar_t const *,int);
	/// <summary>
	/// ??0CTXStringA@@QAE@UtagUTF8@@PB_WH@Z
	/// </summary>
	CTXStringA(struct tagUTF8,wchar_t const *,int);
	/// <summary>
	/// ??0CTXStringA@@QAE@XZ
	/// </summary>
	CTXStringA(void);
	/// <summary>
	/// ??0CTXStringA@@QAE@ABV0@@Z
	/// </summary>
	CTXStringA(class CTXStringA const &);
	/// <summary>
	/// ??0CTXStringA@@QAE@DH@Z
	/// </summary>
	CTXStringA(char,int);
	/// <summary>
	/// ??0CTXStringA@@QAE@PBD@Z
	/// </summary>
	CTXStringA(char const *);
	/// <summary>
	/// ??0CTXStringA@@QAE@PBDH@Z
	/// </summary>
	CTXStringA(char const *,int);
	/// <summary>
	/// ?Compare@CTXStringA@@QBEHPBD@Z
	/// </summary>
	int  Compare(char const *)const ;
	/// <summary>
	/// ?CompareNoCase@CTXStringA@@QBEHPBD@Z
	/// </summary>
	int  CompareNoCase(char const *)const ;
	/// <summary>
	/// ?Delete@CTXStringA@@QAEHHH@Z
	/// </summary>
	int  Delete(int,int);
	/// <summary>
	/// ?Empty@CTXStringA@@QAEXXZ
	/// </summary>
	void  Empty(void);
	/// <summary>
	/// ?Find@CTXStringA@@QBEHDH@Z
	/// </summary>
	int  Find(char,int)const ;
	/// <summary>
	/// ?Find@CTXStringA@@QBEHPBDH@Z
	/// </summary>
	int  Find(char const *,int)const ;
	/// <summary>
	/// ?FindOneOf@CTXStringA@@QBEHPBD@Z
	/// </summary>
	int  FindOneOf(char const *)const ;
	/// <summary>
	/// ?Format@CTXStringA@@QAAXPBDZZ
	/// </summary>
	void __cdecl Format(char const *,...);
	/// <summary>
	/// ?FormatV@CTXStringA@@QAEXPBDPAD@Z
	/// </summary>
	void  FormatV(char const *,char *);
	/// <summary>
	/// ?GetAllocLength@CTXStringA@@QBEHXZ
	/// </summary>
	int  GetAllocLength(void)const ;
	/// <summary>
	/// ?GetAt@CTXStringA@@QBEDH@Z
	/// </summary>
	char  GetAt(int)const ;
	/// <summary>
	/// ?GetBuffer@CTXStringA@@QAEPADH@Z
	/// </summary>
	char *  GetBuffer(int);
	/// <summary>
	/// ?GetBuffer@CTXStringA@@QAEPADXZ
	/// </summary>
	char *  GetBuffer(void);
	/// <summary>
	/// ?GetBufferSetLength@CTXStringA@@QAEPADH@Z
	/// </summary>
	char *  GetBufferSetLength(int);
	/// <summary>
	/// ?GetLength@CTXStringA@@QBEHXZ
	/// </summary>
	int  GetLength(void)const ;
	/// <summary>
	/// ?GetString@CTXStringA@@QBEPBDXZ
	/// </summary>
	char const *  GetString(void)const ;
	/// <summary>
	/// ?Insert@CTXStringA@@QAEHHPBD@Z
	/// </summary>
	int  Insert(int,char const *);
	/// <summary>
	/// ?Insert@CTXStringA@@QAEHHD@Z
	/// </summary>
	int  Insert(int,char);
	/// <summary>
	/// ?IsEmpty@CTXStringA@@QBE_NXZ
	/// </summary>
	bool  IsEmpty(void)const ;
	/// <summary>
	/// ?Left@CTXStringA@@QBE?AV1@H@Z
	/// </summary>
	class CTXStringA  Left(int)const ;
	/// <summary>
	/// ?MakeLower@CTXStringA@@QAEAAV1@XZ
	/// </summary>
	class CTXStringA &  MakeLower(void);
	/// <summary>
	/// ?MakeReverse@CTXStringA@@QAEAAV1@XZ
	/// </summary>
	class CTXStringA &  MakeReverse(void);
	/// <summary>
	/// ?MakeUpper@CTXStringA@@QAEAAV1@XZ
	/// </summary>
	class CTXStringA &  MakeUpper(void);
	/// <summary>
	/// ?Mid@CTXStringA@@QBE?AV1@HH@Z
	/// </summary>
	class CTXStringA  Mid(int,int)const ;
	/// <summary>
	/// ?Mid@CTXStringA@@QBE?AV1@H@Z
	/// </summary>
	class CTXStringA  Mid(int)const ;
	/// <summary>
	/// ?Preallocate@CTXStringA@@QAEXH@Z
	/// </summary>
	void  Preallocate(int);
	/// <summary>
	/// ?ReleaseBuffer@CTXStringA@@QAEXH@Z
	/// </summary>
	void  ReleaseBuffer(int);
	/// <summary>
	/// ?Remove@CTXStringA@@QAEHD@Z
	/// </summary>
	int  Remove(char);
	/// <summary>
	/// ?Replace@CTXStringA@@QAEHPBD0@Z
	/// </summary>
	int  Replace(char const *,char const *);
	/// <summary>
	/// ?Replace@CTXStringA@@QAEHDD@Z
	/// </summary>
	int  Replace(char,char);
	/// <summary>
	/// ?ReverseFind@CTXStringA@@QBEHD@Z
	/// </summary>
	int  ReverseFind(char)const ;
	/// <summary>
	/// ?Right@CTXStringA@@QBE?AV1@H@Z
	/// </summary>
	class CTXStringA  Right(int)const ;
	/// <summary>
	/// ?SetAt@CTXStringA@@QAEXHD@Z
	/// </summary>
	void  SetAt(int,char);
	/// <summary>
	/// ?SetString@CTXStringA@@QAEXPBDH@Z
	/// </summary>
	void  SetString(char const *,int);
	/// <summary>
	/// ?SetString@CTXStringA@@QAEXPBD@Z
	/// </summary>
	void  SetString(char const *);
	/// <summary>
	/// ?Trim@CTXStringA@@QAEAAV1@D@Z
	/// </summary>
	class CTXStringA &  Trim(char);
	/// <summary>
	/// ?Trim@CTXStringA@@QAEAAV1@PBD@Z
	/// </summary>
	class CTXStringA &  Trim(char const *);
	/// <summary>
	/// ?Trim@CTXStringA@@QAEAAV1@XZ
	/// </summary>
	class CTXStringA &  Trim(void);
	/// <summary>
	/// ?TrimLeft@CTXStringA@@QAEAAV1@D@Z
	/// </summary>
	class CTXStringA &  TrimLeft(char);
	/// <summary>
	/// ?TrimLeft@CTXStringA@@QAEAAV1@XZ
	/// </summary>
	class CTXStringA &  TrimLeft(void);
	/// <summary>
	/// ?TrimLeft@CTXStringA@@QAEAAV1@PBD@Z
	/// </summary>
	class CTXStringA &  TrimLeft(char const *);
	/// <summary>
	/// ?TrimRight@CTXStringA@@QAEAAV1@D@Z
	/// </summary>
	class CTXStringA &  TrimRight(char);
	/// <summary>
	/// ?TrimRight@CTXStringA@@QAEAAV1@PBD@Z
	/// </summary>
	class CTXStringA &  TrimRight(char const *);
	/// <summary>
	/// ?TrimRight@CTXStringA@@QAEAAV1@XZ
	/// </summary>
	class CTXStringA &  TrimRight(void);
	/// <summary>
	/// ?Truncate@CTXStringA@@QAEXH@Z
	/// </summary>
	void  Truncate(int);
	/// <summary>
	/// ??BCTXStringA@@QBEPBDXZ
	/// </summary>
	operator char const *(void)const ;
	/// <summary>
	/// ??7CTXStringA@@QBE_NXZ
	/// </summary>
	bool  operator!(void)const ;
	/// <summary>
	/// ??YCTXStringA@@QAEAAV0@ABV0@@Z
	/// </summary>
	class CTXStringA &  operator+=(class CTXStringA const &);
	/// <summary>
	/// ??YCTXStringA@@QAEAAV0@PBD@Z
	/// </summary>
	class CTXStringA &  operator+=(char const *);
	/// <summary>
	/// ??YCTXStringA@@QAEAAV0@D@Z
	/// </summary>
	class CTXStringA &  operator+=(char);
	/// <summary>
	/// ??4CTXStringA@@QAEAAV0@D@Z
	/// </summary>
	class CTXStringA &  operator=(char);
	/// <summary>
	/// ??4CTXStringA@@QAEAAV0@ABV0@@Z
	/// </summary>
	class CTXStringA &  operator=(class CTXStringA const &);
	/// <summary>
	/// ??4CTXStringA@@QAEAAV0@PBD@Z
	/// </summary>
	class CTXStringA &  operator=(char const *);
	/// <summary>
	/// ??ACTXStringA@@QBEDH@Z
	/// </summary>
	char  operator[](int)const ;
	/// <summary>
	/// ??1CTXStringA@@QAE@XZ
	/// </summary>
	~CTXStringA(void);
};

class export CTXStringW
{
public:/*guess member*/
	TCHAR *unused[64];
public:
	/// <summary>
	/// ?AllocSysString@CTXStringW@@QBEPA_WXZ
	/// </summary>
	wchar_t *  AllocSysString(void)const ;
	/// <summary>
	/// ?Append@CTXStringW@@QAEXABV1@@Z
	/// </summary>
	void  Append(class CTXStringW const &);
	/// <summary>
	/// ?Append@CTXStringW@@QAEXPB_W@Z
	/// </summary>
	void  Append(wchar_t const *);
	/// <summary>
	/// ?Append@CTXStringW@@QAEXPB_WH@Z
	/// </summary>
	void  Append(wchar_t const *,int);
	/// <summary>
	/// ?AppendBSTR@CTXStringW@@QAEXPA_W@Z
	/// </summary>
	void  AppendBSTR(wchar_t *);
	/// <summary>
	/// ?AppendChar@CTXStringW@@QAEX_W@Z
	/// </summary>
	void  AppendChar(wchar_t);
	/// <summary>
	/// ?AppendFormat@CTXStringW@@QAAXPB_WZZ
	/// </summary>
	void __cdecl AppendFormat(wchar_t const *,...);
	/// <summary>
	/// ??0CTXStringW@@QAE@PB_WH@Z
	/// </summary>
	CTXStringW(wchar_t const *,int);
	/// <summary>
	/// ??0CTXStringW@@QAE@UtagEN@@PBDH@Z
	/// </summary>
	CTXStringW(struct tagEN,char const *,int);
	/// <summary>
	/// ??0CTXStringW@@QAE@UtagGBK@@PBDH@Z
	/// </summary>
	CTXStringW(struct tagGBK,char const *,int);
	/// <summary>
	/// ??0CTXStringW@@QAE@UtagUTF8@@PBDH@Z
	/// </summary>
	CTXStringW(struct tagUTF8,char const *,int);
	/// <summary>
	/// ??0CTXStringW@@QAE@XZ
	/// </summary>
	CTXStringW(void);
	/// <summary>
	/// ??0CTXStringW@@QAE@_WH@Z
	/// </summary>
	CTXStringW(wchar_t,int);
	/// <summary>
	/// ??0CTXStringW@@QAE@ABU_GUID@@@Z
	/// </summary>
	CTXStringW(struct _GUID const &);
	/// <summary>
	/// ??0CTXStringW@@QAE@ABUtagVARIANT@@@Z
	/// </summary>
	CTXStringW(struct tagVARIANT const &);
	/// <summary>
	/// ??0CTXStringW@@QAE@ABV0@@Z
	/// </summary>
	CTXStringW(class CTXStringW const &);
	/// <summary>
	/// ??0CTXStringW@@QAE@ABVCTXBSTR@@@Z
	/// </summary>
	CTXStringW(class CTXBSTR const &);
	/// <summary>
	/// ??0CTXStringW@@QAE@H@Z
	/// </summary>
	CTXStringW(int);
	/// <summary>
	/// ??0CTXStringW@@QAE@PA_W@Z
	/// </summary>
	CTXStringW(wchar_t *);
	/// <summary>
	/// ??0CTXStringW@@QAE@PB_W@Z
	/// </summary>
	CTXStringW(wchar_t const *);
	/// <summary>
	/// ?Collate@CTXStringW@@QBEHPB_W@Z
	/// </summary>
	int  Collate(wchar_t const *)const ;
	/// <summary>
	/// ?CollateNoCase@CTXStringW@@QBEHPB_W@Z
	/// </summary>
	int  CollateNoCase(wchar_t const *)const ;
	/// <summary>
	/// ?Compare@CTXStringW@@QBEHPB_W@Z
	/// </summary>
	int  Compare(wchar_t const *)const ;
	/// <summary>
	/// ?CompareNoCase@CTXStringW@@QBEHPB_W@Z
	/// </summary>
	int  CompareNoCase(wchar_t const *)const ;
	/// <summary>
	/// ?Delete@CTXStringW@@QAEHHH@Z
	/// </summary>
	int  Delete(int,int);
	/// <summary>
	/// ?Empty@CTXStringW@@QAEXXZ
	/// </summary>
	void  Empty(void);
	/// <summary>
	/// ?Find@CTXStringW@@QBEHPB_WH@Z
	/// </summary>
	int  Find(wchar_t const *,int)const ;
	/// <summary>
	/// ?Find@CTXStringW@@QBEH_WH@Z
	/// </summary>
	int  Find(wchar_t,int)const ;
	/// <summary>
	/// ?FindOneOf@CTXStringW@@QBEHPB_W@Z
	/// </summary>
	int  FindOneOf(wchar_t const *)const ;
	/// <summary>
	/// ?Format@CTXStringW@@QAAXPB_WZZ
	/// </summary>
	void __cdecl Format(wchar_t const *,...);
	/// <summary>
	/// ?FormatV@CTXStringW@@QAEXPB_WPAD@Z
	/// </summary>
	void  FormatV(wchar_t const *,char *);
	/// <summary>
	/// ?GetAllocLength@CTXStringW@@QBEHXZ
	/// </summary>
	int  GetAllocLength(void)const ;
	/// <summary>
	/// ?GetAt@CTXStringW@@QBE_WH@Z
	/// </summary>
	wchar_t  GetAt(int)const ;
	/// <summary>
	/// ?GetBSTR@CTXStringW@@QBEPA_WXZ
	/// </summary>
	wchar_t *  GetBSTR(void)const ;
	/// <summary>
	/// ?GetBSTRPtr@CTXStringW@@QAEPAPA_WXZ
	/// </summary>
	wchar_t * *  GetBSTRPtr(void);
	/// <summary>
	/// ?GetBuffer@CTXStringW@@QAEPA_WH@Z
	/// </summary>
	wchar_t *  GetBuffer(int);
	/// <summary>
	/// ?GetBuffer@CTXStringW@@QAEPA_WXZ
	/// </summary>
	wchar_t *  GetBuffer(void);
	/// <summary>
	/// ?GetBufferSetLength@CTXStringW@@QAEPA_WH@Z
	/// </summary>
	wchar_t *  GetBufferSetLength(int);
	/// <summary>
	/// ?GetByteLength@CTXStringW@@QBEHXZ
	/// </summary>
	int  GetByteLength(void)const ;
	/// <summary>
	/// ?GetLength@CTXStringW@@QBEHXZ
	/// </summary>
	int  GetLength(void)const ;
	/// <summary>
	/// ?GetString@CTXStringW@@QBEPB_WXZ
	/// </summary>
	wchar_t const *  GetString(void)const ;
	/// <summary>
	/// ?Insert@CTXStringW@@QAEHH_W@Z
	/// </summary>
	int  Insert(int,wchar_t);
	/// <summary>
	/// ?Insert@CTXStringW@@QAEHHPB_W@Z
	/// </summary>
	int  Insert(int,wchar_t const *);
	/// <summary>
	/// ?IsEmpty@CTXStringW@@QBE_NXZ
	/// </summary>
	bool  IsEmpty(void)const ;
	/// <summary>
	/// ?Left@CTXStringW@@QBE?AV1@H@Z
	/// </summary>
	class CTXStringW  Left(int)const ;
	/// <summary>
	/// ?LoadStringW@CTXStringW@@QAEHPAUHINSTANCE__@@IG@Z
	/// </summary>
	int  LoadStringW(struct HINSTANCE__ *,unsigned int,unsigned short);
	/// <summary>
	/// ?LoadStringW@CTXStringW@@QAEHPAUHINSTANCE__@@I@Z
	/// </summary>
	int  LoadStringW(struct HINSTANCE__ *,unsigned int);
	/// <summary>
	/// ?MakeLower@CTXStringW@@QAEAAV1@XZ
	/// </summary>
	class CTXStringW &  MakeLower(void);
	/// <summary>
	/// ?MakeReverse@CTXStringW@@QAEAAV1@XZ
	/// </summary>
	class CTXStringW &  MakeReverse(void);
	/// <summary>
	/// ?MakeUpper@CTXStringW@@QAEAAV1@XZ
	/// </summary>
	class CTXStringW &  MakeUpper(void);
	/// <summary>
	/// ?Mid@CTXStringW@@QBE?AV1@HH@Z
	/// </summary>
	class CTXStringW  Mid(int,int)const ;
	/// <summary>
	/// ?Mid@CTXStringW@@QBE?AV1@H@Z
	/// </summary>
	class CTXStringW  Mid(int)const ;
	/// <summary>
	/// ?Preallocate@CTXStringW@@QAEXH@Z
	/// </summary>
	void  Preallocate(int);
	/// <summary>
	/// ?Refresh@CTXStringW@@QBEABV1@XZ
	/// </summary>
	class CTXStringW const &  Refresh(void)const ;
	/// <summary>
	/// ?ReleaseBuffer@CTXStringW@@QAEXH@Z
	/// </summary>
	void  ReleaseBuffer(int);
	/// <summary>
	/// ?Remove@CTXStringW@@QAEH_W@Z
	/// </summary>
	int  Remove(wchar_t);
	/// <summary>
	/// ?Replace@CTXStringW@@QAEH_W0@Z
	/// </summary>
	int  Replace(wchar_t,wchar_t);
	/// <summary>
	/// ?Replace@CTXStringW@@QAEHPB_W0@Z
	/// </summary>
	int  Replace(wchar_t const *,wchar_t const *);
	/// <summary>
	/// ?ReverseFind@CTXStringW@@QBEH_W@Z
	/// </summary>
	int  ReverseFind(wchar_t)const ;
	/// <summary>
	/// ?Right@CTXStringW@@QBE?AV1@H@Z
	/// </summary>
	class CTXStringW  Right(int)const ;
	/// <summary>
	/// ?SetAt@CTXStringW@@QAEXH_W@Z
	/// </summary>
	void  SetAt(int,wchar_t);
	/// <summary>
	/// ?SetString@CTXStringW@@QAEXPB_WH@Z
	/// </summary>
	void  SetString(wchar_t const *,int);
	/// <summary>
	/// ?SetString@CTXStringW@@QAEXPB_W@Z
	/// </summary>
	void  SetString(wchar_t const *);
	/// <summary>
	/// ?SetSysString@CTXStringW@@QBEPA_WPAPA_W@Z
	/// </summary>
	wchar_t *  SetSysString(wchar_t * *)const ;
	/// <summary>
	/// ?SpanIncluding@CTXStringW@@QBE?AV1@PB_W@Z
	/// </summary>
	class CTXStringW  SpanIncluding(wchar_t const *)const ;
	/// <summary>
	/// ?Tokenize@CTXStringW@@QBE?AV1@PB_WAAH@Z
	/// </summary>
	class CTXStringW  Tokenize(wchar_t const *,int &)const ;
	/// <summary>
	/// ?Trim@CTXStringW@@QAEAAV1@PB_W@Z
	/// </summary>
	class CTXStringW &  Trim(wchar_t const *);
	/// <summary>
	/// ?Trim@CTXStringW@@QAEAAV1@_W@Z
	/// </summary>
	class CTXStringW &  Trim(wchar_t);
	/// <summary>
	/// ?Trim@CTXStringW@@QAEAAV1@XZ
	/// </summary>
	class CTXStringW &  Trim(void);
	/// <summary>
	/// ?TrimLeft@CTXStringW@@QAEAAV1@PB_W@Z
	/// </summary>
	class CTXStringW &  TrimLeft(wchar_t const *);
	/// <summary>
	/// ?TrimLeft@CTXStringW@@QAEAAV1@XZ
	/// </summary>
	class CTXStringW &  TrimLeft(void);
	/// <summary>
	/// ?TrimLeft@CTXStringW@@QAEAAV1@_W@Z
	/// </summary>
	class CTXStringW &  TrimLeft(wchar_t);
	/// <summary>
	/// ?TrimRight@CTXStringW@@QAEAAV1@XZ
	/// </summary>
	class CTXStringW &  TrimRight(void);
	/// <summary>
	/// ?TrimRight@CTXStringW@@QAEAAV1@PB_W@Z
	/// </summary>
	class CTXStringW &  TrimRight(wchar_t const *);
	/// <summary>
	/// ?TrimRight@CTXStringW@@QAEAAV1@_W@Z
	/// </summary>
	class CTXStringW &  TrimRight(wchar_t);
	/// <summary>
	/// ?Truncate@CTXStringW@@QAEXH@Z
	/// </summary>
	void  Truncate(int);
	/// <summary>
	/// ??BCTXStringW@@QBEPB_WXZ
	/// </summary>
	operator wchar_t const *(void)const ;
	/// <summary>
	/// ??7CTXStringW@@QBE_NXZ
	/// </summary>
	bool  operator!(void)const ;
	/// <summary>
	/// ??YCTXStringW@@QAEAAV0@ABUtagVARIANT@@@Z
	/// </summary>
	class CTXStringW &  operator+=(struct tagVARIANT const &);
	/// <summary>
	/// ??YCTXStringW@@QAEAAV0@PB_W@Z
	/// </summary>
	class CTXStringW &  operator+=(wchar_t const *);
	/// <summary>
	/// ??YCTXStringW@@QAEAAV0@_W@Z
	/// </summary>
	class CTXStringW &  operator+=(wchar_t);
	/// <summary>
	/// ??YCTXStringW@@QAEAAV0@ABVCTXBSTR@@@Z
	/// </summary>
	class CTXStringW &  operator+=(class CTXBSTR const &);
	/// <summary>
	/// ??YCTXStringW@@QAEAAV0@ABV0@@Z
	/// </summary>
	class CTXStringW &  operator+=(class CTXStringW const &);
	/// <summary>
	/// ??4CTXStringW@@QAEAAV0@ABV0@@Z
	/// </summary>
	class CTXStringW &  operator=(class CTXStringW const &);
	/// <summary>
	/// ??4CTXStringW@@QAEAAV0@ABVCTXBSTR@@@Z
	/// </summary>
	class CTXStringW &  operator=(class CTXBSTR const &);
	/// <summary>
	/// ??4CTXStringW@@QAEAAV0@ABUtagVARIANT@@@Z
	/// </summary>
	class CTXStringW &  operator=(struct tagVARIANT const &);
	/// <summary>
	/// ??4CTXStringW@@QAEAAV0@_W@Z
	/// </summary>
	class CTXStringW &  operator=(wchar_t);
	/// <summary>
	/// ??4CTXStringW@@QAEAAV0@PB_W@Z
	/// </summary>
	class CTXStringW &  operator=(wchar_t const *);
	/// <summary>
	/// ??4CTXStringW@@QAEAAV0@PA_W@Z
	/// </summary>
	class CTXStringW &  operator=(wchar_t *);
	/// <summary>
	/// ??ACTXStringW@@QBE_WH@Z
	/// </summary>
	wchar_t  operator[](int)const ;
	/// <summary>
	/// ??1CTXStringW@@QAE@XZ
	/// </summary>
	~CTXStringW(void);
};
#endif // _TX