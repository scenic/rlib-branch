/********************************************************************
	Created:	2012/06/06  20:14
	Filename: 	RLib_LdrSup.h
	Author:		Lactoferrin, rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_LDRSUP
#define _USE_LDRSUP
#include "../../RLib/native/RLib_Native.h"
namespace LdrSup
{
    bool ValidateHeaders(const void *BaseAddress, PIMAGE_DOS_HEADER *DosHeader, PIMAGE_NT_HEADERS32 *NtHeaders);
    bool ValidateHeaders(const void *BaseAddress, PIMAGE_DOS_HEADER *DosHeader, PIMAGE_NT_HEADERS64 *NtHeaders);
    size_t GetFileOffset32(const void *BaseAddress, size_t RVA, PIMAGE_SECTION_HEADER *SectionHeader = 0);
    size_t GetFileOffset64(const void *BaseAddress, size_t RVA, PIMAGE_SECTION_HEADER *SectionHeader = 0);
    DWORD AlignSections32(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable = false);
    DWORD AlignSections64(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable = false);
    DWORD DuplicateImage32(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable = false);
    DWORD DuplicateImage64(PHANDLE SectionHandle, void * &Destination, const void *Source, bool Inheritable = false);
    bool RelocateImage32(void *BaseAddress, void *RelocateTo = 0, void *RelocateFrom = 0);
    bool RelocateImage64(void *BaseAddress, void *RelocateTo = 0, void *RelocateFrom = 0);
    DWORD GetExportedRVA32(const void *BaseAddress, const char *Name);
    DWORD GetExportedRVA64(const void *BaseAddress, const char *Name);
    bool SetExportedValue32(void *BaseAddress, const char *Name, const void *Buffer, size_t Length);
    bool SetExportedValue64(void *BaseAddress, const char *Name, const void *Buffer, size_t Length);
    template <typename T> bool SetExportedValue32(void *BaseAddress, const char *Name, const T &Object)
    {
        return SetExportedValue32(BaseAddress, Name, &Object, sizeof Object);
    }
    template <typename T> bool SetExportedValue64(void *BaseAddress, const char *Name, const T &Object)
    {
        return SetExportedValue64(BaseAddress, Name, &Object, sizeof Object);
    }
    void *MapImage(HANDLE SectionHandle, HANDLE ProcessHandle, void *BaseAddress = 0, bool Inheritable = false);
    void *MapImage(HANDLE SectionHandle, DWORD ProcessId, void *BaseAddress = 0, bool Inheritable = false);
    HANDLE RunImage(HANDLE ProcessHandle, void *BaseAddress, void *EntryPoint, void *Parameter = 0, bool CreateSuspended = false);
    HANDLE RunImage(HANDLE ProcessHandle, void *BaseAddress, DWORD EntryPointRVA, void *Parameter = 0, bool CreateSuspended = false);
    NTSTATUS UnMapImage(HANDLE ProcessHandle, void *BaseAddress);
	DWORD GetEntryPoint32(const void *BaseAddress);
    DWORD GetEntryPoint64(const void *BaseAddress);
    PPEB WINAPI GetPEB();
    HMODULE GetModuleBase(const wchar_t *Name, wint_t NameLength);
	void SetDllFindDirectory(const wchar_t *path);
	NTSTATUS LoadDll(IN PWSTR DllPath OPTIONAL, IN PULONG DllCharacteristics OPTIONAL, IN PUNICODE_STRING DllName, OUT PVOID *DllHandle);
    bool FixLdrImports();
    bool FixImports(void *BaseAddress = 0, bool FastFix = true);
    bool IsWow64Process(HANDLE ProcessHandle);
	NTSTATUS CloseNativeHandle(HANDLE ProcessHandle);
}
#endif