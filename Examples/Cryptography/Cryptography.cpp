/********************************************************************
	Created:	2012/05/26  16:08
	Filename: 	Loader.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "../../RLib/RLib_Import.h"

#ifdef _DEBUG
#pragma comment(lib, "../../bin/RLib_d.lib")
#else
#pragma comment(lib, "../../bin/RLib.lib")
#endif

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace System::Security::Cryptography;
//-------------------------------------------------------------------------
int __stdcall WinMain( __in HINSTANCE /*hInstance*/, __in_opt HINSTANCE /*hPrevInstance*/, __in LPSTR /*lpCmdLine*/, __in int /*nShowCmd*/ )
{
	//Base64����Unicode�ַ�
	auto b64_encode = Base64::EncodeBytes(L"��ӭʹ��RLIB�������", sizeof(L"��ӭʹ��RLIB�������") - sizeof(wchar_t));
	auto b64_decode  = (wchar_t *)Base64::DecodeBytes(b64_encode, strlen(b64_encode));

	//Obama64�������
	Obama64* ocoder = new Obama64;	
	{
		ocoder->SetSecret('r');

		//��/���봿ASCII�ַ�
		CHAR   c1[] = {"rlib"};
		String e1   = ocoder->EncodeAsciiString(c1);
		String d1   = ocoder->DecodeAsciiString(e1.ToMultiByte());

		//��/���������ַ�
		TCHAR   c2[] = {T("��ӭʹ��RLIB�������")};
		String e2    = ocoder->EncodeString(c2);
		String d2    = ocoder->DecodeString(e2);

		delete ocoder;
	}

	//MD5������� 16λ
	auto md5_encode = MD5::GetHashCode(T("��ӭʹ��RLIB�������"));

	Base64::Collect(b64_encode);
	Base64::Collect(b64_decode);

	return 0;
}