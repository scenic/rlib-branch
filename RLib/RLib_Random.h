/********************************************************************
	Created:	2012/11/17  13:44
	Filename: 	RLib_Random.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "RLib_Variant.h"
//-------------------------------------------------------------------------
namespace System
{
	/// <summary>
	/// 表示伪随机数生成器，一种能够产生满足某些随机性统计要求的数字序列的设备
	/// </summary>
	class export Random
	{
	public:
		/// <summary>
		/// 一种产生唯一种子值的方法是使它与时间相关, 方法使用与时间相关的默认种子值
		/// </summary>
		static void Srand();
		/// <summary>
		/// 如果应用程序需要不同的随机数序列，则使用不同的种子值重复调用此函数。
		/// </summary>
		/// <param name="Seed">用来计算伪随机数序列起始值的数字</param>
		static void Srand(unsigned int Seed);
		/// <summary>
		/// 返回一个指定范围内的随机数
		/// </summary>
		/// <param name="minValue">返回的随机数的下界（随机数可取该下界值）</param>
		/// <param name="maxValue">返回的随机数的上界（随机数不能取该上界值）, maxValue 必须大于或等于 minValue。</param>
		/// <returns>一个大于等于 minValue 且小于 maxValue 的 32 位带符号整数</returns>
		static int GetRnd(int minValue, int maxValue);
		/// <summary>
		/// 返回一个指定范围内的随机数, 方法适用于大范围的随机数要求
		/// </summary>
		/// <param name="minValue">返回的随机数的下界（随机数可取该下界值）</param>
		/// <param name="maxValue">返回的随机数的上界（随机数不能取该上界值）, maxValue 必须大于或等于 minValue。</param>
		/// <returns>一个大于等于 minValue 且小于 maxValue 的 32 位带符号数</returns>
		static float GetWideRnd(int minValue, int maxValue);
	};
}