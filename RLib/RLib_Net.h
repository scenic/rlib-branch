#ifndef _USE_NET
#define _USE_NET
#include "RLib_Winsock.h"
//////////////////////////////////////////////////////////////////////////
using namespace System::IO;
//////////////////////////////////////////////////////////////////////////
namespace System
{
	namespace Net
	{
		/// <summary>
		/// 提供统一资源标识符 (URI) 的对象表示形式和对 URI 各部分的轻松访问
		/// </summary>
		typedef struct export Uri
		{
		public:
			/// <summary>
			///  获取此实例的主机部分
			/// </summary>
			String Host;         
			/// <summary>
			///  获取此 URI 的端口号
			/// </summary>
			USHORT Port;         
			/// <summary>
			///  获取此 URI 的方案名称
			/// </summary>
			String Scheme;       
			/// <summary>
			///  获取 AbsolutePath 和用问号 (?) 分隔的 Query 属性
			/// </summary>
			String PathAndQuery; 
			/// <summary>
			/// 获取传递给 Uri 构造函数的原始 URI 字符串
			/// </summary>
			String OriginalString;
		public:
			/// <summary>
			///  用指定 URI 初始化 Uri 类的新实例
			/// </summary>
			Uri(String);
			/// <summary>
			///  初始化空 Uri 类的新实例
			/// </summary>
			Uri(){};
			RLIB_ClassNewDel;
		public:
			/// <summary>
			/// 提供一种方法对省略的路径进行补足处理
			/// </summary>
			static String ProcessUri(String path, Uri *father);
			/// <summary>
			/// 提供URL编码方法
			/// @默认以且仅支持UTF-8编码
			/// </summary>
			static String UrlEncode(String str, Text::Encoding codepage = Text::UTF8Encoding);
			/// <summary>
			/// 提供URL解码方法
			/// @默认以且仅支持UTF-8解码
			/// </summary>
			static String UrlDecode(String str, Text::Encoding codepage = Text::UTF8Encoding);
		}*LPURI;//示例: Scheme://Host/Path[;URICookie]?Query        http://host/path;cookie?query
		/// <summary>
		/// 实现对 HttpRequest 类的代理访问
		/// </summary>
		typedef struct Proxy
		{
			/// <summary>
			/// 获取此实例的主机部分
			/// </summary>
			String Host;         
			/// <summary>
			/// 获取此 URI 的端口号
			/// </summary>
			USHORT Port;         
			/// <summary>
			/// 用指定格式 URI 初始化 Uri 类的新实例
			/// 示例: 127.0.0.1:80, 其中80端口可省略
			/// </summary>
			export Proxy(String);
			RLIB_ClassNewDel;
		}*LPPROXY;
		/// <summary>
		/// 表示文件压缩和解压缩编码格式，该格式将用来压缩在 HttpRequest 的响应中收到的数据
		/// </summary>
		namespace DecompressionMethod
		{
			// 摘要:
			//     表示文件压缩和解压缩编码格式，该格式将用来压缩在 System.Net.HttpWebRequest 的响应中收到的数据。
			enum DecompressionMethods
			{
				// 摘要:
				//     不使用压缩。
				None = 0,
				//
				// 摘要:
				//     使用 gZip 压缩/解压缩算法。
				GZip = 1,
				//
				// 摘要:
				//     使用 Deflate 压缩/解压缩算法。
				Deflate = 2
			};
		};
		/// <summary>
		/// 包含与请求或响应关联的协议标头
		/// </summary>
		typedef struct export WebHeaderCollection
		{
			/// <summary>
			/// 获取标头数量
			/// </summary>
			int Count;
			/// <summary>
			/// 将具有指定名称和值的标头插入到集合中
			/// </summary>
			void Add(LPCSTR, LPCSTR);
			/// <summary>
			/// 获取集合中特定标头的值，该值由标头名指定
			/// </summary>
			String Get(LPCSTR); 	
			/// <summary>
			/// 从集合中移除所有标头
			/// </summary>
			void Clear();
			/// <summary>
			/// 将 WebHeaderCollection 转换为字节数组, 并返回临时内存地址
			/// </summary>
			char *ToByteArray();
			/// <summary>
			/// 将 字节数组 写入WebHeaderCollection
			/// </summary>
			void WriteByteArray(char *pByteArray, int size);
			/// <summary>
			/// 初始化集合
			/// </summary>
			WebHeaderCollection();
			/// <summary>
			/// 清理内存
			/// </summary>
			~WebHeaderCollection(){ RLIB_Delete(this->m_headers); };
			RLIB_ClassNewDel;
		private:
			//headers内存
			System::IO::BufferedStream *m_headers;
		}WebHeader;
        /// <summary>
        /// 表示Http异常
        /// </summary>
        typedef Exception HttpException;
		/// <summary>
		/// 表示Http请求流
		/// </summary>
		typedef BufferedStream RequestStream, ResponseStream;
	}
}
#endif