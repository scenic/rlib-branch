/********************************************************************
	Created:	2012/04/22  9:00
	Filename: 	RLib_File.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_FILE
#define _USE_FILE
#include "RLib_Http.h"

#ifdef RLIB_DLL
#include "native/RLib_Native.h"
typedef _OBJECT_ATTRIBUTES OBJECT_ATTRIBUTES_STRUCT, *POBJECT_ATTRIBUTES_STRUCT;
typedef _IO_STATUS_BLOCK IO_STATUS_BLOCK_STRUCT, *PIO_STATUS_BLOCK_STRUCT;
typedef UNICODE_STRING UNICODE_STRING_STRUCT,*PUNICODE_STRING_STRUCT;
#else
typedef long NTSTATUS;
typedef struct _OBJECT_ATTRIBUTES_STRUCT
{
	ULONG  Length;
	HANDLE RootDirectory;
	PVOID  ObjectName;
	ULONG  Attributes;
	PVOID  SecurityDescriptor;        // Points to type SECURITY_DESCRIPTOR
	PVOID  SecurityQualityOfService;  // Points to type SECURITY_QUALITY_OF_SERVICE

} OBJECT_ATTRIBUTES_STRUCT, *POBJECT_ATTRIBUTES_STRUCT;
typedef struct _IO_STATUS_BLOCK_STRUCT
{
	union
	{
		NTSTATUS Status;
		PVOID Pointer;
	};
	ULONG_PTR Information;
} IO_STATUS_BLOCK_STRUCT, *PIO_STATUS_BLOCK_STRUCT;
typedef struct _UNICODE_STRING_STRUCT
{
	USHORT Length;
	USHORT MaximumLength;
	PWSTR  Buffer;
} UNICODE_STRING_STRUCT, *PUNICODE_STRING_STRUCT;
#define FILE_SUPERSEDE                  0x00000000
#define FILE_OPEN                       0x00000001
#define FILE_CREATE                     0x00000002
#define FILE_OPEN_IF                    0x00000003
#define FILE_OVERWRITE                  0x00000004
#define FILE_OVERWRITE_IF               0x00000005
#define FILE_MAXIMUM_DISPOSITION        0x00000005
#define FILE_DIRECTORY_FILE                     0x00000001
#define FILE_WRITE_THROUGH                      0x00000002
#define FILE_SEQUENTIAL_ONLY                    0x00000004
#define FILE_NO_INTERMEDIATE_BUFFERING          0x00000008

#define FILE_SYNCHRONOUS_IO_ALERT               0x00000010
#define FILE_SYNCHRONOUS_IO_NONALERT            0x00000020
#define FILE_NON_DIRECTORY_FILE                 0x00000040
#define FILE_CREATE_TREE_CONNECTION             0x00000080

#define FILE_COMPLETE_IF_OPLOCKED               0x00000100
#define FILE_NO_EA_KNOWLEDGE                    0x00000200
#define FILE_OPEN_REMOTE_INSTANCE               0x00000400
#define FILE_RANDOM_ACCESS                      0x00000800

#define FILE_DELETE_ON_CLOSE                    0x00001000
#define FILE_OPEN_BY_FILE_ID                    0x00002000
#define FILE_OPEN_FOR_BACKUP_INTENT             0x00004000
#define FILE_NO_COMPRESSION                     0x00008000
#endif // RLIB_DLL
//////////////////////////////////////////////////////////////////////////
namespace System
{
	namespace IO
	{
		/// <summary>
		/// 表示文件异常
		/// </summary>
		typedef Exception FileException;
		/// <summary>
		/// 表示文件路径
		/// </summary>
		typedef struct export Path
		{
			/// <summary>
			///  用指定 URI 初始化 Path 类的新实例
			/// </summary>
			Path(String);
			/// <summary>
			///  获取 Path 类的所表示路径的绝对形式
			/// </summary>
			String FullPath;
			/// <summary>
			/// 获取实例的Dos路径
			/// </summary>
			String GetDosPath();
			RLIB_ClassNewDel;
		}*LPPATH;
		/// <summary>
		/// 表示文件流的抽象类
		/// </summary>
		class export FileStream:public Stream
		{
		private:
			// Native API参数
			OBJECT_ATTRIBUTES_STRUCT *m_obj;
			// Native API参数
			IO_STATUS_BLOCK_STRUCT *m_isb;
		private:
			FileException m_error;
			// 指示文件内容是否已改变
			bool m_updated;
			// 设置异常
			void setException(NTSTATUS STATUS);
		protected:
			friend class File;
			// 是否共享写
			bool m_share_write;
		public:
			/// <summary>
			/// 使用指定的文件句柄初始化新的FileStream类, 自动接管该句柄的所有工作
			/// 请勿指定参数AutoUpdate
			/// </summary>
			FileStream(HANDLE, bool AutoUpdate = true);
			/// <summary>
			/// 关闭文件并释放文件流内存
			/// </summary>
			~FileStream();
			/// <summary>
			/// 获取文件流关联的文件句柄
			/// </summary>
			operator HANDLE();
			RLIB_ClassNewDel;
		public:
			/// <summary>
			/// @warning 方法不被支持
			/// </summary>
			inline void SetObjectData(LPVOID/* pData*/){};
			/// <summary>
			/// 方法读取全部文件内容到静态缓冲区并返回只读指针
			/// @warning 对文件进行后续操作该静态缓冲区可能失效
			/// </summary>
			LPVOID GetObjectData() const;
			/// <summary>
			/// 获取文件流关联的文件句柄
			/// </summary>
			__declspec(property(get = GetObjectData)) LPVOID ObjectData;
			/// <summary>
			/// @warning 方法不被支持
			/// </summary>
			inline void SetSize(LONG/* size*/){};
			/// <summary>
			/// @warning 方法不被支持
			/// </summary>
			inline LONG GetSize() const{return 0;};
			/// <summary>
			/// @warning 属性不被支持
			/// </summary>
			__declspec(property(get = GetSize)) LONG Size;

			/// <summary>
			/// 更新内部状态, 类外部对HANDLE操作之后调用该方法以更新状态信息
			/// </summary>
			void Update(POBJECT_ATTRIBUTES_STRUCT, PIO_STATUS_BLOCK_STRUCT);
			/// <summary>
			/// 获取文件长度
			/// </summary>
			LONG GetLength() const;
			/// <summary>
			/// 设置文件长度, 方法允许您截断或增加文件大小
			/// </summary>
			void SetLength(LONG);
			// 获取或设置文件长度
			__declspec(property(get = GetLength, put = SetLength)) LONG Length;
			/// <summary>
			/// 返回文件分配的物理大小
			/// </summary>
			LONG GetAllocationSize() const;      
			/// <summary>
			/// 删除类关联的文件
			/// </summary>
			bool Delete(); 	                         
			/// <summary>
			/// 移动指定文件并指定是否替换(若已存在)
			/// </summary>
			bool Move(String path, bool ReplaceIfExists = true); 
			/// <summary>
			/// 追加到文件中,方法调用 Write()
			/// </summary>
			void Append(void *buffer, int count);        
			/// <summary>
			/// 读取文件
			/// </summary>
			int Read(void *buffer, int count) const;
			/// <summary>
			/// 写到文件
			/// </summary>
			void Write(const void *buffer, int count);
			/// <summary>
			/// 强制写入文件缓冲区内容到硬盘
			/// </summary>
			bool Flush();
			/// <summary>
			/// 设置文件偏移指针
			/// </summary>
			void SetPos(LONG Pos);  
			// 获取或设置文件偏移指针
			__declspec(property(get = GetPos, put = SetPos)) LONG Position;
			/// <summary>
			/// 方法仅关闭文件并不释放相关资源(析构负责)
			/// </summary>
			void Close();
			/// <summary>
			/// 获取File发生的异常信息
			/// </summary>
			FileException *GetLastException(); 
		};
		/// <summary>
		/// 提供用于创建、复制、删除、移动和打开文件的静态方法,并协助创建 FileStream 对象
		/// </summary>
		class export RLIB_THREAD_SAFE File
		{
		public:
		/// <summary>
		/// 定义用于控制对文件的读访问、写访问或读/写访问的常数
		/// </summary>
		typedef enum FileAccess
		{
			//对文件的读访问
			ReadAccess = GENERIC_READ, 
			//文件的写访问
			WriteAccess = GENERIC_WRITE, 
			//对文件的读访问和写访问
			ReadWriteAccess = GENERIC_READ|GENERIC_WRITE, 
			//所有权限
			AllAccess = GENERIC_ALL,
			//所有权限
			NT_ALL = FILE_ALL_ACCESS,
			//Read data from the file
			NT_READ = FILE_READ_DATA,
			//Write data to the file
			NT_WRITE = FILE_WRITE_DATA,
			//Delete file
			NT_DELETE = DELETE,
			//Append data to the file
			NT_APPEND = FILE_APPEND_DATA,
			//Read the attributes of the file
			NT_READ_ATTRIBUTES = FILE_READ_ATTRIBUTES,	
			//Read the extended attributes (EAs) of the file. This flag is irrelevant for device and intermediate drivers
			NT_READ_EA = FILE_READ_EA,	
			//Write the attributes of the file
			NT_WRITE_ATTRIBUTES = FILE_WRITE_ATTRIBUTES	,
			//Change the extended attributes (EAs) of the file. This flag is irrelevant for device and intermediate drivers
			NT_WRITE_EA = FILE_WRITE_EA,
			//Use system paging I/O to read data from the file into memory. This flag is irrelevant for device and intermediate drivers
			NT_EXECUTE = FILE_EXECUTE,
			//List the files in the directory
			NT_LIST_DIRECTORY = FILE_LIST_DIRECTORY,
			//Traverse the directory, in other words, include the directory in the path of a file
			NT_TRAVERSE = FILE_TRAVERSE
		};
		/// <summary>
		/// 指定操作系统打开文件的方式
		/// </summary>
		typedef enum FileMode
		{
			//指定操作系统应创建文件, 如果文件存在则失败
			CreateMode = FILE_CREATE, 
			//指定操作系统应创建新文件,如果文件已存在则覆盖
			CreateNewMode = FILE_SUPERSEDE, 
			//指定操作系统应打开现有文件 
			OpenMode = FILE_OPEN, 
			//指定操作系统应打开文件(如果文件存在),否则应创建新文件
			OpenOrCreateMode = FILE_OPEN_IF, 
			//指定操作系统应打开现有文件.文件一旦打开,就将被截断为零字节大小
			TruncateMode = FILE_OVERWRITE, 	
			//若存在文件，则打开该文件并查找到文件尾，或者创建一个新文件
			AppendMode = FILE_OVERWRITE_IF 
		};
		/// <summary>
		/// 提供文件和目录的特性
		/// </summary>
		typedef enum FileAttributes
		{
			//文件正常,没有设置其他的特性,此特性仅在单独使用时有效
			NormalAttribute = FILE_ATTRIBUTE_NORMAL, 
			//该文件或目录是加密的
			EncryptedAttribute = FILE_ATTRIBUTE_ENCRYPTED, 
			//文件是隐藏的
			HiddenAttribute = FILE_ATTRIBUTE_HIDDEN, 
			//文件的存档状态,应用程序使用此特性为文件加上备份或移除标记
			ArchiveAttribute = FILE_ATTRIBUTE_ARCHIVE, 
			//文件已脱机,文件数据不能立即供使用
			OfflineAttribute = FILE_ATTRIBUTE_OFFLINE, 
			//文件为只读
			ReadOnlyAttribute = FILE_ATTRIBUTE_READONLY, 
			//文件为系统文件
			SystemAttribute = FILE_ATTRIBUTE_SYSTEM, 
			//文件是临时文件,文件系统尝试将所有数据保留在内存中以便更快地访问,
			//而不是将数据刷新回大容量存储器中.不再需要临时文件时,应用程序会立即将其删除
			TemporaryAttribute = FILE_ATTRIBUTE_TEMPORARY 
		};
		/// <summary>
		/// 包含用于控制其他 FileStream 对象对同一文件可以具有的访问类型的常数
		/// </summary>
		typedef enum FileShare 
		{
			//谢绝共享当前文件
			NoneShare = NULL, 
			//允许随后打开文件读取
			ReadShare = FILE_SHARE_READ,	
			//允许随后打开文件写入	
			WriteShare = FILE_SHARE_WRITE, 
			//允许随后删除文件
			DeleteShare = FILE_SHARE_DELETE 
		};
		/// <summary>
		/// 表示用于创建 FileStream 对象的附加选项
		/// </summary>
		typedef enum FileOptions
		{
			//指示无其他参数
			NoneOption = NULL, 
			//指示系统应通过任何中间缓存、直接写入磁盘
			WriteThroughOption = FILE_WRITE_THROUGH, 
			//指示随机访问文件, 系统可将此选项用作优化文件缓存的提示
			RandomAccessOption = FILE_RANDOM_ACCESS,
			//指示按从头到尾的顺序访问文件,系统可将此选项用作优化文件缓存的提示
			//如果应用程序移动用于随机访问的文件指针,可能不发生优化缓存,但仍然保证操作的正确性
			//指定此标志可以提高使用顺序访问读取大文件的应用程序的性能
			//对于大多数情况下都按顺序读取大文件、但偶尔跳过小的字节范围的应用程序而言，性能提升可能更明显
			SequentialScanOption = FILE_SEQUENTIAL_ONLY, 
			//指示当不再使用某个文件时,自动删除该文件
			DeleteOnCloseOption = FILE_DELETE_ON_CLOSE 
		};
		public:
			/// <summary>
			/// 以指定的模式和访问权限打开指定路径上的文件并返回 FileStream
			/// </summary>
			/// <returns>失败返回NULL</returns>
			static FileStream *Open(String path, FileMode mode = OpenMode, FileAccess access = ReadWriteAccess, 
				FileShare share = NoneShare, FileOptions options = NoneOption);
			/// <summary>
			/// 创建指定文件,打开并返回 FileStream
			/// </summary>
			static FileStream *Create(String path, FileMode mode = CreateMode, FileAccess access = ReadWriteAccess,
				FileAttributes attr = NormalAttribute, FileShare share = NoneShare, 
				FileOptions options = NoneOption);
			/// <summary>
			/// 获取 Open() 和 Create() 方法发生的错误说明
			/// </summary>
			/// <returns>失败返回false</returns>
			static bool GetLastError(OUT LPVOID buffer, size_t size, IN DWORD err = 0);  
		public:
			/// <summary>
			/// 创建指定目录
			/// </summary>
			static bool NewDirectory(String path);
			/// <summary>
			/// 删除指定文件
			/// </summary>
			static bool Delete(String path);
			/// <summary>
			/// 尝试删除指定文件
			/// </summary>
			static bool TryDelete(String path);
			/// <summary>
			/// 判断指定文件是否存在
			/// </summary>
			static bool Exist(String path);
			/// <summary>
			/// 读取指定文件内容
			/// </summary>
			static String ReadText(String path);
			/// <summary>
			/// 读取指定文件内容到缓冲流
			/// </summary>
			static FileStream *ReadStream(String path);
			/// <summary>
			/// 根据指定编码写入指定文件内容(允许指定长度), 如果文件已存在则续写
			/// </summary>
			static bool WriteText(String path, String text, int length = 0, Text::Encoding codepage = Text::UnknownEncoding);
			/// <summary>
			/// 根据指定编码写入指定流内容, 并指定是否启用BOM
			/// @warning stream采用的编码方式必须是UTF16(_UNICODE编译选项) or GB2312
			/// </summary>
			static bool WriteStream(String path, IN const IO::Stream *pstream, int length = 0);
			/// <summary>
			/// 复制指定文件, 并指定如果目标路径存在是否覆盖
			/// </summary>
			static bool Copy(String path, String new_path, bool ReplaceIfExists = true);
		};
	};
};
#endif