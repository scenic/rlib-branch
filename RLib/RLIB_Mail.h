/********************************************************************
	Created:	2012/03/18  10:08
	Filename: 	RLIB_Mail.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_MAIL
#define _USE_MAIL
#include "RLib_HashMap.h"
/************************************************************************/
namespace System
{
	namespace Net
	{
		namespace Mime
		{
			class export MediaText
			{
			public:
				static LPCTSTR HTML;
				static LPCTSTR TEXT;
				static LPCTSTR XML;
				static LPCTSTR RICHTEXT;
			};
			class export MediaImage
			{
			public:
				static LPCTSTR JPEG;
				static LPCTSTR GIF;
				static LPCTSTR TIFF;
			};
			class export MediaApplication
			{
			public:
				static LPCTSTR RTF;
				static LPCTSTR Octet;
				static LPCTSTR PDF;
				static LPCTSTR SOAP;
				static LPCTSTR ZIP;
			};
		};
		/// <summary>
		/// 包含用于将电子邮件发送到简单邮件传输协议 (SMTP) 服务器进行传送的类
		/// </summary>
		namespace Mail
		{
			/// <summary>
			/// 表示Mail异常
			/// </summary>
			typedef Exception MailException;

			class export Attachment
			{
			public:
				RLIB_ClassNewDel;
			public:
				bool operator ==(const Attachment& other)
				{
					return (Path == other.Path) && (Name == other.Name);
				}
				String Path;
				String Name;
				String Encoding;
				String ContentType;
				String ContentDescription;
			};

			class export MailAddress
			{
			public:
				RLIB_ClassNewDel;
			public:
				bool operator ==(const MailAddress& other)
				{

					return (Address == other.Address &&
						DisplayName == other.DisplayName &&
						Host == other.Host &&
						User == other.User);
				}
				const MailAddress& operator=(const MailAddress& other)
				{
					Address = other.Address;
					DisplayName = other.DisplayName;
					Host = other.Host;
					User = other.User;
					return *this;
				}
				String Address;
				String DisplayName;
				String Host;
				String User;
			};

			class export MailMessage
			{
			public:
				MailMessage()
				{
					this->To = new Collections::Generic::Array<MailAddress>;
					this->Attachments = new Collections::Generic::Array<Attachment>;
					Mime_Version = T("1.0");
					Content_Type = T("multipart/alternative");
					IsHTML = true;
					Boundary = T("==SCUT==");
					BodyEncoding = T("8bit");
				}
				MailMessage(const MailAddress& From, const Collections::Generic::Array<MailAddress>& To)
				{
					this->To = new Collections::Generic::Array<MailAddress>;
					this->Attachments = new Collections::Generic::Array<Attachment>;
					this->From = From.Address;
					for (int i = 0; i < To.Length; i++)
					{
						this->To->Add(To.GetValue(i));
					}
					Mime_Version = T("1.0");
					Content_Type = T("multipart/alternative");
					IsHTML = true;
					Boundary = T("==SCUT==");
					BodyEncoding = T("8bit");
					Sender = From.Address;
				}
				~MailMessage()
				{
					delete this->To;
					delete this->Attachments;
				}
				RLIB_ClassNewDel;
			public:
				MailAddress Address;
				String Date;
				String From;
				String Sender;
				Collections::Generic::Array<MailAddress> *To;
				/// <summary>
				/// 抄送地址
				/// </summary>
				String Cc;
				/// <summary>
				/// 暗抄地址
				/// </summary>
				String Bcc;
				String Message_ID;
				String Mime_Version;
				String Content_Type;
				String Subject;
				String Body;
				bool IsHTML;
				String BodyEncoding ;
				Collections::Generic::Array<Attachment> *Attachments;
				String Boundary;
			};
			/// <summary>
			/// 允许应用程序使用简单邮件传输协议 (SMTP) 来发送电子邮件
			/// </summary>
			class export SmtpClient
			{
			public:
				SmtpClient();
				~SmtpClient();
				RLIB_ClassNewDel;
			public:
				bool Login(String strAccout, String strPassword);
				bool Connect(const String& strSMTPServer, int nPort);
				bool Send(const MailMessage& mailmsg);
				bool Logout();
				/// <summary>
				/// 获取SmtpClient发生的异常信息
				/// </summary>
				MailException *GetLastException() { return &this->m_error; } ; 
			protected:
				int   GetResponseCode();
				bool  SendHeader(const MailMessage& mailmsg);
				bool  SendBody(const MailMessage& mailmsg);
				bool  SendAttachments(const MailMessage& mailmsg);
				bool  SendFile(const String& strPath);
				bool  ReceiveResponse();
				template<typename R> R SetException(R ret);
			private:
				char         recv_buf[256];
				String       strTmp;
				Sockets      *m_socket;
				MailException m_error;
			public:
				int    ResponseCode;
				String ResponseString;
			};
		}
	}
}
#endif // _USE_MAIL