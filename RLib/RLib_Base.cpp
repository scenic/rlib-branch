/********************************************************************
Created:	2011/08/15  10:05
Filename: 	Lib_Base.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_Base.h"
#include "native/RLib_Native.h"
using namespace System;
/// <summary>
/// ��̬ ϵͳ��Ϣ
/// </summary>
static AppBase::SystemInformation Sys_Info = {  0  };
/************************************************************************/
/* AppBase                                                                
/************************************************************************/
AppBase::SystemInformation *AppBase::GetSystemInfo()
{
    if (Sys_Info.PhysicalPageSize == 0)
    {
        NtQuerySystemInformation(SystemBasicInformation, &Sys_Info, sizeof(Sys_Info), nullptr);
    }
    return  &Sys_Info;
}

//-------------------------------------------------------------------------

void AppBase::Exit(DWORD code /* = 0 */)
{
    ExitProcess(code);
}

//-------------------------------------------------------------------------

void AppBase::ExitThread(DWORD code /* = 0 */)
{
    RtlFreeThreadActivationContextStack();
    RtlExitUserThread(code);
}

//-------------------------------------------------------------------------

LPCWSTR AppBase::GetCurrentPath()
{
    return ((PEB *)GetPEBAddress())->ProcessParameters->CurrentDirectory.DosPath.Buffer;
}

//-------------------------------------------------------------------------

static WCHAR StartupPath[260] = {0};

LPCWSTR AppBase::GetStartupPath()
{
	auto image   = ((PEB *)GetPEBAddress())->ProcessParameters->ImagePathName.Buffer;
	auto pfinder = wcsstr(image, L"\\");
	while(pfinder != nullptr)
	{
		auto pfinder_next = wcsstr(pfinder + 1, L"\\");
		if (pfinder_next == nullptr)
		{
			break;
		} //if
		pfinder = pfinder_next;
	}
	assert(pfinder != nullptr);
	int len_copy = pfinder + 1 - image;
	memcpy(StartupPath, image, len_copy * sizeof(wchar_t));
	StartupPath[len_copy] = L'\0';
	return StartupPath;
}

//-------------------------------------------------------------------------

LPVOID AppBase::GetImageBaseAddress()
{
	return ((PEB *)GetPEBAddress())->ImageBaseAddress;
}

//-------------------------------------------------------------------------

#ifdef _M_X64
extern "C" PPEB WINAPI GetPEB64();
void *AppBase::GetPEBAddress()
{
	return GetPEB64();
}

//-------------------------------------------------------------------------

#else 
void *AppBase::GetPEBAddress()
{
	PEB *pPEB;
	__asm 
	{
		mov eax, fs: [30h] 
		mov pPEB, eax
	}
	return pPEB;
}
#endif 

/************************************************************************/
/* Exception                                                                
/************************************************************************/
Exception::Exception()
{
    this->HResult = STATUS_SUCCESS;
    memset(this->Message, 0, sizeof(this->Message));
    #ifdef _DEBUG
        this->Source = NULL;
        this->TargetSite = NULL;
    #endif // _DEBUG
}

//-------------------------------------------------------------------------
Exception::Exception(const Exception &Src)
{
#ifdef _DEBUG
	this->TargetSite = NULL;
#endif // _DEBUG
	this->Ref(Src);
}

//-------------------------------------------------------------------------

Exception::Exception(const TCHAR *Msg)
{
    this->HResult =  - 1;
    memcpy(this->Message, Msg, _tcslen(Msg) *sizeof(TCHAR));
    #ifdef _DEBUG
        this->Source = NULL;
        this->TargetSite = NULL;
    #endif // _DEBUG
}

//-------------------------------------------------------------------------

Exception::Exception(int Id, const TCHAR *Msg)
{
    this->HResult = Id;
    memcpy(this->Message, Msg, _tcslen(Msg) *sizeof(TCHAR));
    #ifdef _DEBUG
        this->Source     = NULL;
		this->TargetSite = NULL;
    #endif // _DEBUG
}

//-------------------------------------------------------------------------
Exception::~Exception()
{
#ifdef _DEBUG
	if (this->TargetSite != nullptr)
	{
		RLIB_GlobalCollect(this->TargetSite);
	} //if
#endif // _DEBUG
}

//-------------------------------------------------------------------------

#ifdef _DEBUG
    void Exception::Set(int HResult, const TCHAR *Message, const TCHAR *Source, const CHAR *pTargetSite)
#else 
    void Exception::Set(int HResult, const TCHAR *Message)
#endif // _DEBUG
{
    this->HResult = HResult;
    memcpy(this->Message, Message, _tcslen(Message) *sizeof(TCHAR));
    #ifdef _DEBUG
        this->Source = Source;
		if (this->TargetSite != nullptr)
		{
			RLIB_GlobalCollect(this->TargetSite);
		} //if
		this->TargetSite = (char *)RLIB_GlobalAlloc(4 * 4 * 4 * 4);
		DLLExport::_unDNameEx(this->TargetSite, pTargetSite, (4 * 4 * 4 * 4) - 1);
    #endif // _DEBUG
}

//-------------------------------------------------------------------------
#ifdef _DEBUG
void Exception::SetDebugInfo(const TCHAR *Source, const CHAR *pTargetSite)
{
	this->Source = Source;
	if (this->TargetSite != nullptr)
	{
		RLIB_GlobalCollect(this->TargetSite);
	} //if
	this->TargetSite = (char *)RLIB_GlobalAlloc(4 * 4 * 4 * 4);
	DLLExport::_unDNameEx(this->TargetSite, pTargetSite, (4 * 4 * 4 * 4) - 1);
}
#endif // _DEBUG

//-------------------------------------------------------------------------

void Exception::Ref(const Exception &Src)
{
    this->HResult = Src.HResult;
    memcpy(this->Message, Src.Message, _tcslen(Src.Message) *sizeof(TCHAR));
    #ifdef _DEBUG
        this->Source = Src.Source;
		if (Src.TargetSite != nullptr)
		{
			if (this->TargetSite != nullptr)
			{
				RLIB_GlobalCollect(this->TargetSite);
			} //if
			int len = strlen(Src.TargetSite);
			this->TargetSite = (char *)RLIB_GlobalAlloc((len +1) * sizeof(char));
			memcpy(this->TargetSite, Src.TargetSite, len * sizeof(char));
		} //if
    #endif // _DEBUG
}

//-------------------------------------------------------------------------

bool Exception::GetLastError(LPVOID buffer, size_t size, DWORD err /* = 0 */)
{
    memset(buffer, 0, size);
    if (err == 0)
    {
        err = RtlGetLastWin32Error();
    }
    /// <summary>
    /// If the function fails, the return value is zero
    /// </summary>
    //#pragma warning(:C2248)
    if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, err, NULL, (LPTSTR)buffer, size - sizeof(TCHAR), nullptr) == 0)
    {
        return false;
    }
    return true;
}

//-------------------------------------------------------------------------

bool Exception::GetException(OUT Exception *Ex, LONG HResult /* = 0 */)
{
    if (HResult == 0)
    {
        HResult = RtlGetLastWin32Error();
    }
	Ex->HResult = HResult;
    return Exception::GetLastError(Ex->Message, sizeof(Ex->Message) / sizeof(TCHAR), HResult);
}

//-------------------------------------------------------------------------

Exception *Exception::GetLastException()
{
    Exception *ex = new Exception();

    Exception::GetException(ex);

    return ex;
}

//-------------------------------------------------------------------------

void Exception::SetLastError(DWORD err)
{
    RtlSetLastWin32Error(err);
}
