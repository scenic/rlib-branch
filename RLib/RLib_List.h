/********************************************************************
	Created:	2012/02/06  19:40
	Filename: 	Lib_Array.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_COLLECTION
#define _USE_COLLECTION
#include "RLib_Array.h"
//#define RLIB_LIST_SYNC   
#ifdef RLIB_LIST_SYNC
#define LockList   this->SyncRoot.Enter()
#define UnLockList this->SyncRoot.Leave();
#else
#define LockList
#define UnLockList
#endif // RLIB_ARRAY_SYNC
//////////////////////////////////////////////////////////////////////////
namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			/// <summary>
			/// 表示双向链接(LinkedList)列表
			/// 提供用于对列表进行搜索、排序和操作的方法
			/// </summary>
			template <class R, Allocator *allocator = nullptr, typename 
				Disposable<R>::IDisposable disposer = Disposable<R>::DefaultDispose>
			class List
			{
			private:
				inline Allocator *_get_allocator()
				{
					return(allocator == nullptr ? (&PublicDefaultAllocator) : allocator);
				}
				/// <summary>
				/// 链表节点结构
				/// </summary>
				typedef struct ListNode
				{
				public:
					struct ListNode *pNext; //直接后继
					struct ListNode *pPrev; //直接前趋
					inline R &GetNode()
					{
						return (R &)this->obj;
					}
					__declspec(property(get = GetNode)) R &Node;
#ifdef _DEBUG
					List<R, allocator, disposer> *pDebugOwner;
#endif // _DEBUG
				private:
					byte obj[sizeof(R)];
				} ListNode;
			public:
				typedef ListNode *ListNodePointer;
			private:
				/// <summary>
				/// 链表首节点
				/// </summary>
				ListNodePointer pFirstNode;
				/// <summary>
				/// 链表尾节点
				/// </summary>
				ListNodePointer pLastNode;
				/// <summary>
				/// 申请节点
				/// </summary>
				inline ListNodePointer newListNode(const R &item)
				{
					ListNodePointer p = (ListNodePointer)this->_get_allocator()->Alloc(sizeof(ListNode));
					if (p != nullptr)
					{
						RLIB_InitClass(&p->Node, R(item));
#ifdef _DEBUG
						p->pDebugOwner = this;
#endif // _DEBUG
					}
					return p;
				}
				/// <summary>
				/// 释放节点
				/// </summary>
				inline void delListNode(ListNodePointer pNode)
				{
					disposer(&pNode->Node);
					this->_get_allocator()->Free(pNode);
				}
				/// <summary>
				/// 建立节点关系
				/// </summary>
				inline void link(ListNodePointer pNode, ListNodePointer pNextNode)
				{
					pNode->pNext     = pNextNode;
					pNextNode->pPrev = pNode;
				}
				/// <summary>
				/// 交换节点
				/// </summary>
				inline void swap(ListNodePointer pNode, ListNodePointer pNextNode)
				{
					if (pNode->pNext == pNextNode)//相邻元素的交换处理
					{
						assert(pNextNode->pPrev == pNode); 
						//交换前 A→←B(pNode)→←C(pNextNode)→←D
						//交换后 A→←C(pNextNode)→←B(pNode)→←D

						if(pNode == (ListNode *)0x004bda0c)
						{
							pNode = pLastNode;
						}
						if(pNode->pPrev != nullptr)//考虑与开始交换
						pNode->pPrev->pNext = pNextNode;
						pNextNode->pPrev    = pNode->pPrev;
						pNode->pPrev        = pNextNode;

						pNode->pNext        = pNextNode->pNext;
						if(pNode->pNext != nullptr)//考虑与结尾交换
					    pNode->pNext->pPrev = pNode;
						pNextNode->pNext    = pNode;
						return;
					}

					ListNodePointer pTempListNode = pNode->pPrev;
					pNode->pPrev                  = pNextNode->pPrev;
					pNextNode->pPrev              = pTempListNode;
					//pNode->pPrev = ListNodePointer(((int)pNode->pPrev) ^ ((int)pNextNode->pPrev));
					//pNextNode->pPrev = ListNodePointer(((int)pNode->pPrev) ^ ((int)pNextNode->pPrev));
					//pNode->pPrev = ListNodePointer(((int)pNode->pPrev) ^ ((int)pNextNode->pPrev));
					pTempListNode    = pNode->pNext;
					pNode->pNext     = pNextNode->pNext;
					pNextNode->pNext = pTempListNode;
					//pNode->pNext = ListNodePointer(((int)pNode->pNext) ^ ((int)pNextNode->pNext));
					//pNextNode->pNext = ListNodePointer(((int)pNode->pNext) ^ ((int)pNextNode->pNext));
					//pNode->pNext = ListNodePointer(((int)pNode->pNext) ^ ((int)pNextNode->pNext));

					if (pNode->pPrev != nullptr)pNode->pPrev->pNext = pNode;
					if (pNode->pNext != nullptr)pNode->pNext->pPrev = pNode;

					if (pNextNode->pPrev != nullptr)pNextNode->pPrev->pNext = pNextNode;
					if (pNextNode->pNext != nullptr)pNextNode->pNext->pPrev = pNextNode;
				}
			protected:
				/// <summary>
				/// 获取 List<Of R> 中实际包含的元素数
				/// </summary>
				LONG Count;
			public:
// 				/// <summary>
// 				/// 无后备存储区的 List<Of R> 
// 				/// 获取 List<Of R> 中元素失败时将返回该值的引用
// 				/// </summary>
// 				byte NullObject[sizeof(R)];
#ifdef RLIB_LIST_SYNC
				/// <summary>
				/// 用于同步 List<Of R> 访问的对象
				/// </summary>
				Threading::CriticalSection SyncRoot;
#endif // RLIB_LIST_SYNC
			public:
				RLIB_ClassNewDel;
				/// <summary>
				/// 初始化空表
				/// </summary>
				List()
				{
					this->pFirstNode   = NULL;
					this->pLastNode    = NULL;
					this->Count        = 0;
				}
				/// <summary>
				/// 初始化表
				/// </summary>
				List(const List<R> &list_from)
				{
					this->pFirstNode   = nullptr;
					this->pLastNode    = nullptr;
					this->Count        = 0;
					foreach(pitem, list_from)
					{
						this->Add(*pitem);
					}
				}
				/// <summary>
				/// 对表执行清理工作
				/// </summary>
				~List()
				{
					if (this->Count > 0) this->Clear();
				}
#ifdef RLIB_LIST_SYNC
				/// <summary>
				/// Begin to synchronize access to the List
				/// </summary>
				void Lock() const
				{
					LockList;
				}
				/// <summary>
				/// End to synchronizing access to the List
				/// </summary>
				void UnLock() const
				{
					UnLockList;
				}
				/// <summary>
				/// 安全获取 List<Of R> 中实际包含的元素数
				/// </summary>
				LONG GetSafeLength() const
				{
					LockList;
					auto length = this->Count;
					UnLockList;
					return length;
				}
				/// <summary>
				/// 安全获取 List<Of R> 中实际包含的元素数
				/// </summary>
				__declspec(property(get = GetSafeLength)) LONG SafeLength;
#endif // RLIB_LIST_SYNC
				/// <summary>
				/// 获取 List<Of R> 中实际包含的元素数
				/// </summary>
				LONG GetLength() const
				{
					return this->Count;
				}
				/// <summary>
				/// 获取 List<Of R> 中实际包含的元素数
				/// </summary>
				__declspec(property(get = GetLength)) const LONG Length;
				/// <summary>
				/// 从 List<Of R> 中移除所有元素
				/// </summary>
				void Clear()
				{
					LockList;
					{
						ListNodePointer pNext = this->pFirstNode;
						while(pNext != nullptr)
						{
							this->pFirstNode = pNext;
							pNext = pNext->pNext;
							delListNode(this->pFirstNode);
						}
						this->pFirstNode = NULL;
						this->pLastNode  = NULL;
						this->Count      = 0;	
					}
					UnLockList;
				}
				/// <summary>
				/// 赋值运算符 复制对象
				/// </summary>
				List<R> &operator=(const List<R> &obj)
				{
					this->Clear();
					foreach(pitem, obj)
					{
						this->Add(*pitem);
					}
					return (List<R> &)*this;
				}
				/// <summary>
				/// 拓展运算符 添加对象
				/// </summary>
				List& operator+=(const R &item)
				{
					this->Add(item);
					return (List<R> &)*this;
				}
				/// <summary>
				/// 添加指定元素到 List<Of R> 并返回该元素链节点指针
				/// 如果添加多次元素, 返回首个元素链节点指针
				/// </summary>
				ListNodePointer Add(const R &item)
				{
					if (this->pFirstNode == nullptr)
					{
						return this->AddFirst(item);
					}
					ListNodePointer newNode = newListNode(item);
					if (newNode != nullptr)
					{
						LockList;
						{
							assert(this->pLastNode != nullptr);
							link(this->pLastNode, newNode);
							//新节点成为最后节点
							this->pLastNode = newNode;
							this->Count++;
						}
						UnLockList;
					} //if
					return newNode;
				} 	
				/// <summary>
				/// 将对象添加到 List<Of R> 的开始处
				/// </summary>
				ListNodePointer AddFirst(const R &item)
				{
					ListNodePointer pAddedNode = newListNode(item);
					if (pAddedNode != nullptr)
					{
						LockList;
						{
							if (this->pFirstNode == nullptr)
							{
								assert(this->Count == 0);
								this->pFirstNode = pAddedNode;
								this->pLastNode  = this->pFirstNode;
								goto unlock;
							}
							link(pAddedNode, this->pFirstNode);
							this->pFirstNode = pAddedNode;
unlock:
							this->Count++;
						}
						UnLockList;
					} //if
					return pAddedNode;
				} 	
				/// <summary>
				/// 将对象添加到 List<Of R> 的结尾处
				/// </summary>
				ListNodePointer AddLast(const R &item)
				{
					if (this->pFirstNode == nullptr)
					{
						return this->AddFirst(item);
					}
					ListNodePointer newNode = newListNode(item);
					if (newNode != nullptr)
					{
						LockList;
						{
							assert(this->pLastNode != nullptr);
							link(this->pLastNode, newNode);
							//新节点成为最后节点
							this->pLastNode = newNode;
							this->Count++;
						}
						UnLockList;
					} //if
					return newNode;
				} 	
				/// <summary>
				/// 将指定集合的元素添加到 List<Of R> 的末尾, 
				/// 并返回首元素链节点指针
				/// </summary>
				ListNodePointer AddRange(const R *items, LONG count)
				{
					assert(items != nullptr);
					ListNodePointer pAddedNode = nullptr;
					if (this->pFirstNode == nullptr)
					{
						pAddedNode = this->AddFirst(*items);
						if (pAddedNode == nullptr)
						{
							goto failed;
						} //if
						count--;
						items++;
					}
					LockList;
					while(count >= 1)
					{
						assert(this->pLastNode != nullptr);
						ListNodePointer newNode = newListNode(*items);
						if (newNode == nullptr)
						{
							goto failed;
						} //if
						link(this->pLastNode, newNode);
						//新节点成为最后节点
						this->pLastNode = newNode;
						this->Count++;

						items++;
						count--;
						if (pAddedNode == nullptr)
						{
							pAddedNode = newNode;
						} //if
					}
					UnLockList;
failed:
					return pAddedNode;
				}
				/// <summary>
				/// 将 List<Of R> 的值转换成 Array<Of R>
				/// </summary>
				Array<R> *ToArray(LONG count = 0)
				{
					if (count <= 0 || count > this->Count)
					{
						count = this->Count;
					} //if
					Array<R> *_array = new Array<R>(count);
					LockList;
					ListNodePointer pNode = pFirstNode;
					while(pNode != nullptr && count > 0)
					{
						_array->Add(pNode->Node);
						pNode = pNode->pNext;
						count--;
					}
					UnLockList;
					return _array;
				}
				/// <summary>
				/// 确定某元素是否在 List<Of R> 中
				/// </summary>
				bool Contains(const R &item) const
				{
					return (this->IndexOf(item) != -1);
				}
				/// <summary>
				/// 将元素插入 List<Of R> 的指定元素前面
				/// </summary>
				ListNodePointer Insert(const R &item, const R &itemToInsert, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer)
				{
					if(this->pFirstNode == nullptr)
					{
						return this->AddFirst(itemToInsert);
					}
					LockList;
					ListNodePointer p = this->pFirstNode;
					while(p != nullptr)
					{
						if (comparer(&p->Node, &item) == 0)
						{
							goto insert_non_last;
						}
						p = p->pNext;
					}
					UnLockList;
					return this->AddLast(itemToInsert);
insert_non_last:
					p/*newNode*/ = newListNode(itemToInsert);
					if (p->pPrev!= nullptr)
					{
						link(p->pPrev, p/*newNode*/);
						link(p/*newNode*/, p);
					}
					else
					{
						assert(p == this->pFirstNode);
						this->pFirstNode = p/*newNode*/;
						link(p/*newNode*/, p);
					}
					this->Count++;
					UnLockList;
					return p;
				}
				/// <summary>
				/// 将元素插入 List<Of R> 的指定元素前面
				/// </summary>
				ListNodePointer Insert(ListNodePointer pNode, const R &item)
				{
					if (pNode == nullptr)
					{
						assert(pNode != nullptr || !"List<Of R> 操作异常");
						return this->AddFirst(item);
					}
#ifdef _DEBUG
					else if (pNode->pDebugOwner != this)
					{
						assert(pNode->pDebugOwner == this || !"List<Of R> 操作异常");
						return nullptr;
					}
#endif // _DEBUG
					ListNodePointer pNew = newListNode(item);
					if (pNew != nullptr)
					{
						LockList;	
						link(pNode->pPrev, pNew);
						link(pNew, pNode);
						this->Count++;
						UnLockList;
					} //if
					return pNew; 
				} 
				/// <summary>
				/// 将元素插入 List<Of R> 的从零开始的指定位置顺序前面
				/// </summary>
				ListNodePointer InsertAt(LONG order, const R &item)
				{
					if (order <= 0)
					{
						assert(order == 0 || !"List<Of R> 操作溢出");
						return this->AddFirst(item);
					}
					else if (order >= (this->Count - 1))
					{
						assert(order == (this->Count - 1) || !"List<Of R> 操作溢出");
						return this->AddLast(item);
					}
					ListNodePointer p = this->pFirstNode, pNew = newListNode(item);
					if (pNew != nullptr)
					{
						LockList;
						{
							while(order > 0)
							{
								order--;
								p = p->pNext;
							}
						}
						assert(p != nullptr);
						link(p->pPrev, pNew);
						link(pNew, p);
						this->Count++;
						UnLockList;
					} //if
					return pNew; 
				} 
				/// <summary>
				/// 将集合中的某个元素插入 List<Of R> 的指定指定元素前面
				/// </summary>
				ListNodePointer InsertRange(const R &item, const R *itemsToInsert, LONG count)
				{
					assert(itemsToInsert != nullptr);
					ListNodePointer pAddedNode = nullptr;
					while(count > 0)
					{
						ListNodePointer p = this->Insert(item, *itemsToInsert);
						if (p == nullptr)
						{
							goto failed;
						} //if
						itemsToInsert++;
						count--;
						if (pAddedNode == nullptr)
						{
							pAddedNode = p;
						} //if
					}
failed:
					return pAddedNode;
				}
				/// <summary>
				/// 从 List<Of R> 中移除特定对象的第一个匹配项 
				/// </summary>
				/// <returns>如果成功移除item, 则为true;否则为false.如果没有找到item, 也会返回false</returns>
				bool Remove(const R &item, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer)
				{
					bool result = false;
					LockList;
					{
						ListNodePointer p = this->pFirstNode;
						while(p != nullptr)
						{
							if (comparer(&p->Node, &item) == 0)
							{
								goto found;
							}
							p = p->pNext;
						}
						goto failed;
found:
						if (p->pPrev != nullptr)
						{
							if (p->pNext != nullptr)
							{
								link(p->pPrev, p->pNext);
							}
							else
							{
								assert(p == this->pLastNode);
								p->pPrev->pNext = NULL;
								this->pLastNode = p->pPrev;
							}
						}
						else
						{
							assert(p == this->pFirstNode);
							this->pFirstNode = p->pNext;
							if (p->pNext != nullptr)
							{
								p->pNext->pPrev = NULL;
							}
						}
						delListNode(p);
						this->Count--;
						result = true;
					}
failed:
					UnLockList;
					return result;
				}
				/// <summary>
				/// 从 List<Of R> 中移除特定链节点
				/// </summary>
				bool Remove(ListNodePointer pNode)
				{
					if (pNode == nullptr)
					{
						assert(pNode != nullptr || !"List<Of R> 操作异常");
						return false;
					}
#ifdef _DEBUG
					else if (pNode->pDebugOwner != this)
					{
						assert(pNode->pDebugOwner == this || !"List<Of R> 操作异常");
						return false;
					}
#endif // _DEBUG
					LockList;
					if (pNode->pPrev == nullptr)
					{
						assert(this->pFirstNode == pNode);
						if (pNode->pNext == nullptr)
						{
							assert(this->pLastNode == pNode && this->Count == 1);
							this->pFirstNode = this->pLastNode = nullptr;
							goto do_next;
						}
						this->pFirstNode = pNode->pNext;
						pNode->pNext->pPrev  = nullptr;
						goto do_next;
					}
					else if (pNode->pNext == nullptr)
					{
						assert(this->pLastNode == pNode);//不再可能是首节点
						this->pLastNode = pNode->pPrev;
						pNode->pPrev->pNext = nullptr;
						goto do_next;
					}
					link(pNode->pPrev, pNode->pNext);
do_next:
					delListNode(pNode);
					this->Count--;
					UnLockList;
					return true; 
				} 
				/// <summary>
				/// 从 List<Of R> 中移除指定位置顺序的对象
				/// </summary>
				/// <returns>如果成功移除item, 则为true</returns>
				bool RemoveAt(LONG order)
				{
					if ((order < 0) || (order > (this->Count - 1)) || (this->Count == 0))
					{
						assert(!"List<Of R> 操作溢出");
						return false;
					}
					ListNodePointer p = this->pFirstNode;
					LockList;
					{
						while(order > 0)
						{
							order--;
							p = p->pNext;
						}
					}
					assert(p != nullptr);
					if (p->pPrev == nullptr)
					{
						assert(this->pFirstNode == p);
						if (p->pNext == nullptr)
						{
							assert(this->pLastNode == p && this->Count == 1);
							this->pFirstNode = this->pLastNode = NULL;
							goto do_next;
						}
						this->pFirstNode = p->pNext;
						p->pNext->pPrev  = NULL;
						goto do_next;
					}
					else if (p->pNext == nullptr)
					{
						assert(this->pLastNode == p);//不再可能是首节点
						this->pLastNode = p->pPrev;
						p->pPrev->pNext = NULL;
						goto do_next;
					}
					link(p->pPrev, p->pNext);
do_next:
					delListNode(p);
					this->Count--;
					UnLockList;
					return true; 
				} 
				/// <summary>
				/// 移除与特定对象相匹配的所有元素
				/// </summary>
				/// <returns>从 List<Of R> 中移除的元素的数目</returns>
				LONG RemoveAll(const R &item)
				{
					LONG removeCount = 0;
					while(this->Remove(item)) removeCount++;
					return removeCount;
				}
				/// <summary>
				/// 移除位于 List(Of R) 开头处的节点
				/// </summary>
				bool RemoveFirst()
				{
					return RemoveAt(0);
				} 
				/// <summary>
				/// 移除位于 List(Of R) 结尾处的节点
				/// </summary>
				bool RemoveLast()
				{
					return RemoveAt(this->Count - 1);
				} 
				/// <summary>
				/// 获取 List<Of R> 中指定位置的元素
				/// </summary>
				R &operator [](LONG order) const{ return this->Get(order); } 
				/// <summary>
				/// 获取 List<Of R> 中指定顺序(从0开始)的元素
				/// </summary>
				R &Get(LONG order) const
				{
					if (order <= 0)
					{
						assert(order == 0 || !"List<Of R> 操作溢出");
						return this->pFirstNode != nullptr ? 
							(R &)this->pFirstNode->Node : (R &)(*((R *)nullptr));
					}
					else if (order >= (this->Count - 1))
					{
						assert(order == (this->Count - 1) || !"List<Of R> 操作溢出");
						return this->pLastNode != nullptr ? 
							(R &)this->pLastNode->Node : (R &)(*((R *)nullptr));
					}
					ListNodePointer p;
					LockList;
					//如果order位于下半段, 采用逆寻方式
					if ((this->Count - 1) >= 8)
					{
						LONG mid_offset = (this->Count - 1) % 2 == 0 ? 
							(this->Count - 1) / 2 : ((this->Count - 1) - 1) / 2;
						if (order > mid_offset)
						{
							p = this->pLastNode;
							order = (this->Count - 1) - order;
							{
								while(order > 0)
								{
									order--;
									p = p->pPrev;
								}
							}
							goto do_next;
						}
					} //if
					p = this->pFirstNode;
					{
						while(order > 0)
						{
							order--;
							p = p->pNext;
						}
					}
do_next:
					UnLockList;
					assert(p != nullptr);
					return (R &)p->Node; 
				} 
				bool IsNull(const R &Node) const
				{
					return &Node == nullptr;
				}
				/// <summary>
				/// 获取 List<Of R> 中指定顺序(从0开始)的元素的链节点指针
				/// </summary>
				ListNodePointer GetListNode/*ByIndex*/(LONG order) const
				{
					if (order <= 0)
					{
						assert(order == 0 || !"List<Of R> 操作溢出");
						return this->pFirstNode != nullptr ? this->pFirstNode : nullptr;
					}
					else if (order >= (this->Count - 1))
					{
						assert(order == (this->Count - 1) || !"List<Of R> 操作溢出");
						return this->pLastNode != nullptr ? this->pLastNode : nullptr;
					}
					ListNodePointer p;
					LockList;
					//如果order位于下半段, 采用逆寻方式
					if ((this->Count - 1) >= 8)
					{
						LONG mid_offset = (this->Count - 1) % 2 == 0 ? (this->Count - 1) / 2 : ((this->Count - 1) - 1) / 2;
						if (order > mid_offset)
						{
							p = this->pLastNode;
							order = (this->Count - 1) - order;
							{
								while(order > 0)
								{
									order--;
									p = p->pPrev;
								}
							}
							goto do_next;
						}
					} //if
					p = this->pFirstNode;
					{
						while(order > 0)
						{
							order--;
							p = p->pNext;
						}
					}
do_next:
					UnLockList;
					assert(p != nullptr);
					return p; 
				} 
				/// <summary>
				/// 将指定范围中元素的顺序反转
				/// </summary>
				/// <param name="order">从0开始的顺序位置</param>
				/// <param name="count">交换次数, 默认0为全部</param>
				void Reverse(LONG order = 0, int count = 0)
				{
					if (order < 0){
						order = 0;
					}
					else if (order >= (this->Count - 1)){
						return;
					}
					if (count <= 0 || count > this->Count){
						count = this->Count / 2;
					}
					LockList;
					{
						ListNodePointer pLeft = this->pFirstNode, pRight = this->pLastNode;
						while(order > 0)
						{
							if (pLeft == nullptr)return;
							pLeft = pLeft->pNext;
							order--;
						}
						if(pLeft->pPrev  == nullptr)this->pFirstNode = pRight;
						if(pRight->pNext == nullptr)this->pLastNode  = pLeft;
						while(pLeft != pRight && count > 0)
						{
							swap(pLeft, pRight);
							order  = (LONG)pLeft->pPrev;//使用order保存
							pLeft  = pRight->pNext;
							pRight = (ListNodePointer)order;
							count--;
						}
					}
					UnLockList;
				}
				/// <summary>
				/// 搜索指定的对象，并返回整个 List<Of R> 中第一个匹配项的从零开始的位置顺序
				/// </summary>
				/// <returns>失败返回-1</returns>
				LONG IndexOf(const R &item, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer) const
				{
					LONG result = 0;
					LockList;
					{
						ListNodePointer p = this->pFirstNode;
						while(p != nullptr)
						{
							if(comparer(&p->Node, &item) == 0){ goto found;}
							p = p->pNext;
							result++;
						}
						result = -1;
					}
found:
					UnLockList;
					return result;
				}
				/// <summary>
				/// 搜索指定的对象，并返回整个 List<Of R> 中最后一个匹配项的从零开始的位置顺序
				/// </summary>
				/// <returns>失败返回-1</returns>
				LONG LastIndexOf(const R &item) const
				{
					LONG result = 0;
					LockList;
					{
						ListNodePointer p = this->pLastNode;
						while(p != nullptr)
						{
							if(p->Node == item){ result = (this->Count - 1) - result; goto found;}
							p = p->pPrev;
							result++;
						}
						result = -1;
					}
found:
					UnLockList;
					return result;
				}
				/// <summary>
				/// 删除 List<Of R> 中所有重复的相邻元素
				/// </summary>
				void Unique()
				{	
					// erase each element matching previous
					ListNodePointer _Pprev = this->pFirstNode;
					ListNodePointer _Pnode = _Pprev->pNext;

					while (_Pnode != nullptr)
						if (_Pprev->Node == _Pnode->Node)
						{	
							// match, remove it
							const ListNodePointer _Perase = _Pnode;
							_Pnode = _Pnode->pNext;

							_Pprev->pNext = _Pnode;
							_Pnode->pPrev = _Pprev;

							delListNode(_Perase);
							--this->Count;
						}
						else
						{	// no match, advance
							_Pprev = _Pnode;
							_Pnode = _Pnode->pNext;
						}
				}
				/// <summary>
				/// 对 List<Of R> 进行简单选择排序
				/// </summary>
				void Sort(LONG begin = 0, LONG count = 0, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer)
				{	
					if (begin < 0 || begin >= this->Length || count < 0) return;
					LockList;
					LONG length = this->Count - begin;
					if (count == 0 || count > length) count = length;

					ListNodePointer pBeginNode = this->pFirstNode;
					while(begin > 0)
					{
						assert(pBeginNode != nullptr);
						pBeginNode = pBeginNode->pNext;
						begin--;
					}
					ListNodePointer pNode, pSelectedNode;
					LONG t_count;
next:
					pNode         = pBeginNode;
					pSelectedNode = pBeginNode;
					t_count       = count;
					while(pNode  != nullptr && t_count > 0)
					{
						if(pNode != pSelectedNode && comparer(&pNode->Node, &pSelectedNode->Node) < 0)
						{
							pSelectedNode = pNode;
						}
						pNode = pNode->pNext;
						t_count--;
					}
					if(pSelectedNode != pBeginNode)
					{
						if (pBeginNode == pFirstNode)
						{
							pFirstNode = pSelectedNode;
						} //if
						swap(pBeginNode, pSelectedNode);
						if (pSelectedNode == pLastNode)
						{
							pLastNode = pBeginNode;
						} //if
					}
					pBeginNode = pSelectedNode->pNext;
					count--;
					if(pBeginNode != nullptr && count > 0) goto next;
					UnLockList;
				}
				// Iteration
				//
				// LPVOID ptr = NULL;
				// for (auto p = List.Start(ptr); *ptr != NULL; p = List.Next(ptr))
				// {
				//   if(!List.IsNull(p))
				//   {
				//       ...
				//   }
				// }
				//
				R *Start(LPVOID *ptr OUT) const
				{
					assert(ptr != nullptr || !"迭代器指针不允许为NULL");

					if (this->pFirstNode != nullptr)
					{
						LockList;
						*ptr = this->pFirstNode;
						return &this->pFirstNode->Node;
					} //if
					*ptr = NULL;
					return (R *)nullptr;
				} 
				R *Next(LPVOID *pv IN OUT) const
				{
					assert(pv != nullptr || !"迭代器指针不允许为NULL");
					if (pv == nullptr || *pv == nullptr)
					{
						return nullptr;
					} //if

					auto ptr = (ListNodePointer)*pv;
					if (ptr->pNext == nullptr)
					{
						UnLockList;
						*pv = NULL;
						return (R *)nullptr;
					} //if
					*pv = ptr->pNext;
					return &ptr->pNext->Node;
				}
			};
			/// <summary>
			/// 表示对象的简单后进先出 (LIFO) 非泛型集合
			/// </summary>
			template <class R> class Stack
			{
			public:
				List<R> BaseList;
			public:
				RLIB_ClassNewDel;
				/// <summary>
				/// 返回位于 Stack(Of R) 顶部的对象但不将其移除
				/// </summary>
				R &Peek()
				{
					R &_temp = BaseList.Get(0);
					if (BaseList.IsNull(_temp))
					{
						assert(!"未将对象引用设置到对象的实例");
					}
					return (R &)_temp;
				}
				/// <summary>
				/// 移除并返回位于 Stack(Of R) 顶部的对象
				/// </summary>
				R Pop()
				{
					R &obj_ref = BaseList.Get(0);
					if (BaseList.IsNull(obj_ref))
					{
						assert(!"未将对象引用设置到对象的实例");
					}
					R obj = (R &)obj_ref;
					BaseList.RemoveFirst();
					return obj;
				}
				/// <summary>
				/// 将对象插入 Stack(Of R) 的顶部
				/// </summary>
				bool Push(const R &item)
				{
					return BaseList.AddFirst(item) != nullptr;
				}
				/// <summary>
				/// 获取 Stack(Of R) 中包含的元素数
				/// </summary>
				const LONG GetCount()const{ return BaseList.Length; };
				/// <summary>
				/// 获取 Stack(Of R) 中包含的元素数
				/// </summary>
				__declspec(property(get = GetCount)) const LONG Count;
			};
			/// <summary>
			/// 表示对象的先进先出集合
			/// </summary>
			template <class R> class Queue
			{
			public:
				List<R> BaseList;
			public:
				RLIB_ClassNewDel;
				/// <summary>
				/// 获取 Queue(Of R) 中包含的元素数
				/// </summary>
				const LONG GetCount()const{ return BaseList.Length; };
				/// <summary>
				/// 获取 Queue(Of R) 中包含的元素数
				/// </summary>
				__declspec(property(get = GetCount)) const LONG Count;
				/// <summary>
				/// 返回位于 Queue(Of R) 顶部的对象但不将其移除
				/// </summary>
				R &Peek()
				{
					R &_temp = BaseList.Get(0);
					if (BaseList.IsNull(_temp))
					{
						assert(!"未将对象引用设置到对象的实例");
					}
					return (R &)_temp;
				}
				/// <summary>
				/// 移除并返回位于 Queue(Of R) 开始处的对象
				/// </summary>
				R Dequeue()
				{
					R &obj_ref = BaseList.Get(0);
					if (BaseList.IsNull(obj_ref))
					{
						assert(!"未将对象引用设置到对象的实例");
					}
					R obj = (R &)obj_ref;
					BaseList.RemoveFirst();
					return obj;
				}
				/// <summary>
				/// 将对象添加到 Queue(Of R) 的结尾处
				/// </summary>
				bool Enqueue(R item)
				{
					return BaseList.AddLast(item) != nullptr;
				}

			};
		}
	}
}
//////////////////////////////////////////////////////////////////////////
#define foreachList(p, list, ptr)  LPVOID ptr = NULL;for(auto p = list.Start(&ptr); ptr != NULL; p = list.Next(&ptr))
//////////////////////////////////////////////////////////////////////////
#endif // _USE_COLLECTION
