/********************************************************************
Created:	2011/02/18  9:29
Filename: 	Lib_Http.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_Http.h"
using namespace System::Net;
/// <summary>
/// 获取Header内容
/// </summary>
inline bool getHttpResponseHeaderValue(char *pheaders, char *pname, char *pout, int nsize)
{
	char *pheader;
	while ((pheader = strstr(pheaders, pname)) != nullptr)
	{
		pheader += strlen(pname);

		if (*pheader != ':') continue;

		pheader += 2;
		char *pheaderEnd = strstr(pheader, "\r\n");
		if ((pheaderEnd - pheader + 1) <= nsize)
		{
			System::IO::Memory::memcpy(pout, pheader, (pheaderEnd - pheader));
			return true;
		}
		debug_warning(T("getHttpResponseHeaderValue时发生溢出"));
		break;
	}
	return false;
}

/************************************************************************/
/* HttpRequest                                                                
/************************************************************************/
HttpRequest::HttpRequest(String url)
{
    #pragma region 初始化类成员
    this->m_socket = new Sockets;
    // 检查Sockets
    if (this->m_socket->GetLastException()->HResult != STATUS_SUCCESS)
    {
        this->m_error.Ref(*this->m_socket->GetLastException());
        return ;
    }
    this->m_realuri = nullptr;
    this->m_request = nullptr;
	//this->m_redirect = false;
    this->AutomaticDecompression = DecompressionMethod::None;
    this->MaximumAutomaticRedirections = 3;
    this->AutoRedirect = true;
    this->RequestProxy = nullptr;
    this->DisKeepAlive = true;
    this->KeepAlive = false;
    this->Address = new Uri(url);
    this->Connection = _T("close");
    this->Method = _T("GET");
    this->ProtocolVer = _T("1.1");
    this->Timeout = 8000;
    this->RecvTimeout = 1000;
    #pragma endregion 初始化成员
}

//-------------------------------------------------------------------------

HttpRequest::~HttpRequest()
{
    delete this->m_socket;
    RLIB_Delete(this->m_realuri);
    RLIB_Delete(this->m_request);
    RLIB_Delete(this->RequestProxy);
    delete Address;
    OnClearRecvBuffer();
    //尝试释放
}

//////////////////////////////////////////////////////////////////////////
//功能函数
//////////////////////////////////////////////////////////////////////////
bool HttpRequest::OnConnect()
{
    if (this->RequestProxy != nullptr)
    {
        //使用代理
        if (!this->m_socket->Connect(this->RequestProxy->Host.ToMultiByte(), this->RequestProxy->Host.GetMultiByteSize(), this->RequestProxy->Port))
        {
            goto UNCONNECT;
        }
        goto CONNECTED;
    }
    if (this->m_socket->Connect(this->Address->Host.ToMultiByte(), this->Address->Host.GetMultiByteSize(), this->Address->Port))
    {
        goto CONNECTED;
    }
UNCONNECT: 
	this->m_error.Ref(*this->m_socket->GetLastException());
    //assert(!"连接远程服务器失败");
    return false;
CONNECTED: 
	return true;
}

//-------------------------------------------------------------------------

bool HttpRequest::OnSend()
{
    if (this->m_request == nullptr)
    {
        this->GetRequestStream();
    }
    //设置发送超时
    this->m_socket->SetSndTimeO(Timeout);
    //长度不需要末尾的'\0'
    int send_total = this->m_request->Length;
    if (!this->m_socket->Send((char*)this->m_request->GetObjectData(), &send_total))
    {
        this->m_error.Ref(*this->m_socket->GetLastException());
        assert(!"发送请求异常, 已发送字节数");
        return false;
    }
    return true;
}

//-------------------------------------------------------------------------

void HttpRequest::OnClearRecvBuffer()
{
	if (this->m_recv_buf.ObjectData != nullptr)
	{
		RLIB_GlobalCollect(this->m_recv_buf.ObjectData);
		this->m_recv_buf.ObjectData = nullptr;
	}
	this->m_recv_buf.Size     = 0;
	this->m_recv_buf.Length   = 0;
	this->m_recv_buf.Position = 0;
}

//-------------------------------------------------------------------------

bool HttpRequest::OnRecv()
{
    //尝试释放之前的缓冲区
    OnClearRecvBuffer();
    //接收缓冲区大小
    int unit_size = this->m_socket->GetRcvBuf();
	assert(unit_size > 0);
    //先申请一块内存
    this->m_recv_buf.ObjectData = RLIB_GlobalAlloc(unit_size);
    if (this->m_recv_buf.ObjectData == nullptr)
    {
        RLIB_SetException((&this->m_error),  - 1, T("接收远程服务器响应时发生异常: 申请内存失败"));
        return false;
    }
	this->m_recv_buf.Size = unit_size;

	//设置接收超时
	this->m_socket->SetRcvTimeO(Timeout);

    return this->OnDataRecv(unit_size);
}

//-------------------------------------------------------------------------

bool HttpRequest::OnAutoRedirect()
{
	char Location[16 * 16] = {0};
	if (getHttpResponseHeaderValue(LPSTR(this->m_recv_buf.ObjectData), "Location", Location, sizeof(Location)))
	{
		LPURI OldURI = this->Address; //记录原地址
		this->Address = new Uri(Uri::ProcessUri(String(Location), this->Address)); //设置新地址

		this->m_realuri = this->Address; //记录重定向最终目的地址

#ifdef _DEBUG
		LPURI debug_A = this->Address;
		LPURI debug_B = this->m_realuri;
		UNREFERENCED_PARAMETER(debug_A);
		UNREFERENCED_PARAMETER(debug_B);
#endif // _DEBUG

		if (OldURI->Host != this->Address->Host || this->DisKeepAlive || !this->KeepAlive)
		{
			//关闭上次的连接
			delete this->m_socket;
			this->m_socket = new Sockets; //建立新的套链接字
			//检查Sockets
			if (this->m_socket->GetLastException()->HResult != STATUS_SUCCESS)
			{
				this->m_error.Ref(*this->m_socket->GetLastException()); //设置错误信息
				goto socket_failed;
			}
			if (this->OnConnect())
			{
				goto do_next_work;
			}
socket_failed: 
			this->Address = OldURI;
			return false;
		} //if

do_next_work: 
		this->ResetRequestStream(); //重置请求数据
		if (!this->OnSend())
		{
			goto socket_failed;/*请求重定向地址失败*/
		}

		int OldMaxRedirect = this->MaximumAutomaticRedirections; //记录原始值
		this->MaximumAutomaticRedirections--; //递减计数值
		bool Result = this->OnRecv();
		//恢复数据
		this->MaximumAutomaticRedirections = OldMaxRedirect;
		if (this->Address != this->m_realuri)
		{
			RLIB_Delete(this->Address);
		} //if
		this->Address = OldURI;

		return Result;
	}
	RLIB_SetException((&this->m_error),  - 1, T("处理远程服务器响应时发生异常: 缺少Location标头"));
	return false;
}

//-------------------------------------------------------------------------

bool HttpRequest::OnDataRecv(int unit_size)
{
	//应接收字节数
	long nSizeToRecv = 0;
	//已接收字节数
	long nReceived = 0;
	//最后一次接收
	long nLastReceived;
	//响应头偏移 \r\n\r\n为始
	LPSTR pHead;
	//是否收完响应头 \r\n\r\n之前   
	bool bRetend = false;
	//是否是HEAD请求
	bool isHEAD = this->Method == T("HEAD");
	//缓冲区指针
	LPSTR pRecvBuffer = LPSTR(this->m_recv_buf.ObjectData);
	//循环接收
	while ((nLastReceived = this->m_socket->Recv(pRecvBuffer + nReceived, unit_size)) > 0)
	{
		nReceived += nLastReceived;

		trace(nReceived <= this->m_recv_buf.Size); 

#ifdef _DEBUG
		LPSTR pRecvBufferDebugAt = pRecvBuffer + nReceived;
		pRecvBufferDebugAt = pRecvBufferDebugAt;
#endif // _DEBUG

		if (!bRetend)//判断是否收完头部
		{
			if ((pHead = strstr(pRecvBuffer, "\r\n\r\n")) != nullptr)
			{
				//出现 \r\n\r\n 假定为响应报文结束
				if (isHEAD)
				{
					long size_head = ((pHead - pRecvBuffer) + 4);
					assert(nReceived >= size_head);
					nReceived = size_head;
					goto finished;//对于HEAD, 到此处理可视为完成
				} //if
				bRetend = true;
				//判断是否需要重定向, 如果需要将放弃原来的数据
				if (this->AutoRedirect && this->MaximumAutomaticRedirections > 0)
				{
					//30X重定向状态标志
					if (*(pRecvBuffer + (sizeof("HTTP/1.1 ") - 1)) == '3' && *(pRecvBuffer + (sizeof("HTTP/1.1 ") - 1) + 1) == '0')
					{
						//转到新网址
						return this->OnAutoRedirect();
					}
				}
				//检查字段Content-Length
				char len_str[16] = {0};
				if (getHttpResponseHeaderValue(pRecvBuffer, "Content-Length", len_str, sizeof(len_str)))
				{
					nSizeToRecv = atoi(len_str);
					if (nSizeToRecv <= 0)
					{
						goto Unknown;
					} //if
					if (nSizeToRecv > 0)
					{
						//加上头部大小
						nSizeToRecv += ((pHead - pRecvBuffer) + 4);
						//此处已知响应内容长度
						if (nReceived >= nSizeToRecv)//数据收完
						{
							goto finished;
						}
						goto do_next_recv;//还没收完, 继续
					}
				}
Unknown:
				this->m_socket->SetRcvTimeO(RecvTimeout);//不知道响应内容长度, 为防止卡死设置更短的超时值
				goto do_next_recv;
			}
		}

		if (nSizeToRecv > 0 && nReceived >= nSizeToRecv) //数据收完
		{
			goto finished;
		}
do_next_recv: 
		if ((this->m_recv_buf.Size - nReceived) < unit_size /*缓冲区不足以再次接收*/)
		{
			if (nSizeToRecv > 0)
			{
				int required_size = nSizeToRecv  - nReceived;
				if (required_size < unit_size)
				{
					unit_size = required_size;
				} //if
				//既然知道所需大小, 不妨一次性申请足够内存
				this->m_recv_buf.Size += required_size;
			}
			else
			{
				this->m_recv_buf.Size += unit_size;
			} //if
			LPVOID new_buf_ptr = AppBase::GetUsingPool()->ReAlloc(this->m_recv_buf.ObjectData, this->m_recv_buf.Size);
			if (new_buf_ptr != nullptr)
			{
				this->m_recv_buf.ObjectData = new_buf_ptr;
				pRecvBuffer = LPSTR(new_buf_ptr);
				continue;
			}
			RLIB_SetException((&this->m_error),  - 1, T("接收远程服务器响应时发生异常: 内存不足"));
			return false;
		}
	}
	if (nLastReceived == SOCKET_ERROR) //SOCKET_ERROR
	{
		if (nReceived > 0 && this->m_socket->GetLastException()->HResult == WSAETIMEDOUT /*Connection timed out.*/)
		{
			goto finished;
		}
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
finished: 
	if (m_recv_buf.Size != nReceived)
	{
		assert(m_recv_buf.Size > nReceived);
		if (AppBase::GetUsingPool()->ReSize(this->m_recv_buf.ObjectData, nReceived))
		{
			m_recv_buf.Size = nReceived;
		} //if
	} //if
	m_recv_buf.Length   = nReceived;
	m_recv_buf.Position = 0;
	return true;

}

//-------------------------------------------------------------------------

HttpResponse *HttpRequest::GetResponse()
{
    UnmanagedMemoryStream *pResponseStream = this->GetResponseStream();
    if (pResponseStream != nullptr)
    {
        HttpResponse *Response = this->AutomaticDecompression == DecompressionMethod::None ? 
			HttpResponse::Create(*pResponseStream) : HttpResponse::Create(*pResponseStream, this->AutomaticDecompression);
        if (Response != nullptr)
        {
            Response->ResponseUri    = new Uri();
            *(Response->ResponseUri) = this->m_realuri != nullptr ? *(this->m_realuri) : *(this->Address);

			//pResponseStream 资源已过时, 释放
			AppBase::Collect(pResponseStream->ObjectData);
			pResponseStream->ObjectData = nullptr;

            return Response;
        }
        //创建HttpResponse失败
        RLIB_SetException((&this->m_error),  - 1, T("未能创建 HttpResponse 对象"));
    }
    return nullptr;
}

//-------------------------------------------------------------------------

UnmanagedMemoryStream *HttpRequest::GetResponseStream()
{
    if (this->m_socket->IsConnect && !this->DisKeepAlive && this->KeepAlive)//再次请求
    {
        if (!this->OnSend())
        {
            goto catch_error;
        }
        goto begin_recv;
    }
    if (!this->OnConnect())//首次请求
    {
        goto catch_error;
    }
    if (!this->OnSend())
    {
        goto catch_error;
    }
begin_recv: 
	if (this->m_realuri != nullptr)
	{
		delete this->m_realuri;
		this->m_realuri = nullptr;
	}
	if (this->OnRecv()) //接收
    {
        if (DisKeepAlive || (!DisKeepAlive && !KeepAlive))//无须保持连接
        {
            this->m_socket->Close();
        }
        return &this->m_recv_buf;
    }
catch_error: 
	this->m_socket->Close();
    return nullptr;
}

//-------------------------------------------------------------------------

RequestStream *HttpRequest::GetRequestStream()
{
    if (this->m_request == nullptr)
    {
        this->m_request = new RequestStream(this->m_socket->GetSndBuf());
        //=====================================================================构造请求头
		RLIB_PStreamWriteAS(this->m_request, this->Method);
		if (this->RequestProxy != nullptr)
		{
			RLIB_PStreamWriteA(this->m_request, (" http://"));
			RLIB_PStreamWriteAS(this->m_request, Address->Host);
			if (Address->Port != 80)
			{
				RLIB_PStreamWriteA(this->m_request, (":"));
				RLIB_PStreamWriteAS(this->m_request, Int32(Address->Port).ToString());
			}
		}
		else
		{	
			RLIB_PStreamWriteA(this->m_request, " ");
		}
		RLIB_PStreamWriteAS(this->m_request, Address->PathAndQuery);
		RLIB_PStreamWriteA(this->m_request, " ");
		RLIB_PStreamWriteAS(this->m_request, Address->Scheme);
		RLIB_PStreamWriteA(this->m_request, "/");
		RLIB_PStreamWriteAS(this->m_request, ProtocolVer);
		RLIB_PStreamWriteA(this->m_request, "\r\n");
        //=====================================================================构造HEADER
        if (!this->UserAgent.IsNullOrEmpty())
        {
            RLIB_PStreamWriteA(this->m_request, "User-Agent: ");
            RLIB_PStreamWriteAS(this->m_request, this->UserAgent);
            RLIB_PStreamWriteA(this->m_request, "\r\n");
        }

		RLIB_PStreamWriteA(this->m_request, "Host: ");
		RLIB_PStreamWriteAS(this->m_request, Address->Host);
		if (Address->Port != 80)
		{
			RLIB_PStreamWriteA(this->m_request, ":");
			RLIB_PStreamWriteAS(this->m_request, Int32(Address->Port).ToString());
		}
		RLIB_PStreamWriteA(this->m_request, "\r\n");

        if (!this->Connection.IsNullOrEmpty())
        {
            RLIB_PStreamWriteA(this->m_request, "Connection: ");
            if (this->DisKeepAlive != true)
            {
                RLIB_PStreamWriteA(this->m_request, this->KeepAlive ? "Keep-Alive" : "close"); //保持连接
            }
            else
            {
                RLIB_PStreamWriteAS(this->m_request, this->Connection);
            }
            RLIB_PStreamWriteA(this->m_request, "\r\n");
        }
        if (!this->Accept.IsNullOrEmpty())
        {
            RLIB_PStreamWriteA(this->m_request, "Accept: ");
            RLIB_PStreamWriteAS(this->m_request, this->Accept);
            RLIB_PStreamWriteA(this->m_request, "\r\n");
        }
		if (!this->Referer.IsNullOrEmpty())
		{
			RLIB_PStreamWriteA(this->m_request, "Referer: ");
			RLIB_PStreamWriteAS(this->m_request, this->Referer);
			RLIB_PStreamWriteA(this->m_request, "\r\n");
		}
        if (!this->ContentType.IsNullOrEmpty())
        {
            RLIB_PStreamWriteA(this->m_request, "Content-Type: ");
            RLIB_PStreamWriteAS(this->m_request, this->ContentType);
            RLIB_PStreamWriteA(this->m_request, "\r\n");
        }
        if (this->ContentLength > 0)//POST才有?
        {
            RLIB_PStreamWriteA(this->m_request, "Content-Length: ");
            RLIB_PStreamWriteAS(this->m_request, ToInt32(this->ContentLength).ToString());
            RLIB_PStreamWriteA(this->m_request, "\r\n");
        }
        if (this->Headers.Count > 0)
        {
            this->m_request->Write(this->Headers.ToByteArray(), strlen(this->Headers.ToByteArray()));
        }
        RLIB_PStreamWriteA(this->m_request, "\r\n\r\n");
    }
    return this->m_request;
}

//-------------------------------------------------------------------------

void HttpRequest::ResetRequestStream()
{
    RLIB_Delete(this->m_request);
    this->m_request = nullptr;
}

//-------------------------------------------------------------------------

HttpException *HttpRequest::GetLastException()
{
    return &this->m_error;
}

//-------------------------------------------------------------------------

// bool HttpRequest::DisConnect()
// {
// 	if (this->m_socket != INVALID_SOCKET)
// 	{
// 		if (DisconnectEx(this->m_socket, NULL, 0, 0) == 0)
// 		{	//On success, the DisconnectEx function returns TRUE.
// 			return true;
// 		}
// 	}
// 	return false;
// }

/************************************************************************/
/* HttpResponse                                                                
/************************************************************************/
HttpException *HttpResponse::GetLastException()
{
    return &this->m_error;
}

//-------------------------------------------------------------------------

ResponseStream *ParseChunkData(char* pChunkData, int nChunkDataSize)
{
	if (pChunkData == nullptr || nChunkDataSize < 2)
	{
		return nullptr;
	} //if
	auto pstream = new ResponseStream;
	if (pstream != nullptr)
	{
		//获取Chunked数据大小
		int crlf = 2, block_size = 0;
		LPSTR pBlockDataBegin = pChunkData, pBlockDataEnd = pChunkData, pDataEnd = pChunkData + nChunkDataSize;
		while(pBlockDataBegin < pDataEnd)
		{
			pBlockDataEnd = strstr(pBlockDataBegin, "\r\n");
			if (pBlockDataEnd == nullptr)
			{
				assert(!"非法Chunk数据!");
				goto err_or_done;
			}
			pBlockDataEnd[0] = '\0';
			block_size = Int32::TryParse(pBlockDataBegin, 16);
			pBlockDataEnd[0] = '\r';
			if (block_size == 0)
			{
				break;
			} //if
			pstream->Write(pBlockDataEnd + crlf, block_size);
			pBlockDataBegin = pBlockDataEnd + crlf + block_size;
			//干掉换行符
			crlf = 0;
			while(*pBlockDataBegin == '\r' || *pBlockDataBegin == '\n')
			{
				crlf++;
				pBlockDataBegin += 1;
			}
		}
err_or_done:
		pstream->Position = 0;
		return pstream;
	} //if
	return nullptr;
}

//-------------------------------------------------------------------------

inline void SetDefaultProperty(HttpResponse *_t)
{
	//这里进行默认属性设置
	_t->ProtocolVer       = T("INVALID");
	_t->StatusCode        =  -1;
	_t->StatusDescription = T("ERROR");
	_t->ContentLength     =  -1;
}

//-------------------------------------------------------------------------

void HttpResponse::SaveResponseBody(UnmanagedMemoryStream &ResponseData)
{
	this->m_response = new ResponseStream(ResponseData.MaxReadSize);
	if (this->m_response != nullptr)
	{
		this->m_response->Write(ResponseData.CurrentPtr, ResponseData.MaxReadSize);
		this->ContentLength = ResponseData.MaxReadSize;
	} //if
}

//-------------------------------------------------------------------------

HttpResponse *HttpResponse::Create(UnmanagedMemoryStream &ResponseData)
{
    HttpResponse *_t = new HttpResponse;
	if (_t == nullptr)
	{
		//这里的错误由 HttpRequest.GetResponse() 方法设置
		return nullptr;
	} //if
    
	char *pData = (LPSTR)ResponseData.CurrentPtr;//可能不是'\0'结尾
	if(pData   == nullptr)
	{
		//这里的错误由 HttpRequest.GetResponse() 方法设置
		return nullptr;
	}

	char *pFirstLine = strstr(pData, "HTTP/");
	if (pFirstLine != pData)
	{
		//响应报头开头HTTP/丢失
		//某些服务器可能出现此类情况
		SetDefaultProperty(_t);
		_t->SaveResponseBody(ResponseData);

		RLIB_SetException((&_t->m_error),  -1, T("服务器返回错误的响应数据: 缺失响应报文"));
		return _t;
	}

	pFirstLine      += (sizeof("HTTP/") - 1);
	char *pNextWhite = strstr(pFirstLine, " ");
	if(pNextWhite   == nullptr)
	{
		SetDefaultProperty(_t);
		_t->SaveResponseBody(ResponseData);

		RLIB_SetException((&_t->m_error),  -1, T("服务器返回错误的响应数据: 缺失空格"));
		return _t;
	}

	//HTTP响应版本
	_t->ProtocolVer.Copy(pFirstLine, pNextWhite - pFirstLine);

	pFirstLine = pNextWhite + (sizeof(" ") - 1);
	pNextWhite = strstr(pFirstLine, " ");
	if (pNextWhite == nullptr)
	{
		_t->StatusCode        =  -1;
		_t->StatusDescription = T("ERROR");
		_t->ContentLength     =  -1;
		_t->SaveResponseBody(ResponseData);

		RLIB_SetException((&_t->m_error),  -1, T("服务器返回错误的响应数据: 缺失空格"));
		return _t;
	}

	//HTTP响应状态
	{
		String strCode;
		strCode.Copy(pFirstLine, pNextWhite - pFirstLine);
		_t->StatusCode = Int32::TryParse(strCode);
	}

	pFirstLine = pNextWhite + (sizeof(" ") - 1);
	pNextWhite = strstr(pFirstLine, "\r\n");
	if (pNextWhite == nullptr)
	{
		_t->StatusDescription = T("ERROR");
		_t->ContentLength     =  -1;
		_t->SaveResponseBody(ResponseData);

		RLIB_SetException((&_t->m_error),  -1, T("服务器返回错误的响应数据: 缺失换行符"));
		return _t;
	}

	//解析HTTP响应状态说明
	_t->StatusDescription.Copy(pFirstLine, pNextWhite - pFirstLine);

	pFirstLine = pNextWhite + (sizeof("\r\n") - 1);
	pNextWhite = strstr(pFirstLine, "\r\n\r\n");
	if (pNextWhite == nullptr)
	{
		_t->ContentLength     =  -1;
		_t->SaveResponseBody(ResponseData);

		RLIB_SetException((&_t->m_error),  -1, T("服务器返回错误的响应数据: 缺失响应报头结尾"));
		return _t;
	};

	//解析HTTP响应标头
	char *pFirstTemp = pFirstLine - (sizeof("\r\n") - 1);
	while ((pFirstTemp = strstr(pFirstTemp + (sizeof("\r\n") - 1), "\r\n")) <= pNextWhite)
	{
		_t->Headers.Count++; //设置标头数量
	}
	_t->Headers.WriteByteArray(pFirstLine, pNextWhite - pFirstLine);

	pFirstLine = pNextWhite + (sizeof("\r\n\r\n") - 1);
	assert(pFirstLine <= pData + ResponseData.MaxReadSize);

	int BodySize = ResponseData.MaxReadSize - (pFirstLine - pData);
	assert(BodySize >= 0);
	if (BodySize > 0)
	{
		_t->m_response = new ResponseStream(BodySize);
		if (_t->m_response == nullptr)
		{
			assert(!"创建 ResponseStream 失败!");
			RLIB_SetException((&_t->m_error),  - 1, T("未能成功创建 ResponseStream 对象"));
			goto ERROR_CONTINUE_;
		}
		_t->m_response->Write(pFirstLine, BodySize);
		_t->m_response->Position = 0;
	}
ERROR_CONTINUE_: 
	_t->Server      = _t->Headers.Get("Server");
	_t->ContentType = _t->Headers.Get("Content-Type");
	{
		string strLength  = _t->Headers.Get("Content-Length");
		_t->ContentLength = strLength.IsNullOrEmpty() ? BodySize : Int32::TryParse(strLength);
	}

	if (_t->m_response != nullptr && _t->Headers.Get("Transfer-Encoding").IndexOf(T("chunked")) != -1)
	{	
		auto pUnchunkedData = ParseChunkData((char *)_t->m_response->CurrentPtr, _t->m_response->MaxReadSize);
		if (pUnchunkedData != nullptr)
		{
			assert(pUnchunkedData->Position == 0 && _t->m_response->Position == 0);
			if (pUnchunkedData->Length > 0)
			{
				if (_t->m_response->Length > pUnchunkedData->Length)
				{
					//减少截断大小
					_t->m_response->Length = pUnchunkedData->Length;
				}
				_t->m_response->Write(pUnchunkedData->CurrentPtr, pUnchunkedData->Length);
				_t->m_response->Position = 0;
			} //if
			delete pUnchunkedData;
		} //if
	} //if

	return _t;
}

//-------------------------------------------------------------------------

HttpResponse *HttpResponse::Create(UnmanagedMemoryStream &ResponseData, int AutomaticDecompression)
{
    HttpResponse *_t = HttpResponse::Create(ResponseData);
    if (_t != nullptr)
    {
        if (_t->m_response == nullptr || _t->Headers.Get("Content-Encoding").IndexOf(T("gzip")) == -1)
			goto RET;

        //解压数据
		switch (AutomaticDecompression)
		{
		case DecompressionMethod::GZip: 
			{
				assert(_t->m_response->Position == 0);
				LPSTR pData = (LPSTR)_t->m_response->ObjectData;
				if (pData == nullptr)
				{
					break;
				} //if

				System::IO::GZipStream *pUnGzipStream = nullptr;
				if (pData[0] == 0x1FU && (byte)pData[1] == 0x8BU && pData[2] == 0x08U)
				{
					//gzip文件头
					UnmanagedMemoryStream Data(pData, _t->ContentLength, _t->ContentLength);
					if (Data.Length > 0)
					{
						//解压数据
						pUnGzipStream = System::IO::GZip::UnGzip(&Data, 0, MAX_WBITS + 32);
					} //if
				}
				else
				{
					//获取GZIP数据大小
					LPSTR pSizeStrEnd = strstr(pData, "\r\n");
					if (pSizeStrEnd == nullptr)
					{
						break;
					}
					//取得gzip数据大小
					pSizeStrEnd[0] = '\0';
					int size = Int32::TryParse(pData, 16);
					pSizeStrEnd[0] = '\r';

					UnmanagedMemoryStream Data(pSizeStrEnd + sizeof("\r\n") - 1, size, size);

					if (Data.Length > 0)
					{
						//解压数据
						pUnGzipStream = System::IO::GZip::UnGzip(&Data, 0, MAX_WBITS + 32);
					} //if
				}
				if (pUnGzipStream == nullptr)
				{
					break;
				}
				assert(pUnGzipStream->Position == 0);
				if (pUnGzipStream->ObjectData != nullptr)
				{
					_t->ContentLength        = pUnGzipStream->Length;
					_t->m_response->Write(pUnGzipStream->ObjectData, pUnGzipStream->Length);
					_t->m_response->Length   = pUnGzipStream->Length;
				} //if
				delete pUnGzipStream;
			}
			break;
		case DecompressionMethod::Deflate: 
		default:
			{
				break;
			}
		}
    }
RET: 
	return _t;
}

//-------------------------------------------------------------------------

void HttpResponse::Close()
{
    RLIB_Delete(this->m_response);
    RLIB_Delete(this->ResponseUri);
    delete this;
}

//-------------------------------------------------------------------------

String HttpResponse::GetResponseHeader(LPCSTR headerName /* = nullptr */)
{
	if (headerName == nullptr)
	{
		return String(this->Headers.ToByteArray());
	} //if
    return this->Headers.Get(headerName);
}

//-------------------------------------------------------------------------

ResponseStream *HttpResponse::GetResponseStream()
{
    return this->m_response;
}
