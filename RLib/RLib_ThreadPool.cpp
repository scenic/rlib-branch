/********************************************************************
	Created:	2012/07/26  19:32
	Filename: 	RLib_ThreadPool.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "RLib_ThreadPool.h"
using namespace System::Threading;
#define _safe_change Interlocked::Exchange 
//-------------------------------------------------------------------------
ThreadPool::ThreadPool(long defaultMinThreads/* = 4*/, long defaultMaxThreads/* = 1024*/)
{
	this->m_ptasks             = new TaskQueue;
	this->m_pworkers           = new WorkerList;
	this->m_pavailable_workers = new TempWorkerList;
	this->m_nDestroying        = 0;
	this->m_nMaxIdleTime       = 10000/*INFINITE*/;


	this->SetMaxThreads(defaultMaxThreads);
	this->SetMinThreads(defaultMinThreads);
}

//-------------------------------------------------------------------------

void ThreadPool::Abort()
{
	Interlocked::Increment(&this->m_nDestroying);
}

//-------------------------------------------------------------------------

ThreadPool::~ThreadPool()
{
	this->Abort();

	auto &workers           = this->m_pworkers->GetObj();
	auto &available_workers = this->m_pavailable_workers->GetObj();
	while(this->IsDestroying())
	{
		if(this->m_pworkers->TryLock())
		{
			if (workers.Length == 0)
			{
				break;
			} //if
			this->m_pworkers->UnLock();
		}
		//尝试唤醒工人
		if(this->m_pavailable_workers->TryLock())
		{
			if (available_workers.Length > 0)
			{
				foreachList(pworker, available_workers, ptr)
				{
					if(!(*pworker)->pThread->Alert())
					{
						assert(!"sth happned");
						break;
					}
				}
			} //if
			this->m_pavailable_workers->UnLock();
		}
		Thread::SleepEx(800/*better?*/);
	}

	assert(this->m_pavailable_workers->GetObj().Length == 0);
	delete this->m_pavailable_workers;
	assert(this->m_pworkers->GetObj().Length == 0);
	delete this->m_pworkers;
	delete this->m_ptasks;
}

//-------------------------------------------------------------------------
long ThreadPool::GetAvailableThreads()
{
	AutoLock locker(this->m_pavailable_workers->GetSyncObj());
	return this->m_pavailable_workers->GetObj().Length;
}

//-------------------------------------------------------------------------
long ThreadPool::GetThreads()
{
	AutoLock locker(this->m_pworkers->GetSyncObj());
	return this->m_pworkers->GetObj().Length;
}

//-------------------------------------------------------------------------

long ThreadPool::GetMaxThreads()
{
	return this->m_nMaxThreads;
}

//-------------------------------------------------------------------------

long ThreadPool::GetMinThreads()
{
	return this->m_nMinThreads;
}

//-------------------------------------------------------------------------

long ThreadPool::GetMaxIdleTime()
{
	return this->m_nMaxIdleTime;
}

//-------------------------------------------------------------------------

long ThreadPool::GetTasks()
{
	AutoLock locker(this->m_ptasks->GetSyncObj());
	return this->m_ptasks->GetObj().Count;
}

//-------------------------------------------------------------------------

bool ThreadPool::AddTask(WaitCallback callBack, Object state /* = nullptr */)
{
	bool bResult = this->QueueUserWorkItem(callBack, state);
	if (bResult) this->Dispatch();
	return bResult;
}

//-------------------------------------------------------------------------

bool ThreadPool::QueueUserWorkItem(WaitCallback callBack, Object state/* = nullptr*/)
{
	AutoLock locker(this->m_ptasks->GetSyncObj());
	return this->m_ptasks->GetObj().Enqueue(ThreadPoolTask(callBack, state));
}

//-------------------------------------------------------------------------

bool ThreadPool::SetMaxThreads(long workerThreads)
{
	assert(workerThreads >= 1);
	{
		if (workerThreads < this->m_nMinThreads)
		{
			return false;
		} //if
		_safe_change(&this->m_nMaxThreads, workerThreads);
		//this->m_nMaxThreads = workerThreads;
	}
	auto &workers  = this->m_pworkers->GetObj();
	this->m_pworkers->Lock();

	auto exist_workers  = workers.Length;

	this->m_pworkers->UnLock();


	//这里需要裁掉部分工人(如果数目超过最大值)
	if (exist_workers > workerThreads/*this->m_nMaxThreads*/)
	{
		//需要干掉的线程数
		exist_workers -= workerThreads/*this->m_nMaxThreads*/;
		//裁掉多余就绪工人 可能不能全部裁掉 exist_workers 数目
		//剩下的依赖工作线程自我销毁
		{
			AutoLock locker(this->m_pavailable_workers->GetSyncObj());
			foreachList(pavailable_worker, this->m_pavailable_workers->GetObj(), pnode)
			{
				(*pavailable_worker)->Abort();
				exist_workers--;
				assert(exist_workers >= 0);
				if (exist_workers == 0)
				{
					break;
				} //if
			}
		}
	}
	else
	{
		this->Dispatch();
	}
	return true;
}

//-------------------------------------------------------------------------

bool ThreadPool::SetMinThreads(long workerThreads)
{
	assert(workerThreads >= 1);
	{
		if (workerThreads > this->m_nMaxThreads)
		{
			return false;
		} //if
		_safe_change(&this->m_nMinThreads, workerThreads);
		//this->m_nMinThreads = workerThreads;
	}
	auto &workers = this->m_pworkers->GetObj();
	AutoLock locker(this->m_pworkers->GetSyncObj());

	//增加常驻工人
	while(workers.Length < workerThreads/*this->m_nMinThreads*/)
	{
		auto pworker = new ThreadPoolWorker(this);
		if (pworker == nullptr || pworker->pThread == nullptr || 
			workers.Add(pworker) == nullptr)
			return false;
	}
	return true;
}

//-------------------------------------------------------------------------

bool ThreadPool::SetMaxIdleTime(long maxIdleTime)
{
	if (maxIdleTime < 0)
	{
		return false;
	} //if
	_safe_change(&this->m_nMaxIdleTime, maxIdleTime);

	return true;
}

//-------------------------------------------------------------------------

bool ThreadPool::Dispatch()
{
	long TasksCount = this->GetTasks();
	if (TasksCount < 1)
	{
		//没有任务不做任何事
		return false;
	} //if
	//尝试唤醒工人
	{
		auto &available_workers = this->m_pavailable_workers->GetObj();
		this->m_pavailable_workers->Lock();

		foreachList(pworker, available_workers, pnode)
		{
			(*pworker)->pThread->Alert();
			TasksCount--;
			if (TasksCount < 1)
			{
				this->m_pavailable_workers->UnLock();
				return true;
			} //if
		}

		this->m_pavailable_workers->UnLock();
	}
	//尝试增加工人
	{
		auto &workers = this->m_pworkers->GetObj();
		AutoLock locker(this->m_pworkers->GetSyncObj());
		
		if (workers.Length < this->m_nMaxThreads)
		{
			while(workers.Length < this->m_nMaxThreads && TasksCount >= 1)
			{
				auto pworker = new ThreadPoolWorker(this);
				if (pworker == nullptr || pworker->pThread == nullptr || 
					workers.Add(pworker) == nullptr)
					return false;
				TasksCount--;
			}
			return true;
		} //if
	}
	return false;
}

//-------------------------------------------------------------------------

void ThreadPool::RemoveWorker(ThreadPoolWorker *pworker_removing, bool available)
{
	auto &available_workers = this->m_pavailable_workers->GetObj();
	auto &workers           = this->m_pworkers->GetObj();

	if(available)
	{
		this->m_pavailable_workers->Lock();

		foreachList(pworker, available_workers, pnode)
		{
			if((*pworker) == pworker_removing)
			{
				if(available_workers.Remove((Collections::Generic::List
					<ThreadPoolWorker *>::ListNodePointer)pnode))
					available = false;
				break;
			}
		}

		this->m_pavailable_workers->UnLock();
		assert(available != true || !"不正常的就绪状态2");
	} //if


	this->m_pworkers->Lock();

	foreachList(pworker, workers, pnode)
	{
		if((*pworker) == pworker_removing)
		{
			workers.Remove((Collections::Generic::List<ThreadPoolWorker *,
				nullptr, ThreadPoolWorker::Dispose>::ListNodePointer)pnode);
			break;
		}
	}

	this->m_pworkers->UnLock();
}

//-------------------------------------------------------------------------

void ThreadPoolWorker::worker_workshop(struct ThreadPoolWorker *pMySelf)
{
	//工人开始工作: 检索任务, 如果没有任务则将自己添加到就绪列表然后睡眠

	auto pManager           = pMySelf->pManager;//必须存放指针拷贝
	auto &available_workers = pManager->m_pavailable_workers->GetObj();
//	auto &workers           = pManager->m_pworkers->GetObj();


	//约定开始工作时工人未就绪
	bool bReady = false;
	while(!pMySelf->IsAborting() && !pMySelf->pManager->IsDestroying() && 
		(pManager->GetThreads() <= pManager->GetMaxThreads()))
	{
		auto &tasks = pManager->m_ptasks->GetObj();
		ThreadPoolTask task;
		long TasksCount;


		pManager->m_ptasks->Lock();

		TasksCount = tasks.Count;
		assert(TasksCount >= 0);
		if(TasksCount > 0) task = tasks.Dequeue();

		pManager->m_ptasks->UnLock();


		if (TasksCount != 0)
		{
			if (bReady)
			{
				//移除就绪状态, 如果存在
				pManager->m_pavailable_workers->Lock();

				foreachList(pworker, available_workers, pnode)
				{
					if((*pworker) == pMySelf)
					{
						if(available_workers.Remove((Collections::Generic::List
							<ThreadPoolWorker *>::ListNodePointer)pnode))
							bReady = false;
						break;
					}
					//顺便唤醒其他工人
					if(TasksCount > 1 && (*pworker)->pThread->Alert()) TasksCount--;
				}

				pManager->m_pavailable_workers->UnLock();
				assert(bReady != true || !"不正常的就绪状态1");
			}
			//执行任务
			assert(task.ExecutionTarget != nullptr);
			task.ExecutionTarget(task.ExecutionParameter);
			continue;
		} //if


		//可能没有任务可做, 检查退出信号以及线程池工人数量是否超过最大值
		if (pMySelf->IsAborting() || pMySelf->pManager->IsDestroying() ||
			(pManager->GetThreads() > pManager->GetMaxThreads()))
		{
			break;
		} //if
		

		//设置就绪状态
		if (!bReady)
		{
			pManager->m_pavailable_workers->Lock();
			if(available_workers.Add(pMySelf) != nullptr)
				bReady = true;
			pManager->m_pavailable_workers->UnLock();
		} //if


		//睡眠
		while (pMySelf->pThread->Sleep(pManager->GetMaxIdleTime()) == WAIT_TIMEOUTED)
		{
			//线程池处于空闲状态且线程等待时间超过最大空闲时间, 自我毁灭
			auto Threads          = pManager->GetThreads();
			auto MinThreads       = pManager->GetMinThreads();
			auto AvailableThreads = pManager->GetAvailableThreads();
			assert(Threads >= AvailableThreads);

			if ( Threads == AvailableThreads && Threads > MinThreads )
			{
				goto exit;
			} //if
			
		}
		//WAIT_ALERTED, 什么都不做
	}
exit:
	//工人欲停止工作, 必须与线程池脱离关系
	pManager->RemoveWorker(pMySelf, bReady);

	Thread::ExitThread();
}