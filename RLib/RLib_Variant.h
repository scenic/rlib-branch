/********************************************************************
	Created:	2012/01/17  21:54
	Filename: 	Lib_Variant.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_VAR
#define _USE_VAR
#include "RLib_String.h"
#include <stdlib.h>
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 基本类型的类模版
	/// 由于C++不允许继承基本类型, 此举只是为了拓展基本类型的成员方便面向对象编程
	/// </summary>
	template <class Type> class Variant
	{
	protected:
		Type m_var;
	public:
		//构造函数
		Variant(const Type& t = 0){ this->m_var = t; };
		Variant(const Variant&c){ this->m_var = c.m_var; };
		//自动转换
		operator Type &(){ return (Type &)this->m_var; };
		operator const Type() const{ return (const Type)this->m_var; };
		//二目运算
		Type& operator=(const Type& t){ this->m_var = t; return this->m_var; };
		Type& operator+=(const Type& t){ this->m_var += t; return this->m_var; };
	    Type& operator-=(const Type& t){ this->m_var -= t; return this->m_var; };
		Type& operator*=(const Type& t){ this->m_var *= t; return this->m_var; };
		Type& operator/=(const Type& t){ this->m_var /= t; return this->m_var; };
		Type& operator%=(const Type& t){ this->m_var %= t; return this->m_var; };
		Type& operator^=(const Type& t){ this->m_var ^= t; return this->m_var; };
		Type& operator&=(const Type& t){ this->m_var &= t; return this->m_var; };
		Type& operator|=(const Type& t){ this->m_var |= t; return this->m_var; };
		Type& operator>>=(const Type& t){ this->m_var >>= t; return this->m_var; };
		Type& operator<<=(const Type& t){ this->m_var <<= t; return this->m_var; };
		const Type operator+(const Type& t){ return this->m_var + t; };
		const Type operator-(const Type& t){ return this->m_var - t; };
		const Type operator*(const Type& t){ return this->m_var * t; };
		const Type operator/(const Type& t){ return this->m_var / t; };
		const Type operator%(const Type& t){ return this->m_var % t; };
		const Type operator^(const Type& t){ return this->m_var ^ t; };
		const Type operator&(const Type& t){ return this->m_var & t; };
		const Type operator|(const Type& t){ return this->m_var | t; };
		const Type operator>>(const Type& t){ return this->m_var >> t; };
		const Type operator<<(const Type& t){ return this->m_var << t; };
		//单目运算
		Type& operator ++(){ this->m_var++; return this->m_var;}; //前缀
		Type& operator --(){ this->m_var--; return this->m_var;};
		const Type operator++(int){ Type t = this->m_var; this->m_var++; return t; }; //后缀
		const Type operator--(int){ Type t = this->m_var; this->m_var--; return t; };
		//比较运算
		bool operator == (const Type& t) const{ return this->m_var == t; };
		bool operator != (const Type& t) const{ return this->m_var != t; };
		bool operator <  (const Type& t) const{ return this->m_var < t; };
		bool operator <= (const Type& t) const{ return this->m_var <= t; };
		bool operator >  (const Type& t) const{ return this->m_var > t; };
		bool operator >= (const Type& t) const{ return this->m_var >= t; };
		/// <summary>
		/// 将数字转换为字符串形式
		/// </summary>
		String ToString(){ TCHAR dst[] = {T("-2147483648")}; _itot(this->m_var, dst, 10); return String(dst); };
		/// <summary>
		/// 将数字的字符串表示形式转换为它的等效 32 位有符号整数
		/// </summary>
		static Type TryParse(const wchar_t *s){ return _wtoi(s); };
		/// <summary>
		/// 将数字的指定进制字符串表示形式转换为它的等效 32 位有符号整数
		/// </summary>
		static Type TryParse(const wchar_t *s, int r){ return (Type)wcstol(s, NULL, r); };
		/// <summary>
		/// 将数字的字符串表示形式转换为它的等效 32 位有符号整数
		/// </summary>
		static Type TryParse(const CHAR *s){ return atoi(s); };
		/// <summary>
		/// 将数字的指定进制字符串表示形式转换为它的等效 32 位有符号整数
		/// </summary>
		static Type TryParse(const CHAR *s, int r){ return (Type)strtol(s, NULL, r); };
	};
	/// <summary>
	/// 表示32位整数
	/// </summary>
	class export Int32:public Variant<int>
	{
	public:
		Int32(int i = 0):Variant(i){};
		Int32(const Int32&c):Variant(c){};
		RLIB_ClassNewDel;
	};
	typedef Int32 Integer, Long;
	/// <summary>
	/// 表示32位无符号整数
	/// </summary>
	class export UInt32:public Variant<unsigned int>
	{
	public:
		UInt32(unsigned int i = 0):Variant(i){};
		UInt32(const UInt32&c):Variant(c){};
		RLIB_ClassNewDel;
	};
	typedef System::UInt32 ULong;
	/// <summary>
	/// 其它类型
	/// </summary>
	typedef void Void;
	typedef bool Boolean;
	typedef unsigned char Byte;
}

/// <summary>
/// 提供相应转换宏 无需调用构造函数
/// </summary>
#define ToInt32Ptr(i) ((System::Int32 *)(&i)) //i不能是常量(立即数)
#define ToInt32(i)    (*ToInt32Ptr(i))        //i不能是常量(立即数)
#define ToUInt32Ptr(ui) ((System::UInt32 *)(&ui)) //ui不能是常量(立即数)
#define ToUInt32(ui)    (*ToUInt32Ptr(ui))        //ui不能是常量(立即数)

#endif