/********************************************************************
	Created:	2012/04/22  8:57
	Filename: 	RLib_Compress.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_COMPRESS
#define _USE_COMPRESS
#include "RLib_Stream.h"
#ifndef MAX_WBITS
#  define MAX_WBITS   15 /* 32K LZ77 window */
#endif
//////////////////////////////////////////////////////////////////////////
namespace System
{
	namespace IO
	{
		// 表示GZip压缩的数据流
		typedef BufferedStream GZipStream;
		// 表示GZip解压缩后的数据流
		typedef BufferedStream UnGZipStream;
		/// <summary>
		/// 提供简单的GZip操作
		/// </summary>
		class export RLIB_THREAD_SAFE GZip
		{
		public:
			/// <summary>
			/// 使用GZIP编码压缩数据流
			/// </summary>
			/// <param name="src">欲操作的数据流</param>
			/// <returns>压缩后的数据流 不使用时请直接delete</returns>
			static GZipStream *Gzip(const Stream *src, long bytes = 0, int windowBits = -MAX_WBITS);
			/// <summary>
			/// 解压缩GZIP编码的数据流
			/// </summary>
			/// <param name="src">欲操作的数据流</param>
			/// <returns>解压缩后的数据流 不使用时请直接delete</returns>
			static UnGZipStream *UnGzip(const Stream *src, long bytes = 0, int windowBits = -MAX_WBITS);
		};
		// 表示UDA压缩的数据流
		typedef BufferedStream UDAStream;
	};
};
#endif