/********************************************************************
	Created:	2012/08/09  17:31
	Filename: 	RLib_ThreadSync.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_THREAD_SYNC
#define _USE_THREAD_SYNC
#include "RLib_Thread.h"
//-------------------------------------------------------------------------
namespace System
{
	namespace Threading
	{
		/// <summary>
		/// 表示一个同步基元, 也可用于进程间同步
		/// </summary>
		class export RLIB_THREAD_SAFE Mutex:public WaitHandle
		{
		private:
			/// <summary>
			/// 不进行任何初始化的构造
			/// </summary>
			Mutex(LPVOID/* unused*/){};
		public:
			RLIB_ClassNewDel;
			/// <summary>
			/// 用一个指示调用线程是否应拥有互斥体的初始所属权的布尔值和一个作为互斥体名称的字符串来初始化 Mutex 类的新实例
			/// </summary>
			Mutex(Boolean Owner = false, String Name = Nothing);
			/// <summary>
			/// 释放 Mutex 相关资源
			/// </summary>
			~Mutex(){};
			/// <summary>
			/// 释放 Mutex 一次
			/// </summary>
			/// <returns>Internal mutant counter state before call ReleaseMutex</returns>
			LONG ReleaseMutex();
		public:
			/// <summary>
			/// 如果它已经存在，打开指定的命名 Mutex
			/// </summary>
			static Mutex *OpenExisting(String);
		};
		/// <summary>
		/// 表示事件类型
		/// </summary>
		typedef enum _EventType {
			Notification_Event,
			Synchronization_Event
		} EventType;
		/// <summary>
		/// 表示线程同步事件
		/// </summary>
		class export RLIB_THREAD_SAFE Event:public WaitHandle
		{
		private:
			/// <summary>
			/// 不进行任何初始化的构造
			/// </summary>
			Event(LPVOID/* unused*/){};
		public:
			/// <summary>
			/// 初始化 Event 类的新实例
			/// </summary>
			/// <param name="InitialState">The initial state of the event object</param>
			Event(EventType Type, Boolean InitialState, String Name = Nothing);
			/// <summary>
			/// 释放 Event 相关资源
			/// </summary>
			~Event(){};
			/// <summary>
			/// 设置事件信号
			/// </summary>
			/// <returns>State of Event Object before function call</returns>
			LONG SetSignal();
			/// <summary>
			/// 清除事件信号
			/// </summary>
			void Clear();
			/// <summary>
			/// 将指定的事件设为发出信号状态
			/// 如果是一个人工重设事件, 正在等候事件的、被挂起的所有线程都会进入活动状态,
			/// 函数随后将事件设回未发信号状态, 并返回.
			/// 如果是一个自动重设事件, 则正在等候事件的、被挂起的单个线程会进入活动状态,
			/// 事件随后设回未发信号状态, 并且函数返回
			/// </summary>
			/// <returns>State of Event Object before function call</returns>
			LONG Pulse();
		public:
			/// <summary>
			/// 如果它已经存在，打开指定的命名 Event
			/// </summary>
			static Event *OpenExisting(String);
		};
		/// <summary>
		/// 限制可同时访问某一资源或资源池的线程数
		/// </summary>
		class export RLIB_THREAD_SAFE Semaphore:public WaitHandle
		{
		private:
			/// <summary>
			/// 不进行任何初始化的构造
			/// </summary>
			Semaphore(LPVOID/* unused*/){};
		public:
			RLIB_ClassNewDel;
			/// <summary>
			/// 初始化 Semaphore 类的新实例，并指定最大并发入口数，还可以选择为调用线程保留某些入口，以及选择指定系统信号量对象的名称
			/// </summary>
			Semaphore(int InitialCount, int MaximumCount, String Name = Nothing);
			/// <summary>
			/// 释放 Mutex 相关资源
			/// </summary>
			~Semaphore(){};
			/// <summary>
			/// 退出信号量并返回前一个计数
			/// </summary>
			LONG Release();
			/// <summary>
			/// 以指定的次数退出信号量并返回前一个计数
			/// </summary>
			LONG Release(ULONG releaseCount);
		public:
			/// <summary>
			/// 如果它已经存在，打开指定的命名 Mutex
			/// </summary>
			static Semaphore *OpenExisting(String);
		};
		/// <summary>
		/// 为多个线程共享的变量提供原子操作
		/// </summary>
		class Interlocked
		{
		public:
			/// <summary>
			/// 比较两个 32 位有符号整数是否相等，如果相等，则替换其中一个值。
			/// </summary>
			/// <param name="location1">其值将与 comparand 进行比较并且可能被替换的目标。</param>
			/// <param name="value">比较结果相等时替换目标值的值。</param>
			/// <param name="comperand">与位于 location1 处的值进行比较的值。</param>
			/// <returns>location1 中的原始值。</returns>
			template<typename T> __forceinline static T __stdcall CompareExchange
				(volatile T *location1, T value, T comperand)
			{
				assert(sizeof(T) == 4/*sizeof(long)*/);
				__asm
				{
					mov  ecx, location1;
					mov  edx, exchange32;
					mov  eax, value;
					lock cmpxchg [ecx], edx;
				}
			}
			/// <summary>
			/// 以原子操作的形式，将 32 位有符号整数设置为指定的值并返回原始值。
			/// </summary>
			/// <param name="location1">要设置为指定值的变量。</param>
			/// <param name="value">location1 参数被设置为的值。</param>
			/// <returns>location1 的原始值。</returns>
			template<typename T> __forceinline static T __stdcall Exchange(volatile T *location1, T value)
			{
				assert(sizeof(T) == 4/*sizeof(long)*/);
				__asm
				{
					mov  ecx, location1;
					mov  eax, value;
					xchg [ecx],eax;
				}
			}
			/// <summary>
			/// 以原子操作的形式递减指定变量的值并存储结果。
			/// </summary>
			/// <param name="location">其值要递减的变量。</param>
			/// <returns>递减的值。</returns>
			template<typename T> __forceinline static T __stdcall Decrement(volatile T *location)
			{
				assert(sizeof(T) == 4/*sizeof(long)*/);
				__asm
				{
					mov   ecx, location;
					mov   eax, 0FFFFFFFFh; //-1
					lock  xadd [ecx], eax; //加-1
					dec   eax;
				}
			}
			/// <summary>
			/// 以原子操作的形式递增指定变量的值并存储结果。
			/// </summary>
			/// <param name="location">其值要递增的变量。</param>
			/// <returns>递增的值。</returns>
			template<typename T> __forceinline static T __stdcall Increment(volatile T *location)
			{
				assert(sizeof(T) == 4/*sizeof(long)*/);
				__asm
				{
					mov  ecx, location;
					mov  eax, 1;
					lock xadd [ecx], eax; //加
					inc  eax;
				}
			}
			/// <summary>
			/// 对两个 32 位整数进行求和并用和替换第一个整数，上述操作作为一个原子操作完成。
			/// </summary>
			/// <param name="location1">一个变量，包含要添加的第一个值。 两个值的和存储在 location1 中。</param>
			/// <param name="value">要添加到整数中的 location1 位置的值。</param>
			/// <returns>存储在 location1 处的新值</returns>
			template<typename T> __forceinline static T __stdcall Add(volatile T *location1, T value)
			{
				assert(sizeof(T) == 4/*sizeof(long)*/);
				__asm
				{
					mov  ecx, location1;
					mov  eax, value;
					lock xadd [ecx], eax;
				}
			}
		};
	}
}
#endif // _USE_THREAD_SYNC