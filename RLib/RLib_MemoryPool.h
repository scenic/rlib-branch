/********************************************************************
	Created:	2012/04/22  8:45
	Filename: 	RLib_MemoryPool.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_MEMORYPOOL
#define _USE_MEMORYPOOL
#include "RLib_Memory.h"
//////////////////////////////////////////////////////////////////////////
namespace System
{
	namespace IO
	{
		/// <summary>
		/// 线程安全的内存池类
		/// </summary>
		class export RLIB_THREAD_SAFE MemoryPool
		{
		private:
			/// <summary>
			/// 记录分配信息
			/// </summary>
			typedef struct MemBlockHeader
			{
#ifdef _RUNTIME_DEBUG
				//调试标志 值 RLIB_VALIDBYTE 表示有效
				LONG    ptr_valid;
#endif // _RUNTIME_DEBUG
				int  index;
				__forceinline void *ptr()
				{
					return (LPBYTE(this) + sizeof(MemBlockHeader));
				}
			}MemBlockHeader, *PMemBlockHeader;
			/// <summary>
			/// 内存池组成
			/// </summary>
			typedef struct MEMORY_PAGE
			{
				Memory                     Page;
				Threading::CriticalSection SyncRoot;
#ifdef _DEBUG
				MEMORY_PAGE                *pNext;
#endif // _DEBUG
			}MEMORY_PAGE, *PMEMPAGE;
		private:
			/// <summary>
			/// 内存页指针
			/// </summary>
			PMEMPAGE MemoryList;
			/// <summary>
			/// 内存页数量
			/// </summary>
			int MemoryCount;
		public:
			RLIB_ClassNewDel;
			/// <summary>
			/// 为内存池执行初始化操作
			/// </summary>
			MemoryPool(int memory_count = RLIB_MemoryPoolCount);
			/// <summary>
			/// 为内存池执行销毁操作
			/// </summary>
			~MemoryPool();
		public:
			/// <summary>
			/// 在内存池上分配内存
			/// </summary>
#ifdef _RUNTIME_DEBUG
			void *AllocByte(LONG, LPCTSTR function, LPCTSTR file);
#else
			void *AllocByte(LONG);
#endif // _RUNTIME_DEBUG
			/// <summary>
			/// 在内存池上重新分配内存
			/// </summary>
			void *ReAlloc(void *, ULONG);
			/// <summary>
			/// 在内存池上重新分配内存, 并尽量在同一内存页中分配
			/// </summary>
			void *TryReAlloc(void *, ULONG);
			/// <summary>
			/// 修改由 AllocByte 返回的已分配内存为指定大小, 为0将调用 Collect 释放
			/// 该方法可在不改变指针情况下缩减、增大内存, 但不保证一定成功
			/// </summary>
			bool ReSize(Object, LONG);
			/// <summary>
			/// 获取特定指针分配的大小
			/// </summary>
			LONG GetSize(Object);
			/// <summary>
			/// 回收内存池上分配的内存
			/// </summary>
			void Collect(void *);
			/// <summary>
			/// 进行垃圾回收, 返回释放字节数
			/// </summary>
			LONG TryGCCollect();
			/// <summary>
			/// 强制进行垃圾回收, 返回释放字节数(方法可能导致阻塞)
			/// </summary>
			LONG WaitForFullGCComplete();
		};
	}
}
//////////////////////////////////////////////////////////////////////////
#endif // _USE_MEMORYPOOL