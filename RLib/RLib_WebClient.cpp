/********************************************************************
Created:	2012/04/21  14:48
Filename: 	RLib_WebClient.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_WebClient.h"
using namespace System::Net;
//////////////////////////////////////////////////////////////////////////
HttpRequest *WebClient::GetHttpRequest(const String Url)
{
    //创建 HttpRequest 请求对象
    System::Net::HttpRequest *Request = new System::Net::HttpRequest(Url);
    //内存允许情况下, 返回的指针总是可用的
    if (Request == nullptr)
    {
        goto do_next_work;
    }
    //初始化Request以适合我们的要求-
    Request->UserAgent = T("RLIB Visitor/WebClient.GetHttpRequest");
    Request->Accept = T("*/*");
	Request->Headers.Add("Accept-Charset", "iso-8859-1,utf-8;q=0.7,*;q=0.7");
    Request->Headers.Add("Accept-Language", "zh-cn, zh;q=1.0,en");
	Request->Headers.Add("Accept-Encoding", "gzip, x-gzip; q=0.9");
//  Request->Headers.Add("Range", "bytes=409600-");
//  Request->Headers.Add("Unless-Modified-Since", "Sun, 15 Apr 2012 03:51:18 GMT");
    //设置解压缩模式
    Request->AutomaticDecompression = System::Net::DecompressionMethod::GZip;
do_next_work: 
	return Request;
}

//-------------------------------------------------------------------------

ResponseStream *WebClient::GetRawResponseStream(const String Url, HttpRequest *Request /* = nullptr */)
{
    ResponseStream *resStream = nullptr;

	bool bDelRequest = false;
    if (Request == nullptr)
    {
        Request = WebClient::GetHttpRequest(Url);
        if (Request == nullptr) goto err_return;

		bDelRequest = true;
    }

    if (Request->GetLastException()->HResult != STATUS_SUCCESS)
    {
err_catch: 
		if(bDelRequest) delete Request;
        goto err_return;
    } //if

	auto responseStream = Request->GetResponseStream();
    if (responseStream == nullptr)
    {
		goto err_catch;
    } //if

    resStream = new ResponseStream(responseStream->Length);
    if (resStream == nullptr)
    {
        goto err_catch;
    } //if
    resStream->Write(responseStream->ObjectData, responseStream->Length);
    resStream->Position = 0;

	goto err_catch;

err_return: 
	return resStream;
}

//-------------------------------------------------------------------------

ResponseStream *WebClient::GetResponseStream(const String Url, HttpRequest *Request /* = nullptr */, HttpResponse **pResponse OUT /* = nullptr */)
{
	ResponseStream *resStream = nullptr;

	bool bDelRequest = false;
	if (Request == nullptr)
	{
		Request = WebClient::GetHttpRequest(Url);
		if (Request == nullptr) goto err_return;
		
		bDelRequest = true;
	}

	if (Request->GetLastException()->HResult != STATUS_SUCCESS)
	{
err_catch: 
		if(bDelRequest) delete Request;
		goto err_return;
	} //if

	HttpResponse *Response = Request->GetResponse();
	if (Response == nullptr)
	{
		goto err_catch;
	} //if
	if (Response->GetLastException()->HResult != STATUS_SUCCESS || Response->GetResponseStream() == nullptr)
	{
err_catch_all: 
		if(pResponse == nullptr) Response->Close();
		goto err_catch;
	} //if
	if(pResponse != nullptr)
	{
		*pResponse = Response;
	}
	resStream = new ResponseStream(Response->GetResponseStream()->Length);
	if (resStream == nullptr)
	{
		goto err_catch_all;
	} //if
	resStream->Write(Response->GetResponseStream()->ObjectData, Response->GetResponseStream()->Length);
	resStream->Position = 0;

	goto err_catch_all;

err_return: 
	return resStream;
}

//-------------------------------------------------------------------------

String WebClient::GetResponseText(const String Url, HttpRequest *Request /* = nullptr */, HttpResponse **pResponse OUT /* = nullptr */)
{
    String ResponseText;

	bool bDelRequest = false;
	if (Request == nullptr)
	{
        Request = WebClient::GetHttpRequest(Url);
        if (Request == nullptr) goto err_return;

		bDelRequest = true;
    }

    if (Request->GetLastException()->HResult != STATUS_SUCCESS)
    {
err_catch: 
		if(bDelRequest) delete Request;
        goto err_return;
    } //if

    HttpResponse *Response = Request->GetResponse();
    if (Response == nullptr)
    {
        goto err_catch;
    } //if
    if (Response->GetLastException()->HResult != STATUS_SUCCESS || Response->GetResponseStream() == nullptr)
    {
err_catch_all: 
		if(pResponse == nullptr) Response->Close();
        goto err_catch;
    } //if
	if(pResponse != nullptr) 
	{
		*pResponse = Response;
	}
    if (Response->GetResponseStream()->Position != 0)
    {
        Response->GetResponseStream()->Position = 0;
    }

    ResponseText = Response->ContentType.ToLower() /*Response->Headers.Get("Content-Type")*/;
    //判断网页编码方式
    if (ResponseText.IndexOf(T("utf-8")) !=  - 1)
    {
        //将UTF-8数据转换为Unicode(UTF-16)
        System::IO::MemoryStream *dStream = System::Text::Encoder::ToWideChar(System::Text::UTF8Encoding, Response->GetResponseStream());
        if (!dStream)
        {
            goto err_catch_all;
        }
        #ifndef _UNICODE
            System::IO::MemoryStream *tStream = dStream;
            dStream = System::Text::Encoder::ToMultiByte(System::Text::ASCIIEncoding, tStream);
            delete tStream;
        #endif // _UNICODE
        ResponseText = (LPTSTR)dStream->ObjectData;
        delete dStream;

        goto work_done;
    }
	else if (ResponseText.IndexOf(T("gb2312")) !=  - 1)
	{
#ifdef _UNICODE
		System::IO::MemoryStream *tStream = System::Text::Encoder::ToWideChar(System::Text::ASCIIEncoding, Response->GetResponseStream());
		ResponseText = (LPTSTR)tStream->ObjectData;
		delete tStream;
#else 
		ResponseText = (LPTSTR)Response->GetResponseStream()->ObjectData;
#endif // _UNICDOE
	}
	else
	{
		System::IO::MemoryStream *tStream = Text::Encoder::ToCurrentEncoding(Text::UnknownEncoding, Response->GetResponseStream());
		if (tStream)
		{
			ResponseText = (LPTSTR)tStream->ObjectData;
			delete tStream;
		} //if
	}
work_done:
	if(pResponse == nullptr) Response->Close();
    if(bDelRequest) delete Request;
    return ResponseText;

    err_return: return Nothing;
}

//-------------------------------------------------------------------------

ResponseStream *WebClient::PostRawResponseStream(const String Url, String Data, HttpRequest *Request IN /* = nullptr */)
{
	ResponseStream *resStream = nullptr;

	bool bDelRequest = false;
	if (Request == nullptr)
	{
		Request = WebClient::GetHttpRequest(Url);
		if (Request == nullptr) goto err_return;

		bDelRequest = true;
	}

	if (Request->GetLastException()->HResult != STATUS_SUCCESS)
	{
err_catch: 
		if(bDelRequest) delete Request;
		goto err_return;
	} //if

	if (!Data.IsNullOrEmpty())
	{
		Request->Method = T("POST");
		Request->ContentType = T("application/x-www-form-urlencoded; charset=gb2312");

		//Data = Net::Uri::UrlEncode(Data);

		Request->ContentLength = Data.GetMultiByteSize();

		Request->GetRequestStream()->Write(Data.ToMultiByte(), Request->ContentLength);
	} //if


	auto responseStream = Request->GetResponseStream();
	if (responseStream == nullptr)
	{
		goto err_catch;
	} //if

	resStream = new ResponseStream(responseStream->Length);
	if (resStream == nullptr)
	{
		goto err_catch;
	} //if
	resStream->Write(responseStream->ObjectData, responseStream->Length);
	resStream->Position = 0;

	goto err_catch;

err_return: 
	return resStream;
}

//-------------------------------------------------------------------------

ResponseStream *WebClient::PostResponseStream(const String Url, String Data, HttpRequest *Request IN /* = nullptr */, HttpResponse **pResponse OUT /* = nullptr */)
{
	ResponseStream *resStream = nullptr;

	bool bDelRequest = false;
	if (Request == nullptr)
	{
		Request = WebClient::GetHttpRequest(Url);
		if (Request == nullptr) goto err_return;

		bDelRequest = true;
	}

	if (Request->GetLastException()->HResult != STATUS_SUCCESS)
	{
err_catch: 
		if(bDelRequest) delete Request;
		goto err_return;
	} //if

	if (!Data.IsNullOrEmpty())
	{
		Request->Method = T("POST");
		Request->ContentType = T("application/x-www-form-urlencoded; charset=gb2312");

		//Data = Net::Uri::UrlEncode(Data);

		Request->ContentLength = Data.GetMultiByteSize();

		Request->GetRequestStream()->Write(Data.ToMultiByte(), Request->ContentLength);
	} //if


	HttpResponse *Response = Request->GetResponse();
	if (Response == nullptr)
	{
		goto err_catch;
	} //if
	if (Response->GetLastException()->HResult != STATUS_SUCCESS || Response->GetResponseStream() == nullptr)
	{
err_catch_all: 
		if(pResponse == nullptr) Response->Close();
		goto err_catch;
	} //if
	if(pResponse != nullptr)
	{
		*pResponse = Response;
	}
	resStream = new ResponseStream(Response->GetResponseStream()->Length);
	if (resStream == nullptr)
	{
		goto err_catch_all;
	} //if
	resStream->Write(Response->GetResponseStream()->ObjectData, Response->GetResponseStream()->Length);
	resStream->Position = 0;

	goto err_catch_all;

err_return: 
	return resStream;
}

//-------------------------------------------------------------------------

String WebClient::PostResponseText(const String Url, String Data, HttpRequest *Request IN /* = nullptr */, HttpResponse **pResponse OUT /* = nullptr */)
{
	String ResponseText;

	bool bDelRequest = false;
	if (Request == nullptr)
	{
		Request = WebClient::GetHttpRequest(Url);
		if (Request == nullptr) goto err_return;

		bDelRequest = true;
	}

	if (Request->GetLastException()->HResult != STATUS_SUCCESS)
	{
err_catch: 
		if(bDelRequest)delete Request;
		goto err_return;
	} //if


	if (!Data.IsNullOrEmpty())
	{
		Request->Method = T("POST");
		Request->ContentType = T("application/x-www-form-urlencoded; charset=gb2312");

		//Data = Net::Uri::UrlEncode(Data);

		Request->ContentLength = Data.GetMultiByteSize();

		Request->GetRequestStream()->Write(Data.ToMultiByte(), Request->ContentLength);
	} //if

	HttpResponse *Response = Request->GetResponse();
	if (Response == nullptr)
	{
		goto err_catch;
	} //if
	if (Response->GetLastException()->HResult != STATUS_SUCCESS || Response->GetResponseStream() == nullptr)
	{
err_catch_all: 
		if(pResponse == nullptr) Response->Close();
		goto err_catch;
	} //if
	if(pResponse != nullptr) 
	{
		*pResponse = Response;
	}
	if (Response->GetResponseStream()->Position != 0)
	{
		Response->GetResponseStream()->Position = 0;
	}

	ResponseText = Response->ContentType.ToLower() /*Response->Headers.Get("Content-Type")*/;
	//判断网页编码方式
	if (ResponseText.IndexOf(T("utf-8")) !=  - 1)
	{
		//将UTF-8数据转换为Unicode(UTF-16)
		System::IO::MemoryStream *dStream = System::Text::Encoder::ToWideChar(System::Text::UTF8Encoding, Response->GetResponseStream());
		if (!dStream)
		{
			goto err_catch_all;
		}
#ifndef _UNICODE
		System::IO::MemoryStream *tStream = dStream;
		dStream = System::Text::Encoder::ToMultiByte(System::Text::ASCIIEncoding, tStream);
		delete tStream;
#endif // _UNICODE
		ResponseText = (LPTSTR)dStream->ObjectData;
		delete dStream;

		goto work_done;
	}
	else if (ResponseText.IndexOf(T("gb2312")) !=  - 1)
	{
#ifdef _UNICODE
		System::IO::MemoryStream *tStream = System::Text::Encoder::ToWideChar(System::Text::ASCIIEncoding, Response->GetResponseStream());
		ResponseText = (LPTSTR)tStream->ObjectData;
		delete tStream;
#else 
		ResponseText = (LPTSTR)Response->GetResponseStream()->ObjectData;
#endif // _UNICDOE
	}
	else
	{
		System::IO::MemoryStream *tStream = Text::Encoder::ToCurrentEncoding(Text::UnknownEncoding, Response->GetResponseStream());
		if (tStream)
		{
			ResponseText = (LPTSTR)tStream->ObjectData;
			delete tStream;
		} //if
	}
work_done:
	if(pResponse == nullptr) Response->Close();
	if(bDelRequest)delete Request;
	return ResponseText;

err_return: 
	return Nothing;
}

//-------------------------------------------------------------------------

bool WebClient::DownloadFile(const String Url, const String FilePath, HttpRequest *Request /* = nullptr */, HttpResponse **pResponse OUT /* = nullptr */)
{
    ResponseStream *resStream = WebClient::GetResponseStream(Url, Request, pResponse);
    if (resStream != nullptr)
    {
		bool result = false;
        IO::FileStream *outFile = IO::File::Create(FilePath, File::CreateNewMode);
        if (outFile != nullptr)
        {
            outFile->Write(resStream->ObjectData, resStream->Length);
            delete outFile;
			result = true;
        } //if
        delete resStream;
		return result;
    } //if
    return false;
}
