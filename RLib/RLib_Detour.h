/********************************************************************
	Created:	2012/06/06  20:13
	Filename: 	RLib_Detour.h
	Author:		Lactoferrin
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_DETOUR
#define _USE_DETOUR
#include "RLib_ThreadPool.h"
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 提供一些运行时支持
	/// </summary>
	namespace Runtimes
	{
		/// <summary>
		/// Detour
		/// </summary>
		namespace Detour
		{
			export int IsHotpatchable(void *Target);
			export int IsHotpatched(void *Target);
			export void *ApplyHotpatch(void *Callback, void *Target);
			export int RemoveHotpatch(void *Target);
			export void __cdecl RemoveHotpatches(void *Target, ...);
			export void *CreateRedirectHeap(void);
			export void *DestroyRedirectHeap(void);
			export void *RedirectProcedure(void *Callback, void *Target);
			export void __cdecl RedirectProcedures(void *Callback, ...); //void*Target,void**Next,...;
			export void *RestoreProcedure(void *Next);
			export void __cdecl RestoreProcedures(void *Next, ...);
			export long SetTargetModule(wchar_t *ModuleName, unsigned NameLength);
			export long CloseLastFileHandle(void);
			export void *RedirectProcedureByName(void *Callback, char *Name, unsigned NameLength);

			//-------------------------------------------------------------------------

			template <typename Callback_Function, typename Target_Function> Callback_Function inline ApplyHotpatch(Callback_Function Callback, Target_Function Target)
			{
				return reinterpret_cast<Callback_Function> (ApplyHotpatch((void *)Callback, (void *)Target));
			}

			//-------------------------------------------------------------------------

			template <typename Callback_Function, typename Target_Function> Callback_Function inline RedirectProcedure(Callback_Function Callback, Target_Function Target)
			{
				return reinterpret_cast<Callback_Function> (RedirectProcedure((void *)Callback, (void *)Target));
			}
			
			//-------------------------------------------------------------------------

			template <typename Callback_Function> Callback_Function inline RedirectProcedureByName(Callback_Function Callback, char *Name, unsigned NameLength)
			{
				return reinterpret_cast<Callback_Function> (RedirectProcedureByName((void *)Callback, Name, NameLength));
			}
		}
	}
}
#define BeginHook(ModuleName)  System::Runtimes::Detour::SetTargetModule(L##ModuleName, sizeof(L##ModuleName) - sizeof(wchar_t))
#define HookIt(Callback, Name) System::Runtimes::Detour::RedirectProcedureByName((Callback), #Name, sizeof(#Name) - sizeof(char))
#define UnHook(Next)           System::Runtimes::Detour::RestoreProcedure(Next)
#define EndHook                System::Runtimes::Detour::CloseLastFileHandle()

#endif