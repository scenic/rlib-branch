/********************************************************************
Created:	2011/02/17  15:15
Filename: 	Lib_Winsock.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_Winsock.h"
#include <Ws2tcpip.h>
using namespace System::Net;
/// <summary>
/// 记录Winsock初始化信息
/// </summary>
static bool WinsockInitializeInfo[4] = { false };
/************************************************************************/
/* Sockets                                                                
/************************************************************************/
Sockets::Sockets(RLIB_SOCKET s /* = RLIB_INVALID_SOCKET */, int af /* = AF_INET */, int type /* = SOCK_STREAM */, int protocol /* = IPPROTO_TCP */)
{
    this->m_end_accept = FALSE;
    /// <summary>
    /// 初始化Winsock dll
    /// </summary>
    if (!WinsockInitializeInfo[0] && s == INVALID_SOCKET)
    {
        WSADATA wsaData;
        /// <summary>
        /// If successful, the WSAStartup function returns zero
        /// </summary>
        if ((this->m_error.HResult = WSAStartup(MAKEWORD(2, 2), &wsaData)) != STATUS_SUCCESS)
        {
            /// <summary>
            /// 设置异常
            /// </summary>
            Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
            //If successful, the WSAStartup function returns zero. Otherwise, it returns one of the error codes. 
            //A call to the WSAGetLastError function is not needed and should not be used.
            RLIB_SetExceptionInfo((&this->m_error));
            return ;
        }
        WinsockInitializeInfo[0] = true;
    }
    /// <summary>
    /// 初始化Winsock成员
    /// </summary>
    if (s != INVALID_SOCKET)
    {
        this->m_socket = s;
        goto continue_;
    }
    if ((this->m_socket = socket(af, type, protocol)) == INVALID_SOCKET)
    {
        /// <summary>
        /// 设置异常
        /// </summary>
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
        RLIB_SetExceptionInfo((&this->m_error));
        return ;
    }
    continue_: 
    /// <summary>
    /// If no error occurs, socket returns a descriptor referencing the new socket
    /// </summary>
    System::IO::Memory::memset(&this->m_addr, 0, sizeof(this->m_addr));
    m_addr.sin_family = AF_INET;
    /// <summary>
    /// 未连接
    /// </summary>
    this->IsConnect = false;
}

//-------------------------------------------------------------------------

Sockets::~Sockets()
{
    this->Close();
}

//-------------------------------------------------------------------------

void Sockets::Dispose()
{
    if (WinsockInitializeInfo[0])
    {
        /// <summary>
        /// The return value is zero if the operation was successful
        /// </summary>
        if (WSACleanup() == 0)
        {
            WinsockInitializeInfo[0] = false;
        }
    }
}

//-------------------------------------------------------------------------

Sockets::operator SOCKET()
{
    return this->m_socket;
}

//-------------------------------------------------------------------------

bool Sockets::Close()
{
    RLIB_CheckThisRet(false);
    if (this->m_socket != INVALID_SOCKET)
    {
        shutdown(m_socket, SD_BOTH);
        /// <summary>
        /// If no error occurs, closesocket returns zero
        /// </summary>
        if (closesocket(this->m_socket) != 0)
        {
            /// <summary>
            /// 设置异常
            /// </summary>
            this->m_error.HResult = WSAGetLastError();
            Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
            RLIB_SetExceptionInfo((&this->m_error));
            return false;
        }
        this->IsConnect = false;
        this->m_socket = INVALID_SOCKET;
    }
    return true;
}

//-------------------------------------------------------------------------

bool Sockets::Connect(char *host, unsigned int host_len, u_short port)
{
    m_addr.sin_port = htons(port);
    if (host[host_len - 1] > 48 && host[host_len - 1] < 58)
    {
        /// <summary>
        /// 主机名为IP地址
        /// </summary>
        m_addr.sin_addr.S_un.S_addr = this->Ipv4StringToAddress(host);
        if (m_addr.sin_addr.S_un.S_addr == INADDR_NONE)
        {
            goto set_exception_return;
        }
        goto continue_work;
    }
    /// <summary>
    /// 主机名为域名
    /// </summary>
    LPHOSTENT pHost = gethostbyname(host);
    if (!pHost)
    {
        goto set_exception_return;
    }
    System::IO::Memory::memcpy(&m_addr.sin_addr.S_un.S_addr, pHost->h_addr_list[0], pHost->h_length);
continue_work: 
	if (connect(m_socket, (sockaddr*) &m_addr, sizeof(m_addr)) != 0)
    {
        // If no error occurs, connect returns zero
set_exception_return: 
        // 设置异常
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
        RLIB_SetExceptionInfo((&this->m_error));
        return false;
    }
    this->IsConnect = true;
    return true;
}

//-------------------------------------------------------------------------

bool Sockets::SetSndTimeO(int timeout)
{
    //设置发送超时
    /// <summary>
    /// If no error occurs, setsockopt returns zero. 
    /// </summary>
    if (setsockopt(m_socket, SOL_SOCKET, SO_SNDTIMEO, (char*) &timeout, sizeof(timeout)) != 0)
    {
        /// <summary>
        /// 设置异常
        /// </summary>
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
        RLIB_SetExceptionInfo((&this->m_error));
        return false;
    }
    return true;
}

//-------------------------------------------------------------------------

int Sockets::GetSndBuf()
{
    int SndBuf, SndBufSize = sizeof(SndBuf);
    /// <summary>
    /// If no error occurs, getsockopt returns zero.
    /// </summary>
    if (getsockopt(m_socket, SOL_SOCKET, SO_RCVBUF, (char*) &SndBuf, &SndBufSize) != 0)
    {
        return 4096; //Use Default
    }
    return SndBuf;
}
//-------------------------------------------------------------------------

bool Sockets::Send(char *data, int size)
{
	int thisSend = 1; //本次已发送
	int allSend = 0; //已发送
	while (size > 0 && thisSend > 0)
	{
		thisSend = send(m_socket, data + allSend,  size, NULL);
		if (thisSend < 0)
			//SOCKET_ERROR
		{
			/// <summary>
			/// 设置异常
			/// </summary>
			this->m_error.HResult = WSAGetLastError();
			Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
			RLIB_SetExceptionInfo((&this->m_error));
			return false;
		}
		allSend += thisSend;
		size -= thisSend;
	}
	return true;
}
//-------------------------------------------------------------------------

bool Sockets::Send(char *data, int *size)
{
    int thisSend = 1; //本次已发送
    int allSend = 0; //已发送
    while (*size > 0 && thisSend > 0)
    {
        thisSend = send(m_socket, data + allSend,  *size, NULL);
        if (thisSend < 0)
        //SOCKET_ERROR
        {
            /// <summary>
            /// 设置异常
            /// </summary>
            this->m_error.HResult = WSAGetLastError();
            Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
            *size = allSend;
            RLIB_SetExceptionInfo((&this->m_error));
            return false;
        }
        allSend += thisSend;
        *size -= thisSend;
    }
    return true;
}

//-------------------------------------------------------------------------

bool Sockets::SetRcvTimeO(int timeout)
{
    //设置发送超时
    /// <summary>
    /// If no error occurs, setsockopt returns zero. 
    /// </summary>
    if (setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO, (char*) &timeout, sizeof(timeout)) != 0)
    {
        /// <summary>
        /// 设置异常
        /// </summary>
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
        RLIB_SetExceptionInfo((&this->m_error));
        return false;
    }
    return true;
}

//-------------------------------------------------------------------------

int Sockets::GetRcvBuf()
{
    int RcvBuf, RcvBufSize = sizeof(RcvBuf);
    /// <summary>
    /// If no error occurs, getsockopt returns zero.
    /// </summary>
    if (getsockopt(m_socket, SOL_SOCKET, SO_RCVBUF, (char*) &RcvBuf, &RcvBufSize) != 0)
    {
        return 4096; //Use Default
    }
    return RcvBuf;
}

//-------------------------------------------------------------------------

int Sockets::Recv(char *buf, int size)
{
    int nRecving = recv(m_socket, buf, size, NULL);
    if (nRecving == SOCKET_ERROR)
    //SOCKET_ERROR
    {
        /// <summary>
        /// 设置异常信息
        /// </summary>
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
        RLIB_SetExceptionInfo((&this->m_error));
        return SOCKET_ERROR;
    }
    return nRecving;
}

//-------------------------------------------------------------------------

int Sockets::RecvEx(char *buf, int *size)
{
    int nWaitRecv =  *size; //待接收
    int nRecv = 0; //已接收
    int nRecving = 1; //本次已接
    while (nRecving > 0)
    {
        nRecving = recv(m_socket, buf + nRecv, nWaitRecv, NULL);
        if (nRecving == SOCKET_ERROR)
        //SOCKET_ERROR
        {
            /// <summary>
            /// 设置异常信息
            /// </summary>
            this->m_error.HResult = WSAGetLastError();
            Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
            *size = nRecv; //发生异常用来接收已接收长度
            RLIB_SetExceptionInfo((&this->m_error));
            return SOCKET_ERROR;
        }
        /// <summary>
        /// 更新大小
        /// </summary>
        nRecv += nRecving;
        nWaitRecv -= nRecving;
        /// <summary>
        /// 如果已接收完指定大小
        /// </summary>
        if (nWaitRecv <= 0)
        {
            goto Ret;
        }
    }
    Ret: return nRecv;
}

//-------------------------------------------------------------------------

int Sockets::Bind(ULONG addr, u_short port)
{
    m_addr.sin_addr.S_un.S_addr = addr;
    m_addr.sin_port = htons(port);
    if (bind(this->m_socket, (LPSOCKADDR) &m_addr, sizeof(m_addr)) == SOCKET_ERROR)
    {
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
        RLIB_SetExceptionInfo((&this->m_error));
        return SOCKET_ERROR;
    }
    return STATUS_SUCCESS;
}

//-------------------------------------------------------------------------

int Sockets::Bind(u_short port)
{
    return this->Bind(INADDR_ANY, port);
}

//-------------------------------------------------------------------------

int Sockets::Listen(int backlog /* = SOMAXCONN */)
{
    if (listen(this->m_socket, backlog) == SOCKET_ERROR)
    {
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
        RLIB_SetExceptionInfo((&this->m_error));
        return SOCKET_ERROR;
    }
    return STATUS_SUCCESS;
}

//-------------------------------------------------------------------------

Sockets *Sockets::Accept(sockaddr_in *addr /* = nullptr */, int *addrlen /* = nullptr */)
{
    SOCKET AcceptSocket = accept(this->m_socket, (LPSOCKADDR)addr, addrlen);
    if (AcceptSocket == INVALID_SOCKET)
    {
        if (this->m_end_accept == TRUE)
        {
            __asm lock dec [this]Sockets.m_end_accept;
            goto RETURN_NULL;
        }
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
		RLIB_SetExceptionInfo((&this->m_error));
RETURN_NULL: 
        return nullptr;
    }
    return (new Sockets(AcceptSocket));
}

//-------------------------------------------------------------------------

void Sockets::AbortAccept()
{
	__asm lock inc [this]Sockets.m_end_accept;
    this->Close();
}

//-------------------------------------------------------------------------

int Sockets::Shutdown(int how /* = SD_BOTH */)
{
    if (shutdown(this->m_socket, how) != 0)
    {
        this->m_error.HResult = WSAGetLastError();
        Sockets::GetLastError(this->m_error.Message, sizeof(this->m_error.Message), this->m_error.HResult);
        RLIB_SetExceptionInfo((&this->m_error));
        return SOCKET_ERROR;
    }
    return STATUS_SUCCESS;
}

//-------------------------------------------------------------------------

Exception *Sockets::GetLastException()
{
    return &this->m_error;
}

//-------------------------------------------------------------------------

bool Sockets::GetLastError(LPVOID buffer, size_t size, int err /* = 0 */)
{
    return Exception::GetLastError(buffer, size, err);
}

//-------------------------------------------------------------------------

bool Sockets::GetLocalHost(char *host, int hostlen)
{
    return (gethostname(host, hostlen) == STATUS_SUCCESS);
}

//-------------------------------------------------------------------------

bool Sockets::GetLocalIpAddress(TCHAR *host, PADDRINFOT *addrinfo)
{
    return (GetAddrInfo(host, NULL, NULL, addrinfo) == STATUS_SUCCESS);
}

//-------------------------------------------------------------------------

void Sockets::FreeLocalIpAddress(PADDRINFOT *addrinfo)
{
    FreeAddrInfo(*addrinfo);
    *addrinfo = NULL;
}

//-------------------------------------------------------------------------

unsigned long Sockets::Ipv4StringToAddress(const char *cp)
{
    return inet_addr(cp);
}

//-------------------------------------------------------------------------

char *Sockets::Ipv4AddressToString(in_addr in)
{
    return inet_ntoa(in);
    //If no error occurs, 
    //inet_ntoa returns a character pointer to a static buffer 
    //containing the text address in standard ".'' notation
}
