/********************************************************************
	Created:	2012/08/05  12:56
	Filename: 	ThreadPool.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_THREADPOOL
#define _USE_THREADPOOL
#include "RLib_ThreadSync.h"
//////////////////////////////////////////////////////////////////////////
namespace System
{
	namespace Threading
	{
		/// <summary>
		/// 表示线程池线程要执行的回调方法
		/// </summary>
		typedef ParameterizedCppThreadStart WaitCallback;
		/// <summary>
		/// 表示线程池工作任务对象
		/// </summary>                                     
		typedef struct export ThreadPoolTask
		{
		public:
			WaitCallback       ExecutionTarget;
			Object             ExecutionParameter;
		public:
			ThreadPoolTask(){};
			ThreadPoolTask(WaitCallback callback, Object param = nullptr)
			{
				this->ExecutionTarget    = callback;
				this->ExecutionParameter = param;
			}
		}ThreadPoolTask;
		/// <summary>
		/// 表示线程池工作线程
		/// </summary>                                     
		typedef struct export ThreadPoolWorker
		{
		private:
			static void worker_workshop(struct ThreadPoolWorker *);
		public:
			Thread            *pThread;
			class ThreadPool  *pManager;
			volatile long      nExit; 
		public:
			RLIB_ClassNewDel;
			ThreadPoolWorker(class ThreadPool *pOwner){
				this->pThread     = new Thread((ParameterizedCppThreadStart)
					worker_workshop, this);
				if (this->pThread == nullptr)
				{
					assert(!"ThreadPoolWorker构造失败");
					return;
				}
				this->pManager = pOwner;
				this->nExit    = 0;
				this->pThread->IsSuppressChangeState = true;
				this->pThread->Start();
			}
			/// <summary>
			/// 销毁工作线程相关资源
			/// </summary>
			~ThreadPoolWorker()
			{
				if (this->pThread != nullptr)
				{
					if(this->pThread->ThreadId != GetCurrentThreadId()) 
					{
						this->pThread->Wait();
					}
					delete this->pThread;
				}
			}
			/// <summary>
			/// 工作线程检查是否需要退出
			/// </summary>
			bool IsAborting(){
				return this->nExit != 0;
			}
			/// <summary>
			/// 销毁接口
			/// </summary>
			static void Dispose(ThreadPoolWorker **obj)
			{
				delete (*obj);
			}
		public:
			/// <summary>
			/// 通知工作线程退出
			/// </summary>
			void Abort(){
				Interlocked::Increment(&this->nExit);
			}
		}ThreadPoolWorker;
		/// <summary>
		/// 线程池结构对象类型
		/// </summary>
		typedef SafeObject<Collections::Generic::Queue<ThreadPoolTask>> TaskQueue;
		typedef SafeObject<Collections::Generic::List<ThreadPoolWorker *, nullptr,
			ThreadPoolWorker::Dispose>> WorkerList;
		typedef SafeObject<Collections::Generic::List<ThreadPoolWorker *>> TempWorkerList;
		/// <summary>
		/// 线程池支持类
		/// </summary>
		class export RLIB_THREAD_SAFE ThreadPool
		{
		private:
			/// <summary>
			/// 线程池任务队列, 工人会自动检索并执行任务
			/// </summary>
			TaskQueue *m_ptasks;
			/// <summary>
			/// 线程池工人列表
			/// </summary>
			WorkerList *m_pworkers;
			/// <summary>
			/// 线程池工人就绪列表
			/// </summary>
			TempWorkerList *m_pavailable_workers;
		protected:
			volatile long m_nMinThreads, m_nMaxThreads, m_nMaxIdleTime, m_nDestroying;
		public:
			ThreadPool(long defaultMinThreads = 4, long defaultMaxThreads = 1024);
			~ThreadPool();
			RLIB_ClassNewDel;
		public:
			//
			// 摘要:
			//     检索线程池是否正在进行销毁工作
			bool IsDestroying()
			{
				return m_nDestroying != 0;
			}
			//
			// 摘要:
			//     检索所有任务是否全部完成(如果同时有新任务被添加可能会返回错误值)
			bool IsTasksComplete()
			{
				if (this->GetTasks() != 0)
				{
					return false;
				} //if
				return this->GetAvailableThreads() == this->GetThreads();
			}
			//
			// 摘要:
			//     执行等待直到所有任务完成(如果同时有新任务被添加可能会中途返回)
			void WaitForTasksComplete(DWORD dwMilliseconds = INFINITE, bool bAlertable = true)
			{
				while(!this->IsTasksComplete())
				{
					if (this->IsDestroying())
					{
						break;
					} //if
					Threading::Thread::SleepEx(dwMilliseconds, bAlertable);
				}
			}
			//
			// 摘要:
			//     设置中止信号以准备进行线程池清理工作
			void Abort();
			//
			// 摘要:
			//     检索由 GetMaxThreads
			//     方法返回的最大线程池线程数和当前活动线程数之间的差值。
			long GetAvailableThreads();
			//
			// 摘要:
			//     检索由 GetMaxThreads
			//     方法返回线程池当前线程数。
			long GetThreads();
			//
			// 摘要:
			//     检索可以同时处于活动状态的线程池请求的数目。所有大于此数目的请求将保持排队状态，直到线程池线程变为可用。
			long GetMaxThreads();
			//
			// 摘要:
			//     检索线程池在新请求预测中维护的空闲线程数。
			long GetMinThreads();
			//
			// 摘要:
			//     检索线程池允许的最大空闲时间(ms)。
			long GetMaxIdleTime();
			//
			// 摘要:
			//     检索线程池滞留(正在排队)的任务数。
			long GetTasks();
			//
			// 摘要:
			//     将方法排入队列并调用 Dispatch 以便执行，并指定包含该方法所用数据的对象。
			//     此方法将在 Dispatch 成功分配任务后被执行。
			//
			// 参数:
			//   callBack:
			//     一个 WaitCallback，表示要执行的方法。
			//
			// 返回结果:
			//     如果此方法成功排队，则为 true；如果未能将该工作项排队，则
			//     返回 false。不考虑 Dispatch 方法执行情况。
			bool AddTask(WaitCallback callBack, Object state = nullptr);
			//
			// 摘要:
			//     将方法排入队列以便执行，并指定包含该方法所用数据的对象。
			//     此方法在有线程池线程变得可用时执行。
			//
			// 参数:
			//   callBack:
			//     System.Threading.WaitCallback，它表示要执行的方法。
			//
			//   state:
			//     包含方法所用数据的对象。
			//
			// 返回结果:
			//     如果此方法成功排队，则为 true；如果未能将该工作项排队，则
			//     返回false
			bool QueueUserWorkItem(WaitCallback callBack, Object state = nullptr);
			//
			// 摘要:
			//     设置可以同时处于活动状态的线程池的请求数目。
			//     所有大于此数目的请求将保持排队状态，直到线程池线程变为可用。
			//
			// 参数:
			//   workerThreads:
			//     线程池中辅助线程的最大数目。
			//
			// 返回结果:
			//     如果更改成功，则为 true；否则为 false。
			bool SetMaxThreads(long workerThreads);
			//
			// 摘要:
			//     设置线程池在新请求预测中维护的空闲线程数。
			//
			// 参数:
			//   workerThreads:
			//     要由线程池维护的新的最小空闲辅助线程数。
			//
			// 返回结果:
			//     如果更改成功，则为 true；否则为 false。
			bool SetMinThreads(long workerThreads);
			//
			// 摘要:
			//     设置线程池允许的最大空闲时间(ms), 超过该时间线程池将缩减线程拥有量。
			//     默认值为8000(8s)
			//
			// 参数:
			//   maxIdleTime:
			//     线程池允许的最大空闲时间。
			bool SetMaxIdleTime(long maxIdleTime);
			//
			// 摘要:
			//     要求线程池尝试为队列中的任务分配工人
			//
			// 返回结果:
			//     如果成功为任务分配工人，则为 true；否则为 false。
			bool Dispatch();
		private:
			//
			// 摘要:
			//     将指定线程从线程池中移除, 不再受线程池管理
			//
			// 参数:
			//   thread:
			//     要移除的线程对象指针。
			//   available:
			//     指示要移除的线程是否在就绪列表中。
			void RemoveWorker(ThreadPoolWorker *pworker, bool available);
		public:
			friend ThreadPoolWorker;
		};
	}
}
#endif //_USE_THREADPOOL