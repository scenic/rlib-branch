/********************************************************************
Created:	2012/02/05  22:41
Filename: 	Lib_Thread.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
//#include "RLib_Thread.h"
#include "RLib_ThreadSync.h"
#include "native/RLib_Native.h"
#define _change_state(p,s) Interlocked::Exchange((long *)(&p->m_state), long(s));
//////////////////////////////////////////////////////////////////////////
using namespace System::IO;
using namespace System::Threading;
/************************************************************************/
/* 全局函数                                                                
/************************************************************************/
DWORD _stdcall Thread::ThreadProcWrapper(Thread *lpMySelf)
{
	assert("线程执行时发生异常" && (lpMySelf != nullptr));

	_change_state(lpMySelf, ThreadState::Running);

    DWORD retval = 0;

    switch (lpMySelf->m_startMode)
    {
	    case CppExpandMode:
		    {
			    ((ParameterizedCppThreadStart)lpMySelf->
					m_startPoint)(lpMySelf->m_parameter);
			    break;
		    }
        case StandardMode:
            {
                retval = ((ParameterizedThreadStart)lpMySelf->
					m_startPoint)(lpMySelf->m_parameter);
                break;
            }
        case ExpandMode:
            {
                retval = ((ExpandThreadStart)lpMySelf->
					m_startPoint)();
                break;
            }
        case VoidMode:
            {
                ((VoidThreadStart)lpMySelf->m_startPoint)();
                break;
            }
        case CppMode:
            {
                ((CppThreadStart)lpMySelf->m_startPoint)();
                break;
            }
    }

    if (lpMySelf->IsBackground)
    {
		lpMySelf->IsSuppressChangeState = true;
        delete lpMySelf;
        goto EXIT;
    }

    _change_state(lpMySelf, ThreadState::Stopped);

EXIT: //结束指定线程
    Thread::ExitThread(retval);
    return retval;
}

/************************************************************************/
/* Thread                                                                
/************************************************************************/
Thread::Thread(HANDLE hThread, DWORD ThreadId, BOOL IsRunning)
{
    this->m_hThread    = hThread;
    this->m_ThreadId   = ThreadId;
    this->m_state      = IsRunning ? ThreadState::Running :
		ThreadState::Suspended;
    this->IsBackground = false;
}

//-------------------------------------------------------------------------

Thread::Thread(ParameterizedThreadStart _ROUTINE, Object lpParameter): 
m_hThread(nullptr), m_ThreadId(0)
{
    this->m_startPoint = _ROUTINE;
    this->m_parameter  = lpParameter;
    this->m_startMode  = StandardMode;
    this->_create();
}

//-------------------------------------------------------------------------

Thread::Thread(ExpandThreadStart _ROUTINE): m_hThread(nullptr), m_ThreadId(0)
{
    this->m_startPoint = _ROUTINE;
    this->m_startMode  = ExpandMode;
    this->_create();
}

//-------------------------------------------------------------------------

Thread::Thread(VoidThreadStart _ROUTINE): m_hThread(nullptr), m_ThreadId(0)
{
    this->m_startPoint = _ROUTINE;
    this->m_startMode  = VoidMode;
    this->_create();
}

//-------------------------------------------------------------------------

Thread::Thread(CppThreadStart _ROUTINE): m_hThread(nullptr), m_ThreadId(0)
{
    this->m_startPoint = _ROUTINE;
    this->m_startMode  = CppMode;
    this->_create();
}

//-------------------------------------------------------------------------

Thread::Thread(ParameterizedCppThreadStart _ROUTINE, Object lpParameter): 
m_hThread(nullptr), m_ThreadId(0)
{
	this->m_startPoint = _ROUTINE;
	this->m_parameter  = lpParameter;
	this->m_startMode  = CppExpandMode;
	this->_create();
}

//-------------------------------------------------------------------------

bool Thread::SetParameter(Object lpParameter)
{
	if (this->m_state != ThreadState::Unstarted)
	{
		assert(!"线程当前状态无法设置参数!");
		return false;
	};
	this->m_parameter = lpParameter;
	return true;
}

//-------------------------------------------------------------------------

Thread::~Thread()
{
#ifdef _DEBUG
	if(this->ThreadId == GetCurrentThreadId() && !this->IsSuppressChangeState) 
	{
		assert(!"If you want to do so, must set IsSuppressChangeState = true"
			"and call ExitThread before thread return");
	}
#endif // _DEBUG
    this->Close();
};

//-------------------------------------------------------------------------

void Thread::_create()
{
    /// <summary>
    /// If the function fails, the return value is NULL
    /// </summary>
    this->m_hThread = CreateRemoteThread(GetCurrentHandle, NULL, 0, 
		(LPTHREAD_START_ROUTINE)ThreadProcWrapper, this, 
		CREATE_SUSPENDED, &this->m_ThreadId);
    if (this->m_hThread == nullptr)
    {
        Exception::GetException(&this->m_error);
        RLIB_SetExceptionInfo((&this->m_error));
        assert(!"创建线程失败");
    }
    this->m_state               = ThreadState::Unstarted;
    this->IsBackground          = false;
	this->IsSuppressChangeState = false;
}

//-------------------------------------------------------------------------

bool Thread::Start()
{
    if (this->m_hThread == nullptr)
    {
        assert(!"指定的线程未成功创建");
        return false;
    }
    return this->Resume();
}

//-------------------------------------------------------------------------

bool Thread::Alert()
{
	if (Thread::AlertThread(this->m_hThread))
	{
		_change_state(this, ThreadState::Running);
		return true;
	} //if
	Exception::GetException(&this->m_error);
	return false;
}

//-------------------------------------------------------------------------

bool Thread::Resume()
{
    if (this->m_state != ThreadState::Unstarted && this->m_state != 
		ThreadState::Suspended)
    {
        assert(!"线程当前状态无法继续!");
		return false;
    }
    NTSTATUS STATUS = NtResumeThread(m_hThread, nullptr);
    if (STATUS != STATUS_SUCCESS)
    {
        this->setException(STATUS);
        RLIB_SetExceptionInfo((&this->m_error));
        assert(!"线程无法继续");
        return false;
    }
	_change_state(this, ThreadState::Running);
    return true;
}

//-------------------------------------------------------------------------

bool Thread::Suspend()
{
    if (this->m_state != ThreadState::Running)
    {
        assert(!"线程当前状态无法暂停!");
		return false;
    }
	NTSTATUS STATUS;
    if ((STATUS = NtSuspendThread(m_hThread, nullptr)) == STATUS_SUCCESS)
    {
		_change_state(this, ThreadState::Suspended);
        return true;
    }
    this->setException(STATUS);
    RLIB_SetExceptionInfo((&this->m_error));
    assert(!"线程无法暂停");
    return false;
}

//-------------------------------------------------------------------------

bool Thread::Abort()
{
    if (this->m_state != ThreadState::Running && this->m_state != 
		ThreadState::Suspended)
    {
        assert(!"线程当前状态无法中止!");
    };
    NTSTATUS STATUS;
    if ((STATUS = NtTerminateThread(m_hThread, 0)) == STATUS_SUCCESS)
    {
		_change_state(this, ThreadState::Aborted);
        return true;
    }
    this->setException(STATUS);
    RLIB_SetExceptionInfo((&this->m_error));
    assert(!"线程无法中止");
    return false;
}

//-------------------------------------------------------------------------

WaitStatus Thread::Wait(int millisecondsTimeout /* = 0 */)
{
    if (this->m_state == ThreadState::Running)
    {
        return WaitHandle::WaitOne(this->m_hThread, TRUE, millisecondsTimeout);
    }
    return WAIT_INVALID_HANDLE;
}

//-------------------------------------------------------------------------

bool Thread::Close()
{
    if (m_hThread != nullptr)
    {
        if (CloseHandle(m_hThread) == TRUE)
        {
            this->m_hThread = NULL;
            return true;
        }
        Exception::GetException(&this->m_error);
        RLIB_SetExceptionInfo((&this->m_error));
    }
    return false;
}

//-------------------------------------------------------------------------

Thread::operator HANDLE()
{
    return m_hThread;
}

//-------------------------------------------------------------------------

const DWORD Thread::GetThreadId()const
{
    return m_ThreadId;
}

//-------------------------------------------------------------------------

bool Thread::GetIsAlive()const
{
    return(this->State == ThreadState::Running || this->State
		== ThreadState::Suspended);
}

//-------------------------------------------------------------------------

ThreadState::Val Thread::GetState()const
{
	return this->m_state;
}

//-------------------------------------------------------------------------

WaitStatus Thread::Sleep(DWORD dwMilliseconds /* = INFINITE */)
{
#ifdef _DEBUG
	assert(this->m_ThreadId == GetCurrentThreadId());
#endif // _DEBUG
	_change_state(this, ThreadState::Sleeped);

	WaitStatus Result = (Thread::SleepEx(dwMilliseconds, true)
		== WAIT_IO_COMPLETION ? WAIT_TIMEOUTED : WAIT_ALERTED);

    _change_state(this, ThreadState::Running);

	return Result;
}

//-------------------------------------------------------------------------

DWORD Thread::SleepEx(DWORD dwMilliseconds /* = INFINITE */, bool bAlertable /* = true */)
{
	LARGE_INTEGER TimeOut;

	if (dwMilliseconds == -1)
	{
		//
		// If Sleep( -1 ) then delay for the longest possible integer
		// relative to now.
		//
		TimeOut.LowPart = 0x0;
		TimeOut.HighPart = 0x80000000;
		goto wait;
	}
	TimeOut.QuadPart  = UInt32x32To64( dwMilliseconds, 10000 );
	TimeOut.QuadPart *= -1;

wait:
	NTSTATUS Status = NtDelayExecution((BOOLEAN)bAlertable, &TimeOut);

	return Status == STATUS_USER_APC ? WAIT_IO_COMPLETION : 0;
}

//-------------------------------------------------------------------------

bool Thread::AlertThread(HANDLE hThread)
{
	NTSTATUS STATUS;
	if ((STATUS = NtAlertThread(hThread)) == STATUS_SUCCESS)
	{
		return true;
	} //if
	RtlSetLastWin32Error(RtlNtStatusToDosError(STATUS));
	return false;
}

//-------------------------------------------------------------------------

LONG Thread::AlertResumeThread(HANDLE hThread)
{
	NTSTATUS STATUS;
	ULONG SuspendCount;
	if ((STATUS = NtAlertResumeThread(hThread, &SuspendCount)) == STATUS_SUCCESS)
	{
		return SuspendCount;
	} //if
	RtlSetLastWin32Error(RtlNtStatusToDosError(STATUS));
	return -1;
}

//-------------------------------------------------------------------------

ThreadPriority Thread::GetPriority()const
{
    ULONG Return;
    THREAD_BASIC_INFORMATION ThreadInfo = 
    {
        0
    };
    NTSTATUS STATUS;
    if ((STATUS = NtQueryInformationThread(this->m_hThread, 
		(THREADINFOCLASS)ThreadPriorityInfo, &ThreadInfo, sizeof(ThreadInfo),
		&Return)) == STATUS_SUCCESS)
    {
        return (ThreadPriority)ThreadInfo.Priority;
    }
    ((Thread *)this)->setException(STATUS);
    RLIB_SetExceptionInfo((&((Thread *)this)->m_error));
    return ErrorPriority;
}

//-------------------------------------------------------------------------

bool Thread::SetPriority(ThreadPriority newPriority)
{
    THREAD_BASIC_INFORMATION ThreadInfo = 
    {
        0
    };
    ThreadInfo.Priority = newPriority;
    NTSTATUS STATUS;
    if ((STATUS = NtSetInformationThread(this->m_hThread, 
		ThreadPriorityInfo, &ThreadInfo, sizeof(ThreadInfo))) == STATUS_SUCCESS)
    {
        return true;
    }
    this->setException(STATUS);
    RLIB_SetExceptionInfo((&this->m_error));
    return false;
}

//-------------------------------------------------------------------------

ThreadException *Thread::GetLastException()
{
    return &this->m_error;
}

//-------------------------------------------------------------------------

bool Thread::GetLastError(LPVOID buffer, size_t size, DWORD err /* = 0 */)
{
    return Exception::GetLastError(buffer, size, err);
}

//-------------------------------------------------------------------------

void Thread::ExitThread(DWORD code /* = 0 */)
{
    AppBase::ExitThread(code);
}

//-------------------------------------------------------------------------

void Thread::setException(NTSTATUS STATUS)
{
    Exception::GetException(&this->m_error, RtlNtStatusToDosError(STATUS));
}
