/********************************************************************
	Created:	2012/02/06  19:40
	Filename: 	Lib_Array.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_ARRAY
#define _USE_ARRAY
#include "RLib_MemoryPool.h"
#include <search.h>
//////////////////////////////////////////////////////////////////////////
#define RLIB_ARRAY_LENGTH sizeof(LONG) * 4
//#define RLIB_ARRAY_SYNC   
#ifdef RLIB_ARRAY_SYNC
    #define LockArray   this->SyncRoot.Enter()
    #define UnLockArray this->SyncRoot.Leave()
#else
    #define LockArray
    #define UnLockArray
#endif // RLIB_ARRAY_SYNC
//实现for each拓展
#define foreach(a, b)      auto *a = &b[0];for(int i = 0; i < (int)b.Length; ++i, a = (i != (int)b.Length) ? &b[i] : nullptr)
#define foreachEx(a, b, i) auto *a = &b[0];for(int i = 0; i < (int)b.Length; ++i, a = (i != (int)b.Length) ? &b[i] : nullptr)
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 包含接口和类，这些接口和类定义各种对象（如列表、队列）的集合
	/// </summary>
	namespace Collections
	{
		/// <summary>
		/// 包含定义泛型集合的接口和类，用户可以使用泛型集合来创建强类型集合
		/// 这种集合能提供比非泛型强类型集合更好的类型安全性和性能
		/// </summary>
		namespace Generic
		{
			/// <summary>
			/// Allocator defines the memory allocator interface
			/// </summary>
			class export RLIB_THREAD_SAFE Allocator
			{
			public:
				Allocator(){};
				virtual ~Allocator(){}
				/// <summary>
				/// This method allocates a block of memory
				/// </summary>
				virtual void *Alloc(ULONG cb)
				{
					return RLIB_GlobalAlloc(cb);
				}
				/// <summary>
				/// This method frees a previously allocated block of memory
				/// </summary>
				virtual void Free(void *pv)
				{
					RLIB_GlobalCollect(pv);
				}
				/// <summary>
				/// This method changes the size of a previously allocated memory block
				/// </summary>
				virtual void* Realloc(void* pv, ULONG cb)
				{
					return AppBase::GetUsingPool()->ReAlloc(pv, cb);
				}
			};
			/// <summary>
			/// 默认使用的接口
			/// </summary>
			static Allocator PublicDefaultAllocator;
			/// <summary>
			/// 公开一种比较两个对象的方法
			/// </summary>
			template <class R> class Comparable
			{
			public:
				/// <summary>
				/// 该方法为升序比较
				/// </summary>
				static int Comparer(const R *p1, const R *p2)
				{
					if (*p1 > *p2)return 1;
					if (*p1 < *p2)return -1;
					return 0;
				}
				/// <summary>
				/// 该方法为降序比较
				/// </summary>
				static int ComparerDesc(const R *p1, const R *p2)
				{
					if (*p1 > *p2)return -1;
					if (*p1 < *p2)return 1;
					return 0;
				}
				/// <summary>
				/// 表示对象比较方法
				/// </summary>
				typedef int (*IComparable)(const R*, const R*);
			};
			/// <summary>
			/// 公开一种析构对象的方法
			/// </summary>
			template <class R>
			class Disposable
			{
			public:
				static void DefaultDispose(R *obj)
				{
#ifdef _DEBUG
					//在代码对基元类型另外的未引用的参数调用析构函数时引发 C4100
					//这是 Visual C++ 编译器的限制, 可忽略
					obj = obj;
#endif // _DEBUG
					obj->~R();
				}
				/// <summary>
				/// 表示对象比较方法
				/// </summary>
				typedef void (*IDisposable)(R *);
			};
			/// <summary>
			/// 提供一些方法，用于创建、处理、搜索数组并对数组进行排序，从而充当所有数组的基类
			/// </summary>
			template <class R, Allocator *allocator = nullptr, typename 
				Disposable<R>::IDisposable disposer = Disposable<R>::DefaultDispose>
			class Array
			{
			protected:
				/// <summary>
				/// 记录 Array<Of R> 中元素存储地址
				/// </summary>
				R *m_pItems; 
			private:
				inline Allocator *_get_allocator()
				{
					return(allocator == nullptr ? (&PublicDefaultAllocator) : allocator);
				}
				/// <summary>
				/// 通过调用构造函数, 初始化元素
				/// </summary>
				inline void _init_item(R *pR, const R&item)
				{
					RLIB_InitClass(pR, R(item));
				}
				/// <summary>
				/// 通过调用析构函数, 删除元素
				/// </summary>
				inline void _del_item(R *pR)
				{
					disposer(pR);
				}
				/// <summary>
				/// 将指定位置下一位置内存前移
				/// </summary>
				inline void _copy_forward(LONG begin)
				{
					if ((this->Length - begin) <= 0) return;
					System::IO::Memory::memcpy(&this->m_pItems[begin], &this->m_pItems[begin + 1],
						sizeof(R) * (this->Length - begin));
				}
				/// <summary>
				/// 将指定位置内存后移
				/// </summary>
				inline void _copy_backward(LONG begin)
				{
					if ((this->Length - begin) <= 0) return;
					System::IO::Memory::memcpy(&this->m_pItems[begin + 1], &this->m_pItems[begin],
						sizeof(R) * (this->Length - begin));
				}
				/// <summary>
				/// 分配指定数量元素所需内存
				/// </summary>
				void _init(LONG length)
				{
					this->m_pItems  = (R *)(this->_get_allocator()
						->Alloc(sizeof(R) * length));
					assert(this->m_pItems != nullptr);
					this->MaxLength = length;
					this->Length    = 0;
				}
				/// <summary>
				/// 增加分配指定数量元素所需内存
				/// </summary>
				void _more(LONG length)
				{
					R *cache_ptr = (R *)this->_get_allocator()->Realloc(this->m_pItems, 
						sizeof(R) * (this->MaxLength + length));
					if(cache_ptr == nullptr)
					{
						assert(!"为元素分配内存失败");
						return;
					}
					this->m_pItems   = cache_ptr;
					this->MaxLength += length;
				}
			public:
				/// <summary>
				/// 获取 Array<Of R> 的所有元素的总数
				/// </summary>
				LONG Length; 
				/// <summary>
				/// 获取在不改变 Array<Of R> 内存结构的情况下所能容纳的最大元素数
				/// </summary>
				LONG MaxLength; 
#ifdef RLIB_ARRAY_SYNC
				/// <summary>
				/// 用于同步 List<Of R> 访问的对象
				/// </summary>
				Threading::CriticalSection SyncRoot;
#endif // RLIB_ARRAY_SYNC
			public:
				/// <summary>
				/// 初始化并预置指定个数的元素
				/// </summary>
				Array(LONG length = RLIB_ARRAY_LENGTH)
				{
					this->_init(length);
				}
				/// <summary>
				/// 使用现有数组初始化 Array<Of R>
				/// </summary>
				Array(const R items[], LONG length)
				{
					this->_init(length);
					for(length -= 1; length >=0; length --)
					{
						this->_init_item(&this->m_pItems[length], items[length]);
						this->Length += 1;
					}
				};
				/// <summary>
				/// 清理 Array<Of R>
				/// </summary>
				~Array()
				{
					this->Clear();
					this->_get_allocator()->Free(this->m_pItems);
				}
			public:
#ifdef RLIB_ARRAY_SYNC
				/// <summary>
				/// Begin to synchronize access to the Array
				/// </summary>
				void Lock() const
				{
					LockArray;
				}
				/// <summary>
				/// End to synchronizing access to the Array
				/// </summary>
				void UnLock() const
				{
					UnLockArray;
				}
				/// <summary>
				/// 安全获取 List<Of R> 中实际包含的元素数
				/// </summary>
				LONG GetSafeLength() const
				{
					LockArray;
					auto length = this->Length;
					UnLockArray;
					return length;
				}
				/// <summary>
				/// 安全获取 List<Of R> 中实际包含的元素数
				/// </summary>
				__declspec(property(get = GetSafeLength)) LONG SafeLength;
#endif // RLIB_ARRAY_SYNC
				/// <summary>
				/// 获取 Array<Of R> 元素列表指针
				/// </summary>
				R *GetObjectPtr()/*operator R *()*/
				{
					return this->m_pItems;
				}
				/// <summary>
				/// 赋值运算符 复制对象
				/// </summary>
				Array<R> &operator=(const Array<R> &obj)
				{
					this->Clear();
					foreach(pitem, obj)
					{
						this->Add(*pitem);
					}
					return (Array<R> &)*this;
				}
				/// <summary>
				/// 拓展运算符 添加对象
				/// </summary>
				Array& operator+=(const R &item)
				{
					this->Add(item);
					return (Array<R> &)*this;
				}
				/// <summary>
				/// 添加指定元素到 Array<Of R> 并返回该元素索引
				/// 如果添加多次元素, 返回首个元素索引
				/// </summary>
				LONG Add(const R &item, LONG count = 1)
				{
					if (count < 1) return -1;
					LockArray;
					if ((this->Length + count) > this->MaxLength)
					{ //如果空间不足
						_more(((this->Length + count) - this->MaxLength));
					}
					LONG index = this->Length;
					while(count >= 1)
					{
						count--;
						this->_init_item(&this->m_pItems[this->Length], item);
						this->Length += 1;
					}
					UnLockArray;
					return index;
				}		
				/// <summary>
				/// 移除 Array<Of R> 中所有数组元素
				/// </summary>
				void Clear()
				{
					LockArray;
					while(this->Length > 0)
					{
						this->Length--;
						this->_del_item(&this->m_pItems[this->Length]);
					}
					UnLockArray;
				}
				/// <summary>
				/// 获取 Array<Of R> 中指定位置的元素
				/// @warning 方法不进行参数检查
				/// </summary>
				R &operator [](LONG index) const{ return this->GetValue(index); } 
				/// <summary>
				/// 获取 Array<Of R> 中指定位置的元素
				/// @warning 方法不进行参数检查
				/// </summary>
				R &GetValue(LONG index) const
				{ 
					assert(((index >= 0) && (index < this->Length)) || !"操作溢出"); 
					return (R &)this->m_pItems[index]; 
				} 
				/// <summary>
				/// 确定某元素是否在当前 Array<Of R> 中
				/// </summary>
				bool Contains(const R &item) const
				{
					return this->IndexOf(item) != -1;
				};
				/// <summary>
				/// 搜索指定的对象，并返回整个 Array<Of R> 中第一个匹配项的索引
				/// </summary>
				/// <returns>失败返回-1</returns>
				LONG IndexOf(const R &item, LONG begin = 0, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer) const
				{
					if (begin < 0 || begin >= this->Length) return -1;
					LockArray;
					LONG length = this->Length - begin;
					R * pfound = (R *)_lfind(&item, &this->m_pItems[begin], (unsigned int *)&length, sizeof(R), 
						(int (*)(const void*, const void*))comparer);
					if (pfound == nullptr)
					{
						length = -1;
					}
					else
					{
						length = LONG(pfound - this->m_pItems);
					}
					UnLockArray;
					return length;
				}
				/// <summary>
				/// 搜索指定的对象，并返回整个 Array<Of R> 中最后一个匹配项的索引
				/// </summary>
				/// <returns>失败返回-1</returns>
				LONG LastIndexOf(const R &item, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer) const
				{
					LONG prev = -1, that;
                    while((that = this->IndexOf(item, prev + 1, comparer)) != -1)
					{
						prev = that;
					}
					return prev;
				}
				/// <summary>
				/// 使用特定的 IComparable 实现, 对 Array<Of R> 进行排序
				/// </summary>
				void Sort(LONG begin = 0, LONG count = 0, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer)
				{
					if (begin < 0 || begin >= this->Length || count < 0) return;
					LockArray;
					LONG length = this->Length - begin;
					if (count == 0 || count > length) count = length;
					qsort(&this->m_pItems[begin], count, sizeof(R), (int (*)(const void*, const void*))comparer);
					UnLockArray;
				}
				/// <summary>
				/// 使用由 Array<Of R> 中每个元素和指定的对象实现的 IComparable 接口, 搜索特定元素
				/// @warning 如果尚未排序或者包含重复值, 则结果未知
				/// </summary>
				/// <returns>失败返回-1</returns>
				LONG BinarySearch(const R &item, LONG begin = 0, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer) const
				{
					if (begin < 0 || begin >= this->Length) return -1;
					R * pfound = (R *)bsearch(&item, &this->m_pItems[begin], this->Length - begin, sizeof(R), 
						(int (*)(const void*, const void*))comparer);
					if (pfound == nullptr) return -1;
					return pfound - this->m_pItems;
				}
				/// <summary>
				/// 移除特定元素的第一个匹配项
				/// </summary>
				void Remove(const R &item, typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer)
				{
				    this->RemoveAt(this->IndexOf(item, 0, comparer));
                }
				/// <summary>
				/// 移除特定元素的所有匹配项
				/// </summary>
				void RemoveAll(const R &item)
				{
					LONG prev = 0, that;
                    while((that = this->IndexOf(item, prev)) != -1)
					{
						prev = that;
					    this->RemoveAt(that);
					}
                }
				/// <summary>
				/// 插入新元素到指定位置
				/// </summary>
				void Insert(LONG index, const R &item)
				{
					if (index < 0) return;
		            if(index >= this->Length) { this->Add(item);return; }
					LockArray;
					if ((this->Length + 1) > this->MaxLength) _more(RLIB_ARRAY_LENGTH);
	                _copy_backward(index);
				    this->Length++;
                    _init_item(&this->m_pItems[index], item);
					UnLockArray;
				}
				/// <summary>
				/// 移除指定索引处的元素
				/// </summary>
				void RemoveAt(LONG index)
				{
					if (index < 0 || index >= this->Length) return;
					LockArray;
					_del_item(&this->m_pItems[index]);
					this->Length--;
					_copy_forward(index);
					UnLockArray;
				}
				/// <summary>
				/// 删除 Array<Of R> 中所有重复的相邻元素
				/// </summary>
				void Unique(typename Comparable<R>::IComparable comparer = Comparable<R>::Comparer)
				{	
					LockArray;
					// erase each element matching previous
					R *_Pnode = this->m_pItems;
					LONG _k;//需要删除的元素个数
					while (_Pnode < &this->m_pItems[this->Length])
					{
						_k = 0;
						while ((&_Pnode[_k] < &this->m_pItems[this->Length - 1]) && 
							comparer(&_Pnode[_k], &_Pnode[_k + 1]) == 0)
						{	//计算连续且重复的个数
							_k += 1;
						}
						_Pnode += 1;
						if (_k != 0)
						{
							//析构对象
							for(LONG _i = 0; _i < _k; _i++) _del_item(&_Pnode[_i]);
							System::IO::Memory::memcpy(_Pnode, _Pnode + _k,
								sizeof(R) * (((R *)&this->m_pItems[this->Length]) - ((R *)(_Pnode + _k))));
							this->Length -= _k;
						}
						// no match, advance
					}
					UnLockArray;
				}
				/// <summary>
				/// 将指定范围中元素的顺序反转
				/// </summary>
				/// <param name="order">从0开始的顺序位置</param>
				/// <param name="count">交换次数, 默认0为全部</param>
				void Reverse(LONG order = 0, int count = 0)
				{
					if (order < 0){
						order = 0;
					}
					else if (order >= (this->Length - 1)){
						return;
					}
					if (count <= 0 || count > this->Length){
						count = this->Length / 2;
					}
					LockArray;
					{
						LONG last_order = (this->Length - 1);
                        #pragma warning(disable:4127)
						if (sizeof(R) == sizeof(int))
						{
							int temp;
							int *pPtr = (int *)this->m_pItems; 
							while(order != last_order)
							{
								temp             = pPtr[order];
								pPtr[order]      = pPtr[last_order];
								pPtr[last_order] = temp;
								order++;
								last_order--;
							}
							goto work_done;
						} //if
						R *temp = (R *)this->_get_allocator()->Alloc(sizeof(R)); 
						while(order != last_order)
						{
							IO::Memory::memcpy(temp, this->m_pItems[order], sizeof(R));
							IO::Memory::memcpy(this->m_pItems[order], this->m_pItems[last_order], sizeof(R));
							IO::Memory::memcpy(this->m_pItems[last_order], temp, sizeof(R));
							order++;
							last_order--;
						}
					}
work_done:
					UnLockArray;
				}
			};
		}
	}
}
#endif // _USE_ARRAY
