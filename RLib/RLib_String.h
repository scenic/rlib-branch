/********************************************************************
	Created:	2012/04/22  8:57
	Filename: 	RLib_String.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_STRING
#define _USE_STRING
#include "RLib_Text.h"

#ifdef _UNICODE
#define TLEN(str)  wcslen(str)
#define _tcspace   iswspace
#else
#define TLEN(str)  strlen(str)
#define _tcspace   isspace
#endif 
#define TSIZE(len) ((len + 1) * sizeof(TCHAR))
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 表示空值
	/// </summary>
	typedef enum Nullable
	{
		Nothing
	};
	/// <summary>
	/// 前向声明
	/// </summary>
	class export StringArray;
	/// <summary>
	/// 表示非线程安全的文本类
	/// </summary>
	class export String
	{
	private:
		/// <summary>
		/// String 内部采用的存储结构
		/// </summary>
		typedef struct StringTag
		{
			//内部字符串指针
			TCHAR  *m_pstr; 
			//引用计数, 约定0为常量引用、1为可写状态、其它为存在引用关系
			UINT   m_ref;  
			//字符串长度
			UINT   m_len;  
			//字符串缓冲区大小
			UINT   m_size;    
			//内部使用
			LPSTR  m_pchar;
			int    m_pchar_size;
			//内部使用
			LPWSTR m_pwchar;  
			int    m_pwchar_size;
			//指示字符串长度是否需要检查
			bool   m_check;   
		private: //内部结构,禁止实例化
			StringTag();
			~StringTag();
		public:
			//待以后拓展使用
			void DecReference(){};
		}*pTag;
	private:
		pTag m_ptag;                        //String 核心信息结构
	private:
		static void *fastalloc(ULONG bytes);//分配指定字节内存
		void prepare(UINT Len);             //为 String 准备指定长度内存, 如是常量引用则将立即拷贝
		void collect_auto();                //实现自动释放m_ptag->m_pchar及m_ptag->m_pwchar(如果存在)
		TCHAR &getTCHAR(int index)const;    //获取指定位置字符
		void update(LPTSTR, UINT, UINT Length = 0);//更新 String 内部记录, 注意第一个参数必须由 fastalloc 分配
	public:
		/// <summary>
		/// 初始化类, 此时 IsNull() 返回true
		/// </summary>
		String();  
		/// <summary>
		/// 初始化类, 此时 IsNull() 返回true
		/// </summary>
		String(Nullable);  
		/// <summary>
		/// 初始化类指定长度(in TCHARs)的字符串
		/// @warning 方法 GetLength() 仍然返回0
		/// </summary>
		String(UINT);
		/// <summary>
		/// 初始化类并拷贝指定长度的字符串到本地
		/// </summary>
		String(TCHAR *);      
		/// <summary>
		/// 初始化类并引用指定长度的字符串(如果编译环境非Unicode), 必须确保对应字符串不会被修改
		/// @warning String(String.GetConstData()); 或者 String((LPCTSTR)String); 将引发错误
		/// </summary>
		String(const CHAR *);  
		/// <summary>
		/// 初始化类并引用指定长度的字符串(如果编译环境为Unicode), 必须确保对应字符串不会被修改
		/// @warning String(String.GetConstData()); 或者 String((LPCTSTR)String); 将引发错误
		/// </summary>
		String(const WCHAR *);  
		/// <summary>
		/// 初始化类, 引用另一 String实例
		/// </summary>
		String(const String &); 
		/// <summary>
		/// 根据引用计数决定是否进行析构操作
		/// </summary>
		~String();
		RLIB_ClassNewDel;

		/// <summary>
		/// 详见 GetData()
		/// </summary>
		operator TCHAR *();    
		/// <summary>
		/// 详见 GetConstData()
		/// </summary>
		operator const TCHAR *() const; 
		/// <summary>
		/// 方法返回内部字符串指针并保证可读写, 对其修改将影响到 String 实例
		/// @warning 该缓冲区具有固定大小并可由 GetSize() 方法获得
		/// </summary>
		LPTSTR GetData();     
		/// <summary>
		/// 返回只读字符串指针, 不应该修改该方法返回的字符串内容
		/// </summary>
		LPCTSTR GetConstData() const;  
		/// <summary>
		/// 返回可安全用于显示(如MsgBox)的只读字符串指针
		/// </summary>
		LPCTSTR GetSafeData() const;  

		/// <summary>
		/// 要求 String 准备容纳指定长度字符串的内存
		/// </summary>
		void PrepareMemory(UINT length)
		{
			this->prepare(length);
		}
		/// <summary>
		/// 防止与构造函数混淆
		/// </summary>
		const String& operator=(UINT)
		{
			assert(!"use PrepareMemory instead!");
			return *this;
		}
		/// <summary>
		/// 拓展语法( String = Nothing;或者 String = null; ), 允许对该实例 String 进行内存释放
		/// @warning 方法仅释放字符串内存, 不会导致 String 被析构
		/// </summary>
		const String& operator=(Nullable);  
		/// <summary>
		/// 将字符串赋给 String, 该方法将立即进行拷贝操作
		/// </summary>
		const String& operator=(TCHAR *);
		/// <summary>
		/// 将字符串赋给 String, 该方法不会立即进行拷贝操作, 必须保证字符串一直可读
		/// 如果编译环境为"Unicode 字符集", 则该方法将自动进行编码转换(默认为GB2312->UTF-16)
		/// @warning String = String.GetConstData(); 或者 String = (LPCTSTR)String; 将引发错误
		/// </summary>
		const String& operator=(const CHAR *);
		/// <summary>
		/// 将字符串赋给 String, 该方法不会立即进行拷贝操作, 必须保证字符串一直可读
		/// 如果编译环境为"多字节字符集", 则该方法将自动进行编码转换(默认为UTF-16->GB2312)
		/// @warning String = String.GetConstData(); 或者 String = (LPCTSTR)String; 将引发错误
		/// </summary>
		const String& operator=(const WCHAR *);
		/// <summary>
		/// 将目标 String 赋给当前 String, 方法将进行引用操作以优化速度
		/// </summary>
		const String& operator=(const String &); 

		/// <summary>
		/// 获取字符串缓冲区大小(in Bytes)
		/// </summary>
		__declspec(property(get = GetSize)) const UINT Size;
		/// <summary>
		/// 获取字符串长度(in TCHARs, 不包括结尾'\0')
		/// </summary>
		__declspec(property(get = GetLength)) const UINT Length;
		/// <summary>
		/// 获取字符串可读取大小(不包含结尾'\0'的大小)
		/// </summary>
		__declspec(property(get = GetCanReadSize)) const UINT CanReadSize;
		/// <summary>
		/// 返回字符串缓冲区大小(in Bytes)
		/// </summary>
		UINT GetSize() const;  
		/// <summary>
		/// 返回字符串长度(in TCHARs, 不包括结尾'\0')
		/// </summary>
		UINT GetLength() const;   
		/// <summary>
		/// 返回字符串可读取大小
		/// </summary>
		UINT GetCanReadSize() const; 

		/// <summary>
		/// 创建一个与指定的 String 具有相同值的 TCHAR * 的新副本,
		/// 该副本大小可由 方法 GetCanReadSize() 获得, 必须手动调用 Collect() 方法释放.
		/// </summary>
		TCHAR *c_str() const;              
		/// <summary>
		/// 如果编译环境非Unicode,该方法直接调用 c_str() ,反之执行转换
		/// 返回的数据无需手动释放, 类析构或者下次调用该方法时将自动释放
		/// </summary>
		LPSTR ToMultiByte() const; 
		/// <summary>
		/// 获取 ToMultiByte 方法返回的字符串大小, in bytes
		/// @warning 调用 ToMultiByte 方法之前调用此方法才能正确获得大小
		/// </summary>
		int GetMultiByteSize() const;
		/// <summary>
		/// 如果编译环境为Unicode,该方法直接调用 c_str() ,反之执行转换		
		/// 返回的数据无需手动释放, 类析构或者下次调用该方法时将自动释放
		/// </summary>
		/// <param name="OutSize">不包括 \0 结尾符大小, in bytes</param>
		LPWSTR ToWideChar() const;  
		/// <summary>
		/// 获取 ToWideChar 方法返回的字符串大小, in bytes
		/// @warning 调用 ToWideChar 方法之前调用此方法才能正确获得大小
		/// </summary>
		int GetWideCharSize() const;
		/// <summary>
		/// 取消对 ToMultiByte/ToWideChar 方法返回的数据进行自动释放
		/// 因此, 必须手动调用 Collect 方法来释放内存
		/// </summary>
		bool SuppressFinalize(LPVOID ptr);
		/// <summary>
		/// 获取当前 String 对象中位于指定字符位置(从0开始)的字符
		/// @wearing 方法不进行参数检查, 请保证不会溢出
		/// </summary>
		TCHAR GetAt(int) const; 
		/// <summary>
		/// 设置当前 string 对象中位于指定字符位置(从0开始)的字符
		/// </summary>
		void SetAt(int, TCHAR);

		/// <summary>
		/// 将指定数目的字符复制到 String 实例, len为零将自动获取长度
		/// </summary>
		void  Copy(const CHAR *, UINT len = 0);   
		/// <summary>
		/// 将指定数目的字符复制到 String 实例, len为零将自动获取长度
		/// </summary>
		void  Copy(const WCHAR *, UINT len = 0); 
		/// <summary>
		/// 尝试将指定数目的字符复制到 String 实例, 该方法不会分配新的内存, 空间不足返回false
		/// </summary>
		bool TryCopy(LPCTSTR, UINT len = 0);   
		/// <summary>
		/// 追加指定长度字符串到此 String 实例, len为零将自动获取长度
		/// </summary>
		String &Append(const TCHAR *, UINT len = 0);  
		/// <summary>
		/// 追加指定长度 String 到当前实例, len为零将自动获取长度
		/// </summary>
		String &Append(const String &, UINT len = 0); 
		/// <summary>
		/// 将指定的 String 中的每个格式项替换为相应对象的值的文本等效项
		/// </summary>
		bool __cdecl Format(LPCTSTR pstrFormat, ...); 

		/// <summary>
		/// 与指定的字符串进行比较(考虑其大小写), 并返回一个整数, 指示二者在排序顺序中的相对位置
		/// </summary>
		int Compare(const TCHAR *) const;
		/// <summary>
		/// 与指定的字符串进行比较(忽略其大小写), 并返回一个整数, 指示二者在排序顺序中的相对位置
		/// </summary>
		int CompareNoCase(const TCHAR *) const;
		/// <summary>
		/// 使 String 成为空字符串
		/// </summary>
		void Empty();
		/// <summary>
		/// 获取字符串是否为引用类型(方法GetConstData()返回的指针确实不可写,否则将可能使程序崩溃) 
		/// </summary>
		bool IsConst() const; 
		/// <summary>
		/// 判断是否为空字符串
		/// </summary>
		bool IsEmpty() const;
		/// <summary>
		/// 判断是否为空字符串指针(nullptr)
		/// @warning 该方法可能导致内存非法访问(内存申请失败的情况下)
		/// </summary>
		bool IsNull() const;
		/// <summary>
		/// 判断是否为空字符串或空字符串指针(nullptr)
		/// @warning 该方法可能导致内存非法访问(内存申请失败的情况下)
		/// </summary>
		bool IsNullOrEmpty() const;
		/// <summary>
		/// 判断字符串实例是否为空引用(内存申请失败时引发)
		/// 该方法不会导致内存非法访问, 可用来判断实例是否可用
		/// </summary>
		bool IsNullReference() const;

		/// <summary>
		/// 确定 String 实例的开头是否与指定的字符串匹配
		/// </summary>
		bool StartsWith(TCHAR) const; 
		/// <summary>
		/// 确定 String 的实例的末尾是否与指定的字符串匹配
		/// </summary>
		bool EndsWith(TCHAR) const;       

		/// <summary>
		/// 报告指定字符在此字符串中的第一个匹配项的索引,如果未找到该字符串，则返回 -1
		/// </summary>
		int IndexOf(TCHAR, unsigned int begin = 0) const;     
		/// <summary>
		/// 报告指定字符在在此实例中的最后一个匹配项的索引位置,如果未找到该字符串，则返回 -1
		/// </summary>
		int LastIndexOf(TCHAR) const; 
		/// <summary>
		/// 报告 string 或一个或多个字符在此字符串中的第一个匹配项的索引,如果未找到该字符串，则返回 -1
		/// </summary>
		int IndexOf(const TCHAR *, unsigned int begin = 0) const;   
		/// <summary>
		/// 报告 string 或一个或多个字符在在此实例中的最后一个匹配项的索引位置,如果未找到该字符串，则返回 -1
		/// </summary>
		int LastIndexOf(const TCHAR *) const;
		/// <summary>
		/// 报告 string 或一个或多个字符在此字符串中的第一个匹配项的索引,
		/// </summary>
		/// <summary>
		/// 并返回末位索引(加上指定字符串长度),如果未找到该字符串，则返回 -1
		/// </summary>
		int IndexOfR(const TCHAR *, unsigned int begin = 0) const;   
		/// <summary>
		/// 报告 string 或一个或多个字符在在此实例中的最后一个匹配项的索引位置,
		/// 并返回末位索引,如果未找到该字符串，则返回 -1
		/// </summary>
		int LastIndexOfR(const TCHAR *) const;

		/// <summary>
		/// 计算指定文本出现的次数
		/// </summary>
		int Count(const TCHAR *, unsigned int begin = 0) const;   
		/// <summary>
		/// 返回首尾匹配项(非贪婪)的中间文本, 首项必须存在才能匹配成功, 尾项不存在则直接返回以首项开始的文本
		/// </summary>
		String Match(const TCHAR *, const TCHAR *, unsigned int begin = 0) const;
		/// <summary>
		/// 返回首尾匹配项(非贪婪)的中间文本, 并严格要求首尾匹配
		/// </summary>
		String MatchStrict(const TCHAR *, const TCHAR *, unsigned int begin = 0) const;   
		/// <summary>
		/// 返回首尾匹配项(非贪婪)的中间文本数组
		/// </summary>
		StringArray *MatchAll(const TCHAR *, const TCHAR *, unsigned int begin = 0) const;   
		/// <summary>
		/// 返回首尾匹配项(非贪婪)的中间文本数组
		/// </summary>
		StringArray *MatchAllStrict(const TCHAR *, const TCHAR *, unsigned int begin = 0) const;   

		/// <summary>
		/// 返回此 String 颠倒字符次序后的副本
		/// </summary>
		String Reverse() const; 
		/// <summary>
		/// 返回此 String 转换为小写形式的副本
		/// </summary>
		String ToLower() const; 
		/// <summary>
		/// 返回此 String 转换为大写形式的副本
		/// </summary>
		String ToUpper() const; 
		/// <summary>
		/// 创建当前 String 的浅表副本
		/// </summary>
		String ToString() const;

		/// <summary>
		/// 将指定长度字符串(len为零将自动获取)拼到当前 String 末尾并返回新实例
		/// 方法不会影响当前实例
		/// </summary>
		String Concat(const TCHAR *, UINT len = 0) const;  
		/// <summary>
		/// 将指定长度 String (len为零将自动获取)拼到当前 String 末尾并返回新实例
		/// 方法不会影响当前实例
		/// </summary>
		String Concat(const String &, UINT len = 0) const; 
		/// <summary>
		/// 从当前 String 对象移除所有前导指定字符和尾部指定字符
		/// </summary>
		String Trim(TCHAR c = 0);    
		/// <summary>
		/// 从当前 String 对象移除数组中指定的一组字符的所有前导指定字符
		/// </summary>
		String TrimStart(TCHAR c = 0);  
		/// <summary>
		/// 从当前 String 对象移除数组中指定的一组字符的所有尾部指定字符
		/// </summary>
		String TrimEnd(TCHAR c = 0);    
		/// <summary>
		/// 填充当前 String 使用指定字符
		/// </summary>
		String Fill(TCHAR c);    
		/// <summary>
		/// 从此实例检索子字符串,子字符串从指定的字符位置(从0开始)开始且具有指定的长度
		/// </summary>
		String Substring(UINT, UINT len = 0) const;         

		/// <summary>
		/// 返回一个新字符串,其中当前实例中出现的 n 个指定字符串都替换为另一个指定的字符串,若n为0则替换全部
		/// </summary>
		String Replace(const TCHAR *, const TCHAR *, UINT n = 0, UINT begin = 0, UINT *replace_count = nullptr) const; 
		/// <summary>
		/// 返回的字符串数组包含此实例中的子字符串（由指定字符串的元素分隔）
		/// 返回的指针不需要时必须delete
		/// </summary>
		StringArray *Split(const TCHAR *) const; 

		/// <summary>
		/// 获取或设置指定位置的字符(不进行溢出检查)
		/// </summary>
		TCHAR &operator[] (int) const; 

		String operator+(const TCHAR *) const;
		String operator+(const String &) const;
		const String& operator+=(const TCHAR c);
		const String& operator+=(const TCHAR *);
		const String& operator+=(const String &);
		bool operator == (LPCTSTR) const;
		bool operator == (const String &) const;
		bool operator != (LPCTSTR) const;
		bool operator != (const String &) const;
		bool operator <= (LPCTSTR) const;
		bool operator <= (const String &) const;
		bool operator <  (LPCTSTR) const;
		bool operator <  (const String &) const;
		bool operator >= (LPCTSTR) const;
		bool operator >= (const String &) const;
		bool operator >  (LPCTSTR) const;
		bool operator >  (const String &) const;

	public:
		/// <summary>
		/// 提供一种对齐后的字符串拷贝方法
		/// </summary>
		static void FastStringCopy(TCHAR * destmem, const TCHAR * srcmem, int Count);
		/// <summary>
		/// 转换为多字符格式(ANSI), 必须手动调用 Collect() 方法以释放内存
		/// </summary>
		/// <param name="out_size">不包括 \0 结尾符大小, in bytes</param>
		static LPSTR  ConvertToMultiByte(LPCWSTR, int length = 0, int *out_size = nullptr);
		/// <summary>
		/// 转换为宽字符格式(Unicode), 必须手动调用 Collect() 方法以释放内存
		/// </summary>
		/// <param name="out_size">不包括 \0 结尾符大小, in bytes</param>
		static LPWSTR ConvertToWideChar(LPCSTR, int length = 0, int *out_size = nullptr); 
		/// <summary>
		/// 分配指定长度的字符串(in TCHARs, 无须考虑结尾'\0', 方法将自动追加)
		/// </summary>
		static TCHAR *FastAllocateString(int length);
		/// <summary>
		/// String 内存统一回收接口
		/// </summary>
		static void Collect(Object);
	public:
		friend class StringArray;
	};
	/// <summary>
	/// String 数组
	/// </summary>
    class export StringArray:public System::Collections::Generic::Array<String>
	{
	public:
		/// <summary>
		/// 初始化具有指定元素数的数组类
		/// </summary>
		StringArray(UINT length = RLIB_ARRAY_LENGTH):Array(length){};
		/// <summary>
		/// 使用现有数组初始化 
		/// </summary>
		StringArray(String items[], LONG length):Array(items, length){};
		/// <summary>
		/// 清理 StringArray
		/// </summary>
		~StringArray(){};
		RLIB_ClassNewDel;
	public:
		/// <summary>
		/// 串联字符串数组的所有元素,其中在每个元素之间使用指定的分隔符(允许为空)
		/// </summary>
		String Join(const String); 
	};
};
typedef System::String string;
//////////////////////////////////////////////////////////////////////////
#endif