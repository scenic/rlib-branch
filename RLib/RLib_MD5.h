/********************************************************************
	Created:	2012/04/14  20:13
	Filename: 	RLib_MD5.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_MD5
#define _USE_MD5
#include "RLib_Obama64.h"
//////////////////////////////////////////////////////////////////////////
namespace System
{
	namespace Security
	{
		namespace Cryptography
		{
			/// <summary>
			/// 表示 MD5 哈希算法的实现
			/// </summary>
			class export MD5
			{
			private:
				/// <summary>
				/// 表示MD5上下文对象
				/// </summary>
				typedef struct 
				{
					//表示哈希计算的状态
					unsigned int  state[4];    
					unsigned int  count[2];    
					unsigned char buffer[64];    
				}MD5Context;
			public:
				/// <summary>
				/// 初始化 HashAlgorithm 类的实现
				/// </summary>
				static void Initialize(MD5Context * context);
				/// <summary>
				/// 将写入对象的数据路由到哈希算法以计算哈希值
				/// </summary>
				static void HashCore(MD5Context * context, unsigned char * buf, int len);
				/// <summary>
				/// 在加密流对象处理完最后的数据后完成哈希计算
				/// </summary>
				static void HashFinal(MD5Context * context, unsigned char digest[16]);
			public:
				/// <summary>
				/// 计算指定字节数组的哈希值
				/// </summary>
				static void ComputeHash(const void *buffer, int count, OUT byte output[16]);
				/// <summary>
				/// 计算指定长度字符串的哈希值
				/// </summary>
				static String GetHashCode(LPCVOID Data, int DataByte);
				/// <summary>
				/// 计算指定长度字符串的哈希值
				/// </summary>
				static String GetHashCode(String str,int length = 0);
			};
		}
	}
}
#endif //_USE_MD5