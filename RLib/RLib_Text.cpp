/********************************************************************
Created:	2012/02/05  22:43
Filename: 	Lib_Text.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_Text.h"
#include <limits.h>
//////////////////////////////////////////////////////////////////////////
using namespace System;
using namespace System::IO;
using namespace System::Text;

//  Bytes	    Encoding Form
// 	00 00 FE FF	UTF-32, big-endian
// 	FF FE 00 00	UTF-32, little-endian
// 	FE FF	    UTF-16, big-endian
// 	FF FE	    UTF-16, little-endian
// 	EF BB BF	UTF-8

//only process Microsoft "lead bytes"
const char UTF8_LEAD_0 = 0xefU;
const char UTF8_LEAD_1 = 0xbbU;
const char UTF8_LEAD_2 = 0xbfU;

const char UTF16_LEAD_0 = 0xffU;
const char UTF16_LEAD_1 = 0xfeU;

const char UTF16F_LEAD_0 = 0xfeU;
const char UTF16F_LEAD_1 = 0xffU;

typedef __declspec(align(1))struct WCHARStruct
{
    CHAR High;
    CHAR Low;
}  *PWCHARStruct;
//////////////////////////////////////////////////////////////////////////
Encoding Encoder::DetectEncodingFromByteOrderMarks(const void *ptr, OUT int *bytes /* = nullptr */)
{
    const char *p = (const char*)ptr;
    if (*p == UTF8_LEAD_0 && *(p + 1) == UTF8_LEAD_1 && *(p + 2) == UTF8_LEAD_2)
    {
        if (bytes)
        {
            *bytes = 3;
        }
        return UTF8Encoding;
    }
    else if (*p == UTF16_LEAD_0 && *(p + 1) == UTF16_LEAD_1)
    {
        if (bytes)
        {
            *bytes = 2;
        }
        return UTF16Encoding;
    }
    else if (*p == UTF16F_LEAD_0 && *(p + 1) == UTF16F_LEAD_1)
    {
        if (bytes)
        {
            *bytes = 2;
        }
        return UTF16FEncoding;
    } //if
    if (bytes)
    {
        *bytes = 0;
    }
    //无法判断
    return UnknownEncoding /*ASCIIEncoding*/;
}

//-------------------------------------------------------------------------

MemoryStream *Encoder::ToCurrentEncoding(Encoding codepage, Stream *stream, int length /* = 0 */, bool detectEncodingFromByteOrderMarks /* = true */)
{
    if (length == 0)
    {
        length = stream->MaxReadSize;
    } //if
    assert(stream->MaxReadSize >= length);

    //判断codepage
    if (codepage == UnknownEncoding)
    {
        if (length <= 3 || detectEncodingFromByteOrderMarks == false)
        {
            //这样的情况只能尝试解码
            goto try_convert;
        } //if

        int mark;
        codepage = DetectEncodingFromByteOrderMarks(stream->CurrentPtr, &mark);
        if (codepage == UnknownEncoding)
        {
            //这样的情况也只能尝试解码
            goto try_convert;
        } //if
        stream->Position += mark;
        length -= mark;
    } //if

    switch (codepage)
    {
        case ASCIIEncoding:
            {
                #ifdef _UNICODE
                    //ascii to utf-16
                    return ToWideChar(ASCIIEncoding, stream, length);
                #else 
                    //ascii to ascii
                    MemoryStream *output_stream = new MemoryStream(length);
                    if (output_stream != nullptr)
                    {
						output_stream->Write(stream, length);
						output_stream->Position = 0;
                    } //if
                    return output_stream;
                #endif // _UNICDOE
            }
            break;
        case UTF16Encoding:
            {
                #ifdef _UNICODE
                    //utf-16 to utf-16
                    MemoryStream *output_stream = new MemoryStream(length);
                    if (output_stream != nullptr)
                    {
						output_stream->Write(stream, length);
						output_stream->Position = 0;
                    } //if
                    return output_stream;
                #else 
                    //utf-16 to ascii
                    return ToMultiByte(ASCIIEncoding, stream, length);
                #endif // _UNICDOE
            }
            break;
        case UTF16FEncoding:
        case UTF8Encoding:
            {
                #ifdef _UNICODE
                    //to utf-16
                    return Text::Encoder::ToWideChar(codepage, stream, length);
                #else 
                    //to ascii
                    auto utf16_stream = Text::Encoder::ToWideChar(codepage, stream, length);
                    if (utf16_stream == nullptr)
                    {
                        return NULL;
                    }
                    //if
                    auto ascii_stream = Text::Encoder::ToMultiByte(codepage, utf16_stream);
                    delete utf16_stream;
                    return ascii_stream;
                #endif // _UNICODE
            }
            break;
        default:
            {
                assert(!"不支持的编码");
                return nullptr;
            }
    }


try_convert: 
    //由于无法判断编码方式只能逐一尝试
    MemoryStream *out_stream = ToWideChar(ASCIIEncoding, stream, length);
    //codepage = ASCIIEncoding;
    if (out_stream == nullptr)
    {
        out_stream = ToWideChar(UTF8Encoding, stream, length);
        //codepage = UTF8Encoding;
        if (out_stream == nullptr)
        {
            out_stream = ToWideChar(UTF16FEncoding, stream, length);
            //codepage = UTF16FEncoding;
        }
    }

#ifdef _UNICODE

	if (out_stream == nullptr)
	{
		//假定 stream 采用UTF-16编码, 直接拷贝
		out_stream = new MemoryStream(length);
		if (out_stream != nullptr)
		{
			out_stream->Write(stream, length);
			out_stream->Position = 0;
		} //if
	} //if
	return out_stream;

#else 

	if (out_stream == nullptr)
	{
		//假定 stream 采用UTF-16编码, 转换成ANSI
		out_stream = ToMultiByte(ASCIIEncoding, stream, length);
		return out_stream;
	} //if

	auto   out_stream_ascii = ToMultiByte(ASCIIEncoding, out_stream, length);
	delete out_stream;
	return out_stream_ascii;

#endif // _UNICODE
}

//-------------------------------------------------------------------------

MemoryStream *Encoder::ToWideChar(Encoding codepage, const System::IO::Stream *stream, 
	INT length /* = 0 */)
{
    if (length == 0)
    {
        length = stream->MaxReadSize; //in CHAR's( = bytes)
    } //if
    assert(stream->MaxReadSize >= length);

    auto src = (LPSTR)stream->CurrentPtr;

    if (codepage == UTF16FEncoding)
    {
        MemoryStream *pUtf16 = new MemoryStream((length % 2) != 0 ? ++length : length);
		//如果 length 不是偶数, 就添加一位置0
		if (pUtf16 == nullptr)
		{
			return nullptr;
		} //if

        //替换高低两位
        auto pUtf16Data = LPWSTR(pUtf16->ObjectData), pUtf16FData = LPWSTR(src);

		assert((length % 2) == 0);
        (length /= sizeof(wchar_t))--; //基址by 0

        while (length >= 0)
        {
            ((PWCHARStruct)(&pUtf16Data[length]))->High = 
				((PWCHARStruct)(&pUtf16FData[length]))->Low;
            ((PWCHARStruct)(&pUtf16Data[length]))->Low  = 
				((PWCHARStruct)(&pUtf16FData[length]))->High;
            length--;
        }

//		pUtf16->Position = 0;
        return pUtf16;
    } //if

	// Get size of destination buffer, in WCHAR's
	int cchBuffer = MultiByteToWideChar(codepage,  // convert from codepage
		MB_ERR_INVALID_CHARS,  // error on invalid chars
		src,  // source data
		length,  // total length of source data, in CHAR's (= bytes)
		NULL,  // unused - no conversion done in this step
		0  // request size of destination buffer, in WCHAR's
		);
	if (cchBuffer == 0)
	{
		return nullptr;
	}

    // Allocate destination buffer to store data
    MemoryStream *pdata = new MemoryStream((cchBuffer /* + 1*/) * sizeof(WCHAR));
	if (pdata == nullptr)
	{
		return nullptr;
	} //if

	// Do the conversion
	int result = MultiByteToWideChar(codepage,  // convert from codepage
		MB_ERR_INVALID_CHARS,  // error on invalid chars
		src,  // source data
		length,  // total length of source data, in CHAR's (= bytes)
		LPWSTR(pdata->GetObjectData()),  // destination buffer
		cchBuffer  // size of destination buffer, in WCHAR's
		);
	if (result == 0)
	{
		delete pdata;
		return nullptr;
	}

    pdata->Length   = result * sizeof(WCHAR); //stream should in bytes
	pdata->Position = 0; //stream rule

    return pdata;
}

//-------------------------------------------------------------------------

MemoryStream *Encoder::ToMultiByte(Encoding codepage, const System::IO::Stream *stream, INT length /* = 0 */)
{
    if (length == 0)
    {
        length = stream->MaxReadSize;
    }
    assert(stream->MaxReadSize >= length);


    if ((length % sizeof(WCHAR)) != 0)
    {
        length--; //数据丢失?
        assert(!"不合法的数据长度");
    }


    length /= sizeof(WCHAR); //in WCHAR's

    auto src = (LPWSTR)stream->CurrentPtr;

	if (codepage == UTF16FEncoding)
	{
		MemoryStream *pUtf16F = new MemoryStream(length *sizeof(WCHAR));
		if (pUtf16F == nullptr)
		{
			return nullptr;
		} //if
		//替换高低两位
		auto pUtf16FData = (WCHAR *)pUtf16F->ObjectData;
		auto pUtf16Data  = (WCHAR *)src;

		pUtf16F->Length = length * sizeof(WCHAR); //bytes
		length--; //基址by 0

		while (length >= 0)
		{
			((PWCHARStruct)(&pUtf16FData[length]))->High = ((PWCHARStruct)(&pUtf16Data[length]))->Low;
			((PWCHARStruct)(&pUtf16FData[length]))->Low  = ((PWCHARStruct)(&pUtf16Data[length]))->High;
			length--;
		}
		return pUtf16F;
	} //if

	// WC_ERR_INVALID_CHARS flag is set to fail if invalid input character is encountered.
	// This flag is supported on Windows Vista and later.
	// Don't use it on Windows XP and previous.
#if (WINVER >= 0x0602)
	DWORD dwConversionFlags = WC_ERR_INVALID_CHARS;
#else 
	DWORD dwConversionFlags = 0;
#endif 

	// Get size of destination ANSI buffer, in CHAR's (= bytes)
	int cbBuffer = WideCharToMultiByte(codepage,  // convert to codepage
		dwConversionFlags,  // specify conversion behavior
		src,  // source data
		static_cast < int > (length),  // total source string length, in WCHAR's
		NULL,  // unused - no conversion required in this step
		0,  // request buffer size
		NULL, nullptr  // unused
		);
	if (cbBuffer == 0)
	{
		return nullptr;
	}

	// Allocate destination buffer for ANSI string
	MemoryStream *pdata = new MemoryStream((cbBuffer /* + 1*/) *sizeof(CHAR)); 
	// sizeof(CHAR) = 1 byte
	if (pdata == nullptr)
	{
		return nullptr;
	} //if

	// Do the conversion from UTF-16 to ANSI
	int result = WideCharToMultiByte(codepage,  // convert to codepage
		dwConversionFlags,  // specify conversion behavior
		src,  // source data
		static_cast < int > (length),  // total source string length, in WCHAR's
		LPSTR(pdata->GetObjectData()),  // destination buffer
		cbBuffer,  // destination buffer size, in bytes
		NULL, nullptr  // unused
		);
	if (result == 0)
	{
		delete pdata;
		return nullptr;
	}

	pdata->Length   = result * sizeof(CHAR);
	pdata->Position = 0;

    return pdata;
}

//-------------------------------------------------------------------------
bool Encoder::WriteTextStream(OUT IO::Stream *output, IN const IO::Stream *stream, int length /* = 0 */, bool useBOM /* = true */, Text::Encoding codepage /* = Text::UnknownEncoding */)
{
	if (length == 0)
	{
		length = stream->MaxReadSize;
	} //if
	assert(stream->MaxReadSize >= length);

	if (codepage == Text::UnknownEncoding)
	{
#ifdef _UNICODE
		codepage = Text::UTF16Encoding;
#else 
		codepage = Text::ASCIIEncoding;
#endif // _UNICODE
	} //if

	//根据编码写入BOM信息
	switch (codepage)
	{
	case System::Text::ASCIIEncoding: 
		{
#ifdef _UNICODE
			//utf-16 to ascii
			auto ascii_stream = Text::Encoder::ToMultiByte(Text::ASCIIEncoding, stream, length);
			if(ascii_stream == nullptr)
			{
				return false;
			}
			output->Write(ascii_stream->CurrentPtr, ascii_stream->MaxReadSize);
			delete ascii_stream;
#else 
			output->Write(stream->CurrentPtr, length);
#endif // _UNICODE
		}
		break;
	case System::Text::UTF16Encoding: 
		{
#ifdef _UNICODE
			if (useBOM)
			{
				byte bom[sizeof(int)] = { 0xffU, 0xfeU };
				output->Write(bom, 2);
			}
			output->Write(stream->CurrentPtr, length);
#else 
			//ascii to utf-16
			auto utf16_stream = Text::Encoder::ToWideChar(Text::ASCIIEncoding, stream, length);
			if(utf16_stream == nullptr)
			{
				return false;
			}
			if (useBOM)
			{
				byte bom[sizeof(int)] = { 0xffU, 0xfeU };
				output->Write(bom, 2);
			}
			output->Write(utf16_stream->CurrentPtr, utf16_stream->MaxReadSize);
			delete utf16_stream;
#endif // _UNICODE
		}
		break;
	case System::Text::UTF8Encoding: 
		{
#ifdef _UNICODE
			//utf-16 to utf-8
			auto t_stream = Text::Encoder::ToMultiByte(codepage, stream, length);
			if(t_stream == nullptr)
			{
				return false;
			}
			if (useBOM)
			{
				byte bom[sizeof(int)] = { 0xefU, 0xbbU, 0xbfU };
				output->Write(bom, 3);
			}
			output->Write(t_stream->CurrentPtr, t_stream->MaxReadSize);
			delete t_stream;
#else 
			//ascii to utf-8
			auto t_stream = Text::Encoder::ToWideChar(Text::ASCIIEncoding, stream, length);
			if (t_stream == nullptr)
			{
				return false;
			} //if

			auto tt_stream = Text::Encoder::ToMultiByte(codepage, t_stream);
			if (tt_stream == nullptr)
			{
				delete t_stream;
				return false;
			} //if

			if (useBOM)
			{
				byte bom[sizeof(int)] = { 0xefU, 0xbbU, 0xbfU };
				output->Write(bom, 3);
			}
			output->Write(tt_stream->CurrentPtr, tt_stream->MaxReadSize);

			delete tt_stream;
			delete t_stream;
#endif // _UNICODE
		}
		break;
	case System::Text::UTF16FEncoding: 
		{
#ifdef _UNICODE
			//utf-16 to utf-16f
			auto t_stream = Text::Encoder::ToMultiByte(codepage, stream, length);
			if(t_stream == nullptr)
			{
				return false;
			}
			if (useBOM)
			{
				byte bom[sizeof(int)] = { 0xfeU, 0xffU };
				output->Write(bom, 2);
			}
			output->Write(t_stream->CurrentPtr, t_stream->MaxReadSize);
			delete t_stream;
#else 
			//ascii to utf-16f
			auto t_stream = Text::Encoder::ToWideChar(Text::ASCIIEncoding, stream, length);
			if (t_stream == nullptr)
			{
				return false;
			} //if

			auto tt_stream = Text::Encoder::ToMultiByte(codepage, t_stream);
			if (tt_stream == nullptr)
			{
				delete t_stream;
				return false;
			} //if

			if (useBOM)
			{
				byte bom[sizeof(int)] = { 0xfeU, 0xffU };
				output->Write(bom, 2);
			}
			output->Write(tt_stream->CurrentPtr, tt_stream->MaxReadSize);

			delete tt_stream;
			delete t_stream;
#endif // _UNICODE
		}
		break;
	default:
		{
			assert(!"不支持的编码格式");
			return false;
		}
	}
	return true;
}
