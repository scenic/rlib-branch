/********************************************************************
	Created:	2012/06/08  22:19
	Filename: 	RLib_HashMap.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_HASHMAP
#define _USE_HASHMAP
#include "RLib_Base64.h"

typedef unsigned int uint32_t;

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			/// <summary>
			/// 表示根据键的哈希代码进行组织的键/值对的集合
			/// </summary>
			class export HashMap
			{
			public:
				// Dummy constructor.  This constructor doesn't set up the hash
				// map properly so don't use it unless you have good reason (e.g.,
				// you know that the HashMap will never be used).
				HashMap();

				// initial_capacity is the size of the initial hash map;
				// it must be a power of 2 (and thus must not be 0).
				HashMap(Comparable<void>::IComparable, Allocator *allocator = &PublicDefaultAllocator, unsigned int initial_capacity = 8);

				~HashMap();

				// HashMap entries are (key, value, hash) triplets.
				// Some clients may not need to use the value slot
				// (e.g. implementers of sets, where the key is the value).
				struct Entry
				{
					void *key;
					void *value;
					uint32_t hash; // the full hash value for key
				};

				// If an entry with matching key is found, Lookup()
				// returns that entry. If no matching entry is found,
				// but insert is set, a new entry is inserted with
				// corresponding key, key hash, and nullptr value.
				// Otherwise, nullptr is returned.
				Entry *Lookup(void *key, uint32_t hash, bool insert);

				// Removes the entry with matching key.
				void Remove(void *key, uint32_t hash);

				// Empties the hash map (occupancy() == 0).
				void Clear();

				// The number of (non-empty) entries in the table.
				uint32_t occupancy()const
				{
					return occupancy_;
				}

				// The capacity of the table. The implementation
				// makes sure that occupancy is at most 80% of
				// the table capacity.
				uint32_t capacity() const
				{
					return capacity_;
				}

				// Iteration
				//
				// for (Entry* p = map.Start(); p != NULL; p = map.Next(p)) {
				//   ...
				// }
				//
				// If entries are inserted during iteration, the effect of
				// calling Next() is undefined.
				Entry *Start() const;
				Entry *Next(Entry *p) const;

			private:
				Allocator *allocator_;
				Comparable<void>::IComparable match_;
				Entry *map_;
				uint32_t capacity_;
				uint32_t occupancy_;

				Entry *map_end() const
				{
					return map_ + capacity_;
				}
				Entry *Probe(void *key, uint32_t hash);
				void Initialize(uint32_t capacity);
				void Resize();
			};
		}
	}
}

//-------------------------------------------------------------------------
#define foreachHashMap(p, map)  for(auto p = map.Start(); p != NULL; p = map.Next(p))

#endif // _USE_HASHMAP
