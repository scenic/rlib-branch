/********************************************************************
	Created:	2011/02/18  9:29
	Filename: 	Lib_Http.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_HTTP
#define _USE_HTTP
#include "RLib_Net.h"
//////////////////////////////////////////////////////////////////////////
namespace System
{
	namespace Net
	{
		/// <summary>
		/// 获取Http远程服务响应信息
		/// </summary>
		class export HttpResponse
		{
		private:
			//不包含响应报头的响应流
			ResponseStream *m_response;
			HttpException   m_error;
		private:
			//初始化成员
			HttpResponse(){ this->m_response = NULL; };
			/// <summary>
			/// 使用 Close 方法替代显式 delete
			/// </summary>
			~HttpResponse(){};
			/// <summary>
			/// 保存某些报文格式错误的响应数据, 也许这是必要的
			/// </summary>
			void SaveResponseBody(UnmanagedMemoryStream &);
		public:
			/// <summary>
			/// 获取请求返回的内容的长度
			/// </summary>
			int ContentLength;
			/// <summary>
			/// 获取发送响应的服务器的名称
			/// </summary>
			String Server; 
			/// <summary>
			/// 获取响应的内容类型
			/// </summary>
			String ContentType; 	
			/// <summary>
			/// 获取来自服务器的与此响应关联的标头
			/// </summary>
			WebHeader Headers;
			/// <summary>
			/// 获取响应中使用的 HTTP 协议的版本
			/// </summary>
			String ProtocolVer; 
			/// <summary>
			/// 获取响应的状态
			/// 参见http://msdn.microsoft.com/zh-cn/library/system.net.httpstatuscode.aspx
			/// </summary>
			int StatusCode; 	 
			/// <summary>
			/// 获取与响应一起返回的状态说明
			/// </summary>
			String StatusDescription; 
			/// <summary>
			/// 获取响应请求的 Internet 资源的 URI
			/// 如果请求被重定向, 将返回最终地址
			/// </summary>
			LPURI ResponseUri; 	  
		public:
			/// <summary>
			/// 获取与响应一起返回的标头的内容
			/// </summary>
			String GetResponseHeader(LPCSTR headerName = nullptr); 
			/// <summary>
			/// 获取流，该流用于读取来自服务器的响应的体
			/// </summary>
			ResponseStream *GetResponseStream();
			/// <summary>
			/// 关闭响应流
			/// </summary>
			void Close();
			/// <summary>
			/// 获取HttpResponse发生的异常信息
			/// </summary>
			HttpException *GetLastException(); 
		public:
			/// <summary>
			/// 根据指定的 Stream 实例初始化 HttpResponse 类的新实例
			/// </summary>
			static HttpResponse *Create(UnmanagedMemoryStream &);
			/// <summary>
			/// 根据指定的 Stream 实例和 解压缩类型 初始化 HttpResponse 类的新实例
			/// </summary>
			static HttpResponse *Create(UnmanagedMemoryStream &, int AutomaticDecompression);
			RLIB_ClassNewDel;
		};
		/// <summary>
		/// 提供对Http远程服务器的访问
		/// </summary>
		class export HttpRequest
		{
		private:
			Sockets              *m_socket;
			RequestStream        *m_request;
			HttpException         m_error;
			//服务器响应地址(重定向后)
			LPURI                 m_realuri;
			UnmanagedMemoryStream m_recv_buf;
		private:
			void OnClearRecvBuffer();
			bool OnDataRecv(int);
			bool OnAutoRedirect();
		private:
			bool OnConnect();//负责连接服务器
			bool OnSend();   //负责发送请求
			bool OnRecv();   //负责接收响应相关处理
		public://HTTP Headers
			/// <summary>
			/// 指定构成 HTTP 标头的名称/值对的集合
			/// </summary>
			WebHeader Headers;
			/// <summary>
			/// 获取或设置 Accept HTTP 标头的值
			/// </summary>
			String Accept; 
			/// <summary>
			/// 获取或设置 RefererHTTP 标头的值
			/// </summary>
			String Referer; 
			/// <summary>
			/// 获取或设置 Connection HTTP 标头的值
			/// </summary>
			String Connection; 
			/// <summary>
			/// 获取或设置 Content-typeHTTP 标头的值
			/// </summary>
			String ContentType;   
			/// <summary>
			/// 获取或设置 User-agentHTTP 标头的值
			/// </summary>
			String UserAgent; 	
			/// <summary>
			/// 获取或设置 Content-length HTTP 标头
			/// </summary>
			long   ContentLength; 
		public://属性
			/// <summary>
			/// 获取或设置所使用的解压缩类型
			/// </summary>
			DecompressionMethod::DecompressionMethods AutomaticDecompression;
			/// <summary>
			/// 获取或设置请求的代理信息
			/// </summary>
			LPPROXY RequestProxy;  
			/// <summary>
			/// 获取或设置请求将跟随的重定向的最大数目, 默认值为3
			/// </summary>
			int MaximumAutomaticRedirections;
			/// <summary>
			/// 获取或设置一个值，该值指示请求是否应跟随重定向响应
			/// 默认值为true
			/// @warning 如果属性 Method 设置为 HEAD, 此值将被忽略即不会自动重定向
			/// </summary>
			bool    AutoRedirect;
			/// <summary>
			/// 获取或设置是否忽略KeepAlive属性
			/// </summary>
			bool    DisKeepAlive;
#pragma region 字节补齐
			/// <summary>
			/// 该属性未使用
			/// </summary>
			bool    Unused;
#pragma endregion
			/// <summary>
			/// 获取或设置一个值，该值指示是否与 Internet 资源建立持久性连接
			/// </summary>
			bool    KeepAlive; 
			/// <summary>
			/// 获取请求的统一资源标识符 (URI) 
			/// </summary>
			LPURI   Address;  
			/// <summary>
			/// 获取或设置请求的方法
			/// </summary>
			String  Method; 
			/// <summary>
			/// 获取或设置用于请求的 HTTP 版本
			/// </summary>
			String  ProtocolVer;  
			/// <summary>
			/// 获取或设置超时值（以毫秒为单位）,默认为8000(8秒)
			/// </summary>
			UINT Timeout;     
			/// <summary>
			/// 获取或设置接收超时值（以毫秒为单位）,默认为1000(1秒), 该属性不一定有效
			/// </summary>
			UINT RecvTimeout; 
		public://构造方法
			/// <summary>
			/// 为指定的 URI 方案初始化新的 HttpRequest 实例
			/// </summary>
			HttpRequest(String);      
			/// <summary>
			/// 为此 HttpRequest 实例执行必要的清理
			/// </summary>
			~HttpRequest();                
			RLIB_ClassNewDel;
		public:
			/// <summary>
			/// 返回来自 Internet 资源的响应
			/// </summary>
			HttpResponse *GetResponse(); 
			/// <summary>
			/// 返回来自 Internet 资源的原始响应流, 该流包含响应头信息
			/// @warning 无需释放且不应与 GetResponse() 方法同时使用 
			/// </summary>
			UnmanagedMemoryStream *GetResponseStream();
			/// <summary>
			/// 获取用于写入请求数据的 RequestStream 对象
			/// </summary>
			RequestStream *GetRequestStream(); 
			/// <summary>
			/// 重置 HttpRequest 的请求数据以重新发起请求
			/// 之前写入的POST数据会丢失, 设置的Header不会丢失
			/// </summary>
			void ResetRequestStream();
			/// <summary>
			/// 获取HttpRequest发生的异常信息
			/// </summary>
			HttpException *GetLastException();                 
		};
	}
}
#endif