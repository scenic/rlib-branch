/********************************************************************
	Created:	2012/03/10  17:53
	Filename: 	RLib_Obama64.h
	Author:		zhongqiu
	Url:	    http://code.google.com/p/aobama/
*********************************************************************/
#ifndef _USE_OBAMA64
#define _USE_OBAMA64
#include "RLib_TimeSpan.h"
namespace System
{
	/// <summary>
	/// 提供安全系统的基础结构
	/// </summary>
	namespace Security
	{
		/// <summary>
		/// 提供加密服务，包括安全的数据编码和解码，以及许多其他操作
		/// </summary>
		namespace Cryptography
		{
			/// <summary>
			/// 公开统一的内存管理接口
			/// </summary>
			class export RLIB_THREAD_SAFE CryptographyBase
			{
			protected:
				static void *Alloc(ULONG size)
				{
					return RLIB_GlobalAlloc(size);
				}
			public:
				static void Collect(LPVOID ptr_data)
				{
					RLIB_GlobalCollect(ptr_data);
				}
			};
			/// <summary>
			/// 提供一种不同于Base64的编解码算法
			/// </summary>
			class export Obama64:public CryptographyBase
			{
			public:
				Obama64();
				~Obama64();
				RLIB_ClassNewDel;
			public:
				/// <summary>
				/// 重初始化
				/// </summary>
				void Init();
				/// <summary>
				/// 初始化解码表
				/// </summary>
				void InitDecode();
			public:
				/// <summary>
				/// DataByte
				///     [in]输入的数据长度, 以字节为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				char  *Encode(const char* Data, int DataByte, int *OutByte = nullptr);
				/// <summary>
				/// DataByte
				///     [in]输入的数据长度,以字节为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				char *Decode(const char* Data, int DataByte, int *OutByte = nullptr);
			public:
				/// <summary>
				/// 编码任意字符
				/// </summary>
				String EncodeString(String content, int content_length = 0)
				{
					if (content.IsNullOrEmpty())
					{
						return Nothing;
					} //if
					if(content_length > 0) content = content.Substring(0, content_length);
                    auto presult = Encode(content.ToMultiByte(), content.GetMultiByteSize(), &content_length);
					content.Copy(presult, content_length);
					Collect(presult);
					return content;
				}
				/// <summary>
				/// 解码任意字符
				/// </summary>
				String DecodeString(String content)
				{
					if (content.IsNullOrEmpty())
					{
						return Nothing;
					} //if
					int content_length;
					auto presult = Decode(content.ToMultiByte(), content.GetMultiByteSize(), &content_length);
					content.Copy(presult, content_length);
					Collect(presult);
					return content;
				}
				/// <summary>
				/// 编码纯ASCII字符
				/// </summary>
				String EncodeAsciiString(const char *content, int content_length = 0);
				/// <summary>
				/// 解码纯ASCII字符
				/// </summary>
				String DecodeAsciiString(const char *content, int content_length = 0);
			public:
				/// <summary>
				/// 设置密钥
				/// </summary>
				void SetSecret(char s);
			public:
				/// <summary>
				/// 扰乱功能, 默认为启用, 每次加密将返回不同结果 
				/// </summary>
				BOOL EnableBluff;
			private:
				//默认密钥
				char m_secret;
				//编码基表
				char *m_encode;
				//解码基表
				int   *m_decode;	
				//加密钩子(用于简单加密)
				int encode(char plaintext, char secret);
				//解密钩子
				int decode(char cryptograph, char secret);
			};
		}
	}
}
#endif // _USE_OBAMA64
