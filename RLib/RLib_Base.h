/********************************************************************
	Created:	2011/08/15  10:05
	Filename: 	Lib_Base.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_BASE
#define _USE_BASE
#include "RLib.h"
//////////////////////////////////////////////////////////////////////////
/// <summary>
/// RLIB 类库是一个由类、接口和值类型组成的库，通过该库中的内容可访问系统功能.
/// System 命名空间包含基本类和基类，这些类定义常用的值、事件和事件处理程序、接口、属性和异常处理
/// </summary>
namespace System
{
	namespace IO
	{
		/// <summary>
		/// 线程安全的内存池类
		/// </summary>
		class export RLIB_THREAD_SAFE MemoryPool;
	}
	/// <summary>
	/// 表示当前程序的基础类
	/// </summary>
	class export RLIB_THREAD_SAFE AppBase
	{
	public:
		/// <summary>
		/// 系统信息结构
		/// </summary>
		typedef struct /*_SYSTEM_BASIC_INFORMATION*/SystemInformation
		{
			ULONG Unknown;               //Always contains zero
			ULONG MaximumIncrement;      //时钟的计量单位
			ULONG PhysicalPageSize;      //内存页的大小
			ULONG NumberOfPhysicalPages; //系统管理着多少个页
			ULONG LowestPhysicalPage;    //低端内存页
			ULONG HighestPhysicalPage;   //高端内存页
			ULONG AllocationGranularity; //未知
			PVOID LowestUserAddress;     //低端用户地址
			PVOID HighestUserAddress;    //高端用户地址
			ULONG ActiveProcessors;      //激活的处理器
			UCHAR NumberProcessors;      //处理器数量
		}/*SYSTEM_BASIC_INFORMATION*/;
	public: 
		/// <summary>
		/// 获取当前进程句柄
		/// </summary>
		static HANDLE GetCurrentProcess(){ return (HANDLE)(LONG_PTR) -1; }
		/// <summary>
		/// 获取执行线程句柄
		/// </summary>
		static HANDLE GetCurrentThread(){ return (HANDLE)(LONG_PTR) -2; }
		/// <summary>
		/// 获取系统信息, 不保证方法的线程安全性
		/// </summary>
		static SystemInformation *GetSystemInfo();
		/// <summary>
		/// 在全局共享内存池上分配内存
		/// </summary>
#ifdef _RUNTIME_DEBUG
		static void *Allocate(ULONG, LPCTSTR function, LPCTSTR file);
#else
		static void *Allocate(ULONG);
#endif // _RUNTIME_DEBUG
		/// <summary>
		/// 回收全局共享内存池上分配的内存
		/// </summary>
		static void Collect(void *);
		/// <summary>
		/// 返回全局共享内存池指针
		/// </summary>
		static IO::MemoryPool *GetUsingPool();
		/// <summary>
		/// 获取系统环境中当前目录的值
		/// </summary>
		static LPCWSTR GetCurrentPath();
		/// <summary>
		/// 获取启动了应用程序的可执行文件的路径，不包括可执行文件的名称
		/// </summary>
		static LPCWSTR GetStartupPath();
		/// <summary>
		/// 获取映像基址
		/// </summary>
		static void *GetImageBaseAddress();
		/// <summary>
		/// 获取PEB结构指针
		/// </summary>
		static void *GetPEBAddress();
		/// <summary>
		/// 退出当前进程
		/// </summary>
		static void Exit(DWORD code = 0);
		/// <summary>
		/// 退出当前线程并释放线程内存
		/// </summary>
		static void ExitThread(DWORD code = 0);
	};
	//-------------------------------------------------------------------------
	class export DLLExport
	{
	public:
		static char *_unDNameEx(char* buffer, const char* mangled, int buflen);
	};
	//-------------------------------------------------------------------------
	//异常前向声明
	class Exception;
	/// <summary>
	/// 表示标准异常类
	/// </summary>
	class export Exception
	{
	public:
		/// <summary>
		/// 获取或设置 HRESULT，它是分配给特定异常的编码数值
		/// </summary>
		LONG HResult; 
		/// <summary>
		/// 获取描述当前异常的消息
		/// </summary>
		TCHAR Message[MAX_REASON_DESC_LEN]; 
#ifdef _DEBUG
		/// <summary>
		/// 获取导致错误的源代码文件名称
		/// </summary>
		const TCHAR *Source;
		/// <summary>
		/// 获取引发当前异常的方法
		/// </summary>
		CHAR *TargetSite; 	
#endif // _DEBUG
	public:
		/// <summary>
		/// 初始化 Exception 类的新实例
		/// </summary>
		Exception();
		/// <summary>
		/// 初始化 Exception 类的拷贝新实例
		/// </summary>
        Exception(const Exception &);
		/// <summary>
		/// 使用指定的错误消息初始化 Exception 类的新实例
		/// </summary>
		Exception(const TCHAR *);
		/// <summary>
		/// 使用指定的错误编号和错误消息初始化 Exception 类的新实例
		/// </summary>
		Exception(int, const TCHAR *);
		/// <summary>
		/// 析构
		/// </summary>
		~Exception();
		RLIB_ClassNewDel;
		/// <summary>
		/// 设置异常信息
		/// </summary>
#ifdef _DEBUG
		void Set(int HResult, const TCHAR *Message, const TCHAR *Source, const CHAR *TargetSite);
        void SetDebugInfo(const TCHAR *Source, const CHAR *TargetSite);
#else
		void Set(int HResult, const TCHAR *Message);
        void SetDebugInfo(const TCHAR *, const CHAR *){};
#endif // _DEBUG
		/// <summary>
		/// 引用异常类
		/// </summary>
		void Ref(const Exception &);
	public:
		/// <summary>
		/// 获取错误信息 不要调用此方法
		/// </summary>
		static bool GetLastError(OUT LPVOID buffer, size_t size, IN DWORD err = 0);
		/// <summary>
		/// 获取错误信息
		/// 允许为Ex->Id指定错误编号
		/// </summary>
		static bool GetException(OUT Exception *Ex, LONG HResult = 0);
		/// <summary>
		/// 获取最近一次的异常对象
		/// </summary>
		static Exception *GetLastException();
		/// <summary>
		/// 设置最近一次发生的错误
		/// </summary>
		static void SetLastError(DWORD err);
	};
	namespace Threading
	{
		/// <summary>
		/// 表示线程临界区
		/// </summary>
		class export RLIB_THREAD_SAFE CriticalSection
		{
		private:
			RTL_CRITICAL_SECTION m_cri;
		public:
			CriticalSection();
			~CriticalSection();
			RLIB_ClassNewDel;
		public:
			/// <summary>
			/// 进入临界区
			/// </summary>
			void Enter() const;
			/// <summary>
			/// 尝试进入临界区
			/// </summary>
			bool TryEnter() const;
			/// <summary>
			/// 退出临界区
			/// </summary>
			void Leave() const;
		};
		/// <summary>
		/// 提供一组利用对象构造/析构来自动进入/退出临界区的方法
		/// </summary>
		class export RLIB_THREAD_SAFE AutoLock
		{
		public:
			AutoLock(CriticalSection *cri)
			{
				this->m_cri = cri;
				this->m_cri->Enter();
			}
			~AutoLock()
			{
				this->m_cri->Leave();
			}
		private:
			AutoLock(){};
			CriticalSection *m_cri;
			RLIB_ClassNewDel;
		};
	};
	/// <summary>
	/// 提供对象的安全包装方法
	/// </summary>
	template<class R> class RLIB_THREAD_SAFE SafeObject
	{
	protected:
		R                          m_unsafe_obj;
		Threading::CriticalSection m_cri;
	public:
		SafeObject(){};
		~SafeObject(){};
		RLIB_ClassNewDel;
	public:
		/// <summary>
		/// SafeObject 要求对象必须具有默认构造函数, 但这可能导致构造函数重载
		/// 无法使用的问题, 因此提供此解决方案
		/// </summary>
		R *ReInitializeObj()
		{ 
			this->m_unsafe_obj.~R();
			return &this->m_unsafe_obj; 
		};
		/// <summary>
		/// 获取对象引用, 必须先调用 Lock 方法获取完全访问权才能确保线程安全
		/// </summary>
		R &GetObj(){ return (R &)this->m_unsafe_obj; }
		/// <summary>
		/// 获取对象引用, 必须先调用 Lock 方法获取完全访问权才能确保线程安全
		/// </summary>
		operator R &(){ return this->GetObj(); }
		/// <summary>
		/// 获取对象指针成员, 必须先调用 Lock 方法获取完全访问权才能确保线程安全
		/// </summary>
		R *operator->(){ return &this->m_unsafe_obj; }
		/// <summary>
		/// 获取用于同步访问的对象
		/// </summary>
		Threading::CriticalSection *GetSyncObj(){ return &this->m_cri; }
	public:
		void Lock(){ this->m_cri.Enter(); }
		bool TryLock(){ return this->m_cri.TryEnter(); }
		void UnLock(){ this->m_cri.Leave(); }
	};
	/// <summary>
	/// 提供对象指针的自动释放方法, 主要用于静态指针用
	/// </summary>
	template<class R> class RLIB_THREAD_SAFE ManagedObjectPtr
	{
	public:
		ManagedObjectPtr(){ this->ObjectPtr = nullptr; };
		~ManagedObjectPtr(){ RLIB_Delete(this->ObjectPtr); };
		RLIB_ClassNewDel;
	public:
		/// <summary>
		/// 获取实例对象托管的指针
		/// </summary>
		R *ObjectPtr;
	};
	/// <summary>
	/// use nullptr instead
	/// </summary>
#ifdef _NATIVE_NULLPTR_SUPPORTED
	typedef RLIB_Type(__nullptr) nullptr_t; 
#endif
};
#define LockObj(so)    so.Lock()
#define TryLockObj(so) so.TryLock()
#define UnLockObj(so)  so.UnLock()
//////////////////////////////////////////////////////////////////////////
#endif // _USE_BASE