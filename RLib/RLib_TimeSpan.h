/********************************************************************
	Created:	2012/03/10  16:56
	Filename: 	RLib_AppTest.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_TIMESPAN
#define _USE_TIMESPAN
#include "RLib_Variant.h"
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 提供一种计算代码执行时间的类
	/// </summary>
	class TimeSpan
	{
	private:
		LARGE_INTEGER m_int;
		LONGLONG m_frequency, m_counter, m_interval;
	public:
		RLIB_ClassNewDel;
		TimeSpan()
		{
			QueryPerformanceFrequency(&this->m_int);
			this->m_frequency = this->m_int.QuadPart;
		}
		/// <summary>
		/// 开始计时
		/// </summary>
		inline void BeginTimer()
		{
			QueryPerformanceCounter(&this->m_int);
			this->m_counter = this->m_int.QuadPart;  
		}
		/// <summary>
		/// 停止计时
		/// </summary>
		inline void EndTimer()
		{
			QueryPerformanceCounter(&this->m_int);  
			m_interval = (this->m_int.QuadPart - this->m_counter);
		}
		/// <summary>
		/// 获取计时值
		/// </summary>
		inline LONGLONG &GetTime()
		{
			return this->m_interval;
		}
		/// <summary>
		/// 获取双精度计时值
		/// </summary>
		inline double GetDoubleTime()
		{
			return double(this->m_interval) / this->m_frequency;
		}
		/// <summary>
		/// 将当前 TimeSpan 对象的值(double)转换为其等效的字符串表示形式
		/// </summary>
		inline String ToString()
		{
			String ret;
			ret.Format(T("%f"), this->GetDoubleTime());
			return ret;
		}
	};
}
#endif // _USE_TIMESPAN