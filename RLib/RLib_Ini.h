/********************************************************************
	Created:	2012/07/26  10:22
	Filename: 	RLib_Ini.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_INI
#define _USE_INI
#include "RLIB_Mail.h"
#pragma warning(disable: 4408)//匿名联合
using namespace System::Collections::Generic;
//-------------------------------------------------------------------------
namespace System
{
	namespace Config
	{
		/// <summary>
		/// 提供一组方法以支持对ini文件的操作
		/// @todo 开发中
		/// </summary>
		namespace Ini
		{
			/// <summary>
			/// 表示Ini异常
			/// </summary>
			typedef Exception IniException;
			//-------------------------------------------------------------------------
			class export IniSection;
			class export IniKey;
			class export IniComment;
			/// <summary>
			/// ini文件元素基类
			/// </summary>
			class export IniElement
			{
			public:
				IniElement(){};
				RLIB_ClassNewDel;
			protected:
				IniElement(String Value)
				{
					this->Value = Value.Trim();
				}
			public:
				String Value;
			public:
				virtual void Delete() const = 0;
				virtual void Print(IO::Stream *) const = 0;
				virtual IniKey *TryGetIniKey() const = 0;
				virtual IniSection *TryGetIniSection() const = 0;
				virtual IniComment *TryGetIniComment() const = 0;
			};
			typedef IniElement *IniElementCollectionItem;
			/// <summary>
			/// ini文件元素集合 公开接口
			/// </summary>
			class export IniElementCollection:public System::Collections::Generic::List<IniElementCollectionItem>
			{				
			public:
				RLIB_ClassNewDel;
			};
			/// <summary>
			/// 参数
			/// </summary>
			class export IniKey:public IniElement
			{
			public:
				RLIB_ClassNewDel;
				IniKey(String Key = Nothing, String Value = Nothing):IniElement(Value)
				{
					this->Name = Key.Trim();
				}
			public:
				String Name;
			public:
				virtual void Delete() const
				{
					delete this;
				}
				void Print(IO::Stream *cout) const;
				IniKey *TryGetIniKey() const
				{
					return (IniKey *)this;
				}
				IniSection *TryGetIniSection() const
				{
					return nullptr;
				}
				IniComment *TryGetIniComment() const
				{
					return nullptr;
				}
			};
			/// <summary>
			/// 节
			/// </summary>
			class export IniSection:public IniElement
			{
			public:
				RLIB_ClassNewDel;
				IniSection(String Value):IniElement(Value){assert(!this->Value.IsNullOrEmpty());};
				~IniSection()
				{
					while(this->Items.Length > 0)
					{
						this->Items[0]->Delete();
						this->Items.RemoveAt(0);;
					}
				}
			public:
				IniElementCollection Items; 
			public:
				virtual void Delete() const
				{
					delete this;
				}
				void Print(IO::Stream *cout) const;
				IniKey *TryGetIniKey() const
				{
					return nullptr;
				}
				IniSection *TryGetIniSection() const
				{
					return (IniSection *)this;
				}
				IniComment *TryGetIniComment() const
				{
					return nullptr;
				}
			public:
				IniKey *GetIniKey(String key_name)
				{
					foreach(pItem, this->Items)
					{
						if ((*pItem)->TryGetIniKey() != nullptr)
						{
							String &Name = (String &)(((IniKey *)*pItem)->Name);
							if (Name.IsNull())
							{
								if (key_name.IsNull())
								{
									return ((IniKey *)*pItem);
								} //if
							} //if
							else if (Name == key_name)
							{
								return ((IniKey *)*pItem);
							} //if
						} //if
					}
					return nullptr;
				}
				bool DelIniKey(String key_name)
				{
					foreach(pItem, this->Items)
					{
						if ((*pItem)->TryGetIniKey() != nullptr)
						{
							String &Name = (String &)(((IniKey *)*pItem)->Name);
							if (Name.IsNull())
							{
								if (key_name.IsNull())
								{
									((IniKey *)*pItem)->Delete();
									this->Items.RemoveAt(i);
									return true;
								} //if
							} //if
							else if (Name == key_name)
							{
								((IniKey *)*pItem)->Delete();
								this->Items.RemoveAt(i);
								return true;
							} //if
						} //if

					}
					return false;
				}
				IniKey *AddIniKey(String key_name = Nothing, String key_value = Nothing)
				{
					this->Items.AddLast(new IniKey(key_name, key_value));
					auto pIniKey = (IniKey *)this->Items.Get(this->Items.Length - 1);
					return pIniKey;
				}
			};
			/// <summary>
			/// 注释
			/// </summary>
			class export IniComment:public IniElement
			{
			protected:
				String CommentPrefix;
			public:
				RLIB_ClassNewDel;
				IniComment(String Value = Nothing, String Prefix = Nothing):IniElement(Value)
				{
					this->CommentPrefix = Prefix;
				};
			public:
				virtual void Delete() const
				{
					delete this;
				}
				void Print(IO::Stream *cout) const;
				IniKey *TryGetIniKey() const
				{
					return nullptr;
				}
				IniSection *TryGetIniSection() const
				{
					return nullptr;
				}
				IniComment *TryGetIniComment() const
				{
					return (IniComment *)this;
				}
			};
			/// <summary>
			/// 表示对ini文件的访问
			/// </summary>
			class export IniFile
			{
			protected:
				String       m_ini_file;
				String       m_ini_data;
				IniException m_error;
			public:
				IniFile(){};
				IniFile(String ini_file)
				{
					this->LoadFromFile(ini_file);
				}
				~IniFile();
				RLIB_ClassNewDel;
			public:
				IniElementCollection Items;
			private:
				bool    parse_ini();
				LPCTSTR next_char(LPCTSTR ptr);
			public:
				void Clear();
				bool LoadFromString(String ini_string);
				bool LoadFromStream(Stream *ini_data, long ini_data_size);
				bool LoadFromFile(String ini_file);
				/// Save a file using the current document value. Returns true if successful.
				bool SaveFile(Text::Encoding codepage = Text::UnknownEncoding) const;
				/// Save a file using the given filename. Returns true if successful.
				bool SaveFile( const TCHAR * filename, Text::Encoding codepage = Text::UnknownEncoding ) const;		
				/// Save a file using the given IO::Stream*. Returns true if successful.
				bool SaveFile( System::IO::Stream*,  Text::Encoding codepage = Text::UnknownEncoding ) const;
				/// <summary>
				/// 获取IniFile发生的异常信息
				/// </summary>
				IniException *GetLastException() { return &this->m_error; } ; 
			public:
				IniSection *GetIniSection(String section_name)
				{
					foreach(pItem, this->Items)
					{
						if ((*pItem)->TryGetIniSection() != nullptr)
						{
							String &Value = (String &)(((IniSection *)*pItem)->Value);
							if (Value.IsNull())
							{
								if (section_name.IsNull())
								{
									return ((IniSection *)*pItem);
								} //if
							} //if
							else if (Value == section_name)
							{
								return ((IniSection *)*pItem);
							} //if
						} //if
					}
					return nullptr;
				}
				bool DelIniSection(String section_name)
				{
					foreach(pItem, this->Items)
					{
						if ((*pItem)->TryGetIniSection() != nullptr)
						{
							String &Value = (String &)(((IniSection *)*pItem)->Value);
							if (Value.IsNull())
							{
								if (section_name.IsNull())
								{
									((IniSection *)*pItem)->Delete();
									this->Items.RemoveAt(i);
									return true;
								} //if
							} //if
							else if (Value == section_name)
							{
								((IniSection *)*pItem)->Delete();
								this->Items.RemoveAt(i);
								return true;
							} //if
						} //if
					}
					return false;
				}
				IniSection *AddIniSection(String section_name)
				{
					this->Items.AddLast(new IniSection(section_name));
					auto pIniSection = (IniSection *)this->Items.Get(this->Items.Length - 1);
					return pIniSection;
				}
			};
		}
	}
}
#endif // _USE_INI