/********************************************************************
	Created:	2012/07/15  9:53
	Filename: 	RLib_Mail.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "RLIB_Mail.h"
using namespace System::Net;
using namespace System::Net::Mail;
using namespace System::Net::Mime;
//-------------------------------------------------------------------------
LPCTSTR MediaText::HTML = T("text/html");
LPCTSTR MediaText::TEXT = T("text/plain");
LPCTSTR MediaText::XML  = T("text/XML");
LPCTSTR MediaText::RICHTEXT = T("text/richtext");

LPCTSTR MediaImage::JPEG = T("image/jpeg");
LPCTSTR MediaImage::GIF = T("image/gif");
LPCTSTR MediaImage::TIFF = T("image/tiff");

LPCTSTR MediaApplication::RTF = T("application/rtf");
LPCTSTR MediaApplication::Octet = T("application/octet-stream");
LPCTSTR MediaApplication::PDF = T("application/pdf");
LPCTSTR MediaApplication::SOAP = T("application/soap+xml");
LPCTSTR MediaApplication::ZIP = T("application/zip");
//-------------------------------------------------------------------------
SmtpClient::SmtpClient()
{
	this->m_socket = new Sockets(INVALID_SOCKET, AF_INET, SOCK_STREAM, IPPROTO_IP);
}
//-------------------------------------------------------------------------


SmtpClient::~SmtpClient()
{
	Logout();
	if (!this->m_socket)
	{
		return;
	} //if
	this->m_socket->Close();
	delete this->m_socket;
}

//-------------------------------------------------------------------------

int SmtpClient::GetResponseCode()
{
	if(!this->ReceiveResponse())
	{
		return -1;
	}
	this->ResponseCode = Int32::TryParse(this->ResponseString.Substring(0, 3), 10);
	return this->ResponseCode;
}

//-------------------------------------------------------------------------

bool SmtpClient::ReceiveResponse()
{
	int ret = this->m_socket->Recv(this->recv_buf, sizeof(this->recv_buf));
	if (ret == -1)
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	};
	this->recv_buf[ret] = '\0';
	this->ResponseString = this->recv_buf;
	return true;
}
template<typename R> R SmtpClient::SetException(R ret)
{
	if (this->m_error.HResult == STATUS_SUCCESS)//防止重复设置异常
	{
		RLIB_SetException((&this->m_error), this->ResponseCode, this->ResponseString);
	} //if
	return ret;
}
//-------------------------------------------------------------------------

bool SmtpClient::Connect(const String& strSMTPServer, int nPort)
{
	if(!m_socket->Connect(strSMTPServer.ToMultiByte(), strSMTPServer.GetMultiByteSize(), nPort))
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}

	if(GetResponseCode() != 220) return SetException<bool>(false);//服务就绪

	//向服务器发送"HELO "+服务器名
	this->strTmp = T("HELO ");
	this->strTmp += (strSMTPServer + T("\r\n"));
	if(!m_socket->Send(this->strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize())) 
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
	if(GetResponseCode() != 250) return SetException<bool>(false);//要求的邮件操作完成

	return true;
}

//-------------------------------------------------------------------------

bool SmtpClient::Login(String strAccount, String strPassword)
{
	this->strTmp = T("AUTH LOGIN\r\n");
	if (!m_socket->Send(this->strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
	if(GetResponseCode() != 334) return SetException<bool>(false);//等待用户输入

	{
		this->strTmp = System::Security::Cryptography::Base64::EncodeBase64String(
			strAccount) + T("\r\n");
		if (!m_socket->Send(strTmp.ToMultiByte(), strTmp.GetMultiByteSize()))
		{
			this->m_error.Ref(*this->m_socket->GetLastException());
			return false;
		}
		if(GetResponseCode() != 334) return SetException<bool>(false);//等待用户输入

		this->strTmp = System::Security::Cryptography::Base64::EncodeBase64String(
			strPassword) + T("\r\n");
		if (!m_socket->Send(strTmp.ToMultiByte(), strTmp.GetMultiByteSize()))
		{
			this->m_error.Ref(*this->m_socket->GetLastException());
			return false;
		}
 		if(GetResponseCode() != 235) return SetException<bool>(false);//身份确认
	}
	return true;
}

bool SmtpClient::Send(const MailMessage& mailmsg)
{
	//发送发送者邮件地址
	this->strTmp.Format(T("MAIL FROM:<%s>\r\n"), mailmsg.From.GetConstData());
	if (!m_socket->Send(strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
	if(GetResponseCode() != 250) return SetException<bool>(false);//要求的邮件操作完成

	//发送接收者地址
	for (int i = 0;i < mailmsg.To->Length; i++)
	{
		if (0 == mailmsg.To->GetValue(i).Address.GetLength())
		{
			RLIB_SetException((&this->m_error), -1, T("目的地址不能为空"));
			return false;
		}

		strTmp.Format(T("RCPT TO:<%s>\r\n"), mailmsg.To->GetValue(i).Address.GetConstData());
		if (!m_socket->Send(strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
		{
			this->m_error.Ref(*this->m_socket->GetLastException());
			return false;
		}
		if(GetResponseCode() != 250) return SetException<bool>(false);//要求的邮件操作完成

	}

	//发送数据
	strTmp = T("DATA\r\n");
	if (!m_socket->Send(strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
	if(GetResponseCode() != 354) return SetException<bool>(false);//准备接收邮件内容

	//发送邮件头
	if(!SendHeader(mailmsg)) return SetException<bool>(false);

	//邮件主体
	if(!SendBody(mailmsg)) return SetException<bool>(false);

	//发送附件
	if(!SendAttachments(mailmsg)) return SetException<bool>(false);

	//界尾
	strTmp = T("--");
	strTmp += mailmsg.Boundary;
	//发送内容结束
	strTmp += T("\r\n.\r\n");

	if(!m_socket->Send(strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
	if(GetResponseCode() != 250) return SetException<bool>(false);//要求的邮件操作完成

	return true;
}

//-------------------------------------------------------------------------

bool SmtpClient::SendHeader(const MailMessage& mailmsg)
{
	this->strTmp.Format(T("From:%s<%s>\r\n"), 
		mailmsg.Sender.GetConstData(), mailmsg.From.GetConstData());

	String str;
	//时间
	if (mailmsg.Date.GetLength())
	{
		str.Format(T("Date:%s"), mailmsg.Date.GetConstData());
		this->strTmp += str;
	}

	//抄送人
	if (mailmsg.Cc.GetLength())
	{
		str.Format(T("Cc:%s"), mailmsg.Cc.GetConstData());
		this->strTmp += str;
	}

	//暗抄送人
	if (mailmsg.Bcc.GetLength())
	{
		str.Format(T("Bcc:%s"), mailmsg.Bcc.GetConstData());
		this->strTmp += str;
	}

	//Message-ID
	if (mailmsg.Message_ID.GetLength())
	{
		str.Format(T("Message-ID:%s"), mailmsg.Message_ID.GetConstData());
		this->strTmp += str;
	}

	//主题
	str.Format(T("Subject:%s\r\n"), mailmsg.Subject.GetConstData());
	this->strTmp += str;


	//"X-Mailer:Smtp Client By xxx"//版权信息
	//this->strTmp += "X-Mailer:"; this->strTmp += "cmdmac"; this->strTmp+="\r\n";

	//MINME_Version
	str.Format(T("Mime_Version:%s\r\n"), mailmsg.Mime_Version.GetConstData());
	this->strTmp += str;

	str.Format(T("Content-type:multipart/mixed;Boundary=%s"), mailmsg.Boundary.GetConstData());
	this->strTmp += str;
	this->strTmp += T("\r\n\r\n");
	//可加入一段文字
	this->strTmp += T("This is a multi-part message in Mime format.\r\n");

	if (!m_socket->Send(this->strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
	return true;
}

//-------------------------------------------------------------------------

bool SmtpClient::SendBody(const MailMessage& mailmsg)
{
	this->strTmp = T("--");
	this->strTmp += mailmsg.Boundary;
	this->strTmp += T("\r\n");

	if (mailmsg.IsHTML)
	{
		this->strTmp += T("Content-type:text/html\r\n");
	}
	else
	{
		this->strTmp += T("Content-type:text/plain\r\n");
	}

	String str;
	//编码形式
	if (mailmsg.BodyEncoding.GetLength())
	{
		str.Format(T("Content-Transfer-Encoding:%s\r\n\r\n"), mailmsg.BodyEncoding);
		this->strTmp += str;
	}
	else
	{
		this->strTmp += T("Content-Transfer-Encoding:8bit\r\n\r\n");
	}

	//邮件内容
	str.Format(T("%s\r\n\r\n"), mailmsg.Body.GetConstData());
	this->strTmp += str;

	//将邮件内容发送出去
	if (!m_socket->Send(this->strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
	return true;
}

//-------------------------------------------------------------------------

bool SmtpClient::SendAttachments(const MailMessage& mailmsg)
{
	String str;
	for (int i = 0; i < mailmsg.Attachments->Length; i++)
	{
		Attachment *pAttach = &mailmsg.Attachments->GetValue(i);
		this->strTmp = T("--");
		this->strTmp += mailmsg.Boundary;
		this->strTmp += T("\r\n");

		str.Format(T("Content-Type:%s;Name="), 
			pAttach->ContentType.GetConstData());
		this->strTmp += str;
		this->strTmp += mailmsg.Attachments->GetValue(i).Name;
		this->strTmp += T("\r\n");

		if (pAttach->ContentDescription.GetLength())
		{
			str.Format(T("Content-Description:%s\r\n"), pAttach->ContentDescription.GetConstData());
			this->strTmp += str;
		}
		this->strTmp += T("Content-Disposition:attachment;FileName=");
		this->strTmp += mailmsg.Attachments->GetValue(i).Name;
		this->strTmp += T("\r\n");
		this->strTmp += T("Content-Transfer-Encoding:Base64\r\n\r\n");

		if (!m_socket->Send(this->strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
		{
			this->m_error.Ref(*this->m_socket->GetLastException());
			return false;
		}

		//读取文件并发送
		if (!SendFile(mailmsg.Attachments->GetValue(i).Path))
		{
			return false;
		} //if
		this->strTmp = T("\r\n");
		if (!m_socket->Send(this->strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
		{
			this->m_error.Ref(*this->m_socket->GetLastException());
			return false;
		}
	}
	return true;
}

//-------------------------------------------------------------------------

bool SmtpClient::SendFile(const String& strPath)
{
	IO::FileStream *file = IO::File::Open(strPath);
	if (!file)
	{
		Exception::GetException(&this->m_error);
		return false;
	}

	char *pBuffer;
	int BufferSize;
	int filesize = 0, filesize_all = file->Length;
	char chBuffer[256] = {0};
	while (filesize_all > 0)
	{
		filesize = file->Read(chBuffer, 256);
		pBuffer = System::Security::Cryptography::Base64::EncodeBytes(chBuffer, filesize, &BufferSize);
		if (!m_socket->Send(pBuffer, BufferSize))
		{
			this->m_error.Ref(*this->m_socket->GetLastException());
			delete file;
			return false;
		}
		filesize_all -= filesize;
	}
	delete file;
	return true;
}

//-------------------------------------------------------------------------

bool SmtpClient::Logout()
{
	this->strTmp = T("QUIT\r\n");
	if (!m_socket->Send(this->strTmp.ToMultiByte(), this->strTmp.GetMultiByteSize()))
	{
		this->m_error.Ref(*this->m_socket->GetLastException());
		return false;
	}
	if(GetResponseCode() != 221) return SetException<bool>(false);//服务关闭
	return true;
}