/********************************************************************
	Created:	2012/03/10  13:37
	Filename: 	RLib.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#pragma once
#ifndef _RLIB
#define _RLIB
#define _RLIB_VER 3.0
#define _WINTERNL_

#pragma warning(disable:4996 4200 4005)
//This function or variable may be unsafe
//使用了非标准扩展 : 结构/联合中的零大小数组
//宏重定义

#define _WINSOCKAPI_
#ifndef _DEBUG
#undef  NDEBUG
#define NDEBUG
#endif // _DEBUG

#include <Windows.h>
#include <tchar.h>
#include <assert.h>
#include <new>

#define STATUS_SUCCESS 0

#define RLIB_THREAD_SAFE

/*
 *是否启用浮点支持
 *必须启用, String::Format才能正常处理浮点数
*/
#define RLIB_EnableFloatSupport
//#undef  RLIB_EnableFloatSupport

/*
 *String::Format内部缓冲区大小, in TCHARs
*/
#define RLIB_STRINGFORMATLENGTH  1024

#define RLIB_MemoryCommit        1024 * 4 * 4/*内存页默认提交16KB*/
#define RLIB_MemoryReserve       1024 * 1024 * 4/*内存页默认保留4MB*/
#define RLIB_MemoryPoolCount     64/*内存池默认维护的内存页数量*/

#ifdef _UNICODE
    #define T(str)        _CRT_WIDE(str)
    #define RLIB_FILE     _STR2WSTR(__FILE__)
    #define RLIB_FUCTION  _STR2WSTR(__FUNCDNAME__)
#else
    #define T(str)        str
    #define RLIB_FILE     __FILE__
    #define RLIB_FUCTION  __FUNCDNAME__
#endif // _UNICODE

#ifdef _DEBUG
    #define RLIB_SetException(p, i, s) p->Set(i, s, RLIB_FILE, __FUNCDNAME__)
    #define RLIB_SetExceptionInfo(p)   p->SetDebugInfo(RLIB_FILE, __FUNCDNAME__)
#else
    #define RLIB_SetException(p, i, s) p->Set(i, s)
    #define RLIB_SetExceptionInfo(p)   void(0)
#endif // _DEBUG

#ifdef _WIN32
# define RLIB_NEWLINE    T("\r\n")
# define RLIB_NEWLINEA   "\r\n"
# define RLIB_NEWLINEW   L"\r\n"
#else // !_WIN32
# define RLIB_NEWLINE   T("\n")
# define RLIB_NEWLINEA   "\n"
# define RLIB_NEWLINEW   L"\n"
#endif // _WIN32

#ifdef RLIB_LIB
#define export
#ifndef RLIB_DLL
#pragma message("RLib已被设置编译成静态链接库, 可能需要自行复制RLib\\native\\ntdll.lib到项目目录.")
#endif // RLIB_DLL
#else
#ifdef RLIB_DLL
#define export __declspec(dllexport) //导出函数
#else
#define export __declspec(dllimport)
#endif
#endif

#ifdef _DEBUG
#define _RUNTIME_DEBUG
#else
#define _RUNTIME_DEBUG
#endif // assert

#ifdef _RUNTIME_DEBUG
extern void export RLib_Warning(LPCTSTR exp, LPCTSTR file, int line);
#define debug_warning(str) RLib_Warning(str, RLIB_FILE, __LINE__)
#define trace(_Expression) ((void)( (!!(_Expression)) || (RLib_Warning(T(#_Expression), T(__FILE__), __LINE__), 0) ))
#else
#define debug_warning(str) ((void)0)
#define trace(_Expression) ((void)0)
#endif // _RUNTIME_DEBUG

#ifdef _RUNTIME_DEBUG
#define RLIB_GlobalAlloc(n)        System::AppBase::Allocate(n, RLIB_FUCTION, RLIB_FILE)
#else
#define RLIB_GlobalAlloc(n)        System::AppBase::Allocate(n)
#endif // _RUNTIME_DEBUG
#define RLIB_GlobalCollect(p)      System::AppBase::Collect(p)

#define RLIB_THREAD              __declspec(thread)
#define RLIB_Type(v)             decltype(v)
#define RLIB_CheckThis           if(this == nullptr){ assert(!"类指针异常"); return; }
#define RLIB_CheckThisRet(a)     if(this == nullptr){ assert(!"类指针异常"); return a; }
#define RLIB_ClassNewDel         void *operator new(size_t size){ return RLIB_GlobalAlloc(size); };void operator delete(void *p){ RLIB_GlobalCollect(p); };
#define RLIB_InitClass(p, c)     (::new (p) c)/*为指定内存调用相应构造函数*/
#define RLIB_Delete(a)           if(a != nullptr){ delete a; }
#define RLIB_PStreamWriteAS(p,a) p->Write(a.ToMultiByte(), a.GetMultiByteSize());
#define RLIB_PStreamWriteA(p,a)  p->Write(a, sizeof(a) - sizeof(char));
#define RLIB_PStreamWriteWS(p,a) p->Write(a.ToWideChar(), a.GetWideCharSize());
#define RLIB_PStreamWriteW(p,a)  p->Write(L##a, sizeof(L##a) - sizeof(WCHAR));
#define RLIB_PStreamWriteTS(p,a) p->Write(a.GetConstData(), a.CanReadSize);
#define RLIB_PStreamWriteT(p,a)  p->Write(a, sizeof(a) - sizeof(TCHAR));

#endif // _RLIB