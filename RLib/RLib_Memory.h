/********************************************************************
	Created:	2011/08/14  20:33
	Filename: 	Lib_Memory.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_MEMORY
#define _USE_MEMORY
#include "RLib_Base.h"
//////////////////////////////////////////////////////////////////////////
typedef void *Object;
/// <summary>
/// 内存页指针状态
/// </summary>
typedef enum _PTR_STATUS
{
	PTR_IN = 0,
	PTR_HIGH,
	PTR_LOW
}PTR_STATUS;
//AccessProtection
#define RLIB_PAGE_NOACCESS          0x01     
#define RLIB_PAGE_READONLY          0x02     
#define RLIB_PAGE_READWRITE         0x04     
#define RLIB_PAGE_WRITECOPY         0x08     
#define RLIB_PAGE_EXECUTE           0x10     
#define RLIB_PAGE_EXECUTE_READ      0x20     
#define RLIB_PAGE_EXECUTE_READWRITE 0x40     
#define RLIB_PAGE_EXECUTE_WRITECOPY 0x80     
#define RLIB_PAGE_GUARD            0x100     
#define RLIB_PAGE_NOCACHE          0x200     
#define RLIB_PAGE_WRITECOMBINE     0x400     
//AllocationType
#define RLIB_MEM_COMMIT           0x1000     
#define RLIB_MEM_RESERVE          0x2000     
#define RLIB_MEM_DECOMMIT         0x4000     
#define RLIB_MEM_RELEASE          0x8000     
#define RLIB_MEM_FREE            0x10000     
#define RLIB_MEM_PRIVATE         0x20000     
#define RLIB_MEM_MAPPED          0x40000     
#define RLIB_MEM_RESET           0x80000     
#define RLIB_MEM_TOP_DOWN       0x100000     
#define RLIB_MEM_WRITE_WATCH    0x200000     
#define RLIB_MEM_PHYSICAL       0x400000     
#define RLIB_MEM_ROTATE         0x800000     
#define RLIB_MEM_LARGE_PAGES  0x20000000     
#define RLIB_MEM_4MB_PAGES    0x80000000     
#define RLIB_MEM_IMAGE        0x1000000     
#define RLIB_VALIDBYTE        0xCCU
//////////////////////////////////////////////////////////////////////////
#define GetCurrentHandle (HANDLE)-1                            //伪句柄
#define AlignSize        (sizeof(LONG))                        //对齐大小
#define ObjSize          (sizeof(Memory::BLOCK_INFO))              //头部大小
#define UserToHandle(p)  (Memory::PBLOCK_INFO(LPSTR(p) - ObjSize)) //转换操作地址
#define HandleToUser(p)  (LPSTR(p) + ObjSize)                  //转换用户地址
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 提供对IO访问的支持, 支持包括在流中读取和写入数据、压缩流中的数据、创建和使用独立存储区
	/// </summary>
	namespace IO
	{
		/// <summary>
		/// 内存页信息
		/// </summary>
		typedef struct _MEMORY_INFORMATION {
			/// <summary>
			/// 页基址
			/// </summary>
			PVOID BaseAddress;
			/// <summary>
			/// 分配基址
			/// </summary>
			PVOID AllocationBase;
			/// <summary>
			/// 分配保护属性
			/// </summary>
			DWORD AllocationProtect;
			/// <summary>
			/// 大小
			/// </summary>
			SIZE_T RegionSize;
			/// <summary>
			/// 未知
			/// </summary>
			DWORD State;
			/// <summary>
			/// 保护属性
			/// </summary>
			DWORD Protect;
			/// <summary>
			/// 未知
			/// </summary>
			DWORD Type;
		} MEMORY_INFORMATION, *PMEMORY_INFORMATION;
		/// <summary>
		/// 虚拟内存申请接口
		/// </summary>
		class export RLIB_THREAD_SAFE MemoryAllocator
		{
		public:
			/// <summary>
			/// 分配内存 按页对齐
			/// </summary>
			static LPVOID Allocate(HANDLE ProcessHandle, PVOID BaseAddress, ULONG RegionSize, ULONG AllocationType, ULONG Protect);
			/// <summary>
			/// 释放内存
			/// </summary>
			/// <returns>被释放内存页地址</returns>
			static LPVOID Free(HANDLE ProcessHandle, PVOID BaseAddress, ULONG RegionSize, ULONG FreeType);
			/// <summary>
			/// changes the access protection on a region of committed pages in the virtual address space of the calling process
			/// return OldAccessProtection, and zero if failed
			/// </summary>
			static ULONG Protect(HANDLE ProcessHandle, PVOID BaseAddress, ULONG RegionSize, ULONG NewAccessProtection);
			/// <summary>
			/// retrieves parameters of queried memory block
			/// 方法返回静态缓冲区指针, 非线程安全, 失败时返回空结构指针(非nullptr)
			/// </summary>
			static PMEMORY_INFORMATION Query(HANDLE ProcessHandle, PVOID BaseAddress);
			/// <summary>
			/// 获取对齐的分页大小
			/// </summary>
			static ULONG GetPageSize();
			/// <summary>
			/// 获取最大可分配页数量
			/// </summary>
			static ULONG GetPhysicalPages();
		};
		/// <summary>
		/// 内存页, 是内存池的核心组成
		/// </summary>
		class export Memory
		{
			friend class MemoryPool;
		private: //内部数据
			/// <summary>
			/// 分块结构
			/// </summary>
			typedef __declspec(align(4)) struct BLOCK_INFO;
			typedef BLOCK_INFO *PBLOCK_INFO;
			typedef __declspec(align(4)) struct BLOCK_INFO
			{
				//对齐大小
				LONG align; 
				//实际大小(用户申请字节数)
				LONG size;
				//前趋节点
				PBLOCK_INFO prior;
				//后继节点
				PBLOCK_INFO next;
#ifdef _RUNTIME_DEBUG
				//调试标志 值 RLIB_VALIDBYTE 表示有效
				LONG    ptr_valid;
				//记录申请位置
				LPCTSTR alloc_function;
				LPCTSTR alloc_file;
#endif // _RUNTIME_DEBUG
			}BLOCK_INFO;
			/// <summary>
			/// 内存页信息
			/// </summary>
			struct tagData
			{
				//内存页基址
				LPVOID m_base;
				//已提交大小
				LONG  m_now;
				//已保留内存
				LONG  m_max; 
				//已分配大小
				LONG  m_used;	
				//页大小
				LONG  m_page;
				//最后节点
				PBLOCK_INFO m_firstptr;
				//最后节点
				PBLOCK_INFO m_lastptr;
#ifdef _DEBUG
				//记录成功次数
				LONG   m_succeed;
				//记录分配次数
				LONG   m_failed;
#endif // _DEBUG
			}Data;
		private:
			PBLOCK_INFO find(LONG);   //从指定位置搜索指定大小的可用内存,失败返回NULL
			Object alloc_block(LONG); //分配指定大小的内存,失败返回NULL
			void   free_block(Object); //释放内存块
			bool   extend(LONG);      //增加至少指定大小的内存,失败返回false
			void   init(LONG, LONG); //必要的初始化
			bool   check_overflow(PBLOCK_INFO);
		public: //构造销毁
			/// <summary>
			/// 该方法调用 init 初始化 Memory 实例,默认提交4KB内存,保留4MB内存
			/// </summary>
			Memory();
			/// <summary>
			/// 该方法调用 init 初始化 Memory 实例,并提交、保留指定大小的内存
			/// </summary>
			Memory(LONG, LONG);
			/// <summary>
			/// 应确保此方法被正常调用以释放所有内存
			/// </summary>
			~Memory(); 
			RLIB_ClassNewDel;
		public: //实例方法
			/// <summary>
			/// 在内存页中分配指定字节大小的内存, 失败返回NULL
			/// 返回的内存已被初始化为空白(填充0)
			/// </summary>
#ifdef _RUNTIME_DEBUG
			void *AllocByte(LONG, LPCTSTR function, LPCTSTR file);
#else
			void *AllocByte(LONG);
#endif // _RUNTIME_DEBUG
			/// <summary>
			/// 回收由 AllocByte 返回的内存以供下次分配
			/// @warning 并不保证内存立即被释放
			/// </summary>
			void Collect(Object);    
			/// <summary>
			/// 修改由 AllocByte 返回的已分配内存为指定大小, 为0将调用 Collect 释放
			/// 该方法可在不改变指针情况下缩减、增大内存, 但不保证一定成功
			/// </summary>
			bool ReSize(Object, LONG);
			/// <summary>
			/// 将整个内存页写到指定文件以供分析
			/// </summary>
			bool DumpToFile(TCHAR *);
			/// <summary>
			/// 尝试释放占用的未分配内存, Collect方法仅回收到内存页而非真正释放
			/// 调用该方法不保证成功释放, 返回释放的字节数
			/// </summary>
			LONG TryClean();                  
			/// <summary>
			/// 获取不含内存块信息的内存大小,该值等于传入 AllocByte 方法的值 
			/// </summary>
			LONG GetSize(LPCVOID);	    
			/// <summary>
			/// 获取内存页中已分配内存大小	
			/// </summary>
			LONG GetUsingSize();      
			/// <summary>
			/// 获取内存页大小(已提交的内存)	
			/// </summary>
			LONG GetMemorySize();    
			/// <summary>
			/// 获取当前页最大能分配的内存大小(不一定十分准确)
			/// </summary>
			LONG GetMaxAllocSize();
			/// <summary>
			/// 重新分配内存为指定大小并拷贝、释放原分配的内存 
			/// 默认 new_size 为0时不改变内存分配大小(但是也可能改变内存位置)
			/// </summary>
			/// <returns>失败返回NULL, 此时原内存不会被修改</returns>
			void *ReAlloc(Object, LONG new_size = 0); 
			/// <summary>
			/// 返回一个值,该值指示传入的的指针是否由此 Memory 类分配的,非0(true)表示指针不属于此内存页
			/// </summary>
			PTR_STATUS Validate(LPCVOID);                     
		public:
			/// <summary>
			/// 等价于::memset
			/// </summary>
			static inline void memset(void *src, int val, LONG size){::memset(src, val, size);};       
			/// <summary>
			/// 等价于::memcpy
			/// ps: memcpy考虑内存重叠问题
			/// </summary>
			static inline void memcpy(void *dest, const void *source, LONG dwsize){::memcpy(dest, source, dwsize);};
		};
	};
};
//////////////////////////////////////////////////////////////////////////
#endif //_USE_MEMORY