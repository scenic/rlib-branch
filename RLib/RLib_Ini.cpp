/********************************************************************
	Created:	2012/07/26  10:47
	Filename: 	RLib_Ini.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "RLib_Ini.h"
using namespace System::Config::Ini;
using namespace System::Collections::Generic;
// ---------------------------------------------------------------------------
//                                  IMPLEMENTATION
// ---------------------------------------------------------------------------
IniFile::~IniFile()
{
	this->Clear();
}

//-------------------------------------------------------------------------
void IniFile::Clear()
{
	while(this->Items.Length > 0)
	{
		this->Items[0]->Delete();
		this->Items.RemoveAt(0);;
	}
}
//-------------------------------------------------------------------------
bool IniFile::LoadFromFile(String ini_file)
{
	auto pFile = IO::File::Open(ini_file, File::OpenMode, File::ReadAccess, File::ReadShare);
	if (pFile == nullptr)
	{
		Exception::GetException(&this->m_error);
		return false;
	} //if
	bool b_Result = this->LoadFromStream(pFile, pFile->Length);
	delete pFile;
	if (b_Result)
	{
		this->m_ini_file = ini_file;
	} //if
	return b_Result;
}

//-------------------------------------------------------------------------
bool IniFile::LoadFromStream(Stream *ini_data, long ini_data_size)
{
	auto pStream = Text::Encoder::ToCurrentEncoding(Text::UnknownEncoding, ini_data, ini_data_size);
	if (pStream == nullptr)
	{
		RLIB_SetException((&this->m_error), -1, T("无法转换数据流到当前编码"));
		return false;
	} //if
	pStream->Position = 0;
	this->m_ini_data.PrepareMemory(pStream->Length / sizeof(TCHAR));
	pStream->Read(this->m_ini_data, pStream->Length);
	delete pStream;
	return this->LoadFromString(Nothing);
}

//-------------------------------------------------------------------------
bool IniFile::LoadFromString(String ini_string)
{
	if (this->m_ini_data.IsNullOrEmpty())
	{
		if (ini_string.IsNullOrEmpty())
		{
			RLIB_SetException((&this->m_error), -1, T("无效数据源"));
			return false;
		} //if
		this->m_ini_data = ini_string;
	} //if
	//忽略对 \r 的考虑, 统一处理 \n 为换行
	this->m_ini_data = this->m_ini_data.Replace(T("\r"), T(""));
	return this->parse_ini();
}

//-------------------------------------------------------------------------
LPCTSTR IniFile::next_char(LPCTSTR ptr)
{
	if (ptr == nullptr)
	{
		return nullptr;
	}
	while (*ptr != T('\0') && *ptr != T('\n') && (_istspace(*ptr)))
	{
		assert(*ptr != T('\r'));
		++ptr;
	}
	return ptr;
}

//-------------------------------------------------------------------------
bool IniFile::parse_ini()
{
	this->Clear();

	IniSection *p_cur_section = nullptr;

	LPCTSTR ptr_begin = this->m_ini_data;
	LPCTSTR ptr_prev  = ptr_begin, ptr_next  = ptr_prev;
	while((ptr_next = this->next_char(ptr_next)) != nullptr)
	{
		switch(*ptr_next)
		{
		case T('\n'):
			{
				if(ptr_next == ptr_prev)
				{
					//纯换行
					(p_cur_section != nullptr ? p_cur_section->Items : this->Items).Add(new IniKey(Nothing, Nothing));
					ptr_prev = ++ptr_next;
					break;
				}
				//读取
				String line_string = String(ptr_prev).Substring(0, ptr_next - ptr_prev);
				int    offset      = line_string.IndexOf(T("="));
				if (offset == -1)
				{
					//没有Value
					(p_cur_section != nullptr ? p_cur_section->Items : this->Items).Add(new IniKey(line_string, Nothing));
				} //if
				else
				{
					(p_cur_section != nullptr ? p_cur_section->Items : this->Items).Add(
						new IniKey(line_string.Substring(0, offset), line_string.Substring(offset + 1)));
				}
			}
			ptr_prev = ++ptr_next;
			break;
		case T('#'):
		case T(';'):
			{
				if((ptr_next != ptr_begin) && *(ptr_next - 1) != T('\n'))
				{
					//忽略非行首注释
					goto parse_ignore;
				}
				//注释
				auto p_comment_end = _tcsstr(ptr_next, T("\n"));
				if (p_comment_end == nullptr)
				{
					//最后一行均是注释
					(p_cur_section != nullptr ? p_cur_section->Items : this->Items).Add(
						new IniComment(String(LPTSTR(ptr_prev + 1)/*强制String拷贝字符串*/), String(ptr_next).Substring(0, 1)));
					goto parse_done;
				} //if
				
				int CommentLength = p_comment_end - ptr_next - 1;//注释内容长度
				(p_cur_section != nullptr ? p_cur_section->Items : this->Items).Add(
					new IniComment(CommentLength != 0 ? String(LPTSTR(ptr_prev + 1)).Substring(0, CommentLength) : Nothing, String(ptr_next).Substring(0, 1)));
				ptr_next = p_comment_end;
			}
			ptr_prev = ++ptr_next;
			break;
		case T('['):
			{
				if((ptr_next != ptr_begin) && *(ptr_next - 1) != T('\n'))
				{
					//忽略非行首节
					goto parse_ignore;
				}
				//节
				auto p_section_end = _tcsstr(ptr_next, T("]"));
				if (p_section_end == nullptr)
				{
					//非法文件
					RLIB_SetException((&this->m_error), -1, T("缺失']', 解析失败"));
					goto parse_fail;
				} //if
				p_cur_section = new IniSection(String(LPTSTR(ptr_prev + 1)).Substring(0, p_section_end - (ptr_prev + 1)));
				this->Items.Add(p_cur_section);
				ptr_next = p_section_end + 1;
				if (*ptr_next != T('\n'))
				{
					RLIB_SetException((&this->m_error), -1, T("']'后面必须是换行符"));
					goto parse_fail;
				} //if
			}
			ptr_prev = ++ptr_next;
			break;
		case T('\0'):
			{
parse_done:
				return true;
			}
			break;
		default:
			{
parse_ignore:
				ptr_next++;
			}
		}
	}
parse_fail:
	return false;
}

//-------------------------------------------------------------------------

bool IniFile::SaveFile(Text::Encoding codepage /* = Text::UnknownEncoding */)const
{
	if (this->m_ini_file.IsNullOrEmpty())
	{
		RLIB_SetException(((IniException *)&this->m_error), -1, T("call LoadFromFile first"));
		return false;
	} //if
	return SaveFile(this->m_ini_file, codepage);
}

//-------------------------------------------------------------------------

bool IniFile::SaveFile(const TCHAR *filename, Text::Encoding codepage /* = Text::UnknownEncoding */)const
{
	System::IO::FileStream *file = System::IO::File::Create(filename, IO::File::CreateNewMode);
	if (file)
	{
		bool result = SaveFile(file, codepage);
		delete file;
		return result;
	}
	Exception::GetException((IniException *)&this->m_error);
	return false;
}

//-------------------------------------------------------------------------

bool IniFile::SaveFile(System::IO::Stream *fp, Text::Encoding codepage /* = Text::UnknownEncoding */)const
{
	if (codepage == 0)
	{
#ifdef _UNICODE
		codepage = Text::UTF16Encoding;
#else 
		codepage = Text::ASCIIEncoding;
#endif // _UNICODE
	} //if

	switch (codepage)
	{
	case System::Text::ASCIIEncoding:
		break;
	case System::Text::UTF16Encoding:
		break;
	case System::Text::UTF16FEncoding:
		break;
	case System::Text::UTF8Encoding:
		break;
	default:
		{
			RLIB_SetException(((IniException *)&this->m_error), -1, T("codepage not supported"));
			assert(!"不支持的编码格式");
			return false;
		}
	}


	IO::BufferedStream *output = new IO::BufferedStream(4096);

	foreach(pItem, this->Items)
	{
		(*pItem)->Print(output);
	}

	output->Position = 0;

	Text::Encoder::WriteTextStream(fp, output, 0, true, codepage);

	delete output;

	return true;
}

//-------------------------------------------------------------------------
void IniSection::Print(IO::Stream *cout) const
{
	RLIB_PStreamWriteT(cout, T("["));
	if(!this->Value.IsNullOrEmpty())
	{
		RLIB_PStreamWriteTS(cout, this->Value);
	}
	else
	{
		RLIB_PStreamWriteT(cout, T("null"));
	}
	RLIB_PStreamWriteT(cout, T("]"));
	RLIB_PStreamWriteT(cout, RLIB_NEWLINE);

	foreach(pItem, this->Items)
	{
		(*pItem)->Print(cout);
	}
}

//-------------------------------------------------------------------------
void IniKey::Print(IO::Stream *cout) const
{
	if (!this->Name.IsNullOrEmpty())
	{
		RLIB_PStreamWriteTS(cout, this->Name);
		if(!this->Value.IsNullOrEmpty())
		{
			RLIB_PStreamWriteT(cout, T(" = "));
			RLIB_PStreamWriteTS(cout, this->Value);
		}
	} //if
	RLIB_PStreamWriteT(cout, RLIB_NEWLINE);
}

//-------------------------------------------------------------------------
void IniComment::Print(IO::Stream *cout) const
{
	if (!this->CommentPrefix.IsNullOrEmpty())
	{
		RLIB_PStreamWriteTS(cout, this->CommentPrefix);
	}
	else
	{
		RLIB_PStreamWriteT(cout, T(";"));
	}
	if(!this->Value.IsNullOrEmpty())
	{
		RLIB_PStreamWriteTS(cout, this->Value);
	}
	RLIB_PStreamWriteT(cout, RLIB_NEWLINE);
}