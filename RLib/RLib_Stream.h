/********************************************************************
	Created:	2012/04/22  8:57
	Filename: 	RLib_Stream.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_STREAM
#define _USE_STREAM
#include "RLib_List.h"
#include <Strsafe.h>
#include <Winternl.h>
//////////////////////////////////////////////////////////////////////////
//#pragma warning(disable:4100) //未引用的形参
namespace System
{
	namespace IO
	{
		/// <summary>
		/// 提供字节序列的一般视图, 是所有流的基类
		/// </summary>
		class export Stream
		{
		protected:
			// 当前流中的位置(下一个就绪位置)
			LONG  m_pos;  
			// 流大小, in bytes
			LONG  m_size; 
			// 流已写长度, in bytes
			LONG  m_length; 
			// 流缓冲区指针, in bytes
			LPVOID m_buffer;  
		private:
			void *operator new(size_t){ return NULL; };
			void operator delete(void *){ };
		public:
			/// <summary>
			/// 初始化
			/// </summary>
			Stream():m_pos(0),m_size(0),m_length(0),m_buffer(nullptr){/*Initialize*/};
			/// <summary>
			/// 用内存指针、内存大小、实际长度初始化
			/// </summary>
			Stream(LPVOID lpbuffer, LONG size, LONG len):m_pos(0){ this->m_buffer  = lpbuffer; this->m_size = size; this->m_length = len; };
			/// <summary>
			/// 获取当前流中的位置
			/// </summary>
			virtual LONG GetPos() const{ return this->m_pos; };
			/// <summary>
			/// 设置当前流中的位置
			/// </summary>
			virtual void SetPos(LONG Pos);
			/// <summary>
			/// 获取或设置当前流中的位置
			/// 约定: 如果是返回新流缓冲对象, Position必须置为0
			/// </summary>
			__declspec(property(get = GetPos, put = SetPos)) LONG Position;
			/// <summary>
			/// 获取流数据指针
			/// </summary>
			virtual LPVOID GetObjectData() const{ return this->m_buffer; }
			/// <summary>
			/// 设置流数据指针
			/// </summary>
			virtual void SetObjectData(LPVOID pData){ this->m_buffer = pData; };
			/// <summary>
			/// 获取或设置流数据指针
			/// </summary>
			__declspec(property(get = GetObjectData, put = SetObjectData)) LPVOID ObjectData;
			/// <summary>
			/// 获取用字节表示的流大小
			/// </summary>
			virtual LONG GetSize() const{ return this->m_size; };
			/// <summary>
			/// 设置用字节表示的流大小
			/// </summary>
			virtual void SetSize(LONG size){ this->m_size = size; };
			/// <summary>
			/// 获取或设置用字节表示的流大小
			/// </summary>
			__declspec(property(get = GetSize, put = SetSize)) LONG Size;
			/// <summary>
			/// 获取用字节表示的流长度
			/// </summary>
			virtual LONG GetLength() const{ return this->m_length; };
			/// <summary>
			/// 设置用字节表示的流长度
			/// </summary>
			virtual void SetLength(LONG len){ this->m_length = len; };
			// 获取用字节表示的流长度
			__declspec(property(get = GetLength, put = SetLength)) LONG Length;
			/// <summary>
			/// 获取流当前位置的指针
			/// </summary>
			void *GetCurrentPtr() const{
				return LPBYTE(this->ObjectData) + this->Position; 
			};
			// 获取流当前位置的指针
			__declspec(property(get = GetCurrentPtr)) void *CurrentPtr;
			/// <summary>
			/// 获取流当前位置起最大可读取长度
			/// </summary>
			LONG GetMaxReadSize() const{
				assert(this->Length >= this->Position) ; 
				return this->Length - this->Position; 
			};
			// 获取流当前位置起最大可读取长度
			__declspec(property(get = GetMaxReadSize)) LONG MaxReadSize;
			/// <summary>
			/// 获取流当前位置起最大可写入长度(不改变内存分配)
			/// </summary>
			LONG GetMaxWriteSize() const{
				assert(this->Size >= this->Position) ; 
				return this->Size - this->Position; 
			};
			// 获取流当前位置起最大可读取长度
			__declspec(property(get = GetMaxWriteSize)) LONG MaxWriteSize;
			/// <summary>
			/// 从当前流中读取字节块并将数据写入 buffer 中
			/// </summary>
			/// <returns>
			/// 写入缓冲区中的总字节数. 如果当前可用字节数不到所请求的字节数,
			/// 则这一总字节数可能小于所请求的字节数,或者如果在读取任何字节前已到达流的末尾则为零
			/// 产生任何异常均返回-1
			/// </returns>
			virtual int Read(LPVOID buffer, int count) const;
			/// <summary>
			/// 使用从缓冲区读取的数据将字节块写入当前流
			/// </summary>
			/// <returns>如果写操作成功,则流中的位置将提升写入的字节数</returns>
			virtual void Write(LPCVOID buffer, int count);
			/// <summary>
			/// 参见另一重载方法 Read
			/// </summary>
			int Read(Stream *dest, int count = 0) const{ 
				if (count <= 0) count = this->MaxReadSize; 
				//@warning 忽略检查dest的大小
				int result = this->Read(dest->CurrentPtr, count); 
				if (result != -1) dest->Position += result;
				return result;
			};
			/// <summary>
			/// 参见另一重载方法 Write
			/// </summary>
			void Write(Stream *dest, int count = 0){
				if (count <= 0) count = dest->MaxReadSize; 
				assert((int)dest->MaxReadSize >= count);
				this->Write(dest->CurrentPtr, count); 
			}
			/// <summary>
			/// 关闭当前流并释放与之关联的所有资源
			/// </summary>
			virtual void Close() = 0;
		};
		/// <summary>
		/// 此类支持使用现有的基于流的模型访问内存, 是对Stream的具体化
		/// </summary>
		class export UnmanagedMemoryStream:public Stream
		{
		private:
			/// <summary>
			/// 实现父类方法, 空函数
			/// </summary>
			void Close(){};
		public:
			RLIB_ClassNewDel;
			/// <summary>
			/// 默认构造函数
			/// </summary>
			inline UnmanagedMemoryStream(){};
			/// <summary>
			/// 用内存指针、内存大小、实际长度初始化
			/// </summary>
			UnmanagedMemoryStream(LPVOID lpbuffer, LONG size, LONG len):Stream(lpbuffer, size, len){};
		};
		/// <summary>
		/// 创建其支持存储区为内存的流
		/// 但该流具有固定大小
		/// </summary>
		class export MemoryStream:public Stream
		{
		public:
			/// <summary>
			/// 初始化指定大小的内存流
			/// </summary>
			MemoryStream(LONG);
			/// <summary>
			/// 负责内存流的回收
			/// </summary>
			~MemoryStream();
			RLIB_ClassNewDel;
		public:
			/// <summary>
			/// @warning 方法不被支持
			/// </summary>
			inline void SetObjectData(LPVOID/* pData*/){};
			/// <summary>
			/// 获取流数据指针
			/// </summary>
			__declspec(property(get = GetObjectData)) LPVOID ObjectData;
			/// <summary>
			/// @warning 方法不被支持
			/// </summary>
			inline void SetSize(LONG/* size*/){};
			/// <summary>
			/// 获取用字节表示的流大小
			/// </summary>
			__declspec(property(get = GetSize)) LONG Size;
			/// <summary>
			/// 设置用字节表示的流长度, 方法允许截断流
			/// </summary>
			void SetLength(LONG/* len*/);
			/// <summary>
			/// 重写基类方法
			/// 关闭当前流并释放与之关联的所有资源
			/// </summary>
			void Close(); 
		};
		/// <summary>
		/// 创建其支持存储区为内存的流
		/// 但该流支持动态内存分配
		/// </summary>
		class export BufferedStream:public MemoryStream
		{
		public:
			/// <summary>
			/// 使类可实例化
			/// </summary>
			BufferedStream(LONG size = sizeof(int)):MemoryStream(size){};
			RLIB_ClassNewDel;
		public:
			/// <summary>
			/// 重载基类方法以支持动态内存分配
			/// </summary>
			void Write(LPCVOID buffer, int count);
			/// <summary>
			/// 动态增加分配指定字节内存, 原来的内容会被保留
			/// @warning 不支持减少分配内存
			/// </summary>
			bool AppendSize(unsigned int count);
		};
	};
};
//////////////////////////////////////////////////////////////////////////
#endif