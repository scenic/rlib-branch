//-----------------------------------------
// file:	obama64.h
// version:	v1.2
// author:	zhongqiu
// Copyright (c) 2011 zhongqiu All rights reserved.
//-----------------------------------------
#pragma warning(disable:4530)

#include <cstdlib>
#include <ctime>
#include <iostream>
#include "RLib_Obama64.h"

using namespace System;
using namespace System::Security::Cryptography;

//掩码：用于限制到码表坐标
#define MASK_64 0x3F

/*-----------------------------------------------------------*/
/*constructor & destructor*/
Obama64::Obama64()
{
    this->EnableBluff = TRUE;
    this->m_secret = 0x43;

    static const char Serializable[64] = 
    {
        // 默认序列
		'P', 'e', 'r', 'Q', 'f', 'w', '7', 'g', 'i', 'p', 	/*  0- 9 */
		'8', '9', 'B', 'd', 'O', 'v', '6', 'S', 'D', 'M', 	/* 10-19 */
		'b', 's', 'R', 'C', 'N', 'c', 'm', '5', 'l', 'z', 	/* 20-29 */
		'I', 'X', 'o', 'j', 'H', '2', 'x', 'W', '1', 'J', 	/* 30-39 */
		'V', 'h', 'G', '0', 'Y', 'q', 'E', 'T', 'k', '3', 	/* 40-49 */
		'a', 'L', 'y', 'n', 't', 'U', 'u', 'Z', '4', 'K', 	/* 50-59 */
		'F', 'A', '_', '-'									/* 60-63 */ //95,45, 
	};

    this->m_encode = (char*)RLIB_GlobalAlloc(64 *sizeof(char));
    this->m_decode = (int*)RLIB_GlobalAlloc(64 * 2 * sizeof(int));

    System::IO::Memory::memcpy(this->m_encode, Serializable, 64 *sizeof(char));

    InitDecode();
}

//-------------------------------------------------------------------------

Obama64::~Obama64()
{
    RLIB_GlobalCollect(this->m_encode);
    RLIB_GlobalCollect(this->m_decode);
}

/*--------------------------------------------------------------------*/
/*重初始化码表*/
inline void _swap_(char *arr, int i, int j)
{
//     arr[i] = arr[i] ^ arr[j];
//     arr[j] = arr[i] ^ arr[j];
//     arr[i] = arr[i] ^ arr[j];
	char tmp = arr[i];
	arr[i]    = arr[j];
	arr[j]    = tmp;
}

//-------------------------------------------------------------------------

void Obama64::Init()
{
    srand((unsigned int)time(nullptr)); // rand seed

    //乱序基表
    for (int i = 64; i > 0; i--)
    {
        _swap_(m_encode, i - 1, rand() % i);
    }

    InitDecode();
}

//-------------------------------------------------------------------------

void Obama64::InitDecode()
{
    System::IO::Memory::memset(m_decode,  -1, 128);

    for (int i = 0; i < 64; i++)
    {
        m_decode[m_encode[i]] = i;
    }
}

/*--------------------------------------------------------------------*/
/*设置密钥*/
void Obama64::SetSecret(char s)
{
    EnableBluff = FALSE;
    m_secret = s;
}

/*--------------------------------------------------------------------*/
/*编码/解码*/
int Obama64::encode(char plaintext, char secret)
{
    return plaintext ^ secret;
}

//-------------------------------------------------------------------------

int Obama64::decode(char cryptograph, char secret)
{
    return cryptograph ^ secret;
}

//-------------------------------------------------------------------------


char *Obama64::Encode(const char* Data, int DataByte, int *OutByte /* = nullptr */)
{
    if (Data == nullptr || DataByte == 0)
    {
        return nullptr;
    }

    int newDataByte = DataByte + (int)ceil(DataByte / 3.0) + 2;
	if (OutByte != nullptr)
	{
		*OutByte = newDataByte;
	} //if

    char *cArray = (char *)Alloc(newDataByte);

    if (this->EnableBluff)
    {
        srand((unsigned int)time(nullptr));
        int s = 0;
        int r = rand() % 26;
        bool u = ((r ^ DataByte) &1) == 1;
        s = u ? 65 : 97;
        cArray[0] = (char)(r + s);
    }
    else
    {
        cArray[0] = m_secret;
    }

    char c = 0;
    unsigned int n = 0;
    int mark = 0;
    int pos = 0;
    int segs = 0;
    for (int i = 0; i < DataByte; i += 3)
    {
        mark = 1 + i + segs;
        cArray[mark] = 0;

        for (int k = 0; (k < 3) && (pos < DataByte); k++)
        {
            c = Data[pos];
            if (c < 0)
            {
                c = (char)~c;
                cArray[mark] |= (2 << (k << 1));
            }
            n = encode(c, cArray[0]);
            n ^= k;
            cArray[mark] |= ((n >> 6) << (k << 1));
            cArray[mark + k + 1] = (char)m_encode[n & MASK_64];
            pos++;
        }
        segs++;
        cArray[mark] = (char)m_encode[cArray[mark]];
    }

    return cArray;
}

//-------------------------------------------------------------------------


char *Obama64::Decode(const char* Data, int DataByte, int *OutByte /* = nullptr */)
{
    if (Data == nullptr || DataByte == 0)
    {
        return nullptr;
    }

    int newDataByte = DataByte - (int)ceil((DataByte - 1) / 4.0);

	char *cArray = (char *)Alloc(newDataByte);

    char secret = Data[0];
    char c = 0;
    unsigned int mark = 0;
    int index = 0;
    int tmp = 0;
    for (int i = 1; i < DataByte; i += 4)
    {
        mark = m_decode[Data[i]];
        for (int k = 0, pos = i + k + 1; k < 3 && pos < DataByte; k++, pos++)
        {
            c = Data[pos];
            tmp = mark >> (k << 1);
            cArray[index] = (char)decode((char)((m_decode[c] + ((tmp &1) << 6)) ^ k), secret);
            if ((tmp & 2) > 0)
            {
                cArray[index] = (char)~cArray[index];
            }
            index++;
        }
    }
	cArray[newDataByte - 1] = T('\0');

	if (OutByte != nullptr)
	{
		*OutByte = index - 1;
	} //if

    return cArray;
}

//-------------------------------------------------------------------------

String Obama64::EncodeAsciiString(const char *content, int content_length /* = 0 */)
{
    if (content == nullptr)
    {
        return Nothing;
    }
	if (content_length == 0)
	{
		content_length = strlen(content);
	}

    int newLen = content_length + (int)ceil(content_length / 6.0) + 2;

    char *cArray = (char *)Alloc(newLen);

    if (EnableBluff)
    {
        int s = 0;
        int r = rand() % 26;
        bool u = ((r ^ content_length) &1) == 1;
        s = u ? 65 : 97;
        cArray[0] = (char)(r + s);
    }
    else
    {
        cArray[0] = m_secret;
    }


    char c = 0;
    unsigned int n = 0;
    int mark = 0;
    int pos = 0;
    int segs = 0;
    for (int i = 0; i < content_length; i += 6)
    {
        mark = 1+i + segs;
        cArray[mark] = 0;
        for (int k = 0; (k < 6) && (pos < content_length); k++)
        {
            c = content[pos];
            n = encode(c, cArray[0]);

            n ^= k;
            cArray[mark] |= ((n >> 6) << k);

            cArray[mark + k + 1] = (char)m_encode[n & MASK_64];
            pos++;
        }
        segs++;
        cArray[mark] = (char)m_encode[cArray[mark]];
    }
	cArray[newLen - 1] = T('\0');

    String Result(cArray);
	Collect(cArray);
	return Result;
}

//-------------------------------------------------------------------------

String Obama64::DecodeAsciiString(const char *content, int content_length /* = 0 */)
{
	if (content == nullptr)
	{
		return Nothing;
	}
	if (content_length == 0)
	{
		content_length = strlen(content);
	}

    int newLen = content_length - (int)ceil((content_length - 1) / 7.0);

	char *cArray = (char *)Alloc(newLen);

    char secret = content[0];
    char c = 0;
    unsigned int mark = 0;
    int index = 0;
    for (int i = 1; i < content_length; i += 7)
    {
        mark = m_decode[content[i]];
        for (int k = 0, pos = i + k + 1; k < 6 && pos < content_length; k++, pos++)
        {
            c = content[pos];
            cArray[index] = (char)decode((char)((m_decode[c] + (((mark >> k) &1) << 6)) ^ k), secret);
            index++;
        }
    }
	cArray[newLen - 1] = T('\0');
	String Result(cArray);
	Collect(cArray);
	return Result;
}
