/********************************************************************
Created:	2011/02/16  15:29
Filename: 	Lib_File.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_File.h"
#pragma comment(lib, "native/Ntdll.lib")
//////////////////////////////////////////////////////////////////////////
using namespace System::IO;
/******************************************************************
 *              collapse_path
 *
 * Get rid of . and .. components in the path.
 */
static inline void collapse_path( TCHAR *path, UINT mark )
{
	TCHAR *p, *next;

	/* collapse duplicate backslashes */
	next = path + mark;
	for (p = next; *p; p++) if (*p != '\\' || next[-1] != '\\') *next++ = *p;
	*next = 0;

	p = path + mark;
	while (*p)
	{
		if (*p == '.')
		{
			switch(p[1])
			{
			case '\\': /* .\ component */
				next = p + 2;
				memmove( p, next, (TLEN(next) + 1) * sizeof(TCHAR) );
				continue;
			case 0:  /* final . */
				if (p > path + mark) p--;
				*p = 0;
				continue;
			case '.':
				if (p[2] == '\\')  /* ..\ component */
				{
					next = p + 3;
					if (p > path + mark)
					{
						p--;
						while (p > path + mark && p[-1] != '\\') p--;
					}
					memmove( p, next, (TLEN(next) + 1) * sizeof(TCHAR) );
					continue;
				}
				else if (!p[2])  /* final .. */
				{
					if (p > path + mark)
					{
						p--;
						while (p > path + mark && p[-1] != '\\') p--;
						if (p > path + mark) p--;
					}
					*p = 0;
					continue;
				}
				break;
			}
		}
		/* skip to the next component */
		while (*p && *p != '\\') p++;
		if (*p == '\\')
		{
			/* remove last dot in previous dir name */
			if (p > path + mark && p[-1] == '.') memmove( p-1, p, (TLEN(p) + 1) * sizeof(TCHAR) );
			else p++;
		}
	}
	/* remove trailing spaces and dots (yes, Windows really does that, don't ask) */
	while (p > path + mark && (p[-1] == ' ' || p[-1] == '.')) p--;
	*p = 0;
}

//-------------------------------------------------------------------------

Path::Path(System::String path)//实现RtlDosPathNameToNtPathName
{
// 	UNICODE_STRING NtPathName;
// 	RtlDosPathNameToNtPathName_U(path, &NtPathName, nullptr, nullptr);
// 	this->FullPath = NtPathName.Buffer;
// 	return;
    if (path.IsNullOrEmpty())
    {
        assert("路径不允许为空");
		return;
    }

	int mark = 0;

    //规范化路径
    path = path.Replace(T("/"), T("\\")).Replace(T("\\.\\"), T("\\"));

	//忽略NT绝对路径
    if (path.IndexOf(T("\\??\\")) == 0)
    {
        this->FullPath = path;
        goto exit_ret;
    }

    //DOS路径前缀
    this->FullPath = T("\\??\\");

    //DOS绝对路径转换为NT绝对路径
    int separator_offset;
	if ((separator_offset = path.IndexOf(T(":\\"))) !=  - 1)
    {
		assert(separator_offset == 1);//盘符默认1位
        this->FullPath.Append(path);
		mark = separator_offset + 2;
        goto exit_ret;
    } //if
	
	//处理UNC路径
	if (path.IndexOf(T("\\\\")) == 0)
	{
		this->FullPath += T("UNC");
		this->FullPath += path.Substring(1)/*去除一个反斜扛*/;
		goto exit_ret;
	}

	//处理DOS/UNC相对路径
	LPCWSTR lpStartupPath = AppBase::GetStartupPath();
	if (lpStartupPath[0] == L'\\' && lpStartupPath[1] == L'\\'/*可能发生读取溢出*/)
	{
		this->FullPath += T("UNC");
		this->FullPath += &lpStartupPath[1]/*去除一个反斜扛*/;
	}
	else
	{
		this->FullPath += lpStartupPath;
	}
	if (path.StartsWith(T('\\')))
	{
		this->FullPath += path.Substring(1);
	}
	else
	{
		this->FullPath += path;
	}

exit_ret: 
    if (mark == 0)
    {
		mark = this->FullPath.IndexOf(T(":\\"), 4) + 2;
		if (mark == (-1 + 2))
		{
			mark = this->FullPath.IndexOf(T("UNC\\"), 4) + 4;
			assert(mark != (-1 + 4));
			mark = this->FullPath.IndexOf(T("\\"), mark) + 1;
			assert(mark != (-1 + 1));
		} //if
    } //if
	collapse_path(this->FullPath, mark);
}

//-------------------------------------------------------------------------

String Path::GetDosPath()
{
	String DosPath = this->FullPath.Substring(sizeof("\\??\\") - 1);
	int unc_offset = DosPath.IndexOf(T("UNC\\")) + 3;
	if (unc_offset != (-1 + 3))
	{
		DosPath = String(T("\\")) + DosPath.Substring(unc_offset);
	} //if
	return DosPath;
}

/************************************************************************/
/* File                                                                
/************************************************************************/
bool File::GetLastError(LPVOID buffer, size_t size, DWORD err /* = 0 */)
{
    return Exception::GetLastError(buffer, size, err);
}

//-------------------------------------------------------------------------

FileStream *File::Open(System::String str_path, FileMode mode /* = OpenMode */, FileAccess access /* = ReadWriteAccess */, FileShare share /* = NoneShare */, FileOptions options /* = NoneOption */)
{
    if (mode == CreateMode || mode == CreateNewMode)
    {
        assert(!"参数错误");
        return NULL;
    }
    Path path(str_path);
	int  size = path.FullPath.GetWideCharSize();
    WCHAR *dos_path = (WCHAR *)RLIB_GlobalAlloc(size + sizeof(WCHAR));
    System::IO::Memory::memcpy(dos_path, path.FullPath.ToWideChar(), size);
    /// <summary>
    /// NT API要求使用Unicode String
    /// </summary>
    UNICODE_STRING *us_path = (UNICODE_STRING*)RLIB_GlobalAlloc(sizeof(UNICODE_STRING));
    RtlInitUnicodeString(us_path, dos_path);
    /// <summary>
    /// 初始化对象
    /// </summary>
    OBJECT_ATTRIBUTES obj_attr /* = {0}*/;
    InitializeObjectAttributes(&obj_attr, us_path, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);
    /// <summary>
    /// 接收IO执行状态
    /// </summary>
    IO_STATUS_BLOCK io_stat /* = {0}*/;
    /// <summary>
    /// 打开文件
    /// </summary>
    HANDLE hFile;
    NTSTATUS STATUS;
    FileStream *file = NULL;
    if ((STATUS = NtOpenFile(&hFile, access, &obj_attr, &io_stat, share, options)) == STATUS_SUCCESS)
    {
        file = new FileStream(hFile, false);
        file->Update(&obj_attr, &io_stat);
        /// <summary>
        /// 打开成功
        /// </summary>
        switch (mode)
        {
            case TruncateMode:
                {
                    file->Length = 0;
                    break;
                }
            case AppendMode:
                {
                    file->Position = file->Length;
                    break;
                }
        }
		file->m_share_write = (share == WriteShare);
        return file;
    }

    RLIB_GlobalCollect(us_path);
    RLIB_GlobalCollect(dos_path);

    if (STATUS == 0xC0000034 /*STATUS_OBJECT_NAME_NOT_FOUND*/)
    {
        /// <summary>
        /// 文件不存在时的处理
        /// </summary>
        switch (mode)
        {
            case AppendMode:
            case OpenOrCreateMode:
                {
                    return File::Create(path.FullPath, mode, access, NormalAttribute, share, options);
                }
        }
    }
	assert(!"打开文件失败");
    Exception::SetLastError(RtlNtStatusToDosError(STATUS));
    return NULL;
}

//-------------------------------------------------------------------------

FileStream *File::Create(System::String str_path, FileMode mode /* = CreateMode */, FileAccess access /* = ReadWriteAccess */, FileAttributes attr /* = NormalAttribute */, FileShare share /* = NoneShare */, FileOptions options /* = NoneOption */)
{
    Path path(str_path);
	int  size = path.FullPath.GetWideCharSize();
	WCHAR *dos_path = (WCHAR *)RLIB_GlobalAlloc(size + sizeof(WCHAR));
	System::IO::Memory::memcpy(dos_path, path.FullPath.ToWideChar(), size);
    /// <summary>
    /// NT API要求使用Unicode String
    /// </summary>
    UNICODE_STRING *us_path = (UNICODE_STRING*)RLIB_GlobalAlloc(sizeof(UNICODE_STRING));
    RtlInitUnicodeString(us_path, dos_path);
    /// <summary>
    /// 初始化对象
    /// </summary>
    OBJECT_ATTRIBUTES obj_attr /* = {0}*/;
    InitializeObjectAttributes(&obj_attr, us_path, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, nullptr, nullptr);
    /// <summary>
    /// <summary>
    /// 接收IO执行状态
    /// </summary>
    IO_STATUS_BLOCK io_stat /* = {0}*/;
    /// 打开文件
    /// </summary>
    HANDLE hFile;
    NTSTATUS STATUS = NtCreateFile(&hFile, access, &obj_attr, &io_stat, nullptr, attr, share, mode, options, nullptr, NULL);
    if (STATUS == STATUS_SUCCESS)
    {
        FileStream *file = new FileStream(hFile, false);
        file->Update(&obj_attr, &io_stat);
		file->m_share_write = (share == WriteShare);
		return file;
    }

    RLIB_GlobalCollect(us_path);
    RLIB_GlobalCollect(dos_path);

	assert(!"创建文件失败详见" && io_stat.Information);
    Exception::SetLastError(RtlNtStatusToDosError(STATUS));
    return NULL;
}

//-------------------------------------------------------------------------

bool File::NewDirectory(String path)
{
    Path nt_path(path);

    UNICODE_STRING us_path /* = {0}*/;
    RtlInitUnicodeString(&us_path, nt_path.FullPath.ToWideChar());

    OBJECT_ATTRIBUTES obj_attr /* = {0}*/;
    InitializeObjectAttributes(&obj_attr, &us_path, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);

    IO_STATUS_BLOCK io_stat /* = {0}*/;

    HANDLE Handle;
    NTSTATUS STATUS = NtCreateFile(&Handle, FILE_LIST_DIRECTORY | SYNCHRONIZE, &obj_attr, &io_stat, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_CREATE, FILE_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT | FILE_OPEN_FOR_BACKUP_INTENT, NULL, 0L);
    if (STATUS != STATUS_SUCCESS)
    {
        Exception::SetLastError(RtlNtStatusToDosError(STATUS));
        return false;
    } //if

    NtClose(Handle);
    return true;
}

//-------------------------------------------------------------------------

bool File::Delete(String path)
{
    FileStream *file = File::Open(path, OpenMode, AllAccess);
    if (file == nullptr)
    {
        return false;
    } //if
    bool result = file->Delete();
    delete file;
    return result;
}

//-------------------------------------------------------------------------

bool File::TryDelete(String path)
{
	if (!File::Exist(path))
	{
		return false;
	} //if
	return File::Delete(path);
}

//-------------------------------------------------------------------------

bool File::Exist(String str_path)
{
    Path path(str_path);
    /// <summary>
    /// NT API要求使用Unicode String
    /// </summary>
    UNICODE_STRING us_path /* = {0}*/;
    RtlInitUnicodeString(&us_path, path.FullPath.ToWideChar());
    /// <summary>
    /// 初始化对象
    /// </summary>
    OBJECT_ATTRIBUTES obj_attr /* = {0}*/;
    InitializeObjectAttributes(&obj_attr, &us_path, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);
    /// <summary>
    /// 接收IO执行状态
    /// </summary>
    IO_STATUS_BLOCK io_stat /* = {0}*/;
    /// <summary>
    /// 打开文件
    /// </summary>
    HANDLE hFile;
    NTSTATUS STATUS;
    if ((STATUS = NtOpenFile(&hFile, (ACCESS_MASK)ReadAccess, &obj_attr, &io_stat, ReadShare, NoneOption)) == STATUS_SUCCESS)
    {
        NtClose(hFile);
        return true;
    }
	switch(STATUS)
	{
	case STATUS_OBJECT_PATH_NOT_FOUND:
	case STATUS_OBJECT_NAME_INVALID:
	case STATUS_OBJECT_NAME_NOT_FOUND:
		return false;
	}
    Exception::SetLastError(RtlNtStatusToDosError(STATUS));
    assert(!"File::Exist 无法判断文件是否存在, 假定存在");
    return true;
}

//-------------------------------------------------------------------------

bool File::Copy(String path, String new_path, bool ReplaceIfExists /* = true */)
{
    FileStream *file = File::Open(path, OpenMode, AllAccess, ReadShare);
    if (file == nullptr)
    {
        return false;
    } //if
    FileStream *new_file = File::Create(new_path, ReplaceIfExists ? CreateNewMode : CreateMode);
    if (file == nullptr)
    {
        delete file;
        return false;
    } //if
    new_file->Write(file->ObjectData, file->Length);
    delete new_file;
    delete file;
    return true;
}

//-------------------------------------------------------------------------

String File::ReadText(String path)
{
    FileStream *file = File::Open(path, OpenMode, ReadAccess, ReadShare);
    if (file == nullptr)
    {
        FAILED: return Nothing;
    } //if
    auto buf_stream = Text::Encoder::ToCurrentEncoding(Text::UnknownEncoding, file, file->Length);
    delete file;
    if (buf_stream == nullptr)
    {
        goto FAILED;
    } //if
	buf_stream->Position = 0;
    String Result = (LPTSTR)buf_stream->CurrentPtr;
    delete buf_stream;
    return Result;
}

//-------------------------------------------------------------------------

FileStream *File::ReadStream(String path)
{
	FileStream *file = File::Open(path, OpenMode, ReadAccess, ReadShare);
	return file;
}
//-------------------------------------------------------------------------

bool File::WriteText(String path, String text, int length /* = 0 */, Text::Encoding codepage /* = 0 */)
{
    IO::UnmanagedMemoryStream ori_stream((LPVOID)text.GetConstData(), text.CanReadSize, text.CanReadSize);

    FileStream *output = File::Open(path, AppendMode);
    if (output == nullptr)
    {
        return false;
    } //if

    bool Result = Text::Encoder::WriteTextStream(output, &ori_stream, length, output->Length != 0 ? false : true, codepage);

    delete output;

    return Result;
}
//-------------------------------------------------------------------------
bool File::WriteStream(String path, IN const IO::Stream *pstream, int length /* = 0 */)
{
	FileStream *output = File::Open(path, AppendMode);
	if (output == nullptr)
	{
		return false;
	} //if

	output->Write(pstream->CurrentPtr, length != 0 ? length : pstream->MaxReadSize);

	delete output;

	return true;
}

/************************************************************************/
/* FileStream                                                                
/************************************************************************/
FileStream::FileStream(HANDLE hFile, bool AutoUpdate /* = true */)
{
    assert(!"非法文件句柄" || hFile != INVALID_HANDLE_VALUE);
    this->m_buffer /*HANDLE hFile*/ = hFile;
    this->m_updated = true;
    this->m_obj = (OBJECT_ATTRIBUTES*)RLIB_GlobalAlloc(sizeof(OBJECT_ATTRIBUTES));
    this->m_isb = (IO_STATUS_BLOCK*)RLIB_GlobalAlloc(sizeof(IO_STATUS_BLOCK));
    if (!AutoUpdate)
    {
        return ;
    }
    //更新状态
    OBJECT_NAME_INFORMATION FileInfo;
    if (NtQueryObject(hFile, (OBJECT_INFORMATION_CLASS)1 /*ObjectNameInformation*/, &FileInfo, sizeof(FileInfo), nullptr) != STATUS_SUCCESS)
    {
        assert(!"非法文件句柄");
        return ;
    }
    UNICODE_STRING *us_path = (UNICODE_STRING*)RLIB_GlobalAlloc(sizeof(UNICODE_STRING));
    WCHAR *dos_path = (LPWSTR)RLIB_GlobalAlloc(FileInfo.Name.Length);
    System::IO::Memory::memcpy(dos_path, FileInfo.Name.Buffer, FileInfo.Name.Length);
    RtlInitUnicodeString(us_path, dos_path);
    InitializeObjectAttributes(this->m_obj, us_path, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);
}

//-------------------------------------------------------------------------

FileStream::~FileStream()
{
    this->Close();
    {
        RLIB_GlobalCollect(this->m_isb);
        RLIB_GlobalCollect(this->m_obj->ObjectName->Buffer);
        RLIB_GlobalCollect(this->m_obj->ObjectName);
        RLIB_GlobalCollect(this->m_obj);
        if (this->m_size /*文件缓冲区指针*/ != NULL)
        {
            RLIB_GlobalCollect((LPVOID)this->m_size);
        }
    }
}

//-------------------------------------------------------------------------

FileStream::operator HANDLE()
{
    return this->m_buffer /*HANDLE hFile*/;
}

//-------------------------------------------------------------------------

LPVOID FileStream::GetObjectData()const
{
    if (this->m_size /*文件缓冲区指针*/ != NULL)
    {
        if (!this->m_updated && this->m_share_write == false)
        {
            goto ret;
        }
        RLIB_GlobalCollect((LPVOID)this->m_size);
    }
    ((FileStream*)this)->m_size = (ULONG)RLIB_GlobalAlloc(this->Length);
    ULONG position = this->Position;
    {
        ((FileStream*)this)->Position = 0;
        this->Read((LPVOID)this->m_size, this->Length);
        ((FileStream*)this)->Position = position;
    }
    ret: return (LPVOID)this->m_size;
}

//-------------------------------------------------------------------------

void FileStream::Update(POBJECT_ATTRIBUTES pAttr, PIO_STATUS_BLOCK pStat)
{
    System::IO::Memory::memcpy(this->m_obj, pAttr, sizeof(OBJECT_ATTRIBUTES));
    System::IO::Memory::memcpy(this->m_isb, pStat, sizeof(IO_STATUS_BLOCK));
}

//-------------------------------------------------------------------------

void FileStream::SetPos(LONG offset)
{
    FILE_POSITION_INFORMATION FilePos;
    FilePos.CurrentByteOffset.QuadPart = offset;
    NTSTATUS STATUS = NtSetInformationFile(this->m_buffer /*HANDLE hFile*/, m_isb, &FilePos, sizeof(FilePos), FilePositionInformation);
    if (STATUS != STATUS_SUCCESS)
    {
        this->setException(STATUS);
        RLIB_SetExceptionInfo((&this->m_error));
        assert(!"设置文件偏移失败");
        return ;
    }
    this->m_pos = offset;
}

//-------------------------------------------------------------------------

void FileStream::SetLength(LONG length)
{
    FILE_END_OF_FILE_INFORMATION FileEndOf;
    FileEndOf.EndOfFile.QuadPart = length;
    NTSTATUS STATUS;
    if ((STATUS = NtSetInformationFile(this->m_buffer /*HANDLE hFile*/, m_isb, &FileEndOf, sizeof(FileEndOf), FileEndOfFileInformation)) != STATUS_SUCCESS)
    {
        this->setException(STATUS);
        RLIB_SetExceptionInfo((&this->m_error));
        assert(!"设置文件长度失败");
        return ;
    }
    this->m_length = length;
    if (this->m_size /*文件缓冲区指针*/ != NULL)
    {
        length /*文件缓冲区内存大小*/ = AppBase::GetUsingPool()->GetSize((LPVOID)this->m_size);
        if (length >= this->m_length)
        {
            this->Read((LPVOID)this->m_size, this->Length);
            //防止与原来数据重叠
            Memory::memset(LPBYTE(this->m_size) + this->Length, 0, length - this->m_length);
            goto next;
        } //if
        //释放静态缓冲区, 该数据可能已经过时
        RLIB_GlobalCollect((LPVOID)this->m_size);
        this->m_size = 0;
    }
    next: this->m_updated = false;
    return ;
}

//-------------------------------------------------------------------------

FileException *FileStream::GetLastException()
{
    return &this->m_error;
}

//-------------------------------------------------------------------------

LONG FileStream::GetLength()const
{
    if (!this->m_updated && this->m_share_write == false)
    {
        goto ret;
    }
    FILE_NETWORK_OPEN_INFORMATION FileInfo /* = {0}*/;
    NTSTATUS STATUS;
    if ((STATUS = NtQueryFullAttributesFile((POBJECT_ATTRIBUTES)m_obj, &FileInfo)) != STATUS_SUCCESS)
    {
        ((FileStream*)this)->setException(STATUS);
        assert(!"获取文件长度失败");
        return 0;
    }
    ((FileStream*)this)->m_length = (LONG)FileInfo.EndOfFile.QuadPart;
    if (this->m_size /*文件缓冲区指针*/ != NULL)
    {
        FileInfo.FileAttributes /*文件缓冲区内存大小*/ = AppBase::GetUsingPool()->GetSize((LPVOID)this->m_size);
        if (LONG(FileInfo.FileAttributes) >= this->m_length)
        {
            this->Read((LPVOID)this->m_size, this->Length);
            //防止与原来数据重叠
            Memory::memset(LPBYTE(this->m_size) + this->Length, 0, FileInfo.FileAttributes - this->m_length);
            goto next;
        } //if
        //释放静态缓冲区, 该数据可能已经过时
        RLIB_GlobalCollect((LPVOID)this->m_size);
        ((FileStream*)this)->m_size = 0;
    }
    next: ((FileStream*)this)->m_updated = false;
    ret: return this->m_length;
}

//-------------------------------------------------------------------------

LONG FileStream::GetAllocationSize()const
{
    FILE_NETWORK_OPEN_INFORMATION FileInfo /* = {0}*/;
    NTSTATUS STATUS;
    if ((STATUS = NtQueryFullAttributesFile((POBJECT_ATTRIBUTES)m_obj, &FileInfo)) != STATUS_SUCCESS)
    {
        ((FileStream*)this)->setException(STATUS);
        assert(!"获取文件物理大小失败");
        return 0;
    }
    ((FileStream*)this)->m_length = (LONG)FileInfo.EndOfFile.QuadPart;
    if (this->m_size /*文件缓冲区指针*/ != NULL)
    {
        FileInfo.FileAttributes /*文件缓冲区内存大小*/ = AppBase::GetUsingPool()->GetSize((LPVOID)this->m_size);
        if (LONG(FileInfo.FileAttributes) >= this->m_length)
        {
            this->Read((LPVOID)this->m_size, this->Length);
            //防止与原来数据重叠
            Memory::memset(LPBYTE(this->m_size) + this->Length, 0, FileInfo.FileAttributes - this->m_length);
            goto next;
        } //if
        //释放静态缓冲区, 该数据可能已经过时
        RLIB_GlobalCollect((LPVOID)this->m_size);
        ((FileStream*)this)->m_size = 0;
    }
    next: ((FileStream*)this)->m_updated = false;
    return static_cast < LONG > (FileInfo.AllocationSize.QuadPart);
}

//-------------------------------------------------------------------------

bool FileStream::Delete()
{
    FILE_DISPOSITION_INFORMATION FileInfo;
    FileInfo.DeleteFile = TRUE;
    NTSTATUS STATUS;
    if ((STATUS = NtSetInformationFile(this->m_buffer /*HANDLE hFile*/, m_isb, &FileInfo, sizeof(FileInfo), FileDispositionInformation)) != STATUS_SUCCESS)
    {
        this->setException(STATUS);
        RLIB_SetExceptionInfo((&this->m_error));
        assert(!"删除文件失败");
        return false;
    }
    this->Close();
    return true;
}

//-------------------------------------------------------------------------

bool FileStream::Move(String new_path, bool ReplaceIfExists)
{
    FILE_RENAME_INFORMATION FileInfo /* = {0}*/;
    NtQueryInformationFile(this->m_buffer /*HANDLE hFile*/, m_isb, &FileInfo, sizeof(FileInfo), FileRenameInformation);
    FileInfo.ReplaceIfExists = ReplaceIfExists == true ? TRUE : FALSE;
    FileInfo.RootDirectory = NULL;
    {
        Path to_path(new_path);
        Memory::memcpy(FileInfo.FileName, to_path.FullPath.ToWideChar(), to_path.FullPath.GetWideCharSize() + sizeof(WCHAR));
        FileInfo.FileNameLength = to_path.FullPath.Length;
    }
    NTSTATUS STATUS;
    if ((STATUS = NtSetInformationFile(this->m_buffer /*HANDLE hFile*/, m_isb, &FileInfo, sizeof(FileInfo), FileRenameInformation)) != STATUS_SUCCESS)
    {
        this->setException(STATUS);
        RLIB_SetExceptionInfo((&this->m_error));
        assert(!"移动文件失败");
        return false;
    }
    return true;
}

//-------------------------------------------------------------------------

int FileStream::Read(void *buffer, int count)const
{
    LARGE_INTEGER FilePoint /* = {0}*/;
    FilePoint.QuadPart = this->Position;
    NTSTATUS STATUS;
    switch ((STATUS = NtReadFile(this->m_buffer /*HANDLE hFile*/, NULL, NULL, NULL, (PIO_STATUS_BLOCK)m_isb, buffer, count, &FilePoint, nullptr)))
    {
        case STATUS_SUCCESS:
            goto next;
        case STATUS_PENDING:
			{
				NTSTATUS WAIT_STATUS = NtWaitForSingleObject(this->m_buffer /*HANDLE hFile*/, TRUE, nullptr);
				switch (WAIT_STATUS)
				{
				case STATUS_SUCCESS:
				case STATUS_ALERTED:
				case STATUS_USER_APC:
				case STATUS_TIMEOUT://?
					goto next;
				}
			}
        default:
            ((FileStream*)this)->setException(STATUS);
            assert(!"读取文件失败");
            return  - 1;
    }
next: 
	count = (ULONG)m_isb->Information;//实际读取字节

	((FileStream*)this)->Position += count;
    return count;
}

//-------------------------------------------------------------------------

void FileStream::Write(const void *buffer, int count)
{
    LARGE_INTEGER FilePoint /* = {0}*/;
    FilePoint.QuadPart = this->Position;
    NTSTATUS STATUS;
    switch ((STATUS = NtWriteFile(this->m_buffer /*HANDLE hFile*/, NULL, NULL, NULL, m_isb, (PVOID)buffer, count, &FilePoint, nullptr)))
    {
        case STATUS_SUCCESS:
            goto next;
        case STATUS_PENDING:
			{
				NTSTATUS WAIT_STATUS = NtWaitForSingleObject(this->m_buffer /*HANDLE hFile*/, TRUE, nullptr);
				switch (WAIT_STATUS)
				{
				case STATUS_SUCCESS:
				case STATUS_ALERTED:
				case STATUS_USER_APC:
				case STATUS_TIMEOUT://?
					goto next;
				}
			}
        default:
            this->setException(STATUS);
            assert(!"写入文件失败");
            return ;
    }
next: 
	this->m_updated = true;
    this->Position += (ULONG)m_isb->Information/*count*/;
}

//-------------------------------------------------------------------------

bool FileStream::Flush()
{
    NTSTATUS STATUS = NtFlushBuffersFile(this->m_buffer /*HANDLE hFile*/, m_isb);
    if (STATUS != STATUS_SUCCESS)
    {
        this->setException(STATUS);
        assert(!"刷新文件失败");
        return false;
    } //if
    return true;
}

//-------------------------------------------------------------------------

void FileStream::Append(void *buffer, int count)
{
    this->SetPos(this->GetLength());
    this->Write(buffer, count);
    return ;
}

//-------------------------------------------------------------------------

void FileStream::Close()
{
    if (this->m_buffer /*HANDLE hFile*/ != INVALID_HANDLE_VALUE)
    {
        NTSTATUS STATUS;
        if ((STATUS = NtClose(this->m_buffer /*HANDLE hFile*/)) != STATUS_SUCCESS)
        {
            this->setException(STATUS);
            assert(!"关闭文件流失败");
            return ;
        }
        this->m_buffer /*HANDLE hFile*/ = INVALID_HANDLE_VALUE;
    }
}

//-------------------------------------------------------------------------

void FileStream::setException(NTSTATUS STATUS)
{
    Exception::GetException(&this->m_error, RtlNtStatusToDosError(STATUS));
}

/************************************************************************/
/* Memory 分部定义                                                       */
/************************************************************************/
bool Memory::DumpToFile(TCHAR *lpFile)
{
    System::String FilePath(lpFile);
    FileStream *DumpFile = File::Create(FilePath, File::CreateNewMode);
    if (DumpFile == nullptr)
    {
        return false;
    }
    DumpFile->Write(Data.m_base, Data.m_now);
    delete DumpFile;
    return true;
}
