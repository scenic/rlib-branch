/********************************************************************
Created:	2012/04/21  14:48
Filename: 	RLib.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_Import.h"
// #pragma comment(linker, "/ENTRY:RLIBMain")
// 
// void main()
// {
// 	//nothing
// };
// BOOL APIENTRY DllMain(HANDLE /* hModule*/, DWORD dwReason, LPVOID /*lpReserved*/)
// {
// #ifdef RLIB_LIB
// #pragma message("RLib已被设置编译成静态链接库, 可能需要自行复制RLib\\native\\ntdll.lib到项目目录.")
// #endif // RLIB_DLL
// 
//     switch (dwReason)
//     {
//         case DLL_PROCESS_DETACH:
//             {
//                 System::Net::Sockets::Dispose();
//             }
//         case DLL_PROCESS_ATTACH:
//             return TRUE;
//         case DLL_THREAD_ATTACH:
//         case DLL_THREAD_DETACH:
//             return FALSE;
//     }
//     return FALSE;
// }
#ifdef _RUNTIME_DEBUG

//-------------------------------------------------------------------------

int __cdecl __crtMessageBox(
	LPCTSTR lpText,
	LPCTSTR lpCaption,
	UINT uType
	)
{
	static int (APIENTRY *pfnMessageBox)(HWND, LPCTSTR, LPCTSTR, UINT) = NULL;
	static HWND (APIENTRY *pfnGetActiveWindow)(void) = NULL;
	static HWND (APIENTRY *pfnGetLastActivePopup)(HWND) = NULL;

	HWND hWndParent = NULL;

	if (NULL == pfnMessageBox)
	{
		HMODULE hlib = (HMODULE)LoadLibraryA("user32.dll");

		if (NULL == hlib || NULL == (pfnMessageBox =
			(int (APIENTRY *)(HWND, LPCTSTR, LPCTSTR, UINT))
#ifndef _UNICODE
			GetProcAddress(hlib, "MessageBoxA")))
#else
			GetProcAddress(hlib, "MessageBoxW")))
#endif // _UNICODE
			return 0;

		pfnGetActiveWindow = (HWND (APIENTRY *)(void))
			GetProcAddress(hlib, "GetActiveWindow");

		pfnGetLastActivePopup = (HWND (APIENTRY *)(HWND))
			GetProcAddress(hlib, "GetLastActivePopup");
	}

	if (pfnGetActiveWindow)
		hWndParent = (*pfnGetActiveWindow)();

	if (hWndParent != NULL && pfnGetLastActivePopup)
		hWndParent = (*pfnGetLastActivePopup)(hWndParent);

	return (*pfnMessageBox)(hWndParent, lpText, lpCaption, uType);
}

//-------------------------------------------------------------------------

void export RLib_Warning(LPCTSTR exp, LPCTSTR file, int line)
{
	TCHAR strexp[1024] = {0}, strline[16] = {0};
	_itot(line, strline, 10);
	_tcscat_s(strexp, 1024, _T("异常:"));
	_tcscat_s(strexp, 1024, exp != nullptr ? exp : T("nullptr"));
	_tcscat_s(strexp, 1024, _T("\r\n文件:"));
	_tcscat_s(strexp, 1024, file);
	_tcscat_s(strexp, 1024, _T("\r\n行号:"));
	_tcscat_s(strexp, 1024, strline);

	auto nCode = __crtMessageBox(strexp, _T("RLib - Simple C++ Framework"),
		MB_ABORTRETRYIGNORE|MB_ICONHAND|MB_SETFOREGROUND|MB_TASKMODAL);
	if (nCode == IDABORT)
	{
		::ExitProcess(INFINITE);
	}

	// Retry: call the debugger 
	if (nCode == IDRETRY)
	{
		__debugbreak();
		// return to user code
		return;
	}

	// Ignore: continue execution
	if (nCode == IDIGNORE) return;
}

#endif // _RUNTIME_DEBUG