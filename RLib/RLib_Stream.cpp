#include "RLib_Stream.h"
//////////////////////////////////////////////////////////////////////////
using namespace System::IO;
/************************************************************************/
/* Stream                                                                
/************************************************************************/
int Stream::Read(void *buffer, int count)const
{
    if (buffer == nullptr || this->ObjectData == nullptr)
    {
        assert(!"buffer 为 null 或者 流不可读取");
        return  - 1;
    }
    if (count < 0)
    {
        assert(!"count 为负");
        return  - 1;
    }
    // 可读取字节
    int max_read = int(this->Size - this->Position);
    if (count > max_read)
    {
        assert(!"warning: 检测到流读取溢出");
        count = max_read;
    }
    // 开始读取
    Memory::memcpy(buffer, this->CurrentPtr, count);
    // Read 方法被标记为const, 故此处必须转换掉类指针的const
    // 移动流指针位置
    ((Stream *)this)->Position += count;
    return count;
}

//-------------------------------------------------------------------------

void Stream::Write(const void *buffer, int count)
{
    if (buffer == nullptr || this->ObjectData == nullptr)
    {
        assert(!"buffer 为 null 或者 流不可写入");
        return ;
    }
    if (count < 0)
    {
        assert(!"count 为负");
        return ;
    }
    // 可写入字节
    int max_write = int(this->Size - this->Position);
	assert(max_write >= 0);
    if (count > max_write)
    {
        assert(!"warning: 检测到流写入溢出");
        count = max_write;
    }
	// 开始写入
	assert((LPBYTE(this->CurrentPtr) + count) <= (LPBYTE(this->m_buffer) + this->m_size));
	Memory::memcpy(this->CurrentPtr, buffer, count);

	// 修改流长度(注意需要考虑覆写问题)
	this->m_length /* += count*/ = ((LPBYTE(this->CurrentPtr) + count) - LPBYTE(this->m_buffer));

    // 移动流指针位置
    this->Position += count;
}

//-------------------------------------------------------------------------

void Stream::SetPos(LONG Pos)
{
    if (Pos <= 0)
    {
        Pos = 0;
    }
    else if (Pos > this->m_size)
    {
        Pos = this->m_size;
    }
    this->m_pos = Pos;
}

/************************************************************************/
/* MemoryStream                                                                
/************************************************************************/
MemoryStream::MemoryStream(LONG size)
{
	this->m_length = 0;
    this->m_buffer = RLIB_GlobalAlloc(size);
	if (this->m_buffer == nullptr)
	{
		assert(!"内存申请失败");
		this->m_size = 0;
		return;
	} //if
    this->m_size = size;
}

//-------------------------------------------------------------------------

MemoryStream::~MemoryStream()
{
    this->Close();
}

//-------------------------------------------------------------------------

void MemoryStream::Close()
{
    if (this->m_buffer != nullptr)
    {
        RLIB_GlobalCollect(this->m_buffer);
        this->m_buffer = nullptr;
    }
}

//-------------------------------------------------------------------------

void MemoryStream::SetLength(LONG length)
{
    if (length <= 0)
    {
        Memory::memset(this->m_buffer, 0, this->m_size);
        this->m_length = this->m_pos = 0;
        goto RET;
    }
    if (length > this->m_size)
    {
        this->m_length = this->m_size;
        goto RET;
    }

	assert((LPBYTE(this->m_buffer) + length + (this->m_size - length)) 
		<= (LPBYTE(this->m_buffer) + this->m_size));
	Memory::memset(LPBYTE(this->m_buffer) + length, 0, this->m_size - length);

    this->m_length = length;
RET: 
	return;
}

/************************************************************************/
/* BufferedStream                                                               
/************************************************************************/
void BufferedStream::Write(const void *buffer, int count)
{
    //虚函数, 不能直接调用父类方法
    if (buffer == nullptr || this->ObjectData == nullptr)
    {
        assert(!"buffer 为 null 或者 流不可写入");
        goto write_done;
    }
    if (count < 0)
    {
        assert(!"count 为负");
        goto write_done;
    }
    // 可写入字节
    int max_write = int(this->Size - this->Position);
	assert(max_write >= 0);
    if (count > max_write)
    {
        // 需要字节
        max_write = (count - max_write);
        if (!this->AppendSize(max_write))
        {
            goto ret;
        }
    }
    // 开始写入
	assert((LPBYTE(this->CurrentPtr) + count) <= (LPBYTE(this->m_buffer) + this->m_size));
    Memory::memcpy(this->CurrentPtr, buffer, count);

    // 修改流长度(注意需要考虑覆写问题)
    this->m_length /* += count*/ = ((LPBYTE(this->CurrentPtr) + count) - LPBYTE(this->m_buffer));

    // 移动流指针位置
    this->Position += count;
    goto write_done;
ret: 
	assert(!"写入失败");
write_done: 
	return ;
}

//-------------------------------------------------------------------------

bool BufferedStream::AppendSize(unsigned int count)
{
	if (count > 0)
	{
		void *ptr;
		if ((ptr = AppBase::GetUsingPool()->ReAlloc(this->m_buffer, this->m_size + count)) != nullptr)
		{
			this->m_buffer = ptr;
			this->m_size  += count;
			return true;
		}
	}
	return false;
}