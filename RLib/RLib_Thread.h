/********************************************************************
	Created:	2012/02/05  22:43
	Filename: 	Lib_Thread.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_THREAD
#define _USE_THREAD
#include "RLib_Xml.h"
#pragma region 表示线程开始要执行的方法
typedef DWORD (_stdcall *Standard_THREADSTART)(LPVOID lpThreadParameter);
typedef DWORD (_stdcall *Expand_THREADSTART)();
typedef void  (_stdcall *Void_THREADSTART)();
typedef void  (_cdecl *Expand_Cpp_THREADSTART)(LPVOID lpThreadParameter);
typedef void  (_cdecl *Cpp_THREADSTART)();

#pragma endregion

typedef enum TMode
{
	/// <summary>
	/// 标准, 该线程方法有参数
	/// </summary>
	StandardMode = 0, 
	/// <summary>
	/// 拓展, 该线程方法无参数
	/// </summary>
	ExpandMode, 
	/// <summary>
	/// 非标准拓展, 该线程方法无参数无返回值
	/// </summary>
	VoidMode, 
	/// <summary>
	/// C++拓展, 该线程方法遵循C++调用约定
	/// </summary>
	CppMode, 
	/// <summary>
	/// C++拓展, 该线程方法遵循C++调用约定并带参数
	/// </summary>
	CppExpandMode, 
};
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 包含启用多线程编程的类型
	/// </summary>
	namespace Threading
	{
		typedef Expand_Cpp_THREADSTART ParameterizedCppThreadStart;
		typedef Standard_THREADSTART   ParameterizedThreadStart;
		typedef Expand_THREADSTART     ExpandThreadStart;
		typedef Void_THREADSTART       VoidThreadStart;
		typedef Cpp_THREADSTART        CppThreadStart;

		/// <summary>
		/// 指定 Thread 的执行状态
		/// </summary>
		struct ThreadState
		{
			enum Val
			{
				/// <summary>
				/// 尚未对线程调用 Thread.Start 方法
				/// </summary>
				/*const static int */Unstarted = 0,
				/// <summary>
				/// 线程已启动，它未被阻塞
				/// </summary>
				/*const static int */Running = 1,	
				/// <summary>
				/// 线程已终止
				/// </summary>
				/*const static int */Stopped = 2,	
				/// <summary>
				/// 线程已挂起
				/// </summary>
				/*const static int */Suspended = 3,	
				/// <summary>
				/// 调用 Thread.Exit 方法中止了线程
				/// </summary>
				/*const static int */Aborted = 4, 	
				/// <summary>
				/// 调用 Thread.Sleep 方法使线程睡眠
				/// </summary>
				/*const static int */Sleeped = 5, 	
			};
		};
		/// <summary>
		/// 指定 Thread 的调度优先级
		/// </summary>
		typedef enum ThreadPriority
		{
			/// <summary>
			/// 获取调度优先级失败
			/// </summary>
			ErrorPriority = -1,
			/// <summary>
			/// Lowest thread priority level
			/// </summary>
			LowestPriority = 0/*LOW_PRIORITY*/,    
			/// <summary>
			/// Highest thread priority level
			/// </summary>
			HighestPriority = 31/*HIGH_PRIORITY*/    
		};
		/// <summary>
		/// 等待状态
		/// </summary>
		typedef enum WaitStatus
		{
			/// <summary>
			/// The caller did not have the required privileges to the event specified by the Handle parameter
			/// </summary>
			WAIT_ACCESS_DENIED,
			/// <summary>
			/// The supplied Handle parameter was invalid
			/// </summary>
			WAIT_INVALID_HANDLE,
			/// <summary>
			/// The specified object satisfied the wait
			/// </summary>
			WAIT_SUCCESS,
			/// <summary>
			/// A time out occurred before the object was set to a signaled state.
			/// This value can be returned when the specified set of wait conditions cannot be immediately met 
			/// and the Timeout parameter is set to zero
			/// </summary>
			WAIT_TIMEOUTED,
			/// <summary>
			/// The wait was aborted to deliver a user APC to the current thread
			/// </summary>
			WAIT_USER_APC,	
			/// <summary>
			/// The wait was aborted to deliver an alert to the current thread
			/// </summary>
			WAIT_ALERTED = WAIT_USER_APC
		};
		/// <summary>
		/// 封装等待对共享资源的独占访问的操作系统特定的对象
		/// </summary>
		class export RLIB_THREAD_SAFE WaitHandle
		{
		protected:
			OBJECT_ATTRIBUTES_STRUCT m_obj;
			UNICODE_STRING_STRUCT    m_name;
		public:
			/// <summary>
			/// Gets the native operating system handle
			/// </summary>
			HANDLE            Handle;
		public:
			/// <summary>
			/// 阻止当前线程，直到当前 Mutex 收到信号，同时使用 32 位带符号整数指定时间间隔
			/// </summary>
			WaitStatus WaitOne(int millisecondsTimeout = 0);
			~WaitHandle();
		public:
			/// <summary>
			/// 等待指定数组中的任一或者全部元素收到信号，使用 32 位带符号整数指定时间间隔
			/// </summary>
			/// <param name="Count">句柄数目</param>
			/// <param name="Handles">句柄数组</param>
			/// <param name="WaitAll">是否等待全部</param>
			/// <param name="Alertable">A boolean value that specifies whether the wait is alertable</param>
			/// <param name="Milliseconds">等待超时值</param>
			static WaitStatus Wait(int Count, HANDLE Handles[], Boolean WaitAll, Boolean Alertable, int Milliseconds);
			/// <summary>
			/// 阻止当前线程，直到当前收到信号，同时使用 32 位带符号整数指定时间间隔
			/// </summary>
			static WaitStatus WaitOne(HANDLE Handle, Boolean Alertable, int millisecondsTimeout);
		};
		/// <summary>
		/// 表示线程异常
		/// </summary>
		typedef Exception ThreadException;
		/// <summary>
		/// 表示线程
		/// </summary>
		class export Thread
		{
		private:
			ThreadException             m_error;
			Object                      m_startPoint; 
			Object                      m_parameter; 
			TMode                       m_startMode;
			HANDLE                      m_hThread;
			DWORD                       m_ThreadId;
			volatile ThreadState::Val   m_state;
		private:
			//创建线程
			void _create();
			//设置异常
			void setException(NTSTATUS STATUS);
		public:
			/// <summary>
			/// 使用指定的线程句柄及线程ID(允许为0)初始化新的Thread类 
			/// 自动接管该句柄的所有工作, 默认该线程正在运行
			/// </summary>
			Thread(HANDLE, DWORD, BOOL IsRunning);
			/// <summary>
			/// 初始化 Thread 类的新实例，并允许传参数给线程
			/// </summary>
			Thread(ParameterizedThreadStart, Object lpParameter);
			/// <summary>
			/// 初始化 Thread 类的新实例
			/// </summary>
			Thread(ExpandThreadStart);
			/// <summary>
			/// 初始化 Thread 类的新实例
			/// </summary>
			Thread(VoidThreadStart);
			/// <summary>
			/// 初始化 Thread 类的新实例
			/// </summary>
			Thread(CppThreadStart);
			/// <summary>
			/// 初始化 Thread 类的新实例，并允许传参数给线程
			/// </summary>
			Thread(ParameterizedCppThreadStart, Object lpParameter);
			/// <summary>
			/// 释放内存并关闭线程对象
			/// 如果在线程过程中销毁线程对象本身, 则必须调用ExitThread以退出线程
			/// </summary>
			~Thread();
			RLIB_ClassNewDel;
		public:
			/// <summary>
			/// 设置一个值，该值表示线程执行时使用的参数
			/// 方法必须在 Start 方法执行前被调用
			/// </summary>
			bool SetParameter(Object lpParameter);
			/// <summary>
			/// 获取线程对象句柄
			/// </summary>
			operator HANDLE();
			/// <summary>
			/// 获取线程ID, 失败返回0
			/// </summary>
			const DWORD GetThreadId() const;
			/// <summary>
			/// 获取线程Id
			/// </summary>
			__declspec(property(get = GetThreadId)) DWORD ThreadId;
			/// <summary>
			/// 获取一个值，该值指示线程的调度优先级
			/// 失败返回ErrorPriority
			/// </summary>
			ThreadPriority GetPriority() const;
			/// <summary>
			/// 设置一个值，该值指示线程的调度优先级
			/// LowestPriority < newPriority <= HighestPriority
			/// </summary>
			bool SetPriority(ThreadPriority newPriority);
			//获取或设置一个值，该值指示线程的调度优先级
			__declspec(property(get = GetPriority, put = SetPriority))
				ThreadPriority Priority; 	
			/// <summary>
			/// 获取或设置一个值，该值指示某个线程是否为后台线程
			/// 如果该属性为true, 则线程结束时将自动销毁 Thread 实例, 这意味着无须
			/// 手动释放 Thread 实例.
			/// @warning 如果您是.NET用户, 请与.NET'后台线程'概念相区别
			/// </summary>
			bool IsBackground; 	
			/// <summary>
			/// 获取或设置一个值，该值指示是否禁止在线程结束时更新为 ThreadState::Stoped 状态
			/// 如果该值被设置为true, 则必须调用 ExitThread 方法结束线程
			/// </summary>
			bool IsSuppressChangeState;
			/// <summary>
			/// 获取一个值，该值指示当前线程的执行状态
			/// 如果此线程已启动并且尚未正常终止或中止，则为 true；否则为 false
			/// </summary>
			bool GetIsAlive() const;
			//如果此线程已启动并且尚未正常终止或中止，则为 true；否则为 false
			__declspec(property(get = GetIsAlive)) bool IsAlive;
			/// <summary>
			/// 获取一个值，该值指示当前线程的状态
			/// </summary>
			ThreadState::Val GetState() const;
			/// <summary>
			/// 获取一个值，该值指示当前线程的状态
			/// </summary>
			__declspec(property(get = GetState)) ThreadState::Val State;
		public:
			/// <summary>
			/// 导致操作系统将当前实例的状态更改为 ThreadState::Running
			/// </summary>
			bool Start();
			/// <summary>
			/// 唤醒线程
			/// </summary>
			bool Alert();
			/// <summary>
			/// 已过时.继续已挂起的线程
			/// </summary>
			bool Resume();
			/// <summary>
			/// 已过时.挂起线程，或者如果线程已挂起，则不起作用
			/// </summary>
			bool Suspend();
			/// <summary>
			/// 已过时.在调用此方法以开始中止此线程, 调用此方法通常会中止线程
			/// </summary>
			bool Abort();
			/// <summary>
			/// 在当前线程上执行等待操作, 调用Start()后立即调用Wait()可能不起作用(线程状态未更新)
			/// </summary>
			WaitStatus Wait(int millisecondsTimeout = 0);
			/// <summary>
			/// 将线程挂起指定的时间并指定是否允许唤醒线程, 方法必须由 当前实例 所表示的线程调用
			/// </summary>
			/// <returns>WAIT_ALERTED or WAIT_TIMEOUTED</returns>
			WaitStatus Sleep(DWORD dwMilliseconds = INFINITE); 
			/// <summary>
			/// 关闭线程对象
			/// </summary>
			bool Close();
			/// <summary>
			/// 获取Thread发生的异常信息
			/// </summary>
			ThreadException *GetLastException(); 
		public:
			/// <summary>
			/// 获取当前正在运行的线程
			/// </summary>
			static Thread *GetCurrentThread(){ return new Thread(AppBase::GetCurrentThread(),
				GetCurrentThreadId(), FALSE); };
			/// <summary>
			/// 退出当前线程并释放线程内存
			/// </summary>
			static void ExitThread(DWORD code = 0);
			/// <summary>
			/// 将当前线程挂起指定的时间并指定是否允许唤醒线程
			/// </summary>
			/// <returns>
			/// Zero if the specified time interval expired.
			/// WAIT_IO_COMPLETION if the function returned due to one or more I/O 
			/// completion callback functions. This can happen only if bAlertable is TRUE, 
			/// and if the thread that called the SleepEx function is the same thread that 
			/// called the extended I/O function</returns>
			static DWORD SleepEx(DWORD dwMilliseconds = INFINITE, bool bAlertable = true); 	
			/// <summary>
			/// 唤醒指定线程
			/// </summary>
			static bool AlertThread(HANDLE hThread);
			/// <summary>
			/// Difference between AlertResumeThread and Thread.Resume it's the first one 
			/// sets Thread Object to alerted state (so before thread will continue execution, 
			/// all APC will be executed).
			/// </summary>
			/// <returns>Returns number of suspend request for thread ThreadHandle before call AlertResumeThread.
			/// If this number is 0, thread will continue execution.</returns>
			static LONG AlertResumeThread(HANDLE hThread);
			/// <summary>
			/// 使用实例方法 GetLastException() 替代此方法
			/// </summary>
			static bool GetLastError(OUT LPVOID buffer, size_t size, IN DWORD err = 0);
		private:
			/// <summary>
			/// 线程的转发函数
			/// </summary>
			static DWORD _stdcall ThreadProcWrapper(Thread *lpMySelf);
		};
	};
};
#endif