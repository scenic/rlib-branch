/********************************************************************
Created:	2011/08/14  20:34
Filename: 	Lib_Memory.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_Memory.h"
#include "native/RLib_Native.h"
//////////////////////////////////////////////////////////////////////////
using namespace System::IO;
/************************************************************************/
/* MemoryAllocator                                                                
/************************************************************************/
LPVOID MemoryAllocator::Allocate(HANDLE ProcessHandle, PVOID BaseAddress, ULONG RegionSize, ULONG AllocationType, ULONG Protect)
{
    if (NtAllocateVirtualMemory(ProcessHandle, &BaseAddress, NULL, &RegionSize, AllocationType, Protect) != STATUS_SUCCESS)
    {
        return NULL;
    }
    return BaseAddress;
}

//-------------------------------------------------------------------------

LPVOID MemoryAllocator::Free(HANDLE ProcessHandle, PVOID BaseAddress, ULONG RegionSize, ULONG FreeType)
{
    if (NtFreeVirtualMemory(ProcessHandle, &BaseAddress, &RegionSize, FreeType) != STATUS_SUCCESS)
    {
        return NULL;
    }
    return BaseAddress; //该页内存基址
}

//-------------------------------------------------------------------------

ULONG MemoryAllocator::Protect(HANDLE ProcessHandle, PVOID BaseAddress, ULONG RegionSize, ULONG NewAccessProtection)
{
    if (NtProtectVirtualMemory(ProcessHandle, &BaseAddress, &RegionSize, NewAccessProtection, &NewAccessProtection) != STATUS_SUCCESS)
    {
        return NULL;
    }
    return NewAccessProtection;
}

//-------------------------------------------------------------------------

PMEMORY_INFORMATION MemoryAllocator::Query(HANDLE ProcessHandle, PVOID BaseAddress)
{
    static MEMORY_BASIC_INFORMATION MEM_INFO;
    if (NtQueryVirtualMemory(ProcessHandle, BaseAddress, MemoryBasicInformation, &MEM_INFO, sizeof(MEM_INFO), nullptr) != STATUS_SUCCESS)
    {
        Memory::memset(&MEM_INFO, 0, sizeof(MEM_INFO));
    }
    return (PMEMORY_INFORMATION) &MEM_INFO;
}

//-------------------------------------------------------------------------

ULONG MemoryAllocator::GetPageSize()
{
    return System::AppBase::GetSystemInfo()->PhysicalPageSize;
}

//-------------------------------------------------------------------------

ULONG MemoryAllocator::GetPhysicalPages()
{
    return System::AppBase::GetSystemInfo()->NumberOfPhysicalPages;
}

/************************************************************************/
/* Memory                                                               */
/************************************************************************/
inline void ROUNDUP(LONG &t, const LONG page)
{
    if (t < page)
    {
        t = page; //最小为一页
    }
    else if (t % page != 0)
    {
        t = ((t / page) + 1) *page; //向上取整数页
    }
}

//-------------------------------------------------------------------------

void Memory::init(LONG SizeTCommit, LONG SizeTReserve)
{
    //	处理数据
    this->Data.m_page = MemoryAllocator::GetPageSize(); //保存系统页大小
    //  初始化内存池
    {
        //  调整以保持是系统页的整数倍
        ROUNDUP(SizeTCommit, this->Data.m_page);
        if (SizeTReserve < SizeTCommit)
        {
            SizeTReserve = SizeTCommit; //保留大小不能小于提交大小
        }
        else if (SizeTReserve % this->Data.m_page != 0)
        {
            SizeTReserve = ((SizeTReserve / this->Data.m_page) + 1) *this->Data.m_page;
        }
    }
    //	保留内存
    this->Data.m_used = 0;
    if ((this->Data.m_base = MemoryAllocator::Allocate(GetCurrentHandle, NULL, SizeTReserve, RLIB_MEM_RESERVE, RLIB_PAGE_READWRITE)) == nullptr)
    {
        goto EXIT; //内存未保留成功,无需释放
    }
    //	提交预备内存
    this->Data.m_now = SizeTCommit; //为了提交失败时~Memory()中能够正常释放已保留的内存
    if (MemoryAllocator::Allocate(GetCurrentHandle, this->Data.m_base, SizeTCommit, RLIB_MEM_COMMIT, RLIB_PAGE_READWRITE) != nullptr)
    {
        this->Data.m_max = SizeTReserve;
        this->Data.m_firstptr = NULL; //必须的初始赋值
        this->Data.m_lastptr = NULL; //必须的初始赋值
        #ifdef _DEBUG
            this->Data.m_succeed = 0;
            this->Data.m_failed = 0;
        #endif // _DEBUG
        goto EXIT; //SUCCESS
    }
    this->~Memory(); //内存已保留,但提交时错误,需释放
    EXIT: return ; /*	__asm _EMIT 0x90*/
}

//-------------------------------------------------------------------------

Memory::Memory()
{
    this->init(RLIB_MemoryCommit, RLIB_MemoryReserve);
}

//-------------------------------------------------------------------------

Memory::Memory(LONG SizeTCommit, LONG SizeTReserve)
{
    this->init(SizeTCommit, SizeTReserve);
}

//-------------------------------------------------------------------------

Memory::~Memory()
{
    if (this->Data.m_now == 0)
    {
        trace(!"异常的内存池析构");
        return ;
    }
    this->Data.m_now = 0;

#ifdef _DEBUG
	if (this->Data.m_used != 0)
	{
		assert(this->Data.m_firstptr != nullptr);

		auto ptr = this->Data.m_firstptr;
		while(ptr != nullptr)
		{
			TCHAR debug_msg[512] = { _T("内存泄漏检查\r\n    泄漏的方法: \0") };
			_tcscat_s(debug_msg, 512, ptr->alloc_function);
			_tcscat_s(debug_msg, 512, _T("\r\n    泄漏的文件: \0"));
			_tcscat_s(debug_msg, 512, ptr->alloc_file);
			debug_warning(debug_msg);
			ptr = ptr->next;
		}
	} //if
	assert("内存泄漏, 忘记delete了吗?"  && this->Data.m_firstptr == nullptr);
	assert("内存泄漏, 忘记Collect了吗?" && this->Data.m_lastptr == nullptr);
#endif // _DEBUG
    MemoryAllocator::Free(GetCurrentHandle, this->Data.m_base, this->Data.m_now,
		RLIB_MEM_RELEASE);
}

//-------------------------------------------------------------------------

bool Memory::extend(LONG _Size)
{
	trace(_Size >= 0);

    ROUNDUP(_Size, this->Data.m_page);
    this->Data.m_now += _Size;
    if ((this->Data.m_now <= this->Data.m_max) && (MemoryAllocator::Allocate(GetCurrentHandle,
		this->Data.m_base, this->Data.m_now, RLIB_MEM_COMMIT, RLIB_PAGE_READWRITE) != nullptr))
    {
        return true;
    }
    this->Data.m_now -= _Size;
    return false;
}

//////////////////////////////////////////////////////////////////////////
Memory::PBLOCK_INFO Memory::find(LONG _Size)
{
    //如果无任何节点不会调用find进行搜索
    trace(this->Data.m_firstptr != nullptr);
    trace(this->Data.m_firstptr->prior == nullptr);
    trace(this->Data.m_lastptr != nullptr);
    trace(this->Data.m_lastptr->next == nullptr);

    //检查第一节点之前的内存
    if (this->Data.m_firstptr != PBLOCK_INFO(this->Data.m_base))
    {
        trace(this->Data.m_firstptr > PBLOCK_INFO(this->Data.m_base));
        if (LONG((LPBYTE(this->Data.m_firstptr) - LPBYTE(this->Data.m_base))) >= _Size)
        {
            PBLOCK_INFO(this->Data.m_base)->prior = NULL;
            PBLOCK_INFO(this->Data.m_base)->next = this->Data.m_firstptr;
            this->Data.m_firstptr->prior = PBLOCK_INFO(this->Data.m_base);
            this->Data.m_firstptr = PBLOCK_INFO(this->Data.m_base);
            return PBLOCK_INFO(this->Data.m_base);
        }
    }

    PBLOCK_INFO FindPtr = NULL; //上次搜索到的节点
    //开始搜索
    if (this->Data.m_firstptr->next == nullptr)
    {
        //只有一个节点
        trace(this->Data.m_lastptr == this->Data.m_firstptr);
		trace(this->Data.m_firstptr->align <= this->GetUsingSize());
		trace(this->Data.m_firstptr->align <= this->GetMemorySize());

        FindPtr = PBLOCK_INFO(LPBYTE(this->Data.m_firstptr) + this->Data.m_firstptr->align);
        switch (Validate(LPBYTE(FindPtr) + _Size))
        {
		case PTR_HIGH:
			//当前内存不足分配,继续提交内存
			{
				if (extend((LPBYTE(FindPtr) + _Size) - (LPBYTE(this->Data.m_base) + this->Data.m_now)))
				{
					break;
				}
				return nullptr;
			}
#ifdef _DEBUG
		case PTR_LOW:
			{
				assert(!"Game Over! Never get here...");
				return nullptr;
			}
#endif // _DEBUG
        }
        trace(FindPtr->align == 0 || !"可能内存溢出");
		trace(FindPtr->size  == 0 || !"可能内存溢出");
		trace(FindPtr->prior == 0 || !"可能内存溢出");
		trace(FindPtr->next  == 0 || !"可能内存溢出");
		assert(FindPtr->alloc_function == nullptr || !"可能内存溢出");

        this->Data.m_firstptr->next = FindPtr;
        trace(LPBYTE(FindPtr) >= (LPBYTE(this->Data.m_firstptr) + this->Data.m_firstptr->align));

        FindPtr->prior       = this->Data.m_firstptr;
        FindPtr->next        = nullptr;
        this->Data.m_lastptr = FindPtr;
        return FindPtr;
    }

    FindPtr = this->Data.m_firstptr->next;
    while (FindPtr->next != nullptr)
    {
        trace(LPBYTE(FindPtr->next) >= (LPBYTE(FindPtr) + FindPtr->align));
		trace(FindPtr->next->prior == FindPtr);
		trace(FindPtr->align <= this->GetUsingSize());
		trace(FindPtr->align <= this->GetMemorySize());

        if (LONG((LPBYTE(FindPtr->next) - (LPBYTE(FindPtr) + FindPtr->align))) >= _Size)
        {
            //内存碎片可用
            PBLOCK_INFO TempPtr = PBLOCK_INFO(LPBYTE(FindPtr) + FindPtr->align);

			trace(this->Validate(TempPtr) == PTR_IN);
            trace(TempPtr->align == 0 || !"可能内存溢出");
			trace(TempPtr->size  == 0 || !"可能内存溢出");
			trace(TempPtr->prior == 0 || !"可能内存溢出");
			trace(TempPtr->next  == 0 || !"可能内存溢出");
			assert(TempPtr->alloc_function == nullptr || !"可能内存溢出");
           

            FindPtr->next->prior = TempPtr;
            TempPtr->next        = FindPtr->next;
            FindPtr->next        = TempPtr;

            trace(LPBYTE(TempPtr) >= (LPBYTE(FindPtr) + FindPtr->align));

            TempPtr->prior = FindPtr;
            return TempPtr;
        }
        FindPtr = FindPtr->next;
    }

    trace(FindPtr == this->Data.m_lastptr); //当前位置一定是最后节点
    
	FindPtr = PBLOCK_INFO(LPBYTE(FindPtr) + FindPtr->align);

	switch (Validate(LPBYTE(FindPtr) + _Size))
	{
	case PTR_HIGH: //当前内存不足分配,继续提交内存
		{
			if (extend((LPBYTE(FindPtr) + _Size) - (LPBYTE(this->Data.m_base) + this->Data.m_now)))
			{
				break;
			}
			//继续提交内存失败
			return nullptr;
		}
#ifdef _DEBUG
	case PTR_LOW:
		{
			assert(!"Game Over! Never get here...");
			return nullptr;
		}
#endif // _DEBUG
	}
	trace(FindPtr->align == 0 || !"可能内存溢出");
	trace(FindPtr->size  == 0 || !"可能内存溢出");
	trace(FindPtr->prior == 0 || !"可能内存溢出");
	trace(FindPtr->next  == 0 || !"可能内存溢出");
	assert(FindPtr->alloc_function == nullptr || !"可能内存溢出");

	this->Data.m_lastptr->next = FindPtr;
	FindPtr->prior             = this->Data.m_lastptr;
	FindPtr->next              = nullptr;

	trace(FindPtr > this->Data.m_lastptr);

	this->Data.m_lastptr = FindPtr;

	return FindPtr;
}

//-------------------------------------------------------------------------

Object Memory::alloc_block(LONG _Size)
{
    register LONG _Real = _Size > 0 ? _Size : AlignSize;
    ROUNDUP(_Size, AlignSize);
    _Size += ObjSize;

    PBLOCK_INFO _p = (PBLOCK_INFO)this->Data.m_base;
    {
        if (this->Data.m_used != 0) //并非首次分配
        {
            if ((_p = this->find(_Size)) == nullptr) //搜索可分配块
            {
                goto EXIT; //搜索失败
            }
        }
        else //从开始处分配
        {
            if (_Size > this->Data.m_now) //没有足够空间
            {
                if (!extend(_Size - this->Data.m_now))
                {
                    goto EXIT; //分配失败
                }
            }
            trace(this->Data.m_firstptr == nullptr);
            this->Data.m_firstptr = _p;
            trace(this->Data.m_lastptr == nullptr);
            this->Data.m_lastptr  = _p;
        }
        this->Data.m_used += _Size; //记录已申请数
        _p->align          = _Size;
        _p->size           = _Real;
        #ifdef _RUNTIME_DEBUG
		    assert(sizeof(_p->ptr_valid) == 4);
            ((char *)&_p->ptr_valid)[0] = char(RLIB_VALIDBYTE);
			((char *)&_p->ptr_valid)[1] = char(RLIB_VALIDBYTE);
			((char *)&_p->ptr_valid)[2] = char(RLIB_VALIDBYTE);
			((char *)&_p->ptr_valid)[3] = char(RLIB_VALIDBYTE);
        #endif // _RUNTIME_DEBUG

    }
    #ifdef _DEBUG
        this->Data.m_succeed++;
    #endif // _DEBUG

    return HandleToUser(_p);
EXIT: 
    #ifdef _DEBUG
        this->Data.m_failed++;
    #endif // _DEBUG

    return nullptr;
}

//-------------------------------------------------------------------------

bool Memory::check_overflow(PBLOCK_INFO handle)
{
#ifdef _RUNTIME_DEBUG
	if (LONG(handle->align - ObjSize) != handle->size)
	{
		//利用对齐间隙检测
		byte *pbytes = (((byte*)HandleToUser(handle)) + handle->size);
		int check_bytes = (((byte*)handle) + handle->align) - (((byte*)HandleToUser(handle)) + handle->size);
		while (check_bytes > 0)
		{
			if (*pbytes != (byte)0U)
			{
				debug_warning(handle->alloc_function);
				return true;
			} //if
			pbytes++;
			check_bytes--;
		}
	} //if
	return false;
#else 
	return false; //Release不检查溢出
#endif // _RUNTIME_DEBUG
}

//-------------------------------------------------------------------------

#ifdef _RUNTIME_DEBUG
void *Memory::AllocByte(LONG bytes, LPCTSTR function, LPCTSTR file)
{
	auto ptr = (PBLOCK_INFO)this->alloc_block(bytes);
	if (ptr == nullptr)
	{
		return nullptr;
	} //if
	ptr = UserToHandle(ptr);
	ptr->alloc_function = function;
	ptr->alloc_file = file;
	return HandleToUser(ptr);
}
#else
void *Memory::AllocByte(LONG bytes)
{
	return this->alloc_block(bytes);
}
#endif // _RUNTIME_DEBUG

//-------------------------------------------------------------------------

void Memory::Collect(Object user)
{
    this->free_block(user);
}

//-------------------------------------------------------------------------

void Memory::free_block(Object user)
{
	if (Validate(user) == PTR_IN) //判断是否指向本内存池
	{
		PBLOCK_INFO handle = UserToHandle(user);
#ifdef _RUNTIME_DEBUG
		assert(sizeof(handle->ptr_valid) == 4);
		bool bcheck = (((char *)&handle->ptr_valid)[0] == char(RLIB_VALIDBYTE) 
			&& ((char *)&handle->ptr_valid)[1] == char(RLIB_VALIDBYTE)
			&& ((char *)&handle->ptr_valid)[2] == char(RLIB_VALIDBYTE)
			&& ((char *)&handle->ptr_valid)[3] == char(RLIB_VALIDBYTE));
		trace(bcheck || !"请检查内存溢出, 点击忽略查看所在方法");
		if(!bcheck) debug_warning(handle->alloc_function);
#endif // _RUNTIME_DEBUG
		{
			if (handle->prior != nullptr)
			{
				handle->prior->next = handle->next;
#ifdef _DEBUG
				if (handle->next != nullptr)
				{
					assert(LPBYTE(handle->next) >= (LPBYTE(handle->prior) + handle->prior->align));
				}
#endif // _DEBUG
			}
#ifdef _DEBUG
			if (this->Data.m_firstptr != nullptr)
			{
				assert(handle >= this->Data.m_firstptr); //小于就是非法情况, 释放第一节点之前的节点
			}
			if (this->Data.m_lastptr != nullptr)
			{
				assert(handle <= this->Data.m_lastptr); //大于就是非法情况, 释放最后节点之后的节点
			}
#endif // _DEBUG
			if (handle == this->Data.m_firstptr)
			{
				trace(handle->prior == nullptr); //第一个节点必须无前趋节点
#ifdef _DEBUG
				assert(this->Data.m_lastptr != nullptr); //有第一节点必有最后节点
				if (this->Data.m_firstptr != this->Data.m_lastptr)
				{
					assert(this->Data.m_firstptr->next != nullptr); //第一节点必有后继节点
				}
#endif // _DEBUG
				this->Data.m_firstptr = handle->next; //下个节点成为第一节点
				if (handle->next != nullptr)
				{
					handle->next->prior = nullptr;
				}
				//前趋节点被释放必须置nullptr
				if (handle == this->Data.m_lastptr)
				{
					this->Data.m_lastptr = handle->next;
				}
			}
			else if (handle == this->Data.m_lastptr)
			{
				trace(handle->next == nullptr); //最后一个节点必须无后继节点
				this->Data.m_lastptr = handle->prior; //上个节点成为最后节点
				trace(handle->prior != nullptr);
				if (handle == this->Data.m_firstptr)
				{
					this->Data.m_firstptr = handle->prior;
				}
#ifdef _DEBUG
				else
				{
					assert(handle->prior != nullptr); //是最后节点, 但不是第一节点, 必有前趋节点
				}
#endif // _DEBUG
			}
			else
			{
				trace(handle->prior != nullptr); //非第一、最后一个节点必须有前趋节点
				trace(handle->next != nullptr); //非第一、最后一个节点必须有后继节点
#ifdef _RUNTIME_DEBUG
				if (handle->prior == nullptr || handle->next == nullptr)
				{
					debug_warning(handle->alloc_function);
				} //if
#endif // _RUNTIME_DEBUG
				handle->next->prior = handle->prior;
			}
			this->Data.m_used -= handle->align;
#ifdef _RUNTIME_DEBUG
			trace(check_overflow(handle) == false);
#endif // _RUNTIME_DEBUG
			//清0
			memset(handle, 0, handle->align);
		}
	}
#ifdef _DEBUG
	else
	{
		if (user != nullptr) assert(!"异常的内存回收");
		//规定 回收nullptr是安全的
	}
#endif // _DEBUG
}

//-------------------------------------------------------------------------

LONG Memory::TryClean()
{
	LONG byteCleaned = 0;
	{
		//已提交内存大小至少大于默认提交大小才能释放
		if (this->Data.m_now <= RLIB_MemoryCommit)
		{
			goto EXIT;
		}
		//正使用的内存大小为零,予以释放
		if (this->Data.m_used == 0)
		{
			trace(this->Data.m_lastptr == nullptr); //该情况无任何节点
			if (MemoryAllocator::Free(GetCurrentHandle, (LPBYTE(this->Data.m_base)
				+ RLIB_MemoryCommit), (this->Data.m_now - RLIB_MemoryCommit)
				/*恢复到1页内存*/, RLIB_MEM_DECOMMIT) != nullptr)
			{
				byteCleaned = this->Data.m_now - RLIB_MemoryCommit;
				this->Data.m_now = RLIB_MemoryCommit;
			}
			goto EXIT;
		}
		trace(this->Data.m_lastptr != nullptr); //该情况必须有最后节点
		LONG byteFree = ((LPBYTE(this->Data.m_base) + this->Data.m_now) /*内存边界*/
			- (LPBYTE(this->Data.m_lastptr) + this->Data.m_lastptr->align)) 
			/*内存池末尾闲置已提交内存*/;
		//至少一页才能释放
		if (byteFree > this->Data.m_page)
		{
			(byteFree /= this->Data.m_page) *= this->Data.m_page; //最大能释放的内存大小

			if (MemoryAllocator::Free(GetCurrentHandle, ((LPBYTE(this->Data.m_base) + 
				this->Data.m_now) /*内存边界*/ - byteFree), byteFree, RLIB_MEM_DECOMMIT) != nullptr)
			{
				byteCleaned = byteFree;
				this->Data.m_now -= byteFree;
			}
		}
	}
EXIT: return byteCleaned;
}

//-------------------------------------------------------------------------

bool Memory::ReSize(Object user, LONG new_size)
{
    if (this->Validate(user) != PTR_IN)
    {
		trace(!"调用ReSize方法时发生异常, 请使用对应的内存页对象进行调用");
        goto RET_FALSE;
    }
    if (new_size == 0)//约定为释放内存
    {
        this->Collect(user);
        return true;
    }
    else if (new_size < AlignSize) //该操作无效果
    {
        goto RET_FALSE;
    }

    LONG _Real = new_size;
    if (new_size % AlignSize != 0)
    {
        new_size = (new_size / AlignSize + 1) *AlignSize; //向上取整
    }
    new_size += ObjSize;

    PBLOCK_INFO handle = UserToHandle(user); //取得操作地址
    if (new_size < handle->align) //缩减分配的内存
    {
        //memset(LPSTR(handle) + new_size, 0, (handle->align - new_size)/*减少的大小*/);
        {
            this->Data.m_used -= (handle->align - new_size) /*减少的大小*/;
            memset((LPBYTE(handle) + new_size), 0, (handle->align - new_size) /*减少的大小*/);
            handle->align = new_size;
        }
        goto RET_TRUE;
    }
    else if (new_size > handle->align) //扩大分配的内存
    {
        if (handle->next == nullptr)
        {
            trace(handle == this->Data.m_lastptr); //最后节点
            switch (Validate((LPBYTE(handle) + new_size)))
            {
                case PTR_HIGH:
                    {
                        if (!extend((LPBYTE(handle) + new_size) - (LPBYTE(this->Data.m_base)
							+ this->Data.m_now)))
                        {
							goto RET_FALSE;
                        }
                    }
					break;
#ifdef _DEBUG
				case PTR_LOW:
					{
						assert(!"Nerver get here...");
						goto RET_FALSE;
					}
#endif // _DEBUG
            }
            goto SUCCEEDED;
        }
        if (LPBYTE(handle->next) < (LPBYTE(handle) + new_size))
        {
            goto RET_FALSE;
        }
SUCCEEDED:
        this->Data.m_used +=  (new_size - handle->align) /*扩大的大小*/;
        handle->align = new_size;
RET_TRUE: 
		handle->size = _Real;

        #ifdef _DEBUG
            this->Data.m_succeed++;
        #endif // _DEBUG

        return true;
    }
RET_FALSE: 
    #ifdef _DEBUG
        this->Data.m_failed++;
    #endif // _DEBUG

    return false;
}

//-------------------------------------------------------------------------

void *Memory::ReAlloc(Object puser, LONG new_size /* = 0 */)
{
    if (this->Validate(puser) != PTR_IN)
    {
		trace(!"调用ReAlloc方法时发生异常, 请使用对应的内存页对象进行调用");
        goto RET_FALSE;
    }
#ifdef _RUNTIME_DEBUG
	LPCTSTR function = UserToHandle(puser)->alloc_function, file = UserToHandle(puser)->alloc_file;
#endif // _RUNTIME_DEBUG
    LONG old_size = UserToHandle(puser)->size;

    if (new_size == 0) new_size = old_size;

    Object puser_new = alloc_block(new_size);
    if (puser_new != nullptr)
    {
        if (old_size > new_size) old_size = new_size; //防止溢出
 
        Memory::memcpy(puser_new, puser, old_size);

        this->Collect(puser);

        #ifdef _DEBUG
            this->Data.m_succeed++;
        #endif // _DEBUG
#ifdef _RUNTIME_DEBUG
		UserToHandle(puser_new)->alloc_function = function;
		UserToHandle(puser_new)->alloc_file = file;
#endif // _RUNTIME_DEBUG

        return puser_new;
    }
RET_FALSE: 

    #ifdef _DEBUG
        this->Data.m_failed++;
    #endif // _DEBUG

    return nullptr;
}

//-------------------------------------------------------------------------

// bool Memory::DumpToFile(TCHAR *lpFile)
// {
// 	//具体实现见RLib_File.cpp
// }

//------------------------------------------------------------------------

PTR_STATUS Memory::Validate(LPCVOID p)
{
    if (p >= (LPBYTE(this->Data.m_base) + this->Data.m_now)) //注意:不能等于末指针
    {
        return PTR_HIGH;
    }
    else if (p < this->Data.m_base)
    {
        return PTR_LOW;
    }
    return PTR_IN;
}

//-------------------------------------------------------------------------

LONG Memory::GetSize(LPCVOID user)
{
    if (Validate(user) == PTR_IN) //判断是否指向本内存池
    {
        return UserToHandle(user)->size;
    }
    return 0;
}

//-------------------------------------------------------------------------

LONG Memory::GetMaxAllocSize()
{
    return (this->Data.m_max - this->Data.m_now);
}

//-------------------------------------------------------------------------

LONG Memory::GetUsingSize()
{
    return this->Data.m_used;
}

//-------------------------------------------------------------------------

LONG Memory::GetMemorySize()
{
    return this->Data.m_now;
}
