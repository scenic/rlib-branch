#ifndef _USE_WINSOCK
#define _USE_WINSOCK
#include <Winsock2.h>
#include "RLib_RegExp.h"
//////////////////////////////////////////////////////////////////////////
using namespace System;
/*
 * Maximum queue length specifiable by listen.
 */
#define RLIB_SOMAXCONN       0x7fffffff
/*
 * WinSock 2 extension -- manifest constants for shutdown()
 */
#define RLIB_SD_RECEIVE      0x00
#define RLIB_SD_SEND         0x01
#define RLIB_SD_BOTH         0x02
/*
 * The new type to be used in all
 * instances which refer to sockets.
 */
typedef UINT_PTR             RLIB_SOCKET;
/*
 * This is used instead of -1, since the
 * SOCKET type is unsigned.
 */
#define RLIB_INVALID_SOCKET  (RLIB_SOCKET)(~0)

#ifdef UNICODE
typedef ADDRINFOW       ADDRINFOT, *PADDRINFOT;
#else
typedef ADDRINFOA       ADDRINFOT, *PADDRINFOT;
#endif
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 提供适用于许多网络协议的简单编程接口, 撰写和发送电子邮件, 代表多用途 Internet 邮件交换 (MIME) 标头, 访问网络流量数据和网络地址信息
	/// 另外，还实现了 Windows 套接字 (Winsock) 接口，能让您访问网络流以实现主机之间的安全通信
	/// </summary>
	namespace Net
	{
		/// <summary>
		/// 提供对Winsock的标准访问
		/// </summary>
		class export Sockets
		{
		protected:
			RLIB_SOCKET   m_socket;
			sockaddr_in   m_addr;
			Exception     m_error;
		    volatile BOOL m_end_accept;
		public:
			Sockets(RLIB_SOCKET s = RLIB_INVALID_SOCKET, int af = AF_INET, int type = SOCK_STREAM, int protocol = IPPROTO_TCP);
			~Sockets();
			RLIB_ClassNewDel;
		public:
			/// <summary>
			/// 方法允许您获取SOCKET以便进行更多的操作
			/// 您亦可继承并拓展Sockets类的功能
			/// </summary>
			operator RLIB_SOCKET();
			/// <summary>
			/// 设置发送超时
			/// </summary>
			bool SetSndTimeO(int timeout);
			/// <summary>
			/// 获取发送缓冲区
			/// </summary>
			int GetSndBuf();
			/// <summary>
			/// 设置接收超时
			/// </summary>
			bool SetRcvTimeO(int timeout);
			/// <summary>
			/// 获取接收缓冲区
			/// </summary>
			int GetRcvBuf();
			/// <summary>
			/// Closes an existing socket
			/// </summary>
			bool Close();
			/// <summary>
			/// Establishes a connection to a specified socket
			/// </summary>
			bool Connect(char *host, unsigned int host_len, unsigned short port);
			/// <summary>
			/// Sends data on a connected socket
			/// </summary>
			bool Send(char *data, int size);
			/// <summary>
			/// Sends data on a connected socket
			/// 当发生错误时,size用来接收已发送长度
			/// </summary>
			bool Send(char *data, int *size OUT);
			/// <summary>
			/// Receives data from a connected or bound socket
			/// 当发生错误时,size用来接收已接收长度
			/// </summary>
			int RecvEx(char *buf, int *size);  
			/// <summary>
			/// Receives data from a connected or bound socket
			/// 该方法不保证收完指定 size
			/// </summary>
			int Recv(char *buf, int size);  
			/// <summary>
			/// associates a local address with a socket
			/// </summary>
			int Bind(ULONG addr, unsigned short port);  
			/// <summary>
			/// associates a local address with a socket
			/// 方法绑定到所有地址上
			/// </summary>
			int Bind(unsigned short port);  
			/// <summary>
			/// Places a socket a state where it is listening for an incoming connection
			/// </summary>
			int Listen(int backlog = RLIB_SOMAXCONN);  
			/// <summary>
			/// Permits an incoming connection attempt on a socket
			/// An optional pointer to a buffer that receives the address of the connecting entity,
			/// as known to the communications layer.
			/// </summary>
			Sockets *Accept(sockaddr_in *addr = NULL, int *addrlen = nullptr);  
			/// <summary>
			/// 方法关闭socket以使Accept停止阻塞并立即返回
			/// Sockets类资源仍需用户进行释放(Sockets.~Sockets()或delete SocketsPtr)
			/// </summary>
			void AbortAccept();
			/// <summary>
			/// Disables sends or receives on a socket
			/// </summary>
			int Shutdown(int how = RLIB_SD_BOTH);
			/// <summary>
			/// 获取Wincock发生的异常信息
			/// 该方法仅提供于实例对象
			/// </summary>
			Exception *GetLastException(); 
			/// <summary>
			/// 获取一个值，该值指示是否与 Internet 已建立连接
			/// </summary>
			bool IsConnect; 
		public:
			/// <summary>
			/// 获取本地计算机名称
			/// </summary>
			static bool GetLocalHost(char *host, int hostlen);
			/// <summary>
			/// 获取本地计算机地址
			/// </summary>
			static bool GetLocalIpAddress(TCHAR *host, PADDRINFOT *addrinfo);
			/// <summary>
			/// 释放由GetLocalIpAddress获得的addrinfo
			/// </summary>
			static void FreeLocalIpAddress(PADDRINFOT *addrinfo);
			/// <summary>
			/// Converts a string containing an (Ipv4) Internet Protocol dotted address into a proper address for the in_addr structure
			/// </summary>
			static unsigned long Ipv4StringToAddress(const char *cp);
			/// <summary>
			/// Converts an (IPv4) Internet network address into a string in Internet standard dotted format
			/// </summary>
			static char *Ipv4AddressToString(in_addr in);
			/// <summary>
			/// 获取Wincock发生的错误说明
			/// </summary>
			static bool GetLastError(LPVOID buffer, size_t size, int err = 0);
			/// <summary>
			/// 当不再使用Winsock的时候调用此方法会释放网络模块内存
			/// </summary>
			static void Dispose();
		};
	}
}
#endif