/********************************************************************
	Created:	2012/11/17  13:44
	Filename: 	RLib_Random.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "RLib_Random.h"
using namespace System;
//-------------------------------------------------------------------------
#define GetRandom(min, max) ((rand() % (int)(((max)   +   1) - (min))) + (min)) //max-min < 256时精度比较高
#define GetBigRandom(min, max) (rand() * ((float)((max) - (min))) / (float)(RAND_MAX) + (min))
//-------------------------------------------------------------------------
/// <summary>
/// 一种产生唯一种子值的方法是使它与时间相关, 方法使用与时间相关的默认种子值
/// </summary>
void Random::Srand()
{
	Random::Srand((unsigned int)GetCurrentTime());
}
/// <summary>
/// 如果应用程序需要不同的随机数序列，则使用不同的种子值重复调用此函数。
/// </summary>
/// <param name="Seed">用来计算伪随机数序列起始值的数字</param>
void Random::Srand(unsigned int Seed)
{
	srand(Seed);
}
/// <summary>
/// 返回一个指定范围内的随机数
/// </summary>
/// <param name="minValue">返回的随机数的下界（随机数可取该下界值）</param>
/// <param name="maxValue">返回的随机数的上界（随机数不能取该上界值）, maxValue 必须大于或等于 minValue。</param>
/// <returns>一个大于等于 minValue 且小于 maxValue 的 32 位带符号整数</returns>
int Random::GetRnd(int minValue, int maxValue)
{
	return GetRandom(minValue, maxValue);
}
/// <summary>
/// 返回一个指定范围内的随机数, 方法适用于大范围的随机数要求
/// </summary>
/// <param name="minValue">返回的随机数的下界（随机数可取该下界值）</param>
/// <param name="maxValue">返回的随机数的上界（随机数不能取该上界值）, maxValue 必须大于或等于 minValue。</param>
/// <returns>一个大于等于 minValue 且小于 maxValue 的 32 位带符号整数</returns>
float Random::GetWideRnd(int minValue, int maxValue)
{
	return GetBigRandom(minValue, maxValue);
}