#include "RLib_String.h"
//////////////////////////////////////////////////////////////////////////
using namespace System;
using namespace System::IO;
using namespace System::Text;
//////////////////////////////////////////////////////////////////////////
//String类函数的实现
//////////////////////////////////////////////////////////////////////////
#ifdef RLIB_DLL
    #define _this (const_cast<String *>(this))
#endif // RLIB_DLL
////////////////////////////////////////////////////////////////////////// 
//负责string的构造及销毁
//////////////////////////////////////////////////////////////////////////
String::String()
{
    m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
    assert(m_ptag != nullptr);
}

//-------------------------------------------------------------------------

String::String(Nullable)
{
    m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
    assert(m_ptag != nullptr);
}

//-------------------------------------------------------------------------

String::String(UINT Len)
{
    m_ptag = (pTag)(Object)this->fastalloc(sizeof(StringTag));
    assert(m_ptag != nullptr);

    m_ptag->m_len = 0;
    m_ptag->m_size = TSIZE(Len);
    m_ptag->m_pstr = (TCHAR *)this->fastalloc(m_ptag->m_size);
    m_ptag->m_ref = 1;
}

//-------------------------------------------------------------------------

String::String(TCHAR *p)
{
    m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
    assert(m_ptag != nullptr);

    m_ptag->m_len = TLEN(p);
    m_ptag->m_size = TSIZE(m_ptag->m_len);
    m_ptag->m_pstr = (TCHAR *)this->fastalloc(m_ptag->m_size);
    String::FastStringCopy(m_ptag->m_pstr, p, m_ptag->m_len);
    m_ptag->m_ref = 1;
}

//-------------------------------------------------------------------------

String::String(const CHAR *p)
{
    m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
    assert(m_ptag != nullptr);
    #ifndef UNICODE
        m_ptag->m_pstr = (TCHAR *)p;
        m_ptag->m_len = TLEN(m_ptag->m_pstr);
        //fastalloc 分配的内存约定为被初始化为0
        //m_ptag->m_ref   = 0;
    #else 
        //CHAR to WCHAR
        m_ptag->m_pstr = (LPWSTR)String::ConvertToWideChar(p);
        update(m_ptag->m_pstr, AppBase::GetUsingPool()->GetSize(m_ptag->m_pstr));
    #endif 
}

//-------------------------------------------------------------------------

String::String(const WCHAR *p)
{
    m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
    assert(m_ptag != nullptr);
    #ifdef UNICODE
        m_ptag->m_pstr = (TCHAR *)p;
        m_ptag->m_len = TLEN(m_ptag->m_pstr);
        //fastalloc 分配的内存约定为被初始化为0
        //m_ptag->m_ref   = 0;
    #else 
        //WCHAR to CHAR
        m_ptag->m_pstr = (LPSTR)String::ConvertToMultiByte(p);
        update(m_ptag->m_pstr, AppBase::GetUsingPool()->GetSize(m_ptag->m_pstr));
    #endif 
}

//-------------------------------------------------------------------------

String::String(const String &src)
{
    if (src.m_ptag->m_ref >= 1)
    //src非常量引用则直接共享m_ptag
    {
        m_ptag = src.m_ptag; //此时m_ptag并未申请内存故无需释放
        m_ptag->m_ref += 1; //更新引用计数
    }
    else
    //src为常量引用则必须引用常量而不是共享m_ptag
    {
        m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
        m_ptag->m_pstr = src.m_ptag->m_pstr;
        m_ptag->m_len = src.GetLength();
        //fastalloc 分配的内存约定为被初始化为0
        //m_ptag->m_ref = 0;
        //m_ptag->m_canwrite = false;
        //m_ptag->m_size  = 0;
    }
}

//-------------------------------------------------------------------------

String::~String()
{
    //检查引用计数
    if (this->m_ptag->m_ref > 1)
    {
        //更新字符串引用计数,然后扔了它
        this->m_ptag->m_ref -= 1;
        goto RET; //此时m_str不能被释放
    }
    else if (this->m_ptag->m_ref == 1)
    //检查能否释放字符串指针
    {
        //回收将未引用的字符串
        String::Collect(this->m_ptag->m_pstr);
    }
    this->collect_auto();
    RLIB_GlobalCollect(m_ptag);
    RET: return ;
}

//-------------------------------------------------------------------------
typedef struct mark
{
	char a, b;
	void *p;
	void set_mark()
	{
		a = 0x1U;
		b = 0x2U;
	}
	bool is_mark()
	{
		return a == 0x1U && b == 0x2U;
	}
}*pmark;
//-------------------------------------------------------------------------

void String::collect_auto()
{
    if (this->m_ptag->m_pchar != nullptr)
    {
		this->m_ptag->m_pchar_size = 0;
		if(pmark(this->m_ptag->m_pchar)->is_mark())
		{
			String::Collect(pmark(this->m_ptag->m_pchar)->p);
		}
        String::Collect(this->m_ptag->m_pchar);
    }
    if (this->m_ptag->m_pwchar != nullptr)
    {
		this->m_ptag->m_pwchar_size = 0;
		if(pmark(this->m_ptag->m_pwchar)->is_mark())
		{
			String::Collect(pmark(this->m_ptag->m_pwchar)->p);
		}
        String::Collect(this->m_ptag->m_pwchar);
    }
}

//-------------------------------------------------------------------------

void *String::fastalloc(ULONG bytes)
{
	void *ptr = RLIB_GlobalAlloc(bytes);
	assert(!IsBadReadPtr(ptr, bytes) && !IsBadWritePtr(ptr, bytes));
    return ptr;
}

//-------------------------------------------------------------------------

TCHAR *String::FastAllocateString(int length)
{
    return (TCHAR *)RLIB_GlobalAlloc(TSIZE(length));
}

//-------------------------------------------------------------------------

void String::Collect(Object ptr)
{
    RLIB_GlobalCollect(ptr);
}

//////////////////////////////////////////////////////////////////////////
//内部方法实现
//////////////////////////////////////////////////////////////////////////
void String::prepare(UINT length)
{
    UINT size = TSIZE(length);
    //!参数为长度
    TCHAR *tmp = (TCHAR *)this->fastalloc(size);
    if (this->m_ptag->m_ref == 0)
    {
        //直接拷贝
        if (this->GetLength() != 0)
        {
            String::FastStringCopy(tmp, m_ptag->m_pstr, this->m_ptag->m_len);
        }
    }
    else
    {
        assert(m_ptag->m_pstr != nullptr);
        String::FastStringCopy(tmp, m_ptag->m_pstr, this->Length);
        if (this->m_ptag->m_ref > 1)
        {
            this->m_ptag->m_ref -= 1; //更新字符串引用计数,然后扔了它
            m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
			assert(m_ptag != nullptr);
            goto NEXT;
        }
        //当前字符串未被其它实例引用, 则m_ptag仍然可以使用
        String::Collect(this->m_ptag->m_pstr); //释放将未引用的字符串
    }
    NEXT: this->update(tmp, size);
}

//-------------------------------------------------------------------------

void String::update(LPTSTR ptr, UINT size, UINT length /* = 0 */)
{
    m_ptag->m_pstr = ptr;
    m_ptag->m_ref = 1;
    m_ptag->m_check = false;
    m_ptag->m_len = length != 0 ? length : TLEN(m_ptag->m_pstr);
    m_ptag->m_size = size;
    assert(m_ptag->m_len < m_ptag->m_size);
}

//////////////////////////////////////////////////////////////////////////
//类自动转换
//////////////////////////////////////////////////////////////////////////
String::operator TCHAR *()
{
    return this->GetData();
}

//-------------------------------------------------------------------------

String::operator const TCHAR *()const
{
    return this->GetConstData();
}

//-------------------------------------------------------------------------

LPTSTR String::GetData()
{
    if (this->m_ptag->m_ref == 1)
    {
        //常量引用时 m_ref == 0，故排除
        goto RET;
    }
    prepare(this->GetLength());
    RET: this->m_ptag->m_check = true;
    return this->m_ptag->m_pstr;
}

//-------------------------------------------------------------------------

LPCTSTR String::GetConstData()const
{
    return this->m_ptag->m_pstr;
}

//-------------------------------------------------------------------------

LPCTSTR String::GetSafeData()const
{
    if (this->IsNull())
    {
        _this->Empty();
    } //if
    return this->GetConstData();
}

//////////////////////////////////////////////////////////////////////////
//赋值方法实现
//////////////////////////////////////////////////////////////////////////
const String &String::operator = (Nullable /* Len*/)
{
    if (this->m_ptag->m_ref > 1)
    {
        //更新字符串引用计数,然后扔了它
        this->m_ptag->m_ref -= 1;
        this->m_ptag = (pTag)this->fastalloc(sizeof(StringTag)); //此时m_str不能被释放
		assert(this->m_ptag != nullptr);
        goto RETN;
    }
    else if (this->m_ptag->m_ref == 1)
    //如果是常量引用那么不能释放字符串指针
    {
        //释放将未引用的字符串
        String::Collect(this->m_ptag->m_pstr);
    }
    this->collect_auto();
    Memory::memset(this->m_ptag, 0, sizeof(StringTag));
    RETN: return  *this;
}

//-------------------------------------------------------------------------

const String &String::operator = (TCHAR *p)
{
    UINT length = TLEN(p);
    if (length == 0)
    {
        this->Empty();
        goto ret;
    }

    if (!TryCopy(p, length))
    {
        Copy(p, length);
    }
    ret: return  *this;
}

//-------------------------------------------------------------------------

const String &String::operator = (const CHAR *pStr)
{
    int length = strlen(pStr);
    if (length == 0)
    {
        this->Empty();
        goto exit;
    }
    #ifdef UNICODE
        //CHAR to WCHAR
        LPTSTR pTStr = (LPWSTR)String::ConvertToWideChar(pStr, length);
    #endif 

    if (!this->IsNull())
    {
        if (this->m_ptag->m_ref > 1)
        {
            //更新字符串引用计数, 重新分配字符串信息结构
            this->m_ptag->m_ref -= 1;
            m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
			assert(m_ptag != nullptr);
            goto do_next;
        }
        //当前字符串未被其它实例引用, 我们处理一下它
        #ifndef UNICODE
            if (TryCopy(pStr))
            //当前缓冲区足以容纳, 直接拷贝
            {
                goto exit;
            }
        #endif 
        if (this->m_ptag->m_ref == 1)
        {
            String::Collect(this->m_ptag->m_pstr); //释放将未引用的字符串
        }
    }
    do_next: this->collect_auto();
    Memory::memset(m_ptag, 0, sizeof(StringTag));

    #ifdef UNICODE
        update(pTStr, TSIZE(length), length);
    #else 
        m_ptag->m_pstr = const_cast < TCHAR * > (pStr); //引用关系
        m_ptag->m_len = length;
        //  m_ptag->m_ref      = 0;
        //  m_ptag->m_size     = 0;
        //  m_ptag->m_check    = false;
    #endif 

    exit: return  *this;
}

//-------------------------------------------------------------------------

const String &String::operator = (const WCHAR *pStr)
{
    int length = wcslen(pStr);
    if (length == 0)
    {
        this->Empty();
        goto exit;
    }

    #ifndef UNICODE
        //WCHAR to CHAR
        LPTSTR pTStr = String::ConvertToMultiByte(pStr, length);
    #endif 

    if (!this->IsNull())
    {
        if (this->m_ptag->m_ref > 1)
        {
            //更新字符串引用计数, 重新分配字符串信息结构
            this->m_ptag->m_ref -= 1;
            m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
			assert(m_ptag != nullptr);
            goto do_next;
        }
        //当前字符串未被其它实例引用, 我们处理一下它
        #ifdef UNICODE
            if (TryCopy(pStr))
            //当前缓冲区足以容纳, 直接拷贝
            {
                goto exit;
            }
        #endif 
        if (this->m_ptag->m_ref == 1)
        {
            String::Collect(this->m_ptag->m_pstr); //释放将未引用的字符串
        }
    }
    do_next: this->collect_auto();
    Memory::memset(m_ptag, 0, sizeof(StringTag));

    #ifndef UNICODE
        update(pTStr, TSIZE(length), length);
    #else 
        m_ptag->m_pstr = const_cast < TCHAR * > (pStr); //引用关系
        m_ptag->m_len = length;
        //  m_ptag->m_ref      = 0;
        //  m_ptag->m_size     = 0;
        //  m_ptag->m_check    = false;
    #endif 

    exit: return  *this;
}

//-------------------------------------------------------------------------

const String &String::operator = (const String &src)
{
    if (src.IsNull())
    {
        *this = Nothing;
        goto Ret;
    } //if
    else if (src.IsEmpty())
    {
        this->Empty();
        goto Ret;
    } //if

    if (this->m_ptag->m_ref > 1)
    {
        //更新字符串引用计数,然后扔了它
        this->m_ptag->m_ref -= 1; //非常量引用则引用计数减1,然后不管它
        if (src.m_ptag->m_ref != 0)
        //对方不是常量引用
        {
            goto NOT_CONST_REF;
        }
        m_ptag = (pTag)this->fastalloc(sizeof(StringTag)); //为了下一步的拷贝不影响到其它string实例
		assert(m_ptag != nullptr);
    }
    else
    {
        if (TryCopy(src.m_ptag->m_pstr, src.GetLength()))
        //此类情况直接拷贝而非二次引用
        {
            goto Ret;
        }
        if (this->m_ptag->m_ref == 1)
        {
            String::Collect(this->m_ptag->m_pstr); //释放将未引用的字符串
        }
        if (src.m_ptag->m_ref != 0)
        //对方不是常量引用
        {
            this->collect_auto(); //此处释放m_ptag前必须调用
            RLIB_GlobalCollect(m_ptag);
            goto NOT_CONST_REF;
        }
    }
    this->collect_auto();
    Memory::memset(this->m_ptag, 0, sizeof(StringTag));
    this->m_ptag->m_pstr = src.m_ptag->m_pstr;
    this->m_ptag->m_len = src.m_ptag->m_len;
    goto Ret;
    NOT_CONST_REF: m_ptag = src.m_ptag;
    m_ptag->m_ref += 1;
    Ret: return  *this;
}

//////////////////////////////////////////////////////////////////////////
//基本属性
//////////////////////////////////////////////////////////////////////////
UINT String::GetSize()const
{
    return this->m_ptag->m_size; //该属性 m_ptag->m_canwrite == false 时为0
}

//-------------------------------------------------------------------------

UINT String::GetLength()const
{
    if (this->IsNull())
    {
        return 0;
    }
    if (this->m_ptag->m_check)
    {
        this->m_ptag->m_len = TLEN(this->m_ptag->m_pstr);
        this->m_ptag->m_check = false;
    }
    return m_ptag->m_len;
}

//-------------------------------------------------------------------------

UINT String::GetCanReadSize()const
{
    return this->IsNullOrEmpty() ? 0 : TSIZE(this->GetLength() - 1);
}

//-------------------------------------------------------------------------

int String::GetMultiByteSize() const
{
	if (this->ToMultiByte() == nullptr)
	{
		assert(this->m_ptag->m_pchar == nullptr);
		return 0;
	} //if
	auto ptr        = (pmark)(_this->fastalloc(sizeof(mark)));
	ptr->set_mark();
	ptr->p          = m_ptag->m_pchar;
	m_ptag->m_pchar = (LPSTR)ptr;
	return this->m_ptag->m_pchar_size;
}

//-------------------------------------------------------------------------

LPSTR String::ToMultiByte() const
{
	if(this->m_ptag->m_pchar != nullptr)
	{
		if(pmark(this->m_ptag->m_pchar)->is_mark())
		{
			auto p = pmark(this->m_ptag->m_pchar);
			this->m_ptag->m_pchar = (char *)p->p;
			String::Collect(p);
			return this->m_ptag->m_pchar;
		}
		String::Collect(this->m_ptag->m_pchar);
		this->m_ptag->m_pchar = nullptr;
	}
    #ifndef UNICODE
	    this->m_ptag->m_pchar_size = this->Length;
		this->m_ptag->m_pchar      = this->c_str();
		return this->m_ptag->m_pchar;
    #else 
        if (this->IsNull())
        {
			this->m_ptag->m_pchar_size = 0;
            return nullptr;
        }
        else if (this->IsEmpty())
        {
            //对于此类情况, 直接返回空字符串无须转换
            this->m_ptag->m_pchar = (CHAR *)_this->fastalloc(sizeof(CHAR));
            goto do_next;
        }
		{
			//WCHAR to CHAR
			UnmanagedMemoryStream temp((LPVOID)this->GetConstData(), 
				this->Length * sizeof(WCHAR), this->Length * sizeof(WCHAR));
			MemoryStream *_pstream = Encoder::ToMultiByte(ASCIIEncoding, &temp);
			if (_pstream == nullptr)
			{
				this->m_ptag->m_pchar_size = 0;
				return nullptr;
			}
			this->m_ptag->m_pchar = (CHAR*)_this->fastalloc(_pstream->Length + sizeof(TCHAR));
			Memory::memcpy(this->m_ptag->m_pchar, _pstream->GetObjectData(), _pstream->Length);
			this->m_ptag->m_pchar_size = _pstream->Length;
			delete _pstream;
		}
do_next:
        return this->m_ptag->m_pchar;
    #endif 
}

//-------------------------------------------------------------------------

int String::GetWideCharSize() const
{
	if (this->ToWideChar() == nullptr)
	{
		assert(this->m_ptag->m_pwchar == nullptr);
		return 0;
	} //if
	auto ptr         = (pmark)(_this->fastalloc(sizeof(mark)));
	ptr->set_mark();
	ptr->p           = m_ptag->m_pwchar;
	m_ptag->m_pwchar = (LPWSTR)ptr;
	return this->m_ptag->m_pwchar_size;
}

//-------------------------------------------------------------------------

bool String::SuppressFinalize(LPVOID ptr)
{
	if (this->m_ptag->m_pchar == ptr)
	{
		this->m_ptag->m_pchar = NULL;
		return true;
	}
	else if (this->m_ptag->m_pwchar == ptr)
	{
		this->m_ptag->m_pwchar = NULL;
		return true;
	} //if
	return false;
}

//-------------------------------------------------------------------------

LPWSTR String::ToWideChar() const
{
	if(this->m_ptag->m_pwchar != nullptr)
	{
		if(pmark(this->m_ptag->m_pwchar)->is_mark())
		{
			auto p = pmark(this->m_ptag->m_pwchar);
			this->m_ptag->m_pwchar = (wchar_t *)p->p;
			String::Collect(p);
			return this->m_ptag->m_pwchar;
		}
		String::Collect(m_ptag->m_pwchar);
		this->m_ptag->m_pwchar = nullptr;
	}
    #ifdef UNICODE
	    this->m_ptag->m_pwchar_size = this->Length * sizeof(wchar_t);
		this->m_ptag->m_pwchar = this->c_str();
		return m_ptag->m_pwchar;
    #else 
        if (this->IsNull())
        {
			this->m_ptag->m_pwchar_size = 0;
            return nullptr;
        }
        else if (this->IsEmpty())
        {
            //对于此类情况, 直接返回空字符串无须转换
            this->m_ptag->m_pwchar = (WCHAR*)_this->fastalloc(sizeof(WCHAR));
            goto do_next;
        }
		{
			//CHAR to WCHAR
			UnmanagedMemoryStream _tstream((LPVOID)this->GetConstData(), this->GetLength(), this->GetLength());
			MemoryStream *_pstream = Encoder::ToWideChar(ASCIIEncoding, &_tstream);
			if (_pstream == nullptr)
			{
				this->m_ptag->m_pwchar_size = 0;
				return nullptr;
			}
			this->m_ptag->m_pwchar = (LPWSTR)_this->fastalloc(_pstream->Length + sizeof(TCHAR));
			Memory::memcpy(this->m_ptag->m_pwchar, _pstream->ObjectData, _pstream->Length);
			this->m_ptag->m_pwchar_size = _pstream->Length;
			delete _pstream;
		}
do_next: 
        return this->m_ptag->m_pwchar;
    #endif 
}

//-------------------------------------------------------------------------

TCHAR *String::c_str()const
{
    if (this->IsNull())
    {
        return NULL;
    }
    TCHAR *_tmp = (TCHAR *)_this->fastalloc(TSIZE(this->GetLength()));
    String::FastStringCopy(_tmp, m_ptag->m_pstr, this->m_ptag->m_len);
    return _tmp;
}

//-------------------------------------------------------------------------

void String::Copy(const CHAR *p, UINT len /* = 0 */)
{
    #ifndef UNICODE
        if (len == 0)
        {
            len = TLEN(p);
            if (len == 0)
            {
                return this->Empty();
            }
        }
        if (this->m_ptag->m_ref > 1)
        {
            //更新字符串引用计数,然后扔了它
            this->m_ptag->m_ref -= 1;
            m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
        }
        else
        {
            //当前字符串未被其它实例引用,我们处理一下它
            if (TryCopy(p, len))
            //当前缓冲区足以容纳,直接拷贝.
            {
                goto Ret;
            }
            if (m_ptag->m_ref == 1)
            {
                String::Collect(this->m_ptag->m_pstr);
            }
            //释放将未引用的字符串
        }
        m_ptag->m_len = len;
        m_ptag->m_size = TSIZE(m_ptag->m_len);
        m_ptag->m_pstr = (TCHAR *)this->fastalloc(this->m_ptag->m_size);
        m_ptag->m_ref = 1;
        m_ptag->m_check = false;
        String::FastStringCopy(m_ptag->m_pstr, p, m_ptag->m_len);
        Ret: m_ptag->m_pstr[this->m_ptag->m_len] = T('\0');
    #else 
        if (len == 0)
        {
            len = strlen(p);
            if (len == 0)
            {
                return this->Empty();
            }
        }
        // CHAR to WCHAR
        // 只需要转换 len 长度即可
        LPWSTR _t = String::ConvertToWideChar(p, len);
        {
            if (_t == nullptr)
            {
				return this->Empty();
            }
            // 调用另一重载函数
            this->Copy((LPCWSTR)_t, len);
        }
        String::Collect(_t);
    #endif 
}

//-------------------------------------------------------------------------

void String::Copy(const WCHAR *p, UINT len /* = 0 */)
{
    #ifdef UNICODE
        if (len == 0)
        {
            len = TLEN(p);
            if (len == 0)
            {
				return this->Empty();
            }
        }
        if (this->m_ptag->m_ref > 1)
        {
            //更新字符串引用计数,然后扔了它
            this->m_ptag->m_ref -= 1;
            m_ptag = (pTag)this->fastalloc(sizeof(StringTag));
        }
        else
        {
            //当前字符串未被其它实例引用,我们处理一下它
            if (TryCopy(p, len))
            //当前缓冲区足以容纳,直接拷贝.
            {
                goto Ret;
            }
            if (m_ptag->m_ref == 1)
            {
                String::Collect(this->m_ptag->m_pstr);
            }
            //释放将未引用的字符串
        }
        m_ptag->m_len = len;
        m_ptag->m_size = TSIZE(m_ptag->m_len);
        m_ptag->m_pstr = (TCHAR *)this->fastalloc(this->m_ptag->m_size);
        m_ptag->m_ref = 1;
        m_ptag->m_check = false;
        String::FastStringCopy(m_ptag->m_pstr, p, m_ptag->m_len);
        Ret: m_ptag->m_pstr[this->m_ptag->m_len] = T('\0');
    #else 
        if (len == 0)
        {
            len = wcslen(p);
            if (len == 0)
            {
                return this->Empty();
            }
        }
        /// <summary>
        /// WCHAR to CHAR
        /// 只需要转换 len 长度即可
        /// </summary>
        LPSTR _t = String::ConvertToMultiByte(p, len);
        if (_t == nullptr)
        {
            return this->Empty();
        }
        this->Copy((LPCSTR)_t, len);
        String::Collect(_t);
    #endif 
}

//-------------------------------------------------------------------------

bool String::TryCopy(LPCTSTR p, UINT len /* = 0 */)
{
    //包含常量引用0和对象引用大于1
    if (this->m_ptag->m_ref != 1)
    {
        goto FAIL;
    }

    //假定为NULL
    if (p == nullptr)
    {
        this->Empty();
        return true;
    } //if

    bool isNullTerminate = true; //能否保证NULL结尾
    UINT size;
    if (len == 0)
    {
        len = TLEN(p);
        if (len == 0)
        {
            goto FAIL;
        }
        size = TSIZE(len);
        goto NEXT;
    }
    isNullTerminate = false;
    size = TSIZE(len); //可能多了sizeof(TCHAR)
    NEXT: if (size <= m_ptag->m_size)
    //当前缓冲区足以容纳,直接拷贝.
    {
        //此处this->m_str->m_size不能改变
        if (isNullTerminate)
        {
            String::FastStringCopy(m_ptag->m_pstr, p, len + 1);
        }
        else
        {
            size -= sizeof(TCHAR);
            Memory::memcpy(m_ptag->m_pstr, (Object)p, size);
            //NULL结尾
            #ifdef UNICODE
                *LPWSTR(LPBYTE(m_ptag->m_pstr) + size) = T('\0');
            #else 
                *LPSTR(LPBYTE(m_ptag->m_pstr) + size) = '\0';
            #endif // UNICODE
        }
        m_ptag->m_len = len;
        m_ptag->m_check = false;
        return true;
    }
    FAIL: return false;
}

//-------------------------------------------------------------------------

String &String::Append(const TCHAR *p, UINT len /* = 0 */)
{
    if (len == 0)
    {
        len = TLEN(p);
        if (len == 0)
        {
            return *this;
        }
    }
    if (TSIZE(len + this->GetLength() /*新字符串长度*/) > this->m_ptag->m_size)
    {
        prepare(len + this->GetLength() /*新字符串长度*/); //保证有足够空间追加字符串
    }
	auto copy_at_address = LPTSTR(LPBYTE(this->m_ptag->m_pstr) + TSIZE(this->Length) - sizeof(TCHAR));
    FastStringCopy(copy_at_address, p, len);
    this->m_ptag->m_check = true;

    *(copy_at_address +len) = T('\0');
	return *this;
}

//-------------------------------------------------------------------------

String &String::Append(const String &src, UINT len /* = 0 */)
{
    if (src.IsNullOrEmpty())
    {
        goto RET; //nothing to do
    }
    if (this->IsNullOrEmpty())
    {
        *this = len == 0 ? src : ((String)src).Substring(0, len);
        goto RET;
    }
    if (len == 0)
    {
        len = src.GetLength();
        if (len == 0)
        {
            goto RET;
        }
    }
    if (TSIZE(len + this->GetLength() /*新字符串长度*/) > this->m_ptag->m_size)
    {
        prepare(len + this->GetLength() /*新字符串长度*/); //保证有足够空间追加字符串
    }
    

	auto copy_at_address = LPTSTR(LPBYTE(this->m_ptag->m_pstr) + TSIZE(this->Length) - sizeof(TCHAR));
	FastStringCopy(copy_at_address, src.GetConstData(), len);
	this->m_ptag->m_check = true;

	*(copy_at_address + len) = T('\0');
    RET: return *this;
}

//-------------------------------------------------------------------------

bool __cdecl String::Format(LPCTSTR pstrFormat, ...)
{
    bool result = false;
    String *pstr = this;
    if (this->m_ptag->m_size < TSIZE(RLIB_STRINGFORMATLENGTH))
    {
        pstr = new String(RLIB_STRINGFORMATLENGTH);
        if (pstr == nullptr)
        {
            goto ret; //false
        }
    }
    //check if float used
    #ifdef RLIB_EnableFloatSupport
        float enable_float;
        enable_float = 0.0;
    #endif // RLIB_EnableFloatSupport
    va_list argList;
    va_start(argList, pstrFormat);
    #pragma warning(disable:4995) //for StringVPrintfWorker
    #ifdef UNICODE
        if (StringVPrintfWorkerW(pstr->GetData(), RLIB_STRINGFORMATLENGTH, NULL, pstrFormat, argList) == S_OK)
        {
            result = true;
        }
    #else 
        if (StringVPrintfWorkerA(pstr->GetData(), RLIB_STRINGFORMATLENGTH, NULL, pstrFormat, argList) == S_OK)
        {
            result = true;
        }
    #endif // UNICODE
    va_end(argList);
    if (pstr != this)
    {
        this->Copy(pstr->GetConstData());
        delete pstr;
    }
    ret: return result;
}

//////////////////////////////////////////////////////////////////////////
//操作符
//////////////////////////////////////////////////////////////////////////
String String::operator + (const TCHAR *p) const
{
    if (this->IsNullOrEmpty())
    {
        return p;
    }

    int length = TLEN(p);
    if (length == 0)
    {
        return  *this;
    }

    String _tstr(TSIZE(this->GetLength() + length));
    _tstr.Copy(this->GetConstData(), this->m_ptag->m_len);
    _tstr.Append(p);
    return _tstr;
}

//-------------------------------------------------------------------------

String String::operator + (const String &src) const
{
    if (this->IsNullOrEmpty())
    {
        if (src.IsNullOrEmpty())
        {
            return  *this;
        }
        return src;
    }
    String _tstr(TSIZE(this->GetLength() + src.GetLength()));
    _tstr.Copy(this->GetConstData(), this->m_ptag->m_len);
    _tstr.Append(src);
    return _tstr;
}
//-------------------------------------------------------------------------

const String &String::operator += (const TCHAR c)
{
	TCHAR cs[2] = { c, 0 };
	return this->Append(cs, 1);
}

//-------------------------------------------------------------------------

const String &String::operator += (const TCHAR *p)
{
    return this->Append(p);
}

//-------------------------------------------------------------------------

const String &String::operator += (const String &src)
{
    return this->Append(src);
}

//-------------------------------------------------------------------------

int String::Compare(LPCTSTR lpsz)const
{
	assert(!this->IsNull());
    return _tcscmp(m_ptag->m_pstr, lpsz);
}

//-------------------------------------------------------------------------

int String::CompareNoCase(LPCTSTR lpsz)const
{
    return _tcsicmp(m_ptag->m_pstr, lpsz);
}

//-------------------------------------------------------------------------

bool String::operator == (LPCTSTR str)const
{
    return (Compare(str) == 0);
}

//-------------------------------------------------------------------------

bool String::operator != (LPCTSTR str)const
{
    return (Compare(str) != 0);
}

//-------------------------------------------------------------------------

bool String::operator <= (LPCTSTR str)const
{
    return (Compare(str) <= 0);
}

//-------------------------------------------------------------------------

bool String::operator < (LPCTSTR str)const
{
    return (Compare(str) < 0);
}

//-------------------------------------------------------------------------

bool String::operator >= (LPCTSTR str)const
{
    return (Compare(str) >= 0);
}

//-------------------------------------------------------------------------

bool String::operator > (LPCTSTR str)const
{
    return (Compare(str) > 0);
}

//-------------------------------------------------------------------------

bool String::operator == (const String &str)const
{
    return this->operator == (str.GetConstData());
}

//-------------------------------------------------------------------------

bool String::operator != (const String &str)const
{
    return this->operator != (str.GetConstData());
}

//-------------------------------------------------------------------------

bool String::operator <= (const String &str)const
{
    return this->operator <= (str.GetConstData());
}

//-------------------------------------------------------------------------

bool String::operator < (const String &str)const
{
    return this->operator < (str.GetConstData());
}

//-------------------------------------------------------------------------

bool String::operator >= (const String &str)const
{
    return this->operator >= (str.GetConstData());
}

//-------------------------------------------------------------------------

bool String::operator > (const String &str)const
{
    return this->operator > (str.GetConstData());
}

//-------------------------------------------------------------------------

TCHAR &String::operator[](int nIndex)const
{
    return getTCHAR(nIndex);
}

//-------------------------------------------------------------------------

bool String::IsEmpty()const
{
    return this->GetLength() == 0;
}

//-------------------------------------------------------------------------

bool String::IsNull()const
{
    return (this->m_ptag->m_pstr == nullptr);
}

//-------------------------------------------------------------------------

bool String::IsNullReference()const
{
	RLIB_CheckThisRet(true);
	return (this->m_ptag == nullptr);
}

//-------------------------------------------------------------------------

bool String::IsNullOrEmpty()const
{
    return IsNull() || IsEmpty();
}

//-------------------------------------------------------------------------

bool String::StartsWith(TCHAR c)const
{
    return GetAt(0) == c;
}

//-------------------------------------------------------------------------

bool String::EndsWith(TCHAR c)const
{
    return GetAt(this->GetLength() - 1) == c;
}

//-------------------------------------------------------------------------

void String::Empty()
{
    this->SetAt(0, 0);
	assert(this->m_ptag->m_ref == 1);
	//Memory::memset(this->m_ptag->m_pstr, 0, this->m_ptag->m_size);
}

//-------------------------------------------------------------------------

bool String::IsConst()const
{
    if (this->IsNull() || this->m_ptag->m_ref == 0)
    {
        return true;
    }
    return false;
}

//-------------------------------------------------------------------------

TCHAR &String::getTCHAR(int index)const
{
    if (this->IsNull())
    {
        static TCHAR null_TCHAR;
        null_TCHAR = 0;
        return null_TCHAR;
    }
    if (this->m_ptag->m_ref != 1)
    {
        _this->prepare(this->GetLength());
    }
    this->m_ptag->m_check = true; //可能被修改
    return m_ptag->m_pstr[index];
}

//-------------------------------------------------------------------------

TCHAR String::GetAt(int nIndex)const
{
    if (this->IsNull())
    {
        return NULL;
    }
    return m_ptag->m_pstr[nIndex];
}

//-------------------------------------------------------------------------

void String::SetAt(int nIndex, TCHAR ch)
{
    if (this->IsNull())
    {
        this->prepare((nIndex + 1) + 1);
    }
    else if (this->m_ptag->m_ref != 1)
    {
        this->prepare((nIndex + 1) >= int(this->Length) ? (nIndex + 1): this->Length);
    }
    m_ptag->m_pstr[nIndex] = ch;
    this->m_ptag->m_check = true;
}

//////////////////////////////////////////////////////////////////////////
//拓展方法
//////////////////////////////////////////////////////////////////////////
int String::IndexOf(TCHAR c, unsigned int begin /* = 0 */)const
{
    if (this->IsNullOrEmpty())
    {
        goto EXIT;
    }
    if (begin >= this->Length)
    {
        goto EXIT;
    }
    TCHAR *p = _tcschr(this->m_ptag->m_pstr + begin, c);
    if (p == nullptr)
    {
        goto EXIT;
    }
    return (p - this->m_ptag->m_pstr);
    EXIT: return  - 1;
}

//-------------------------------------------------------------------------

int String::LastIndexOf(TCHAR c)const
{
    if (this->IsNullOrEmpty())
    {
        goto EXIT;
    }
    for (int i = this->GetLength(); i >= 0; i--)
    {
        if (this->m_ptag->m_pstr[i] == c)
        {
            return i;
        }
    }
    EXIT: return  - 1;
}

//-------------------------------------------------------------------------

INT String::IndexOf(const TCHAR *p, unsigned int begin /* = 0 */)const
{
    if (this->IsNullOrEmpty())
    {
        goto EXIT;
    }
    if (begin >= this->Length)
    {
        goto EXIT;
    }
    p = _tcsstr(this->m_ptag->m_pstr + begin, p);
    if (p == nullptr)
    {
        goto EXIT;
    }
    return (p - this->m_ptag->m_pstr);
    EXIT: return  - 1;
}

//-------------------------------------------------------------------------

INT String::LastIndexOf(const TCHAR *p)const
{
    if (this->IsNullOrEmpty())
    {
        goto EXIT;
    }
    size_t Len = TLEN(p);
    TCHAR *p2 = this->m_ptag->m_pstr - Len,  *p3 = NULL;
    while ((p2 = _tcsstr(p2 + Len, p)) != nullptr)
    {
        p3 = p2;
    }
    if (p3 == nullptr)
    {
        goto EXIT;
    }
    return (p3 - this->m_ptag->m_pstr);
    EXIT: return  - 1;
}

//-------------------------------------------------------------------------

INT String::IndexOfR(const TCHAR *p, unsigned int begin /* = 0 */)const
{
    int result = this->IndexOf(p, begin);
    if (result ==  - 1)
    {
        return  - 1;
    }
    return result + TLEN(p);
}

//-------------------------------------------------------------------------

INT String::LastIndexOfR(const TCHAR *p)const
{
    int result = this->LastIndexOf(p);
    if (result ==  - 1)
    {
        return  - 1;
    }
    return result + TLEN(p);
}

//-------------------------------------------------------------------------

#ifndef _UNICODE
__forceinline void CharReverse(char* pstr, int length)//char字符串反转, 针对中文处理
{
	char  *buffer = (char *)RLIB_GlobalAlloc((length + 1) * sizeof(char));
	for(int i = 0, j = length - 1 ; i < length; ++i, --j)
	{
		if(pstr[i] & (char)0x80U)
		{
			buffer[j-1] = pstr[i];
			buffer[j--] = pstr[++i];
		}
		else
		{
			buffer[j] = pstr[i];
		}
	}
	Memory::memcpy(pstr, buffer, length * sizeof(char));
	RLIB_GlobalCollect(buffer);
	//pstr[length] = '\0';
}
#endif // _UNICODE
//-------------------------------------------------------------------------

String String::Reverse()const
{
    String tmp;
    #ifdef UNICODE
        if (!this->IsNullOrEmpty())
        {
            tmp.Copy(this->m_ptag->m_pstr, this->GetLength());
            //翻转算法
            UINT n = tmp.GetLength();
            INT i = (n / 2) - 1; //必须用INT
            n -= 1;
            for (; i >= 0; i--)
            {
                tmp.m_ptag->m_pstr[i] = tmp.m_ptag->m_pstr[i] ^ tmp.m_ptag->m_pstr[n - i];
                tmp.m_ptag->m_pstr[n - i] = tmp.m_ptag->m_pstr[i] ^ tmp.m_ptag->m_pstr[n - i];
                tmp.m_ptag->m_pstr[i] = tmp.m_ptag->m_pstr[i] ^ tmp.m_ptag->m_pstr[n - i];
            }
        }
    #else 
		CharReverse(this->m_ptag->m_pstr, this->GetLength());
//         //将CHAR转换成WCHAR,处理中文乱码
//         LPWSTR _t = String::ConvertToWideChar(this->GetConstData());
//         //翻转算法
//         UINT n = wcslen(_t);
//         INT i = (n / 2) - 1; //必须用INT
//         n -= 1;
//         for (; i >= 0; i--)
//         {
//             _t[i] = _t[i] ^ _t[n - i];
//             _t[n - i] = _t[i] ^ _t[n - i];
//             _t[i] = _t[i] ^ _t[n - i];
//         }
//         tmp.Copy(_t);
//         String::Collect(_t);
    #endif // UNICODE
    return tmp;
}

//-------------------------------------------------------------------------

String String::ToLower()const
{
    String tmp;
    if (!this->IsNullOrEmpty())
    {
        tmp.Copy(this->m_ptag->m_pstr, this->GetLength());
        _tcslwr(tmp);
    }
    return tmp;
}

//-------------------------------------------------------------------------

String String::ToUpper()const
{
    String tmp;
    if (!this->IsNullOrEmpty())
    {
        tmp.Copy(this->m_ptag->m_pstr, this->GetLength());
        _tcsupr(tmp);
    }
    return tmp;
}

//-------------------------------------------------------------------------

String String::Concat(const TCHAR *p, UINT len /* = 0 */)const
{
    String ret_str;
    if (this->IsNullOrEmpty())
    {
        if (len != 0)
        {
            TCHAR *pstr = String::FastAllocateString(len);
            String::FastStringCopy(pstr, p, len);
            ret_str.update(pstr, TSIZE(len));
            goto ret;
        }
        ret_str = (TCHAR *)p; //默认强制拷贝??
        goto ret;
    }
    ret_str =  *this;
    ret_str.Append(p, len);
    ret: return ret_str;
}

//-------------------------------------------------------------------------

String String::Concat(const String &s, UINT len /* = 0 */)const
{
    if (this->IsNullOrEmpty())
    {
        if (len != 0)
        {
            return ((String &)s).Substring(0, len);
        }
        return s;
    }
    String ret_str =  *this;
    ret_str.Append(s, len);
    return ret_str;
}

//-------------------------------------------------------------------------

String String::Trim(TCHAR c /* = 0 */)
{
    if (this->IsNullOrEmpty())
    {
        return Nothing;
    }
    return this->TrimStart(c).TrimEnd(c);
}

//-------------------------------------------------------------------------

String String::TrimStart(TCHAR c /* = 0 */)
{
    if (this->IsNullOrEmpty())
    {
        return Nothing;
    }
	int FirstIndex = 0;
	for (UINT i = 0; i < this->Length; i++)
	{
		if ((c == 0 && !_tcspace(this->m_ptag->m_pstr[i]))
			|| (c != 0 && this->m_ptag->m_pstr[i] != c))
		{
			break;
		}
		FirstIndex++;
	}
	if (FirstIndex == 0)
	{
		return *this;
	}
	return this->Substring(FirstIndex);
}

//-------------------------------------------------------------------------

String String::TrimEnd(TCHAR c /* = 0 */)
{
	if (this->IsNullOrEmpty())
	{
		return Nothing;
	}
	int LastIndex = this->Length;
	for (int i = this->Length - 1; i >= 0; i--)
	{
		if ((c == 0 && !_tcspace(this->m_ptag->m_pstr[i]))
			|| (c != 0 && this->m_ptag->m_pstr[i] != c))
		{
			break;
		}
		LastIndex--;
	}
	if (LastIndex == (int)this->Length)
	{
		return *this;
	}
	return this->Substring(0, LastIndex);
}

//-------------------------------------------------------------------------

String String::Fill(TCHAR c)
{
	if (this->IsNullOrEmpty())
	{
		return Nothing;
	} //if
	String szFilled(this->Length);

	for(unsigned int i = 0; i < this->Length; i++)
	{
		szFilled.SetAt(i, c);
	}

	return szFilled;
}

//-------------------------------------------------------------------------

String String::Substring(UINT nIndex /*从0开始*/, UINT nLen /* = 0*/)const
{
    if (this->IsNullOrEmpty() || nIndex >= this->GetLength())
    {
        return Nothing;
    }
    if (nLen > (this->m_ptag->m_len - nIndex))
    //没有必要调用this->GetLength();
    {
        nLen = (this->m_ptag->m_len - nIndex);
    }
    else if (nLen == 0)
    {
        nLen = (this->m_ptag->m_len - nIndex);
    }
	String tmp;
    tmp.Copy(&this->m_ptag->m_pstr[nIndex], nLen);
    return tmp;
}

//-------------------------------------------------------------------------

String String::Replace(const TCHAR *pstrFrom, const TCHAR *pstrTo, UINT n /* = 0 */, UINT begin /* = 0 */, UINT *replace_count /* = nullptr */) const
{
    if (this->IsNull())
    {
        return Nothing;
    } //if
    if (this->IsEmpty() || begin >= this->Length || this->IndexOf(pstrFrom, begin) ==  - 1)
    {
        return  *this;
    } //if
    String tmp;

	//是否进行次数检查
	bool diff_check = (n == 0) ? false : true;
	//计算每次需要增加的大小
	size_t sF = TLEN(pstrFrom);
	size_t sT = TLEN(pstrTo);
	if (sF == sT)
	{
		tmp.Copy(this->GetConstData(), this->Length);

		TCHAR *sL = tmp.GetData() + begin;
		TCHAR *sP = sL - sF;
		while ((sP = _tcsstr(sP + sF, pstrFrom)) != nullptr)
		{
			String::FastStringCopy(sP, pstrTo, sT);

			if (replace_count != nullptr)
			{
				*replace_count = (*replace_count) + 1;
			} //if
			if (diff_check)
			{
				if (n == 1)
				{
					break;
				}
				n -= 1;
			}
		}
		goto work_done;
	} //if

	TCHAR *sL = (TCHAR *)this->GetConstData() + begin;
	TCHAR *sP = sL - sF;
	UINT sO = begin;
	while ((sP = _tcsstr(sP + sF, pstrFrom)) != nullptr)
	{
		if ((sP - sL) > 0)
		{
			tmp += this->Substring(sO, sP - sL).Append(pstrTo, sT);
		}
		else
		{
			tmp.Append(pstrTo, sT);
		}
		sO = (sP - this->m_ptag->m_pstr) + sF;
		sL = sP + sF;

		if (replace_count != nullptr)
		{
			*replace_count = (*replace_count) + 1;
		} //if
		if (diff_check)
		{
			if (n == 1)
			{
				break;
			}
			n -= 1;
		}
	}
	if (sO < this->Length)
	{
		tmp += this->Substring(sO);
	}

work_done:
    return begin == 0 ? tmp : this->Substring(0, begin) + tmp;
}

//-------------------------------------------------------------------------

int String::Count(const TCHAR *p1, unsigned int begin/* = 0*/) const
{
	int count = 0, len = TLEN(p1);
	while((begin = this->IndexOf(p1, begin)) != -1)
	{
		begin += len;
		++count;
	}
	return count;
}

//-------------------------------------------------------------------------

StringArray *String::MatchAll(const TCHAR *p1, const TCHAR *p2, unsigned int begin_offset /* = 0 */) const
{
	StringArray *p = new StringArray();
	if (p != nullptr)
	{
		int len = TLEN(p1);
		while((begin_offset = IndexOf(p1, begin_offset)) != -1)
		{
			String ps = Match(p1, p2, begin_offset);
			if (ps.IsNull())
			{
				break;//一次都不匹配, 返回空集合
			} //if
			p->Add(ps);
			begin_offset += len;
		}
	} //if
	return p;
}

//-------------------------------------------------------------------------

StringArray *String::MatchAllStrict(const TCHAR *p1, const TCHAR *p2, unsigned int begin_offset /* = 0 */) const
{
	StringArray *p = new StringArray();
	if (p != nullptr)
	{
		int len = TLEN(p1);
		while((begin_offset = IndexOf(p1, begin_offset)) != -1)
		{
			String ps = MatchStrict(p1, p2, begin_offset);
			if (ps.IsNull())
			{
				break;//一次都不匹配, 返回空集合
			} //if
			p->Add(ps);
			begin_offset += len;
		}
	} //if
	return p;
}

//-------------------------------------------------------------------------

String String::Match(const TCHAR *p1, const TCHAR *p2, unsigned int begin_offset /* = 0 */) const
{
    int begin = IndexOf(p1, begin_offset);
    if (begin == -1)
    {
        return Nothing;
    } //if
    String Result = this->Substring(begin + TLEN(p1));
    int end = Result.IndexOf(p2);
    if (end == -1)
    {
        return Result;
    } //if
    return Result.Substring(0, end);
}

//-------------------------------------------------------------------------

String String::MatchStrict(const TCHAR *p1, const TCHAR *p2, unsigned int begin_offset /* = 0 */) const
{
	int begin = IndexOf(p1, begin_offset);
	if (begin == -1)
	{
		return Nothing;
	} //if
	String Result = this->Substring(begin + TLEN(p1));
	int end = Result.IndexOf(p2);
	if (end == -1)
	{
		return Nothing;
	} //if
	return Result.Substring(0, end);
}

//-------------------------------------------------------------------------

String String::ToString()const
{
    return string(LPTSTR(this->GetConstData()));
}

//-------------------------------------------------------------------------

StringArray *String::Split(const TCHAR *separator)const
{
    StringArray *Result = NULL;
    if (this->IsNullOrEmpty() || TLEN(separator) == 0)
    {
        goto RET;
    }
    int LenOfSeparator = TLEN(separator);
    int LastOffset = 0;
    Result = new StringArray;
    LPTSTR FindPtr = NULL;
    LPTSTR LastFindPtr = this->m_ptag->m_pstr;
    next: if ((FindPtr = _tcsstr(LastFindPtr, separator)) == nullptr)
    {
        // 没有找到分割项
        if (LastOffset == 0)
        //一项也没有
        {
            delete Result;
            Result = new StringArray(_this, 1);
            goto RET;
        }
        // 添加最后面那项
        Result->Add(this->Substring(LastOffset));
        goto RET;
    }
    if ((FindPtr - LastFindPtr) != 0)
    {
        Result->Add(this->Substring(LastOffset, FindPtr - LastFindPtr));
    }
    else
    {
        Result->Add(T(""));
    }
    LastOffset += ((FindPtr - LastFindPtr) + LenOfSeparator);
    LastFindPtr += ((FindPtr - LastFindPtr) + LenOfSeparator);
    goto next;
RET: 
	return Result;
}

//-------------------------------------------------------------------------

LPSTR String::ConvertToMultiByte(LPCWSTR p, int length /* = 0 */, int *out_size /* = nullptr */)
{
    if (length == 0)
    {
        length = wcslen(p); // + 1
        if (length == 0)
        {
			if (out_size != nullptr)
			{
				*out_size = 0;
			} //if
            return (char*)String::fastalloc(sizeof(char));
        }
    }
    //WCHAR to CHAR
    UnmanagedMemoryStream temp((LPVOID)p, length *sizeof(WCHAR), length *sizeof(WCHAR));
    MemoryStream *_pstream = Encoder::ToMultiByte(ASCIIEncoding, &temp);
    if (_pstream == nullptr)
    {
        assert(!"String::ConvertToMultiByte方法发生异常: 无法进行编码转换");
		if (out_size != nullptr)
		{
			*out_size = 0;
		} //if
		return NULL;
    }
    LPSTR _tstr = (CHAR*)String::fastalloc(_pstream->GetLength() + sizeof(TCHAR));
    if (_tstr)
    {
        _pstream->Position = 0;
        _pstream->Read(_tstr, _pstream->GetLength());
    }
	if (out_size != nullptr)
	{
		*out_size = _pstream->Length;
	} //if
    delete _pstream;
    return _tstr;
}

//-------------------------------------------------------------------------

LPWSTR String::ConvertToWideChar(LPCSTR p, int length /* = 0 */, int *out_size /* = nullptr */)
{
    //CHAR to WCHAR
    if (length == 0)
    {
        length = strlen(p); // + 1
        if (length == 0)
        {
			if (out_size != nullptr)
			{
				*out_size = 0;
			} //if
            return (WCHAR*)String::fastalloc(sizeof(WCHAR));
        }
    }
    UnmanagedMemoryStream _tstream((Object)p, length, length);
    MemoryStream *_pstream = Encoder::ToWideChar(ASCIIEncoding, &_tstream);
    if (_pstream == nullptr)
    {
        assert(!"String::ConvertToWideChar方法发生异常: 无法进行编码转换");
		if (out_size != nullptr)
		{
			*out_size = 0;
		} //if
		return NULL;
    }
    LPWSTR _tstr = (LPWSTR)String::fastalloc(_pstream->Length + sizeof(TCHAR));
    if (_tstr)
    {
        Memory::memcpy(_tstr, _pstream->GetObjectData(), _pstream->Length);
    }
	if (out_size != nullptr)
	{
		*out_size = _pstream->Length;
	} //if
    delete _pstream;
    return _tstr;
}

//-------------------------------------------------------------------------

void String::FastStringCopy(TCHAR *dmem, const TCHAR *smem, int charCount)
{
    assert(charCount >= 0);
    //Ptr Align
    if ((((int)dmem) &2) != 0)
    {
        dmem[0] = smem[0];
        dmem++;
        smem++;
        charCount--;
    }
    while (charCount >= 8)
    {
        *((int*)dmem) = *((unsigned int*)smem);
        #ifdef _UNICODE
            *((int*)(dmem + 2)) = *((unsigned int*)(smem + 2));
            *((int*)(dmem + 4)) = *((unsigned int*)(smem + 4));
            *((int*)(dmem + 6)) = *((unsigned int*)(smem + 6));
        #else 
            *((int*)(dmem + 4)) = *((unsigned int*)(smem + 4));
        #endif // _UNICODE
        dmem += 8;
        smem += 8;
        charCount -= 8;
    }
    if ((charCount &4) != 0)
    {
        *((int*)dmem) = *((unsigned int*)smem);
        #ifdef _UNICODE
            *((int*)(dmem + 2)) = *((unsigned int*)(smem + 2));
        #endif // _UNICODE
        dmem += 4;
        smem += 4;
    }
    if ((charCount &2) != 0)
    {
        #ifdef _UNICODE
            *((int*)dmem) = *((unsigned int*)smem);
        #else 
            *((short*)dmem) = *((unsigned short*)smem);
        #endif // _UNICODE
        dmem += 2;
        smem += 2;
    }
    if ((charCount &1) != 0)
    {
        dmem[0] = smem[0];
    }
}

//////////////////////////////////////////////////////////////////////////
String StringArray::Join(const String separator)
{
    String str;
    if (this->Length == 0)
    {
        goto RET;
    }
    /*null*/
    if (separator.IsNullOrEmpty())
    {
        goto NOTSEP;
    }
    /*没有分隔符*/

    //将 String 的字符串指针取出并计算总长度
    LPCTSTR *ptrList = (LPCTSTR*)str.fastalloc(this->Length *sizeof(LPCTSTR));
    INT length = 0, length_separator = separator.GetLength();
    for (int i = 0; i < this->Length; i++)
    {
        ptrList[i] = this->m_pItems[i].GetConstData(); //存储字符串指针
        length += this->m_pItems[i].GetLength(); //字符串长度
        length += length_separator; //分隔符长度
    }
    length -= separator.GetLength() /*末尾不需要分隔符*/;

    //直接为 String 分配内存
    str.m_ptag->m_pstr = (LPTSTR)str.fastalloc( /*计算该长度字符串所需内存大小*/TSIZE(length));
    str.update(str.m_ptag->m_pstr,  /*计算该长度字符串所需内存大小*/TSIZE(length)) /*更新 String 状态*/;
    str.m_ptag->m_len = length;

    LPTSTR pDest = str.m_ptag->m_pstr /*循序体使用*/;
    //拷贝字符串到 String 中
    for (int k = 0; k < this->Length; k++)
    {
        String::FastStringCopy(pDest, ptrList[k], this->m_pItems[k].m_ptag->m_len /*GetLength() has been called*/);
        pDest += this->m_pItems[k].m_ptag->m_len /*GetLength() has been called*/;
        //判断是否需要拷贝分隔符
        if (k != this->Length - 1)
        {
            String::FastStringCopy(pDest, const_cast < String & > (separator).GetConstData(), length_separator);
            pDest += length_separator;
        }
    }
    String::Collect(ptrList);
    RET: return str;
    NOTSEP:  //没有分隔符
    for (int x = 0; x < this->Length; x++)
    {
        str.Append(this->m_pItems[x].GetConstData());
    }
    goto RET;
}
