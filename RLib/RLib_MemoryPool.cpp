/********************************************************************
Created:	2012/04/22  8:46
Filename: 	RLib_MemoryPool.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_MemoryPool.h"
#include "native/RLib_Native.h"
using namespace System::IO;
#define UserToPALLOCINFO(p)  (PMemBlockHeader(LPBYTE(p) - sizeof(MemBlockHeader)))
//////////////////////////////////////////////////////////////////////////
MemoryPool::MemoryPool(int memory_count /* = DEFAULT_MEMORYCOUNT */)
{
    //通过PEB获取进程堆
    PEB *pPEB;
    __asm
    {
        mov eax, FS: [0x30] 
		mov pPEB, eax
    }
    this->MemoryList  = (PMEMPAGE)RtlAllocateHeap(pPEB->ProcessHeap, HEAP_ZERO_MEMORY,
		sizeof(MEMORY_PAGE) *memory_count);
    this->MemoryCount = memory_count;

    for (int index = 0; index < this->MemoryCount; index++)
    {
        RLIB_InitClass(&this->MemoryList[index], MEMORY_PAGE);
#ifdef _DEBUG
		this->MemoryList[index].pNext = (index + 1) == this->MemoryCount ? nullptr : 
			&this->MemoryList[index + 1];
#endif // _DEBUG
    }
}

//-------------------------------------------------------------------------

MemoryPool::~MemoryPool()
{
    for (int index = 0; index < this->MemoryCount; index++)
    {
        this->MemoryList[index]. ~MEMORY_PAGE();
    }
    //通过PEB获取进程堆
    PEB *pPEB;
    __asm
    {
        mov eax, FS: [0x30]
		mov pPEB, eax
    }
    RtlFreeHeap(pPEB->ProcessHeap, NULL, this->MemoryList);
}

//-------------------------------------------------------------------------
#ifdef _RUNTIME_DEBUG
void *MemoryPool::AllocByte(LONG bytes, LPCTSTR function, LPCTSTR file)
{
#else
void *MemoryPool::AllocByte(LONG bytes)
{
#endif // _RUNTIME_DEBUG
    bytes += sizeof(MemBlockHeader);

    PMemBlockHeader ptr = nullptr;
    int index;

continue_alloc: 
	for (index = 0; index < this->MemoryCount; index++)
    {
        if (this->MemoryList[index].SyncRoot.TryEnter())
        {
            if (bytes <= this->MemoryList[index].Page.GetMaxAllocSize())
            {
#ifdef _RUNTIME_DEBUG
				ptr = (PMemBlockHeader)this->MemoryList[index].Page.AllocByte(bytes, function, file);
#else
				ptr = (PMemBlockHeader)this->MemoryList[index].Page.AllocByte(bytes);
#endif // _RUNTIME_DEBUG
            }
            this->MemoryList[index].SyncRoot.Leave();
            if (ptr == nullptr)
            {
                continue;
            }

            break;
        }
    }

    if (ptr == nullptr)
    {
        goto continue_alloc;
    }
    ptr->index = index;
#ifdef _RUNTIME_DEBUG
	assert(sizeof(ptr->ptr_valid) == 4);
	((char *)&ptr->ptr_valid)[0] = char(RLIB_VALIDBYTE);
	((char *)&ptr->ptr_valid)[1] = char(RLIB_VALIDBYTE);
	((char *)&ptr->ptr_valid)[2] = char(RLIB_VALIDBYTE);
	((char *)&ptr->ptr_valid)[3] = char(RLIB_VALIDBYTE);
#endif // _RUNTIME_DEBUG
	return ptr->ptr();
}

//-------------------------------------------------------------------------

void MemoryPool::Collect(void *ptr)
{
	if (ptr == nullptr)
	{
		trace(!"指针异常, 释放nullptr是安全的, 但仍会引发警告");
		return ;
	} //if

    ptr = UserToPALLOCINFO(ptr);

#ifdef _RUNTIME_DEBUG
	auto handle = PMemBlockHeader(ptr);
	assert(sizeof(handle->ptr_valid) == 4);
	bool bcheck = (((char *)&handle->ptr_valid)[0] == char(RLIB_VALIDBYTE) 
		&& ((char *)&handle->ptr_valid)[1] == char(RLIB_VALIDBYTE)
		&& ((char *)&handle->ptr_valid)[2] == char(RLIB_VALIDBYTE)
		&& ((char *)&handle->ptr_valid)[3] == char(RLIB_VALIDBYTE));
	trace(bcheck || !"请检查内存溢出, 点击忽略查看所在方法");
	if(!bcheck) debug_warning(UserToHandle(handle)->alloc_function);
#endif // _RUNTIME_DEBUG

    int index = PMemBlockHeader(ptr)->index;

    if (index < 0 || index >= this->MemoryCount)
    {
        assert(!"指针异常, 很可能是溢出了");
        return ;
    } //if

    this->MemoryList[index].SyncRoot.Enter();
    this->MemoryList[index].Page.Collect(ptr);
    this->MemoryList[index].SyncRoot.Leave();
}

//-------------------------------------------------------------------------

void *MemoryPool::TryReAlloc(void *ptr, ULONG bytes)
{
    if (ptr == nullptr)
    {
#ifdef _RUNTIME_DEBUG
		return MemoryPool::AllocByte(bytes, _T("MemoryPool::TryReAlloc 1"), RLIB_FILE);
#else
		return MemoryPool::AllocByte(bytes);
#endif // _RUNTIME_DEBUG
    } //if

	ptr = UserToPALLOCINFO(ptr);

#ifdef _RUNTIME_DEBUG
	auto handle = PMemBlockHeader(ptr);
	assert(sizeof(handle->ptr_valid) == 4);
	bool bcheck = (((char *)&handle->ptr_valid)[0] == char(RLIB_VALIDBYTE) 
		&& ((char *)&handle->ptr_valid)[1] == char(RLIB_VALIDBYTE)
		&& ((char *)&handle->ptr_valid)[2] == char(RLIB_VALIDBYTE)
		&& ((char *)&handle->ptr_valid)[3] == char(RLIB_VALIDBYTE));
	trace(bcheck || !"请检查内存溢出, 点击忽略查看所在方法");
	if(!bcheck) debug_warning(UserToHandle(handle)->alloc_function);
#endif // _RUNTIME_DEBUG

    int index = PMemBlockHeader(ptr)->index;

    if (index < 0 || index >= this->MemoryCount)
    {
        assert(!"重新分配指针异常, 很可能是溢出了");
        return nullptr;
    } //if

    bytes += sizeof(MemBlockHeader);

	LPVOID ptr_temp = nullptr;
	
    this->MemoryList[index].SyncRoot.Enter();

	int ptr_size = this->MemoryList[index].Page.GetSize(ptr);
#ifdef _DEBUG
	assert(ptr_size >= 0 && ptr_size <= int(this->MemoryList[index].Page.GetMemorySize()));
#endif // _DEBUG

    if (this->MemoryList[index].Page.ReSize(ptr, bytes))
    {
        ptr_temp = PMemBlockHeader(ptr)->ptr();
		goto check_success;
    }

	if (((ptr_temp = this->MemoryList[index].Page.ReAlloc(ptr, bytes)) != nullptr))
	{
		ptr_temp = PMemBlockHeader(ptr_temp)->ptr();
	}

check_success:
	this->MemoryList[index].SyncRoot.Leave();
	if (ptr_temp != nullptr)
	{
		return ptr_temp;
	}

	//如果在同一内存页进行重新分配失败, 只能分配在其他内存页
	bytes -= sizeof(MemBlockHeader);

#ifdef _RUNTIME_DEBUG
	ptr_temp = MemoryPool::AllocByte(bytes, _T("MemoryPool::TryReAlloc 2"), RLIB_FILE);
#else
	ptr_temp = MemoryPool::AllocByte(bytes);
#endif // _RUNTIME_DEBUG

	if (ptr_temp != nullptr)
	{
		if ((ptr_size - sizeof(MemBlockHeader)) < bytes)
		{
			bytes = (ptr_size - sizeof(MemBlockHeader));
		} //if
		Memory::memcpy(ptr_temp, PMemBlockHeader(ptr)->ptr(), bytes);

		this->MemoryList[index].SyncRoot.Enter();

		this->MemoryList[index].Page.Collect(ptr);

		this->MemoryList[index].SyncRoot.Leave();
	} //if

    return ptr_temp;
}

//-------------------------------------------------------------------------

void *MemoryPool::ReAlloc(void *ptr, ULONG bytes)
{
	if (ptr == nullptr)
	{
#ifdef _RUNTIME_DEBUG
		return MemoryPool::AllocByte(bytes, _T("MemoryPool::ReAlloc 1"), RLIB_FILE);
#else
		return MemoryPool::AllocByte(bytes);
#endif // _RUNTIME_DEBUG
	} //if

	ptr = UserToPALLOCINFO(ptr);

#ifdef _RUNTIME_DEBUG
	auto handle = PMemBlockHeader(ptr);
	assert(sizeof(handle->ptr_valid) == 4);
	bool bcheck = (((char *)&handle->ptr_valid)[0] == char(RLIB_VALIDBYTE) 
		&& ((char *)&handle->ptr_valid)[1] == char(RLIB_VALIDBYTE)
		&& ((char *)&handle->ptr_valid)[2] == char(RLIB_VALIDBYTE)
		&& ((char *)&handle->ptr_valid)[3] == char(RLIB_VALIDBYTE));
	trace(bcheck || !"请检查内存溢出, 点击忽略查看所在方法");
	if(!bcheck) debug_warning(UserToHandle(handle)->alloc_function);
#endif // _RUNTIME_DEBUG

	int index = PMemBlockHeader(ptr)->index;

	if (index < 0 || index >= this->MemoryCount)
	{
		assert(!"重新分配指针异常, 很可能是溢出了");
		return nullptr;
	} //if

#ifdef _RUNTIME_DEBUG
	LPVOID ptr_temp = MemoryPool::AllocByte(bytes, _T("MemoryPool::ReAlloc 2"), RLIB_FILE);
#else
	LPVOID ptr_temp = MemoryPool::AllocByte(bytes);
#endif // _RUNTIME_DEBUG
	if (ptr_temp != nullptr)
	{
		this->MemoryList[index].SyncRoot.Enter();

		int ptr_size = this->MemoryList[index].Page.GetSize(ptr);
		if ((ptr_size - sizeof(MemBlockHeader)) < bytes)
		{
			bytes = (ptr_size - sizeof(MemBlockHeader));
		} //if
		Memory::memcpy(ptr_temp, PMemBlockHeader(ptr)->ptr(), bytes);

		this->MemoryList[index].Page.Collect(ptr);

		this->MemoryList[index].SyncRoot.Leave();
	} //if

	return ptr_temp;
}

//-------------------------------------------------------------------------

bool MemoryPool::ReSize(Object ptr, LONG bytes)
{
    #ifdef _DEBUG
        if (ptr == nullptr)
        {
            assert(!"指针异常: nullptr");
            return false;
        } //if
    #endif // _DEBUG

	ptr = UserToPALLOCINFO(ptr);

#ifdef _RUNTIME_DEBUG
		auto handle = PMemBlockHeader(ptr);
		assert(sizeof(handle->ptr_valid) == 4);
		bool bcheck = (((char *)&handle->ptr_valid)[0] == char(RLIB_VALIDBYTE) 
			&& ((char *)&handle->ptr_valid)[1] == char(RLIB_VALIDBYTE)
			&& ((char *)&handle->ptr_valid)[2] == char(RLIB_VALIDBYTE)
			&& ((char *)&handle->ptr_valid)[3] == char(RLIB_VALIDBYTE));
		trace(bcheck || !"请检查内存溢出, 点击忽略查看所在方法");
		if(!bcheck) debug_warning(UserToHandle(handle)->alloc_function);
#endif // _RUNTIME_DEBUG

    int index = PMemBlockHeader(ptr)->index;

    if (index < 0 || index >= this->MemoryCount)
    {
        assert(!"指针异常, 很可能是溢出了");
        return false;
    } //if

    bytes += sizeof(MemBlockHeader);

    this->MemoryList[index].SyncRoot.Enter();

    bool result = this->MemoryList[index].Page.ReSize(PMemBlockHeader(ptr), bytes);

    this->MemoryList[index].SyncRoot.Leave();

    return result;
}

//-------------------------------------------------------------------------

LONG MemoryPool::GetSize(Object ptr)
{
    #ifdef _DEBUG
        if (ptr == nullptr)
        {
            assert(!"指针异常: nullptr");
            return 0;
        } //if
    #endif // _DEBUG

		ptr = UserToPALLOCINFO(ptr);

#ifdef _RUNTIME_DEBUG
		auto handle = PMemBlockHeader(ptr);
		assert(sizeof(handle->ptr_valid) == 4);
		bool bcheck = (((char *)&handle->ptr_valid)[0] == char(RLIB_VALIDBYTE) 
			&& ((char *)&handle->ptr_valid)[1] == char(RLIB_VALIDBYTE)
			&& ((char *)&handle->ptr_valid)[2] == char(RLIB_VALIDBYTE)
			&& ((char *)&handle->ptr_valid)[3] == char(RLIB_VALIDBYTE));
		trace(bcheck || !"请检查内存溢出, 点击忽略查看所在方法");
		if(!bcheck) debug_warning(UserToHandle(handle)->alloc_function);
#endif // _RUNTIME_DEBUG

    int index = PMemBlockHeader(ptr)->index;

    if (index < 0 || index >= this->MemoryCount)
    {
        assert(!"指针异常, 很可能是溢出了");
        return 0;
    } //if

    this->MemoryList[index].SyncRoot.Enter();

    LONG result = this->MemoryList[index].Page.GetSize(PMemBlockHeader(ptr));

    this->MemoryList[index].SyncRoot.Leave();

    return result - sizeof(MemBlockHeader);
}

//-------------------------------------------------------------------------

LONG MemoryPool::TryGCCollect()
{
    LONG bytesCollected = 0;
    for (int i = 0; i < this->MemoryCount; i++)
    {
        if (this->MemoryList[i].SyncRoot.TryEnter())
        {
            bytesCollected += this->MemoryList[i].Page.TryClean();
            this->MemoryList[i].SyncRoot.Leave();
        }
    }
    return bytesCollected;
}

//-------------------------------------------------------------------------

LONG MemoryPool::WaitForFullGCComplete()
{
    LONG bytesCollected = 0;
    for (int i = 0; i < this->MemoryCount; i++)
    {
        this->MemoryList[i].SyncRoot.Enter();
        {
            bytesCollected += this->MemoryList[i].Page.TryClean();
        }
        this->MemoryList[i].SyncRoot.Leave();
    }
    return bytesCollected;
}

/************************************************************************/
/* AppBase分部定义                                                       */
/************************************************************************/
static MemoryPool global_BasePool;

#ifdef _RUNTIME_DEBUG
LPVOID System::AppBase::Allocate(ULONG bytes, LPCTSTR function, LPCTSTR file)
{
	return global_BasePool.AllocByte(bytes, function, file);
}
#else
LPVOID System::AppBase::Allocate(ULONG bytes)
{
	return global_BasePool.AllocByte(bytes);
}
#endif // _RUNTIME_DEBUG

//-------------------------------------------------------------------------

void System::AppBase::Collect(LPVOID p)
{
    global_BasePool.Collect(p);
}

//-------------------------------------------------------------------------

MemoryPool *System::AppBase::GetUsingPool()
{
    return  &global_BasePool;
}
