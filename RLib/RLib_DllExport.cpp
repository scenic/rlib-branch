/********************************************************************
	Created:	2012/08/04  17:45
	Filename: 	RLib_DllExport.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "RLib_Base.h"
/************************************************************************
 * 
 */
typedef void* (*rlib_alloc_func)  (ULONG);
typedef void  (*rlib_collect_func)(void*);
/*********************************************************************
 *		__unDNameEx (MSVCRT.@)
 *
 * Demangle a C++ identifier.
 *
 * PARAMS
 *  buffer   [O] If not NULL, the place to put the demangled string
 *  mangled  [I] Mangled name of the function
 *  buflen   [I] Length of buffer
 *  memget   [I] Function to allocate memory with
 *  memfree  [I] Function to free memory with
 *  unknown  [?] Unknown, possibly a call back
 *  flags    [I] Flags determining demangled format
 *
 * RETURNS
 *  Success: A string pointing to the unmangled name, allocated with memget.
 *  Failure: NULL.
 */
extern "C" char* CDECL __unDNameEx(char* buffer, const char* mangled, 
	int buflen,rlib_alloc_func memget, rlib_collect_func memfree,
	void* unknown = 0, unsigned short int flags = 0);

//-------------------------------------------------------------------------
#ifdef _RUNTIME_DEBUG
static void *alloc(ULONG bytes)
{
	return RLIB_GlobalAlloc(bytes);
}
#endif // _RUNTIME_DEBUG
//-------------------------------------------------------------------------
char *System::DLLExport::_unDNameEx(char* buffer, const char* mangled, int buflen)
{
#ifdef _RUNTIME_DEBUG
	return __unDNameEx(buffer, mangled, buflen, alloc, AppBase::Collect);
#else
	return __unDNameEx(buffer, mangled, buflen, AppBase::Allocate, AppBase::Collect);
#endif // _RUNTIME_DEBUG
}
		;