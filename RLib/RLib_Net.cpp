/********************************************************************
	Created:	2012/08/03  19:36
	Filename: 	RLib_Net.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "RLib_Net.h"
#include <ctype.h>
#include <Shlwapi.h>

#pragma comment(lib,"Ws2_32.lib")
#pragma warning(disable:4996)

#define MAX_PATH_LONG 4 * 4 * 4 * 4 * 4
//////////////////////////////////////////////////////////////////////////
using namespace System::Net;
/************************************************************************/
/* Uri                                                                
/************************************************************************/
Uri::Uri(String url)
{
    // 	PARSEDURL pu;
    // 	pu.cbSize = sizeof(pu);
    // 	HRESULT hr = ParseURL(url.GetConstData(), &pu);//URL_SCHEME_HTTP
    // 	if (FAILED(hr))
    // 	{
    // 	}
    // 	return;
    this->OriginalString = url;

    if (url.IsNullOrEmpty())
    {
        goto EXIT;
    }
    int Offset = url.IndexOf(_T("://")); //解析Scheme
    if (Offset ==  - 1)
    //不符合
    {
        goto EXIT;
    }
    this->Scheme = _T("HTTP"); //目前仅支持http协议
    url = url.Substring(Offset + 3).Replace(_T("//"), _T("/"));
    Offset = url.IndexOf(_T(":")); //解析host
    if (Offset ==  - 1 || (url.IndexOf(_T("/")) !=  - 1 && Offset > url.IndexOf(_T("/"))))
    //没有指定端口
    {
        this->Port = 80; //使用默认端口
        Offset = url.IndexOf(_T("/"));
        if (Offset ==  - 1)
        //没有指定路径,连末尾的斜杠也没有
        {
            this->Host = url.ToLower(); //将主机名转换为小写
            this->PathAndQuery = _T("/");
            goto EXIT; //解析完成 http:// hostname
        }
        else
        {
            this->Host = url.Substring(0, Offset).ToLower();
            this->PathAndQuery = url.Substring(Offset);
            goto EXIT; //解析完成 http:// hostname /file?query=...
        }
    }
    else
    {
        this->Host = url.Substring(0, Offset).ToLower();
        url = url.Substring(Offset + 1); //不要":"
        Offset = url.IndexOf(_T("/"));
        {
            if (Offset ==  - 1)//没有末尾的斜杠
            {
                this->Port = static_cast < USHORT > (Int32::TryParse(url));
                this->PathAndQuery = _T("/");
                //解析完成 http:// hostname:port
            }
            else
            {
				String turl_str = url.Substring(0, Offset);
                this->Port = static_cast < USHORT > (Int32::TryParse(turl_str));
                this->PathAndQuery = url.Substring(Offset);
                //解析完成 http:// hostname:port /file?query=...
            }
        }
    }
    EXIT: return ;
}

//-------------------------------------------------------------------------
string Uri::UrlEncode(String str, Text::Encoding codepage /* = Text::UTF8Encoding */)
{  
	assert(codepage == Text::UTF8Encoding);

	//-------------------------------------------------------------------------

	IO::UnmanagedMemoryStream ori_data((LPVOID)str.GetConstData(), str.GetCanReadSize(), str.GetCanReadSize());
#ifdef _UNICODE
	auto convert = Text::Encoder::ToMultiByte(Text::UTF8Encoding, &ori_data);
#else
	auto prepare_convert = Text::Encoder::ToWideChar(Text::UTF8Encoding, &ori_data);
	assert(prepare_convert != nullptr);
	auto convert = Text::Encoder::ToMultiByte(Text::UTF8Encoding, prepare_convert);
	delete prepare_convert;
#endif // _UNICODE
	if(convert == nullptr) return str;

	//-------------------------------------------------------------------------
	static char hex[] = "0123456789ABCDEF";  
	string dst;  

	for (int i = 0; i < convert->Length; i++)  
	{  
		auto ch = (unsigned char)((char *)convert->ObjectData)[i];  
		if (isalnum(ch))  
		{  
			dst += ch;  
		}  
		else  
			if (((char *)convert->ObjectData)[i] == ' ')
			{  
				dst += T('+');  
			}  
			else  
			{  
				dst += T('%');  
				auto c = static_cast<unsigned char>(((char *)convert->ObjectData)[i]);  
				char cs[4] = { hex[c / 16], hex[c % 16], 0};
				auto p = string::ConvertToWideChar(cs, 2);
				dst += p;
				string::Collect(p);
			}  
	}  
	delete convert;
	return dst;  
}

//-------------------------------------------------------------------------

String Uri::UrlDecode(String str, Text::Encoding codepage/* = Text::UTF8Encoding*/)
{
	assert(codepage == Text::UTF8Encoding);

	String dst;
	int first_encode = str.IndexOf(T("%"));
	if (first_encode == -1)
	{
		return str;
	}
	else if (first_encode != 0)
	{
		dst += str.Substring(0, first_encode);
		str  = str.Substring(first_encode);
	}

	char buf[MAX_PATH_LONG] = { 0 };
	char *pbuf              = buf;

	int   src_size = str.GetMultiByteSize();
	char *src      = str.ToMultiByte();

	src_size = src_size > MAX_PATH_LONG ? MAX_PATH_LONG : src_size;

	for (int i = 0; i < src_size; i++, ++pbuf)
	{
		if (src[i] == '%')
		{
			if(isxdigit(src[i + 1]) && isxdigit(src[i + 2]))
			{
				char c1 = src[++i];
				char c2 = src[++i];
				c1 = c1 - 48 - ((c1 >= 'A') ? 7 : 0) - ((c1 >= 'a') ? 32 : 0);
				c2 = c2 - 48 - ((c2 >= 'A') ? 7 : 0) - ((c2 >= 'a') ? 32 : 0);
				pbuf[0] = (unsigned char)(c1 * 16 + c2);
			}
		}
		else
		{
			if (src[i] == '+')
			{
				pbuf[0] = ' ';
			}
			else
			{
				pbuf[0] = src[i];
			}
		}
	}

	IO::UnmanagedMemoryStream ori_data(buf, pbuf - buf, pbuf - buf);

	auto pdata = Text::Encoder::ToCurrentEncoding(Text::UTF8Encoding, &ori_data);
	if (pdata != nullptr)
	{
		dst += LPTSTR(pdata->ObjectData);
		delete pdata;
	}

	return dst;
}
//-------------------------------------------------------------------------

String Uri::ProcessUri(String path, Uri *father)
{
    if (path.ToLower().IndexOf(T("http://")) == 0)
    {
        return path;
    }
    if (path.StartsWith(T('/')))
    {
        return (String("http://") + (father->Port != 80 ? ((father->Host + T(":")) + ToUInt32(father->Port).ToString()): father->Host) + path);
    }
    String beginUrl = (String("http://") + (father->Port != 80 ? ((father->Host + T(":")) + ToUInt32(father->Port).ToString()): father->Host));
    return beginUrl + father->PathAndQuery.Substring(0, father->PathAndQuery.LastIndexOf(T('/')) + 1) + path;
}

/************************************************************************/
/* Proxy                                                              
/************************************************************************/
Proxy::Proxy(String proxystr)
{
    if (proxystr.IsNullOrEmpty())
    {
        goto EXIT;
    }
    int Offset = proxystr.IndexOf(_T(":"));
    if (Offset ==  - 1)
    {
        this->Host = proxystr;
        this->Port = 80; //默认端口为80;
        goto EXIT;
    }
    this->Host = proxystr.Substring(0, Offset);
    this->Port = (USHORT)UInt32::TryParse(proxystr.Substring(Offset + 1));
    EXIT: return ;
}

/************************************************************************/
/* WebHeaderCollection                                                               
/************************************************************************/
WebHeaderCollection::WebHeaderCollection(): Count(0)
{
    this->m_headers = new System::IO::BufferedStream();
};
void WebHeaderCollection::Add(LPCSTR name, LPCSTR val)
{
    if (this->m_headers->Length != 0)
    {
        this->m_headers->Write("\r\n", 2);
    }
    this->m_headers->Write(name, strlen(name));
    this->m_headers->Write(": ", 2);
    this->m_headers->Write(val, strlen(val));
    this->Count += 1;
}

//-------------------------------------------------------------------------

String WebHeaderCollection::Get(LPCSTR name)
{
    //保证NULL结尾
    this->m_headers->Write("\0", 1);
    //NULL结尾不计
    this->m_headers->Position -= 1;
    ((Stream*)this->m_headers)->Length -= 1;

    char *pstr = (char*)this->m_headers->ObjectData;
    int len_name = strlen(name);
    while ((pstr = strstr(pstr, name)) != nullptr)
    {
        pstr += len_name;
        if (*pstr == ':')
        {
            pstr += 2;
            String ret;
            ret.Copy(pstr, strstr(pstr, "\r\n") != nullptr ? (strstr(pstr, "\r\n") - pstr): strlen(pstr));
            return ret;
        }
        pstr = strstr(pstr, ": "); //防止XXzXX: ZZ导致获取错误
        if (pstr != nullptr)
        {
            pstr += 2;
        }
    }
    return String();
}

//-------------------------------------------------------------------------

void WebHeaderCollection::Clear()
{
    RLIB_Delete(this->m_headers);
    this->m_headers = new BufferedStream();
    this->Count = 0;
}

//-------------------------------------------------------------------------

char *WebHeaderCollection::ToByteArray()
{
    //保证NULL结尾
    this->m_headers->Write("\0", 1);
    //NULL结尾不计
    this->m_headers->Position -= 1;
    ((Stream*)this->m_headers)->Length -= 1;

    return (char*)this->m_headers->GetObjectData();
}

//-------------------------------------------------------------------------

void WebHeaderCollection::WriteByteArray(char *pByteArray, int size)
{
    this->m_headers->Write(pByteArray, size);
}
