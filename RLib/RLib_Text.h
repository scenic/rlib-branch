/********************************************************************
	Created:	2012/02/05  22:43
	Filename: 	Lib_Text.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#ifndef _USE_TEXT
#define _USE_TEXT
#include "RLib_Compress.h"
//////////////////////////////////////////////////////////////////////////
namespace System
{
	/// <summary>
	/// 包含用于字符编码和字符串操作的类型
	/// </summary>
	namespace Text
	{
		/// <summary>
		/// 表示编码格式
		/// </summary>
		enum Encoding
		{
			/// <summary>
			/// 未知编码
			/// </summary>
			UnknownEncoding = 0,
			/// <summary>
			/// 表示GB2312编码
			/// </summary>
			ASCIIEncoding = 936,
			/// <summary>
			/// 表示UTF8编码
			/// </summary>
			UTF8Encoding = 65001,
			/// <summary>
			/// 表示UTF16编码
			/// Unicode UTF-16, little endian byte order (BMP of ISO 10646)
			/// available only to managed applications
			/// </summary>
			UTF16Encoding = 1200,
			/// <summary>
			/// 表示UTF16(big endian)编码
			/// Unicode UTF-16, big endian byte order; available only to managed applications
			/// </summary>
			UTF16FEncoding = 1201,
		};
		/// <summary>
		/// 提供一组字符编码的转换方法
		/// For more informations, visit http://msdn.microsoft.com/en-us/library/dd317756(v=vs.85).aspx
		/// </summary>
		class export RLIB_THREAD_SAFE Encoder
		{
		public:
			/// <summary>
			/// 在头部查找字节顺序标记并返回编码方式
			/// @warnin 确保传入的缓冲区具有BOM头部(至少有3个可读字节)
			/// </summary>
			static Encoding DetectEncodingFromByteOrderMarks(const void *ptr, OUT int *bytes = nullptr);
			/// <summary>
			/// 将指定编码方式的数据流转换为当前编码格式(UTF-16(_UNICODE编译选项) or GB2312)
			/// </summary>
			static IO::MemoryStream *ToCurrentEncoding(Encoding codepage, IO::Stream *stream, int length = 0, bool detectEncodingFromByteOrderMarks = true);
			/// <summary>
			/// 将指定编码方式的多字节数据流转换为宽字符数据流(any to utf-16)
			/// </summary>
			/// <returns>返回的数据流必须由用户释放(delete), 且默认Pos = 0</returns>
			static IO::MemoryStream *ToWideChar(Encoding codepage, const IO::Stream *stream, int length = 0);
			/// <summary>
			/// 将宽字符数据流转换为指定编码方式的多字节数据流(utf-16 to any)
			/// </summary>
			/// <returns>返回的数据流必须由用户释放(delete), 且默认Pos = 0</returns>
			static IO::MemoryStream *ToMultiByte(Encoding codepage, const IO::Stream *stream, int length = 0);
			/// <summary>
			/// 根据指定编码写入指定流内容, 并指定是否启用BOM
			/// @warning stream采用的编码方式必须是UTF16(_UNICODE编译选项) or GB2312
			/// </summary>
			static bool WriteTextStream(OUT IO::Stream *output, IN const IO::Stream *stream,
				int length = 0, bool useBOM = true, Text::Encoding codepage = Text::UnknownEncoding);
		};
	};
};
#endif