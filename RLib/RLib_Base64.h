/********************************************************************
	Created:	2012/06/06  21:51
	Filename: 	RLib_Base64.h
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/

#ifndef _USE_BASE64
#define _USE_BASE64

#include "RLib_Detour.h"

namespace System
{
	namespace Security
	{
		namespace Cryptography
		{
			class export RLIB_THREAD_SAFE Base64:public CryptographyBase
			{
			private:
				/// <summary>
				/// DataByte
				///     [in]输入的数据长度, 以字节为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				static char  *encode_data(const char* Data, int DataByte, int *OutByte = nullptr);
				/// <summary>
				/// 输出UTF-16(Unicode)编码的数据以便更好的在Unicode环境中处理
				/// DataByte
				///     [in]输入的数据长度, 以字节为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				static wchar_t *encode_data_output_as_unicode(const char* Data, int DataByte, int *OutByte = nullptr);
				/// <summary>
				/// DataByte
				///     [in]输入的数据长度,以字节为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				static char *decode_data(const char* Data, int DataByte, int *OutByte = nullptr);
				/// <summary>
				/// 宽字符版本对应的解码方法
				/// DataLen
				///     [in]输入的数据长度,以WCHAR长度为单位
				/// </summary>
				static char *decode_data_input_as_unicode(const wchar_t *Data, int DataLen, int *OutByte = nullptr);
			public:
				/// <summary>
				/// DataByte
				///     [in]输入的数据长度, 以字节为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				static char *EncodeBytes(LPCVOID Data, int DataByte, int *OutByte = nullptr)
				{
					return encode_data((const char *)Data, DataByte, OutByte);
				}
				/// <summary>
				/// 输出UTF-16(Unicode)编码的数据以便更好的在Unicode环境中处理
				/// DataByte
				///     [in]输入的数据长度, 以WCHAR长度为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				static wchar_t *EncodeBytesUnicode(LPCVOID Data, int DataByte, int *OutByte = nullptr)
				{
					return encode_data_output_as_unicode((const char *)Data, DataByte, OutByte);
				}
				/// <summary>
				/// DataByte
				///     [in]输入的数据长度,以字节为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				static LPVOID DecodeBytes(const char *Data, int DataByte, int *OutByte = nullptr)
				{
					return (LPVOID)decode_data(Data, DataByte, OutByte);
				}
				/// <summary>
				/// 宽字符版本对应的解码方法
				/// DataByte
				///     [in]输入的数据长度,以WCHAR长度为单位
				/// OutByte
				///     [out]输出的数据长度,以字节为单位,请不要通过返回值计算
				///     输出数据的长度
				/// </summary>
				static LPVOID DecodeBytesUnicode(const wchar_t *Data, int DataByte, int *OutByte = nullptr)
				{
					return (LPVOID)decode_data_input_as_unicode(Data, DataByte, OutByte);
				}
			public:
				static String EncodeBase64String(String str, int str_length = 0)  
				{  
					if (str_length > 0)
					{
						str = str.Substring(0, str_length);
					}
#ifndef _UNICODE
					auto result_ptr = System::Security::Cryptography::Base64::EncodeBytes
						(str.GetConstData(), str_length);
#else
					auto result_ptr = System::Security::Cryptography::Base64::EncodeBytesUnicode
						(str.ToMultiByte(), str.GetMultiByteSize());
#endif // _UNICODE
					if (result_ptr == nullptr)
					{
						return Nothing;
					} //if
					String result_str = (TCHAR *)result_ptr;
					System::Security::Cryptography::Base64::Collect(result_ptr);
					return result_str;
				}  
				static String DecodeBase64String(const String str_base64)  
				{  
#ifndef _UNICODE
					auto result_ptr = System::Security::Cryptography::Base64::DecodeBytes
						(str_base64.GetConstData(), str_base64.Length);
#else
					auto result_ptr = System::Security::Cryptography::Base64::DecodeBytesUnicode
						(str_base64.GetConstData(), str_base64.Length);
#endif // _UNICODE
					if (result_ptr == nullptr)
					{
						return Nothing;
					} //if
					String result_str = (CHAR *)result_ptr/*@warning 强制转换*/;
					System::Security::Cryptography::Base64::Collect(result_ptr);
					return result_str;
				} 
			};
		}
	}
}

#endif /* _USE_BASE64 */
