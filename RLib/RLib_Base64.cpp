/********************************************************************
	Created:	2012/06/06  21:52
	Filename: 	RLib_Base64.cpp
	Author:		rrrfff
	Url:	    http://blog.csdn.net/rrrfff
*********************************************************************/
#include "RLib_Base64.h"
//-------------------------------------------------------------------------
using namespace System::Security::Cryptography;
//-------------------------------------------------------------------------
char *Base64::encode_data(const char* Data, int DataByte, int *OutByte /* = nullptr */)
{
	//编码表
	const static char EncodeTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	
	IO::BufferedStream *out_data = new IO::BufferedStream;
	if (out_data == nullptr)
	{
		return nullptr;
	} //if
	unsigned char Tmp[4] = {0};
	int LineLength       = 0;
	for (int i = 0; i < (int)(DataByte / 3); i++)
	{
		Tmp[1] =  *Data++;
		Tmp[2] =  *Data++;
		Tmp[3] =  *Data++;
		out_data->Write(&EncodeTable[Tmp[1] >> 2], 1);
		out_data->Write(&EncodeTable[((Tmp[1] << 4) | (Tmp[2] >> 4)) &0x3F], 1);
		out_data->Write(&EncodeTable[((Tmp[2] << 2) | (Tmp[3] >> 6)) &0x3F], 1);
		out_data->Write(&EncodeTable[Tmp[3] &0x3F], 1);
		if (LineLength += 4, LineLength == 76)
		{
			RLIB_PStreamWriteA(out_data, RLIB_NEWLINEA);
			LineLength = 0;
		}
	}
	//对剩余数据进行编码
	int Mod = DataByte % 3;
	if (Mod == 1)
	{
		Tmp[1] =  *Data++;
		out_data->Write(&EncodeTable[(Tmp[1] &0xFC) >> 2], 1);
		out_data->Write(&EncodeTable[((Tmp[1] &0x03) << 4)], 1);
		RLIB_PStreamWriteA(out_data, "==");
	}
	else if (Mod == 2)
	{
		Tmp[1] =  *Data++;
		Tmp[2] =  *Data++;
		out_data->Write(&EncodeTable[(Tmp[1] &0xFC) >> 2], 1);
		out_data->Write(&EncodeTable[((Tmp[1] &0x03) << 4) | ((Tmp[2] &0xF0) >> 4)], 1);
		out_data->Write(&EncodeTable[((Tmp[2] &0x0F) << 2)], 1);
		RLIB_PStreamWriteA(out_data, "=");
	}

	auto result_ptr    = (char *)Alloc(out_data->Length + sizeof(char));
	out_data->Position = 0;
	out_data->Read(result_ptr, out_data->Length);
	if (OutByte != nullptr)
	{
		*OutByte = out_data->Length;
	} //if
	delete out_data;
	return result_ptr;
}
//-------------------------------------------------------------------------
char *Base64::decode_data(const char* Data, int DataByte, int *OutByte /* = nullptr */)
{
	//解码表
	const static char DecodeTable[] = 
	{
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62,  // '+'
		0, 0, 0, 63,  // '/'
		52, 53, 54, 55, 56, 57, 58, 59, 60, 61,  // '0'-'9'
		0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,  // 'A'-'Z'
		0, 0, 0, 0, 0, 0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,  // 'a'-'z'
	};

	IO::BufferedStream *out_data = new IO::BufferedStream;
	if (out_data == nullptr)
	{
		return nullptr;
	} //if
	char data;
	int i = 0, nValue;
	while (i < DataByte)
	{
		if (*Data != '\r' &&  *Data != '\n')
		{
			nValue  = DecodeTable[ *Data++] << 18;
			nValue += DecodeTable[ *Data++] << 12;
			data = (char)((nValue &0x00FF0000) >> 16);
			out_data->Write(&data, 1);
			if (*Data != '=')
			{
				nValue += DecodeTable[ *Data++] << 6;
				data = (char)((nValue &0x0000FF00) >> 8);
				out_data->Write(&data, 1);
				if (*Data != '=')
				{
					nValue += DecodeTable[ *Data++];
					data = (char)(nValue &0x000000FF);
					out_data->Write(&data, 1);
				}
			}
			i += 4;
		}
		else// 回车换行,跳过
		{
			Data++;
			i++;
		}
	}
	auto result_ptr    = (char *)Alloc(out_data->Length + sizeof(char));
	out_data->Position = 0;
	out_data->Read(result_ptr, out_data->Length);
	if (OutByte != nullptr)
	{
		*OutByte = out_data->Length;
	} //if
	delete out_data;
	return result_ptr;
}
//-------------------------------------------------------------------------
char *Base64::decode_data_input_as_unicode(const wchar_t *Data, int DataLen, int *OutByte /* = nullptr */)
{
	auto pData = String::ConvertToMultiByte(Data, DataLen);
	if(pData == nullptr)
	{
		return nullptr;
	}
	auto pResult = decode_data(pData, DataLen, OutByte);
	String::Collect(pData);
	return pResult;
}
//-------------------------------------------------------------------------
WCHAR *Base64::encode_data_output_as_unicode(const char* Data, int DataByte, int *OutByte /* = nullptr */)
{
	auto pResult = encode_data(Data, DataByte, OutByte);
	if (pResult != nullptr)
	{
		int size;
		auto pData = String::ConvertToWideChar(pResult, OutByte != nullptr ? *OutByte : (strlen(pResult) + 1), &size);
		if(pData != nullptr)
		{
			LPWSTR pResultW = (LPWSTR)Base64::Alloc(size);
			if (pResultW != nullptr)
			{
				memcpy(pResultW, pData, size);
				String::Collect(pData);
				return pResultW;
			} //if
			String::Collect(pData);
		}
		Base64::Collect(pResult);
	} //if
	return nullptr;
}


