/********************************************************************
Created:	2011/10/15  8:09
Filename: 	Thread2.cpp
Author:		rrrfff
Url:	    http://blog.csdn.net/rrrfff
 *********************************************************************/
#include "RLib_ThreadSync.h"
#include "native/RLib_Native.h"
using namespace System::Threading;
/************************************************************************/
/*CriticalSection                                                       */
/************************************************************************/
CriticalSection::CriticalSection()
{
    // 初始化线程临界区
    // Windows Vista And Later http://msdn.microsoft.com/en-us/library/aa383745(v=VS.85).aspx
    // InitializeCriticalSectionEx(&this->m_cri, 4000/*自旋锁周期*/, CRITICAL_SECTION_NO_DEBUG_INFO);
    NTSTATUS STATUS = RtlInitializeCriticalSectionAndSpinCount(&this->m_cri, 4000 /*自旋锁周期*/);
    assert(STATUS == STATUS_SUCCESS);
}

//-------------------------------------------------------------------------

CriticalSection::~CriticalSection()
{
    // 删除线程临界区
    RtlDeleteCriticalSection(&this->m_cri);
}

//-------------------------------------------------------------------------

void CriticalSection::Enter() const
{
    RtlEnterCriticalSection((PRTL_CRITICAL_SECTION)&this->m_cri);
}

//-------------------------------------------------------------------------

bool CriticalSection::TryEnter() const
{
    return RtlTryEnterCriticalSection((PRTL_CRITICAL_SECTION)&this->m_cri) == TRUE;
}

//-------------------------------------------------------------------------

void CriticalSection::Leave() const
{
    RtlLeaveCriticalSection((PRTL_CRITICAL_SECTION)&this->m_cri);
}

/************************************************************************/
/* WaitHandle                                                           */
/************************************************************************/
WaitStatus WaitHandle::WaitOne(HANDLE Handle, Boolean Alertable, int millisecondsTimeout)
{
    LARGE_INTEGER Timeout;//100纳秒
    //Timeout.QuadPart = -10000 * millisecondsTimeout;
	Timeout.QuadPart  = UInt32x32To64( millisecondsTimeout, 10000 );
	Timeout.QuadPart *= -1;

    NTSTATUS STATUS = NtWaitForSingleObject(Handle, Alertable, millisecondsTimeout != 0 ? &Timeout: nullptr);
	switch(STATUS)
	{
	case STATUS_ACCESS_DENIED:
		return WAIT_ACCESS_DENIED;
	case STATUS_ALERTED:
		return WAIT_ALERTED;
	case STATUS_INVALID_HANDLE:
		return WAIT_INVALID_HANDLE;
	case STATUS_SUCCESS:
		return WAIT_SUCCESS;
	case STATUS_TIMEOUT:
		return WAIT_TIMEOUTED;
	case STATUS_USER_APC:
		return WAIT_USER_APC;
	}
    assert(!"等待方法发生异常");
    Exception::SetLastError(RtlNtStatusToDosError(STATUS));
	return (WaitStatus)STATUS;
}

//-------------------------------------------------------------------------

WaitStatus WaitHandle::Wait(int Count, HANDLE Handles[], Boolean IsWaitAll, Boolean Alertable, int Milliseconds)
{
	LARGE_INTEGER Timeout;//100纳秒
	//Timeout.QuadPart = -10000 * Milliseconds;
	Timeout.QuadPart  = UInt32x32To64( Milliseconds, 10000 );
	Timeout.QuadPart *= -1;

    NTSTATUS STATUS = NtWaitForMultipleObjects(Count, Handles, IsWaitAll ? WaitAll : WaitAny, Alertable, Milliseconds != 0 ? &Timeout: nullptr);
	switch(STATUS)
	{
	case STATUS_ACCESS_DENIED:
		return WAIT_ACCESS_DENIED;
	case STATUS_ALERTED:
		return WAIT_ALERTED;
	case STATUS_INVALID_HANDLE:
		return WAIT_INVALID_HANDLE;
	case STATUS_SUCCESS:
			return WAIT_SUCCESS;
	case STATUS_TIMEOUT:
			return WAIT_TIMEOUTED;
	case STATUS_USER_APC:
			return WAIT_USER_APC;
	}
	assert(!"等待方法发生异常");
	Exception::SetLastError(RtlNtStatusToDosError(STATUS));
	return (WaitStatus)STATUS;
}

//-------------------------------------------------------------------------

WaitHandle::~WaitHandle()
{
	if (this->m_name.Buffer != nullptr)
	{
		String::Collect(this->m_name.Buffer);
	}
	NTSTATUS STATUS = NtClose(this->Handle);
	if (STATUS != STATUS_SUCCESS)
	{
		assert(!"句柄异常");
		Exception::SetLastError(RtlNtStatusToDosError(STATUS));
	}
}

//-------------------------------------------------------------------------

WaitStatus WaitHandle::WaitOne(int millisecondsTimeout /* = 0 */)
{
	return WaitHandle::WaitOne(this->Handle, TRUE, millisecondsTimeout);
}

/************************************************************************/
/* Mutex                                                                */
/************************************************************************/
Mutex::Mutex(Boolean Owner /* = false */, String Name /* = Nothing */)
{
    //
    //If Owner is true, Mutant is created with non-signaled state,
    //Caller should call NtReleaseMutant after program initialization.
    // 

    NTSTATUS STATUS;

    if (Name.IsNullOrEmpty())
    {
        this->m_name.Buffer = nullptr;
        STATUS = NtCreateMutant(&this->Handle, MUTANT_ALL_ACCESS, NULL, Owner ? TRUE : FALSE);
        goto next;
    } //if

    Name = String(T("\\BaseNamedObjects\\")) + Name;

    RtlInitUnicodeString(&this->m_name, Name.ToWideChar());
	bool bResult = Name.SuppressFinalize(this->m_name.Buffer);
	assert(bResult);

    InitializeObjectAttributes(&this->m_obj, &this->m_name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);

    STATUS = NtCreateMutant(&this->Handle, MUTANT_ALL_ACCESS, &this->m_obj, Owner ? TRUE : FALSE);

    next: if (STATUS != STATUS_SUCCESS)
    {
        Exception::SetLastError(RtlNtStatusToDosError(STATUS));
    } //if
}

//-------------------------------------------------------------------------

LONG Mutex::ReleaseMutex()
{
    LONG PreviousCount;
    NTSTATUS STATUS = NtReleaseMutant(this->Handle, &PreviousCount);
    if (STATUS != STATUS_SUCCESS)
    {
        assert(!"句柄异常");
        Exception::SetLastError(RtlNtStatusToDosError(STATUS));
        return  - 1;
    }
    return PreviousCount;
}

//-------------------------------------------------------------------------

Mutex *Mutex::OpenExisting(String Name)
{
    assert(!Name.IsNullOrEmpty());

    Name = String(T("\\BaseNamedObjects\\")) + Name;

    Mutex *mutex = new Mutex(LPVOID(nullptr));

    RtlInitUnicodeString(&mutex->m_name, Name.ToWideChar());
	bool bResult = Name.SuppressFinalize(mutex->m_name.Buffer);
	assert(bResult);

    InitializeObjectAttributes(&mutex->m_obj, &mutex->m_name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);

    NTSTATUS STATUS = NtOpenMutant(&mutex->Handle, MUTANT_ALL_ACCESS, &mutex->m_obj);

    if (STATUS != STATUS_SUCCESS)
    {
        Exception::SetLastError(RtlNtStatusToDosError(STATUS));
        delete mutex;
        return NULL;
    } //if

    return mutex;
}

/************************************************************************/
/* Event                                                                */
/************************************************************************/
Event::Event(EventType Type, Boolean InitialState, String Name /* = Nothing */)
{
    NTSTATUS STATUS;

    if (Name.IsNullOrEmpty())
    {
        this->m_name.Buffer = nullptr;
        STATUS = NtCreateEvent(&this->Handle, EVENT_ALL_ACCESS, NULL, (EVENT_TYPE)Type, InitialState ? TRUE : FALSE);
        goto next;
    } //if

    Name = String(T("\\BaseNamedObjects\\")) + Name;

	RtlInitUnicodeString(&this->m_name, Name.ToWideChar());
	bool bResult = Name.SuppressFinalize(this->m_name.Buffer);
	assert(bResult);

    InitializeObjectAttributes(&this->m_obj, &this->m_name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);

    STATUS = NtCreateEvent(&this->Handle, EVENT_ALL_ACCESS, &this->m_obj, (EVENT_TYPE)Type, InitialState ? TRUE : FALSE);

    next: if (STATUS != STATUS_SUCCESS)
    {
        Exception::SetLastError(RtlNtStatusToDosError(STATUS));
    } //if
}

//-------------------------------------------------------------------------

LONG Event::SetSignal()
{
    LONG PreviousCount;
    NTSTATUS STATUS = NtSetEvent(this->Handle, &PreviousCount);
    if (STATUS != STATUS_SUCCESS)
    {
        assert(!"句柄异常");
        Exception::SetLastError(RtlNtStatusToDosError(STATUS));
        return  - 1;
    }
    return PreviousCount;
}

//-------------------------------------------------------------------------

LONG Event::Pulse()
{
	LONG PreviousCount;
	NTSTATUS STATUS = NtPulseEvent(this->Handle, &PreviousCount);
	// Function sets event to signaled state, releases all (or one - dependly of EVENT_TYPE)
	// waiting threads, and resets event to non-signaled state. 
	// If they're no waiting threads, NtPulseEvent just clear event state.
	if (STATUS != STATUS_SUCCESS)
	{
		assert(!"句柄异常");
		Exception::SetLastError(RtlNtStatusToDosError(STATUS));
		return  - 1;
	}
	return PreviousCount;
}

//-------------------------------------------------------------------------

void Event::Clear()
{
    NTSTATUS STATUS = NtClearEvent(this->Handle);
    if (STATUS != STATUS_SUCCESS)
    {
        assert(!"句柄异常");
        Exception::SetLastError(RtlNtStatusToDosError(STATUS));
    }
}

//-------------------------------------------------------------------------

Event *Event::OpenExisting(String Name)
{
    assert(!Name.IsNullOrEmpty());

    Name = String(T("\\BaseNamedObjects\\")) + Name;

    Event *event = new Event(LPVOID(nullptr));

    RtlInitUnicodeString(&event->m_name, Name.ToWideChar());
	bool bResult = Name.SuppressFinalize(event->m_name.Buffer);
	assert(bResult);

    InitializeObjectAttributes(&event->m_obj, &event->m_name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);

    NTSTATUS STATUS = NtOpenEvent(&event->Handle, EVENT_ALL_ACCESS, &event->m_obj);

    if (STATUS != STATUS_SUCCESS)
    {
        Exception::SetLastError(RtlNtStatusToDosError(STATUS));
        delete event;
        return NULL;
    } //if

    return event;
}
/************************************************************************/
/* Semaphore                                                            */
/************************************************************************/
Semaphore::Semaphore(int InitialCount, int MaximumCount, String Name /* = Nothing */)
{
	//
	//If Owner is true, Mutant is created with non-signaled state,
	//Caller should call NtReleaseMutant after program initialization.
	// 

	NTSTATUS STATUS;
	if (Name.IsNullOrEmpty())
	{
		this->m_name.Buffer = nullptr;
		STATUS = NtCreateSemaphore(&this->Handle, SEMAPHORE_ALL_ACCESS, NULL,
			InitialCount, MaximumCount);
		goto next;
	} //if

	Name = String(T("\\BaseNamedObjects\\")) + Name;

	RtlInitUnicodeString(&this->m_name, Name.ToWideChar());
	bool bResult = Name.SuppressFinalize(this->m_name.Buffer);
	assert(bResult);

	InitializeObjectAttributes(&this->m_obj, &this->m_name, 
		OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);

	STATUS = NtCreateSemaphore(&this->Handle, SEMAPHORE_ALL_ACCESS, &this->m_obj,
		InitialCount, MaximumCount);

next: if (STATUS != STATUS_SUCCESS)
	  {
		  Exception::SetLastError(RtlNtStatusToDosError(STATUS));
	  } //if
}

//-------------------------------------------------------------------------

LONG Semaphore::Release()
{
	return this->Release(1);
}

//-------------------------------------------------------------------------

LONG Semaphore::Release(ULONG releaseCount)
{
	ULONG PreviousCount;
	NTSTATUS STATUS = NtReleaseSemaphore(this->Handle, releaseCount, &PreviousCount);
	if (STATUS != STATUS_SUCCESS)
	{
		assert(!"句柄异常");
		Exception::SetLastError(RtlNtStatusToDosError(STATUS));
		return  - 1;
	}
	return PreviousCount;
}

//-------------------------------------------------------------------------

Semaphore *Semaphore::OpenExisting(String Name)
{
	assert(!Name.IsNullOrEmpty());

	Name = String(T("\\BaseNamedObjects\\")) + Name;

	Semaphore *semaphore = new Semaphore(LPVOID(nullptr));

	RtlInitUnicodeString(&semaphore->m_name, Name.ToWideChar());
	bool bResult = Name.SuppressFinalize(semaphore->m_name.Buffer);
	assert(bResult);

	InitializeObjectAttributes(&semaphore->m_obj, &semaphore->m_name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, nullptr);

	NTSTATUS STATUS = NtOpenSemaphore(&semaphore->Handle, SEMAPHORE_ALL_ACCESS, &semaphore->m_obj);

	if (STATUS != STATUS_SUCCESS)
	{
		Exception::SetLastError(RtlNtStatusToDosError(STATUS));
		delete semaphore;
		return NULL;
	} //if

	return semaphore;
}